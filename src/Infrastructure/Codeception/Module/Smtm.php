<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Codeception\Module;

use Codeception\TestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Smtm extends \Codeception\Module
{
    protected array $config = [
        'config' => null,
    ];

    public function _initialize(): void
    {
        chdir(__DIR__ . '/../../../../../../../');

        if (!empty($this->config['configFile'])) {
            putenv('CODECEPTION_CONFIG_OVERRIDE_FILE=' . $this->config['configFile']);
        } else {
            putenv('CODECEPTION_CONFIG_OVERRIDE_FILE');
        }

        if (!empty($this->config['configValues'])) {
            putenv('CODECEPTION_CONFIG_OVERRIDE_VALUES=' . base64_encode(serialize($this->config['configValues'])));
        } else {
            putenv('CODECEPTION_CONFIG_OVERRIDE_VALUES');
        }
    }

    public function _before(TestInterface $test): void
    {

    }

    public function _after(TestInterface $test): void
    {

    }
}
