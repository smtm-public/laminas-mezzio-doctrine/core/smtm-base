<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\Factory;

use Smtm\Base\Http\Middleware\ErrorHandler;
use Smtm\Base\Http\Middleware\Mezzio\Cors\CorsMiddleware;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Mezzio\Middleware\WhoopsErrorResponseGenerator;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ErrorHandlerFactory
{
    public function __invoke(ContainerInterface $container) : ErrorHandler
    {
        $errorHandler = new ErrorHandler(
            $container->get('config')['base']['http']['error_handling'],
            $container->get(ResponseInterface::class),
            $container->get(WhoopsErrorResponseGenerator::class)
        );
        /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

        foreach ($container->get('config')['base']['http']['error_handling']['loggers'] as $loggerName => $loggerServiceName) {
            $errorHandler->addLogger(
                $loggerName,
                $infrastructureServicePluginManager->get($loggerServiceName)
            );
        }

        $corsMiddleware = $container->get(CorsMiddleware::class);
        $errorHandler->attachResponseMutatorListener(
            [$corsMiddleware, 'processException']
        );

        return $errorHandler;
    }
}
