<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\Mezzio\Cors\Service\Factory;

use Smtm\Base\Http\Middleware\Mezzio\Cors\Service\ResponseFactory;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface as PsrResponseFactoryInterface;
use Webmozart\Assert\Assert;

class ResponseFactoryFactory
{
    public function __invoke(ContainerInterface $container): ResponseFactory
    {
        $responseFactory = $container->get(PsrResponseFactoryInterface::class);
        Assert::isInstanceOf($responseFactory, PsrResponseFactoryInterface::class);
        return new ResponseFactory($responseFactory);
    }
}
