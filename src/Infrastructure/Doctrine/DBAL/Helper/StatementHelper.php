<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\DBAL\Helper;

use Doctrine\DBAL\ParameterType;
use Doctrine\DBAL\Statement;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class StatementHelper
{
    public static function bindValues(Statement $stmt, array $params): void
    {
        foreach ($params as $name => $value) {
            if ($value === null) {
                $stmt->bindValue($name, $value, ParameterType::NULL);
            } else {
                $stmt->bindValue($name, $value, ParameterType::STRING);
            }
        }
    }
}
