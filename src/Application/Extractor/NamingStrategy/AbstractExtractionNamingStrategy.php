<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\NamingStrategy;

use Laminas\Hydrator\NamingStrategy\NamingStrategyInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractExtractionNamingStrategy implements NamingStrategyInterface
{
    protected array $options = [];

    /**
     * @inheritdoc
     */
    public function hydrate(string $name, ?array $data = null): string
    {
        return $name;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): static
    {
        $this->options = $options;

        return $this;
    }
}
