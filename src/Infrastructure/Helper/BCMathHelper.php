<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class BCMathHelper
{
    /**
     * @return numeric-string
     */
    public static function castToString(
        string|float|int|null $value
    ): string {
        if ($value === null) {
            return '0';
        }

        if (is_string($value)) {
            return $value;
        }

        if (is_int($value)) {
            return (string) $value;
        }

        return number_format($value, bcscale(), '.', '');
    }

    /**
     * @param numeric-string $number
     *
     * @return numeric-string
     */
    public static function bcceil(string $number): string
    {
        if (str_contains($number, '.')) {
            if (preg_match('#\.0+$#', $number)) {
                return bcround($number, 0);
            }

            if ($number[0] != '-') {
                return bcadd($number, '1', 0);
            }

            return bcsub($number, '0', 0);
        }

        return $number;
    }

    /**
     * @param numeric-string $number
     *
     * @return numeric-string
     */
    public static function bcfloor(string $number): string
    {
        if (str_contains($number, '.')) {
            if (preg_match('#\.0+$#', $number)) {
                return bcround($number, 0);
            }

            if ($number[0] != '-') {
                return bcadd($number, '0', 0);
            }

            return bcsub($number, '1', 0);
        }
        return $number;
    }

    /**
     * @param numeric-string $number
     *
     * @return numeric-string
     */
    public static function bcround(string $number, int $precision = 0)
    {
        if (str_contains($number, '.')) {
            if ($number[0] != '-') {
                return bcadd($number, '0.' . str_repeat('0', $precision) . '5', $precision);
            }

            return bcsub($number, '0.' . str_repeat('0', $precision) . '5', $precision);
        }

        return $number;
    }

    /**
     * @param numeric-string[] $numbers
     *
     * @return numeric-string
     */
    public static function bcmin(string ...$numbers): string
    {
        $result = $numbers[0];
        array_shift($numbers);

        foreach ($numbers as $number) {
            $result = match (bccomp($result, $number)) {
                1 => $number,
                default => $result,
            };
        }

        return $result;
    }

    /**
     * @param numeric-string[] $numbers
     *
     * @return numeric-string
     */
    public static function bcmax(string ...$numbers): string
    {
        $result = $numbers[0];
        array_shift($numbers);

        foreach ($numbers as $number) {
            $result = match (bccomp($result, $number)) {
                1 => $result,
                default => $number,
            };
        }

        return $result;
    }
}
