<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService;

use Smtm\Base\Application\Service\ApplicationService\DbService\ArchivableEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Doctrine\ORM\QueryBuilder;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ArchivableUuidAwareEntityDbServiceTrait
{

    use ArchivableEntityDbServiceTrait;

    public function archiveByUuid(
        string $uuid,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): void {
        if ($this->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
            $entity = $this->getOneByUuid($uuid);
            $this->archiveFunc($entity, $options);

            return;
        }

        $this->transactional(
            function () use ($uuid, $options): void {
                $entity = $this->getOneByUuid($uuid);
                $this->archiveFunc($entity, $options);
            }
        );
    }

    public function bulkArchiveByUuids(
        array $uuids,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): int {
        return $this->doBulkArchive($this->prepareBulkArchiveByUuids($uuids, $options));
    }

    public function unarchiveByUuid(
        string $uuid,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): NotArchivedAwareEntityInterface {
        if ($this->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
            $entity = $this->getOneByUuid($uuid);

            return $this->unarchiveFunc($entity, $options);
        }

        return $this->transactional(
            function () use ($uuid, $options): NotArchivedAwareEntityInterface {
                $entity = $this->getOneByUuid($uuid);

                return $this->unarchiveFunc($entity, $options);
            }
        );
    }

    protected function prepareBulkArchiveByUuids(
        array $uuids,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): QueryBuilder {
        /** @var QueryBuilder $qb */
        $qb = $this->prepareBulkArchive($options);
        $qb->where($qb->expr()->in('o.uuid', $uuids));

        return $qb;
    }
}
