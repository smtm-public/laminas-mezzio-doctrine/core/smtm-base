<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface UnarchivedByIpAddressAwareEntityInterface
{
    public function getUnarchivedByIpAddress(): ?string;
    public function setUnarchivedByIpAddress(?string $unarchivedByIpAddress): static;
}
