<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Mezzio\Router\Middleware;

use Smtm\Base\Http\Exception\MethodNotAllowedException;
use Mezzio\Router\RouteResult;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MethodNotAllowedMiddleware implements MiddlewareInterface
{
    public function __construct(protected \Mezzio\Router\Middleware\MethodNotAllowedMiddleware $wrapped)
    {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $routeResult = $request->getAttribute(RouteResult::class);

        if (! $routeResult instanceof RouteResult || ! $routeResult->isMethodFailure()) {
            return $handler->handle($request);
        }

        $allowedMethods = $routeResult->getAllowedMethods();
        assert(is_array($allowedMethods));

        throw new MethodNotAllowedException(
            'Method not allowed. Supported methods for this route are [' . implode(', ', $allowedMethods) . ']'
        );
    }
}
