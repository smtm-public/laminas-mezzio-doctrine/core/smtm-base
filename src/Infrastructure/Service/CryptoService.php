<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;
use JetBrains\PhpStorm\Pure;
use Ramsey\Uuid\Uuid;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CryptoService extends AbstractInfrastructureService
{
    #[Pure] public function __construct(
        InfrastructureServicePluginManager $infrastructureServiceManager,
        protected Key $key
    ) {
        parent::__construct($infrastructureServiceManager);
    }

    public function encrypt(string $data): string
    {
        return Crypto::encrypt($data, $this->key);
    }

    public function decrypt(string $data): string
    {
        return Crypto::decrypt($data, $this->key);
    }

    public static function generateSalt(): string
    {
        return hash(
            'sha256',
            Uuid::uuid4() . microtime()
        );
    }
}
