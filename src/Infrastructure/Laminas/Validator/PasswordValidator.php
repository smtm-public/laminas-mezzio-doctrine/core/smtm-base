<?php

namespace Smtm\Base\Infrastructure\Laminas\Validator;

use Laminas\Validator\AbstractValidator;
use ZxcvbnPhp\Zxcvbn;

/**
 * Password Validation rules:
 * - Minimum length of 8 characters
 * - Maximum length of more than 64 characters
 * - All allowed characters should be acceptable
 * - Strong password complexity
 *
 * COMPLEXITY SORES:
 * - 0 means the password is extremely guessable (within 10^3 guesses), dictionary words like 'password' or 'mother' score a 0
 * - 1 is still very guessable (guesses < 10^6), an extra character on a dictionary word can score a 1
 * - 2 is somewhat guessable (guesses < 10^8), provides some protection from unthrottled online attacks
 * - 3 is safely unguessable (guesses < 10^10), offers moderate protection from offline slow-hash scenario
 * - 4 is very unguessable (guesses >= 10^10) and provides strong protection from offline slow-hash scenario
 *
 * PASSWORD_VALIDATION_RULES='{"minLength": 8, "maxLength": 64, "allowedCharsRegex": "%^[ -~]+$%", "minComplexityScore": 2}'
*/
class PasswordValidator extends AbstractValidator
{
    const MSG_MIN_CHARS_LENGTH = 'min-length';
    const MSG_MAX_CHARS_LENGTH = 'max-length';
    const MSG_ALLOWED_CHARACTERS = 'allowed-chars';
    const MSG_WEAK_COMPLEXITY = 'weak-complexity';

    /**
     * Validation failure message template definitions
     *
     * @var string[]
     */
    protected $messageTemplates = [
        self::MSG_MIN_CHARS_LENGTH    => 'Password length is less than minimum of 8 characters.',
        self::MSG_MAX_CHARS_LENGTH    => 'Password length is more than maximum of 64 characters.',
        self::MSG_ALLOWED_CHARACTERS   => 'Password include not allowed characters.',
        self::MSG_WEAK_COMPLEXITY   => 'Password complexity is too weak.',
    ];

    protected array $options = [];

    public function isValid(mixed $value)
    {
        $options = $this->options;

        if (isset($options['allowedCharsRegex']) && !preg_match($options['allowedCharsRegex'], $value, $matches)) {
            $this->error(self::MSG_ALLOWED_CHARACTERS);

            return false;
        }

        if (isset($options['minLength']) && strlen($value) < $options['minLength']) {
            $this->error(self::MSG_MIN_CHARS_LENGTH);

            return false;
        }

        if (isset($options['maxLength']) && strlen($value) > $options['maxLength']) {
            $this->error(self::MSG_MAX_CHARS_LENGTH);

            return false;
        }

        $zxcvbn = new Zxcvbn();
        $complexity = $zxcvbn->passwordStrength($value);

        if (isset($options['minComplexityScore']) && $complexity['score'] < $options['minComplexityScore']) {
            $this->error(self::MSG_WEAK_COMPLEXITY);

            return false;
        }

        return true;
    }
}
