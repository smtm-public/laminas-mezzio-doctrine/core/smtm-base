<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\NamingStrategy;

use Smtm\Base\Infrastructure\Helper\LetterCaseHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LetterCaseExtractionNamingStrategy extends AbstractExtractionNamingStrategy
{
    public const OPTION_KEY_LETTER_CASE = 'letterCase';

    public function extract(string $name, ?object $object = null): string
    {
        return LetterCaseHelper::format($name, $this->options[static::OPTION_KEY_LETTER_CASE]);
    }
}
