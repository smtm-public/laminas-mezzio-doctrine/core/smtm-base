#!/usr/bin/env bash

set -evx

# Example:
# vendor/smtm/smtm-base/bin/db_import_sqlite.sh "source.sql" "data/db/target.sqlite"

DB_FILE_SOURCE="$1"
DB_FILE_TARGET="$2"

#cat "${DB_DUMP_FILE_NAME}" | sqlite3 "${DB_FILE_TARGET}"

while read -r LINE; do
  echo "${LINE}";
  echo "${LINE}" | sqlite3 "${DB_FILE_TARGET}";
done <"${DB_FILE_SOURCE}"
