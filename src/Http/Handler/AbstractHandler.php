<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;
use Smtm\Base\Application\Extractor\DomainObjectExtractorAwareInterface;
use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationServiceAwareInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Domain\DomainObjectInterface;
use Smtm\Base\Http\Exception\BadRequestException;
use Smtm\Base\Http\InputFilter\AbstractRequestValidatingInputFilterCollection;
use Smtm\Base\Http\Middleware\RequestValidatingInputFilterCollectionMiddleware;
use Smtm\Base\Infrastructure\Collection\CollectionInterface;
use Smtm\Base\Infrastructure\Collection\PaginatedCollection;
use Smtm\Base\Infrastructure\Collection\PaginatedCollectionInterface;
use Smtm\Base\Infrastructure\Helper\ArrayHelper;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractHandler implements
    ConfigAwareInterface,
    ApplicationServiceAwareInterface,
    DomainObjectExtractorAwareInterface,
    RequestHandlerInterface
{

    use ConfigAwareTrait;

    public const IGNORE_QUERY_PARAMS = [
        'XDEBUG_SESSION_START',
        'XDEBUG_SESSION',
        'XDEBUG_PROFILE',
    ];
    public const DEFAULT_PARSED_QUERY_PARAMS = [
        'criteria' => null,
        'filters' => null,
        'page' => null,
        'pageSize' => null,
        'with' => '',
        'includes' => '',
        'orderBy' => [],
    ];

    public ?string $domainObjectExtractorName = null;
    public ?string $applicationServiceName = null;
    protected ?AbstractApplicationService $applicationService = null;
    protected ?AbstractDomainObjectExtractor $domainObjectExtractor = null;

    public function __construct(
        protected ApplicationServicePluginManager $applicationServicePluginManager,
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected ExtractorPluginManager $extractorPluginManager,
        protected array $config
    ) {
        $this->applicationService = $this->applicationServiceName !== null
            ? $this->applicationServicePluginManager->get($this->applicationServiceName)
            : null;

        if ($this->domainObjectExtractorName !== null) {
            $this->domainObjectExtractor = $this->extractorPluginManager->get($this->domainObjectExtractorName);
        }
    }

    /**
     * Parses the raw query params and returns an array with the different
     * segments
     *
     * [
     *      'page' => 1,
     *      'pageSize' => 1,
     *      'includes' => ['include1', 'include2'],
     *      'criteria' => [
     *              'and',
     *              ['=', 'property1', 'value1'],
     *              ['=', 'property2', 'value2'],
     *      ],
     *      'orderBy' => ['field' => 'DESC']
     * ]
     */
    protected static function parseQueryParams(array $queryParams = null): array
    {
        $result = static::DEFAULT_PARSED_QUERY_PARAMS;

        if ($queryParams === null) {
            return $result;
        }

        if (isset($queryParams['criteria'])) {
            $result['criteria'] = $queryParams['criteria'];
            unset($queryParams['criteria']);
        }

        if (isset($queryParams['filters'])) {
            try {
                $filtersCriteria = json_decode($queryParams['filters'], true, flags: JSON_THROW_ON_ERROR);
            } catch (\Throwable $t) {
                throw new BadRequestException('Malformed `filters` query param', 0, $t);
            }

            if ($filtersCriteria) {
                $result['criteria'] = array_replace_recursive(
                    $queryParams['criteria'] ?? [],
                    $filtersCriteria
                );
            }

            unset($queryParams['filters']);
        }

        if (isset($queryParams['page'])) {
            $result['page'] = (int) $queryParams['page'];
            unset($queryParams['page']);
        }

        if (isset($queryParams['pageSize'])) {
            $result['pageSize'] = (int) $queryParams['pageSize'];
            unset($queryParams['pageSize']);
        }

        if (!empty($queryParams['orderBy'])) {
            $orderBy = is_array($queryParams['orderBy'])
                ? $queryParams['orderBy']
                : explode(',', $queryParams['orderBy']);

            $result['orderBy'] = [];

            foreach ($orderBy as $key => $value) {
                $field = null;
                $order = null;

                if (is_numeric($key)) {
                    if (str_starts_with($value, '-')) {
                        $field = substr($value, 1, strlen($value));
                        $order = str_starts_with($value, '-') ? 'DESC' : 'ASC';
                    } else {
                        $field = $value;
                        $order = 'ASC';
                    }
                } else {
                    $field = $key;
                    $order = strtoupper($value);

                    if (!in_array($order, ['ASC', 'DESC'])) {
                        $order = 'ASC';
                    }
                }

                if ($field !== null) {
                    $result['orderBy'][$field] = $order;
                }
            }

            unset($queryParams['orderBy']);
        }

        if (isset($queryParams['with'])) {
            $result['includes'] = $queryParams['with'];
            unset($queryParams['with']);
        } elseif (isset($queryParams['includes'])) {
            $result['includes'] = $queryParams['includes'];
            unset($queryParams['includes']);
        }

        if (is_array($result['includes'])) {
            $result['includes'] = ArrayHelper::implodeRecursive(',', $result['includes'], '.');
        }

        return $result;
    }

    public function getApplicationService(): ?AbstractApplicationService
    {
        return $this->applicationService;
    }

    public function setApplicationService(?AbstractApplicationService $applicationService): static
    {
        $this->applicationService = $applicationService;

        return $this;
    }

    public function getDomainObjectExtractor(): ?AbstractDomainObjectExtractor
    {
        return $this->domainObjectExtractor;
    }

    public function setDomainObjectExtractor(?AbstractDomainObjectExtractor $domainObjectExtractor): static
    {
        $this->domainObjectExtractor = $domainObjectExtractor;

        return $this;
    }

    protected function getMergedFilteredHeaders(ServerRequestInterface $request): array
    {
        $resultHeaders = $request->getHeaders();
        /** @var AbstractRequestValidatingInputFilterCollection $requestValidatingInputFilterCollection */
        $requestValidatingInputFilterCollection = $request->getAttribute(
            RequestValidatingInputFilterCollectionMiddleware::REQUEST_ATTRIBUTE_NAME_REQUEST_VALIDATING_INPUT_FILTER_COLLECTION
        );

        if ($requestValidatingInputFilterCollection) {
            $resultHeaders = array_replace_recursive(
                $resultHeaders,
                $requestValidatingInputFilterCollection->queryParamsInputFilter->getValues()
            );
        }

        return $resultHeaders;
    }

    protected function getMergedFilteredQueryParams(ServerRequestInterface $request): array
    {
        $resultQueryParams = $request->getQueryParams();
        /** @var AbstractRequestValidatingInputFilterCollection $requestValidatingInputFilterCollection */
        $requestValidatingInputFilterCollection = $request->getAttribute(
            RequestValidatingInputFilterCollectionMiddleware::REQUEST_ATTRIBUTE_NAME_REQUEST_VALIDATING_INPUT_FILTER_COLLECTION
        );

        if ($requestValidatingInputFilterCollection) {
            $resultQueryParams = array_replace_recursive(
                $resultQueryParams,
                $requestValidatingInputFilterCollection->queryParamsInputFilter->getValues()
            );
        }

        return $resultQueryParams;
    }

    protected function getMergedFilteredAttributes(ServerRequestInterface $request): array
    {
        $resultAttributes = $request->getAttributes();
        /** @var AbstractRequestValidatingInputFilterCollection $requestValidatingInputFilterCollection */
        $requestValidatingInputFilterCollection = $request->getAttribute(
            RequestValidatingInputFilterCollectionMiddleware::REQUEST_ATTRIBUTE_NAME_REQUEST_VALIDATING_INPUT_FILTER_COLLECTION
        );

        if ($requestValidatingInputFilterCollection) {
            $resultAttributes = array_replace_recursive(
                $resultAttributes,
                $requestValidatingInputFilterCollection->pathParamsInputFilter->getValues()
            );
        }

        return $resultAttributes;
    }

    protected function getMergedFilteredParsedBody(ServerRequestInterface $request): array
    {
        $resultParsedBody = $request->getParsedBody();
        /** @var AbstractRequestValidatingInputFilterCollection $requestValidatingInputFilterCollection */
        $requestValidatingInputFilterCollection = $request->getAttribute(
            RequestValidatingInputFilterCollectionMiddleware::REQUEST_ATTRIBUTE_NAME_REQUEST_VALIDATING_INPUT_FILTER_COLLECTION
        );

        if ($requestValidatingInputFilterCollection) {
            $resultParsedBody = array_replace_recursive(
                $resultParsedBody,
                $requestValidatingInputFilterCollection->parsedBodyInputFilter->getValues()
            );
        }

        return $resultParsedBody;
    }

    /**
     * Prepares the response which will be returned by the action
     */
    protected function prepareResponse(
        array|DomainObjectInterface|CollectionInterface|PaginatedCollectionInterface $data,
        array $parsedQueryParams = [],
        int $statusCode = null,
        AbstractDomainObjectExtractor | string | null $extractor = null
    ): ResponseInterface {
        $headers = [];

        if (is_array($data) && isset($data['totalCount'])) {
            $data = new PaginatedCollection($data['data'], $data['totalCount']);
        }

        $extracted = $this->extract($data, $extractor, $parsedQueryParams['includes'] ?? '');

        if (is_array($extracted) && $data instanceof PaginatedCollectionInterface) {
            $headers['x-pagination-total-count'] = $data->getTotalCount();
        }

        $responseData = $extracted;

        if ($statusCode === null) {
            $statusCode = HttpHelper::STATUS_CODE_OK;
        }

        return new JsonResponse($responseData, $statusCode, $headers);
    }

    public function extract(
        CollectionInterface|PaginatedCollectionInterface|DomainObjectInterface|array $data,
        AbstractDomainObjectExtractor | string | null $extractor = null,
        string $includes = ''
    ): ?array {
        $requestedExtractor =
            ($extractor !== null)
                ? (
                    is_object($extractor)
                        ? $extractor
                        : $this->extractorPluginManager->get($extractor)
                    )
                : $this->domainObjectExtractor;

        if ($requestedExtractor instanceof AbstractDomainObjectExtractor && $includes !== '') {
            $requestedExtractor->setIncludes($includes);
        }

        return AbstractApplicationService::extract($data, $requestedExtractor);
    }

    abstract public function handle(ServerRequestInterface $request): ResponseInterface;
}
