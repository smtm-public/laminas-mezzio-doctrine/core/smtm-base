<?php

declare(strict_types=1);

namespace Smtm\Base\Http\InputFilter\Factory;

use Smtm\Base\Http\InputFilter\RequestValidatingInputFilterCollectionPluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RequestValidatingInputFilterCollectionPluginManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new RequestValidatingInputFilterCollectionPluginManager(
            $container,
            [
                'abstract_factories' => [
                    RequestValidatingInputFilterCollectionAbstractFactory::class,
                ],
            ]
        );
    }
}
