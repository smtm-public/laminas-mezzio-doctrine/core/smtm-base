<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Mezzio\Router\Middleware;


use Mezzio\Router\Middleware\MethodNotAllowedMiddleware as MezzioMethodNotAllowedMiddleware;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MethodNotAllowedMiddlewareFactory
{
    public function __invoke(ContainerInterface $container): MethodNotAllowedMiddleware
    {
        return new MethodNotAllowedMiddleware(
            $container->get(MezzioMethodNotAllowedMiddleware::class)
        );
    }
}
