<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator;

use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterFactoryAwareInterface;
use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterFactoryAwareTrait;
use Laminas\InputFilter\InputFilter;
use Laminas\Validator\AbstractValidator;
use Laminas\Validator\Exception\ExtensionNotLoadedException;
use Laminas\Validator\Exception\InvalidArgumentException;
use Laminas\Validator\ValidatorInterface;
use Laminas\Validator\ValidatorPluginManagerAwareInterface;
use Traversable;

/**
 * Bidirectional (is|is not) validation for expected value type: array
 *
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IsArray extends AbstractValidator implements InputFilterFactoryAwareInterface,
                                                   ValidatorPluginManagerAwareInterface
{

    use InputFilterFactoryAwareTrait,
        ValidatorPluginManagerAwareTrait;

    public const OPTION_KEY_IS = 'is';
    public const OPTION_KEY_TYPE_VALIDATOR = 'typeValidator';
    public const OPTION_KEY_REQUIRED_KEYS = 'requiredKeys';
    public const OPTION_KEY_INPUTS = 'inputs';

    public const INVALID_TRUE = 'invalidValueIs';
    public const INVALID_FALSE = 'invalidValueIsNot';
    public const INVALID_MISSING_KEYS = 'invalidMissingKeys';
    public const INVALID_VALUES_TYPE = 'invalidValuesType';
    public const INVALID_ARRAY_INPUT = 'invalidArrayInput';

    protected array $messageTemplates = [
        self::INVALID_TRUE => 'Invalid value given. Array expected.',
        self::INVALID_FALSE => 'Invalid value given. Cannot be an array.',
        self::INVALID_MISSING_KEYS => 'Some of the required array keys are missing: [%missingKeys%].',
        self::INVALID_VALUES_TYPE => 'Some of the array values have invalid data type.',
        self::INVALID_ARRAY_INPUT =>
            "Invalid array input. Errors: '%inputMessages%'.",
    ];

    protected $messageVariables = [
        'missingKeys' => 'missingKeys',
        'inputMessages' => 'inputMessages',
    ];

    protected array $options = [];

    protected ?string $missingKeys = null;
    protected ?string $inputMessages = null;

    /**
     * $options can have the following keys:
     * - is - true|false - whether it should be of type array (true) or not (false)
     * - typeValidator - any of the type validators - whether the array should cosist of elements of the specified type
     *
     * @param array|Traversable $options
     *
     * @throws ExtensionNotLoadedException if ext/intl is not present
     */
    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    /**
     * @param $value
     *
     * @return bool
     *
     * @throws InvalidArgumentException
     */
    public function isValid($value)
    {
        $is = $this->options[static::OPTION_KEY_IS] ?? true;

        if ($is) {
            if (!is_array($value)) {
                $this->error(constant('self::INVALID_' . strtoupper(var_export($is, true))));

                return false;
            }

            if (!empty($this->options[static::OPTION_KEY_TYPE_VALIDATOR])) {
                /** @var ValidatorInterface $validator */
                $validator = $this->validatorPluginManager->get(
                    $this->options[static::OPTION_KEY_TYPE_VALIDATOR]['name'],
                    $this->options[static::OPTION_KEY_TYPE_VALIDATOR]['options'] ?? []
                );
                $result = true;

                foreach ($value as $key => $arrayElement) {
                    if (!$validator->isValid($arrayElement)) {
                        $this->error(self::INVALID_VALUES_TYPE);
                        $result = false;
                    }
                }

                return $result;
            }

            if (!empty($this->options[static::OPTION_KEY_REQUIRED_KEYS])) {
                $requiredKeys = array_combine(
                    $this->options[static::OPTION_KEY_REQUIRED_KEYS],
                    $this->options[static::OPTION_KEY_REQUIRED_KEYS]
                );

                if (
                    count($providedKeys = array_intersect_key(
                        $requiredKeys,
                        $value
                    )) < count($this->options[static::OPTION_KEY_REQUIRED_KEYS])
                ) {
                    $this->setMissingKeys(
                        implode(
                            ',',
                            array_diff(
                                $requiredKeys,
                                $value
                            )
                        )
                    );
                    $this->error(self::INVALID_MISSING_KEYS);

                    return false;
                }
            }

            if (!empty($this->options[static::OPTION_KEY_INPUTS])) {
                $inputFilter = new InputFilter();

                foreach ($this->options[static::OPTION_KEY_INPUTS] as $inputSpecification) {
                    $inputFilter->add($this->inputFilterFactory->createInput($inputSpecification));
                }

                $inputFilter->setData($value);

                if (!$result = $inputFilter->isValid()) {
                    $this->setInputMessages(json_encode($inputFilter->getMessages()));
                    $this->error(static::INVALID_ARRAY_INPUT);
                }

                return $result;
            }
        }

        if (!$is && is_array($value)) {
            $this->error(constant('self::INVALID_' . strtoupper(var_export($is, true))));

            return false;
        }

        return true;
    }

    public function getMissingKeys(): ?string
    {
        return $this->missingKeys;
    }

    public function setMissingKeys(?string $missingKeys): static
    {
        $this->missingKeys = $missingKeys;

        return $this;
    }

    public function getInputMessages(): ?string
    {
        return $this->inputMessages;
    }

    public function setInputMessages(?string $inputMessages): static
    {
        $this->inputMessages = $inputMessages;

        return $this;
    }
}
