<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

use DateTime;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface UnarchivedAtAwareEntityInterface
{
    public function getUnarchivedAt(): ?\DateTime;
    public function setUnarchivedAt(?DateTime $archivedAt): static;
}
