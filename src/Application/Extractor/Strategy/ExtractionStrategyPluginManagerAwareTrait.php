<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ExtractionStrategyPluginManagerAwareTrait
{
    protected ExtractionStrategyPluginManager $extractionStrategyPluginManager;

    public function getExtractionStrategyPluginManager(): ExtractionStrategyPluginManager
    {
        return $this->extractionStrategyPluginManager;
    }

    public function setExtractionStrategyPluginManager(ExtractionStrategyPluginManager $extractionStrategyPluginManager): static
    {
        $this->extractionStrategyPluginManager = $extractionStrategyPluginManager;

        return $this;
    }
}
