<?php

declare(strict_types=1);

namespace Smtm\Base\Domain\Helper;

use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Infrastructure\Collection\CollectionInterface;

/**
 * @author Alek Nikolov <a.nikolov@smtm.com>
 */
class CollectionHelper
{
    public const COLLECTION_ITEM_TYPE_OBJECT = 1;
    public const COLLECTION_ITEM_TYPE_ARRAY = 2;

    public static function indexCollectionById(
        array|CollectionInterface $collection,
        int $itemType = self::COLLECTION_ITEM_TYPE_OBJECT
    ): array {
        return self::indexCollectionByField($collection, 'id', $itemType);
    }

    public static function indexCollectionByUuid(
        array|CollectionInterface $collection,
        int $itemType = self::COLLECTION_ITEM_TYPE_OBJECT
    ): array {
        return self::indexCollectionByField($collection, 'uuid', $itemType);
    }

    public static function indexCollectionByField(
        array|CollectionInterface $collection,
        string $field,
        int $itemType = self::COLLECTION_ITEM_TYPE_OBJECT
    ): array {
        if ($collection instanceof CollectionInterface) {
            $collection = $collection->getValues();
        }

        $indexedCollection = [];

        switch ($itemType) {
            case self::COLLECTION_ITEM_TYPE_OBJECT:
                array_walk(
                    $collection,
                    function (EntityInterface $item) use (&$indexedCollection, $field) {
                        $indexValue = EntityHelper::accessField($item, $field);
                        $indexedCollection[$indexValue] = $item;
                    }
                );
                break;
            case self::COLLECTION_ITEM_TYPE_ARRAY:
                array_walk(
                    $collection,
                    function (EntityInterface $item) use (&$indexedCollection, $field) {
                        $indexedCollection[$item[$field]] = $item;
                    }
                );
                break;
            default:
                break;
        }

        return $indexedCollection;
    }

    public static function mapCollectionIds(
        array|CollectionInterface $collection,
        int $itemType = self::COLLECTION_ITEM_TYPE_OBJECT
    ): array {
        return self::mapCollectionField($collection, 'id', $itemType);
    }

    public static function mapCollectionUuids(
        array|CollectionInterface $collection,
        int $itemType = self::COLLECTION_ITEM_TYPE_OBJECT
    ): array {
        return self::mapCollectionField($collection, 'uuid', $itemType);
    }

    public static function mapCollectionField(
        array|CollectionInterface $collection,
        string $field,
        int $itemType = self::COLLECTION_ITEM_TYPE_OBJECT
    ): array {
        if ($collection instanceof CollectionInterface) {
            $collection = $collection->getValues();
        }

        $mappedCollection = [];

        switch ($itemType) {
            case self::COLLECTION_ITEM_TYPE_OBJECT:
                $mappedCollection = array_map(
                    function (EntityInterface $item) use ($field) {
                        return EntityHelper::accessField($item, $field);
                    },
                    $collection
                );
                break;
            case self::COLLECTION_ITEM_TYPE_ARRAY:
                $mappedCollection = array_map(
                    function (array $item) use ($field) {
                        return $item[$field];
                    },
                    $collection
                );
                break;
            default:
                break;
        }

        return $mappedCollection;
    }
}
