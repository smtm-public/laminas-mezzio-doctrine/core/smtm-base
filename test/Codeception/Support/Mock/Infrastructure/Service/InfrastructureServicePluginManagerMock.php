<?php

declare(strict_types=1);

namespace SmtmTest\Base\Codeception\Support\Mock\Infrastructure\Service;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use SmtmTest\Base\Codeception\Support\Mock\MockPluginManagerTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InfrastructureServicePluginManagerMock extends InfrastructureServicePluginManager
{
    use MockPluginManagerTrait;
}
