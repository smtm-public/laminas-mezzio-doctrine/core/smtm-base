<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Factory;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Extractor\ExtractorPluginManagerAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExtractorPluginManagerAwareDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var ExtractorPluginManagerAwareInterface $object */
        $object = $callback();

        $object->setExtractorPluginManager($container->get(ExtractorPluginManager::class));

        return $object;
    }
}
