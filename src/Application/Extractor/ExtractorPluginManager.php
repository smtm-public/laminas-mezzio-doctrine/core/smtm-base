<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor;

use Laminas\ServiceManager\AbstractPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExtractorPluginManager extends AbstractPluginManager
{
    protected $instanceOf = AbstractDomainObjectExtractor::class;
}
