<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ArchiveUpdateQueryExecutionMethodCheckIntColumnMigrationTrait
{
    protected function addArchiveUpdateQueryExecutionMethodCheckIntColumn(Table $table): void
    {
        $table->addColumn(
            'archive_update_query_execution_method_check',
            Types::INTEGER,
            ['notNull' => false]
        );
    }

    protected function addArchiveUpdateQueryExecutionMethodCheckIndex(Table $table): void
    {
        $table->addIndex(
            ['archive_update_query_execution_method_check'],
            substr(
                'fk_' . $table->getName() . '_archive_update_query_execution_method_check',
                0,
                SqlHelper::IDENTIFIER_LENGTH_INDEX
            )
        );
    }
}
