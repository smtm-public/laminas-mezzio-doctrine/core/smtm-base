<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Collection;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface PaginatedCollectionInterface extends CollectionInterface
{
    public function getTotalCount(): int;
}
