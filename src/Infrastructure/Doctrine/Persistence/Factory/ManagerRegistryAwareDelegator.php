<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Persistence\Factory;

use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryAwareInterface;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ManagerRegistryAwareDelegator implements DelegatorFactoryInterface
{
    protected string $entityManagerName;

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var ManagerRegistryAwareInterface $object */
        $object = $callback();
        $object->setManagerRegistry(
            $container
                ->get(InfrastructureServicePluginManager::class)
                ->get(ManagerRegistryInterface::class)
        );

        return $object;
    }
}
