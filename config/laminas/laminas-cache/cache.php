<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../../../.env.laminas.laminas-cache')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../../../',
        '.env.laminas.laminas-cache'
    );
    $dotenv->load();
}

return [
    'storage' => [
        'adapter' => [
            'redis' => [
                'server' => [
                    'host' =>
                        EnvHelper::getEnvFromProcessOrSuperGlobal('LAMINAS_LAMINAS_CACHE_STORAGE_ADAPTER_REDIS_SERVER_HOST'),
                    'port' =>
                        EnvHelper::getEnvFromProcessOrSuperGlobal('LAMINAS_LAMINAS_CACHE_STORAGE_ADAPTER_REDIS_SERVER_PORT'),
                ],
            ],
        ],
    ],
];
