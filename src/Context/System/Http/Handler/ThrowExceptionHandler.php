<?php

declare(strict_types=1);

namespace Smtm\Base\Context\System\Http\Handler;

use Smtm\Base\Http\Exception\RuntimeException;
use Smtm\Base\Http\Handler\AbstractHandler;
use Laminas\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ThrowExceptionHandler extends AbstractHandler
{
    public function handle(ServerRequestInterface $request): HtmlResponse
    {
        throw new RuntimeException('Test HTTP exception');

        return new HtmlResponse('Somehow this response got returned!');
    }
}
