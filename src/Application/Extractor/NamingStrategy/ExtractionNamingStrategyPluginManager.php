<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\NamingStrategy;

use Laminas\ServiceManager\AbstractPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExtractionNamingStrategyPluginManager extends AbstractPluginManager
{
    protected $instanceOf = AbstractExtractionNamingStrategy::class;
}
