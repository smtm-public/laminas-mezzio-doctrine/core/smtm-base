<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm\Query;

use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class FilterCollection extends \Doctrine\ORM\Query\FilterCollection
{
    protected Configuration $config;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->config = $em->getConfiguration();
    }

    public function getConfiguration(): Configuration
    {
        return $this->config;
    }

    public function add($name, $class): static
    {
        $this->config->addFilter($name, $class);

        return $this;
    }
}
