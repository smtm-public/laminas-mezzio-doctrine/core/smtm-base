<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter\Factory;

use Laminas\Filter\AbstractFilter;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class OptionsAwareFilterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var AbstractFilter $filter */
        $filter = new $requestedName($options);

        return $filter;
    }
}
