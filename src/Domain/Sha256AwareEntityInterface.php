<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface Sha256AwareEntityInterface extends EntityInterface
{
    public function getSha256(): string;
}
