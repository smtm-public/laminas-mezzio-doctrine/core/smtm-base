<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\BodyParamsMiddlware;

use Smtm\Base\Http\Exception\BadRequestException;
use Smtm\Base\Infrastructure\Enum\Error;
use Mezzio\Helper\BodyParams\BodyParamsMiddleware as MezzioBodyParamsMiddleware;
use Mezzio\Helper\Exception\MalformedRequestBodyException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class BodyParamsMiddleware extends MezzioBodyParamsMiddleware
{
    /**
     * @inheritDoc
     */
    public function __construct()
    {
        parent::__construct();

        $this->addStrategy(new FormDataStrategy());
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            return parent::process($request, $handler);
        } catch (MalformedRequestBodyException $e) {
            throw new BadRequestException(
                Error::getMessage(Error::MALFORMED_REQUEST_BODY),
                Error::MALFORMED_REQUEST_BODY
            );
        }
    }
}
