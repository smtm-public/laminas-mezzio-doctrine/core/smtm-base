<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Persistence;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ManagerRegistryAwareInterface
{
    public function getManagerRegistry(): ManagerRegistryInterface;
    public function setManagerRegistry(ManagerRegistryInterface $managerRegistry): static;
}
