<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\DbPersistStrategy;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareTrait;
use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareTrait;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class NestedTransactionsDbPersistStrategy extends AbstractDbPersistStrategy implements
    ApplicationServicePluginManagerAwareInterface,
    InfrastructureServicePluginManagerAwareInterface
{

    use ApplicationServicePluginManagerAwareTrait,
        InfrastructureServicePluginManagerAwareTrait;

    public const OPTION_KEY_DB_SERVICES = 'dbServices';

    protected array $options = [
        self::OPTION_KEY_DB_SERVICES => [],
    ];

    public function create(
        AbstractDbService $dbService,
        array $data,
        ?EntityInterface $entity = null,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
        $callback = function () use ($dbService, $data, $entity, $options): EntityInterface {
            return call_user_func_array([$dbService, 'createFunc'], [$data, $entity, $options]);
        };

        $dbServiceQueue = $this->options[self::OPTION_KEY_DB_SERVICES];

        /** @var AbstractDbService $nestedDbService */
        while ($nestedDbService = array_shift($dbServiceQueue)) {
            if (is_string($nestedDbService)) {
                $nestedDbService = $this->applicationServicePluginManager->get($nestedDbService);
            }

            if (!$nestedDbService->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
                $callback = function () use (
                    $nestedDbService,
                    $callback,
                    $data,
                    $entity,
                    $options
                ): EntityInterface {
                    return $nestedDbService->transactional($callback);
                };
            }
        }

        if (!$dbService->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
            return $dbService->transactional($callback);
        }

        return $callback();
    }

    public function update(
        AbstractDbService $dbService,
        EntityInterface $entity,
        array $data,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): EntityInterface {
        $callback = function () use ($dbService, $entity, $data, $options): EntityInterface {
            return call_user_func_array([$dbService, 'updateFunc'], [$entity, $data, $options]);
        };

        $dbServiceStack = $this->options[self::OPTION_KEY_DB_SERVICES];

        /** @var AbstractDbService $nestedDbService */
        while ($nestedDbService = array_shift($dbServiceStack)) {
            if (is_string($nestedDbService)) {
                $nestedDbService = $this->applicationServicePluginManager->get($nestedDbService);
            }

            if (!$nestedDbService->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
                $callback = function () use (
                    $nestedDbService,
                    $callback,
                    $entity,
                    $data,
                    $options
                ): EntityInterface {
                    return $nestedDbService->transactional($callback);
                };
            }
        }

        if (!$dbService->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
            return $dbService->transactional($callback);
        }

        return $callback();
    }

    public function save(
        AbstractDbService $dbService,
        EntityInterface $entity,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): EntityInterface {
        $callback = function () use ($dbService, $entity, $options): EntityInterface {
            return call_user_func_array([$dbService, 'saveFunc'], [$entity, $options]);
        };

        $dbServiceStack = $this->options[self::OPTION_KEY_DB_SERVICES];

        /** @var AbstractDbService $nestedDbService */
        while ($nestedDbService = array_shift($dbServiceStack)) {
            if (is_string($nestedDbService)) {
                $nestedDbService = $this->applicationServicePluginManager->get($nestedDbService);
            }

            if (!$nestedDbService->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
                $callback = function () use (
                    $nestedDbService,
                    $callback,
                    $entity,
                    $options
                ): EntityInterface {
                    return $nestedDbService->transactional($callback);
                };
            }
        }

        if (!$dbService->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
            return $dbService->transactional($callback);
        }

        return $callback();
    }
}
