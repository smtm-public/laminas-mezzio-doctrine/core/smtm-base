<?php

declare(strict_types=1);

namespace Smtm\Base;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ConfigCollectionAwareInterface
{
    public function getConfigCollection(): array;
    public function setConfigCollection(array $configCollection): static;
    public function addConfig(string $name, array $config): static;
    public function getConfigByName(string $name): ?array;
    public function removeConfig(string $name): static;
}
