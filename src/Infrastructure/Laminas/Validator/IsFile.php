<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator;

use Laminas\Validator\AbstractValidator;
use Laminas\Validator\Exception\ExtensionNotLoadedException;
use Laminas\Validator\Exception\InvalidArgumentException;
use Traversable;

/**
 * Bidirectional (is|is not) validation for expected value type: path to file
 *
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IsFile extends AbstractValidator
{
    const INVALID_TRUE = 'invalidValueIs';
    const INVALID_FALSE = 'invalidValueIsNot';

    protected array $messageTemplates = [
        self::INVALID_TRUE => 'Invalid value given. Path to file expected.',
        self::INVALID_FALSE => 'Invalid value given. Path should not point to a file.',
    ];

    protected array $options = [];

    /**
     * $options can have the following keys:
     * - is - true|false - whether the value should be of type boolean (true) or not (false)
     *
     * @param array|Traversable $options
     *
     * @throws ExtensionNotLoadedException if ext/intl is not present
     */
    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    /**
     * @param $value
     *
     * @return bool
     *
     * @throws InvalidArgumentException
     */
    public function isValid($value)
    {
        $is = $this->options['is'] ?? true;
        $nullable = $this->options['nullable'] ?? true;

        if (($is && !is_file($value)) || (!$is && is_file($value)) || (!$nullable && $value === null)) {
            $this->error(constant('self::INVALID_' . strtoupper(var_export($is, true))));

            return false;
        }

        return true;
    }
}
