<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait HydratorPluginManagerAwareTrait
{
    protected HydratorPluginManager $hydratorPluginManager;

    public function getHydratorPluginManager(): HydratorPluginManager
    {
        return $this->hydratorPluginManager;
    }

    public function setHydratorPluginManager(HydratorPluginManager $hydratorPluginManager): static
    {
        $this->hydratorPluginManager = $hydratorPluginManager;

        return $this;
    }
}
