<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait EntityManagerAwareTrait
{
    protected ?EntityManagerInterface $entityManager;

    public function getEntityManager(): ?EntityManagerInterface
    {
        return $this->entityManager;
    }

    public function setEntityManager(EntityManagerInterface $entityManager): static
    {
        $this->entityManager = $entityManager;

        return $this;
    }
}
