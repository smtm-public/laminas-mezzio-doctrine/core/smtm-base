<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler\Exception;

class InvalidConfigException extends InvalidArgumentException
{

}
