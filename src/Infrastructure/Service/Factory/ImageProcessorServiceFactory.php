<?php

namespace Smtm\Base\Infrastructure\Service\Factory;

use Smtm\Base\Infrastructure\Service\ImageProcessor;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Imagick;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ImageProcessorServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        return new ImageProcessor(
            $container->get(InfrastructureServicePluginManager::class),
            new Imagick()
        );
    }
}
