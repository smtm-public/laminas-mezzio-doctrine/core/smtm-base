<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Smtm\Base\Application\Extractor\Strategy\Exception\CannotHydrateValuesException;
use Smtm\Base\OptionsAwareInterface;
use Smtm\Base\OptionsAwareTrait;
use Laminas\Hydrator\Strategy\StrategyInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractExtractionStrategy implements OptionsAwareInterface, StrategyInterface
{

    use OptionsAwareTrait;

    /**
     * This class cannot hydrate values
     *
     * @inheritdoc
     */
    public function hydrate($value, ?array $data)
    {
        throw new CannotHydrateValuesException('The ' . __CLASS__ . ' cannot hydrate values.');
    }
}
