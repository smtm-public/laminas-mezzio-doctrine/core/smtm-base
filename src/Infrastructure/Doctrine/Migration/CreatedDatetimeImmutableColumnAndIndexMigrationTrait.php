<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait CreatedDatetimeImmutableColumnAndIndexMigrationTrait
{
    public function addCreatedDatetimeImmutableColumn(
        Table $table,
        array $options = ['notNull' => true, 'default' => '1970-01-01 00:00:00']
    ): void {
        $name = $options['name'] ?? 'created';
        unset($options['name']);

        $table->addColumn(
            $name,
            Types::DATETIME_IMMUTABLE,
            $options
        );
    }

    public function addCreatedDatetimeImmutableColumnAndIndex(
        Table $table,
        ?string $indexName = null,
        array $options = ['notNull' => true, 'default' => '1970-01-01 00:00:00']
    ): void {
        $name = $options['name'] ?? 'created';

        $this->addCreatedDatetimeImmutableColumn($table, $options);

        $table->addIndex(
            [$name],
            substr(
                $indexName ?? 'idx_' . $table->getName() . '_' . $name,
                0,
                SqlHelper::IDENTIFIER_LENGTH_INDEX
            )
        );
    }
}
