#!/usr/bin/env php
<?php

declare(strict_types=1);

chdir(__DIR__ . '/../../../../');

$sopt = [];
$lopt = [];
$opts = getopt(implode($sopt), $lopt, $optInd);

$argumentsWithoutOptions = array_slice($argv, $optInd);

if (!isset($argumentsWithoutOptions[0])) {
    echo 'You must provide 1 argument - the file name of the private key.' . PHP_EOL;
    exit(1);
}

/**
 * Self-called anonymous function that creates its own scope and keep the global namespace clean.
 */
(function (string $keyFileName) {
    if (file_exists('data/keys/' . $keyFileName)) {
        echo 'File ' . getcwd() . '/data/keys/' . $keyFileName . ' exists. Exiting.' . PHP_EOL;
        exit(0);
    }

    if (file_exists('data/keys/' . $keyFileName . '.pub')) {
        echo 'File ' . getcwd() . '/data/keys/' . $keyFileName . '.pub exists. Exiting.' . PHP_EOL;
        exit(0);
    }

    echo 'Generating private (data/db/keys/' . $keyFileName . ')'
        . ' and public (data/db/keys/' . $keyFileName . '.pub) key pair...' . PHP_EOL;
    $resPrivateKey = openssl_pkey_new([
                                          'digest_alg' => 'sha512',
                                          'private_key_bits' => 4096,
                                          'private_key_type' => OPENSSL_KEYTYPE_RSA,
                                      ]);

    if (!file_exists('data/keys')) {
        mkdir('data/keys', 0755, true);
    } else {
        if (is_file('data/keys')) {
            echo 'Directory "' . getcwd() . '/data/keys" cannot be created. The path exists as a file. Exiting.'
                . PHP_EOL;
            exit(0);
        }
    }

    openssl_pkey_export_to_file($resPrivateKey, 'data/keys/' . $keyFileName);
    $publicKeyParams = openssl_pkey_get_details($resPrivateKey);
    $publicKey = $publicKeyParams['key'];
    file_put_contents('data/keys/' . $keyFileName . '.pub', $publicKey);
    echo 'Done!' . PHP_EOL;
    exit(0);
})($argumentsWithoutOptions[0]);
