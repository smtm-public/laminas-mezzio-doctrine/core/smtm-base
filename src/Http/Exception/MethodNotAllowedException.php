<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Exception;

use Smtm\Base\Infrastructure\Helper\HttpHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MethodNotAllowedException extends AbstractHttpException
{
    public const MESSAGE = 'Method not allowed';
    public const CODE = 0;
    public const STATUS = HttpHelper::STATUS_CODE_METHOD_NOT_ALLOWED;
    public const TYPE = 'https://example.com/problems/method_not_allowed';

    protected int $status = self::STATUS;
    protected string $title = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_METHOD_NOT_ALLOWED];
    protected string $detail = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_METHOD_NOT_ALLOWED];
    protected string $type = self::TYPE;
}
