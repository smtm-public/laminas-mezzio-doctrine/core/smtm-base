<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware;

use Smtm\Base\Application\Exception\ProblemDetailsApplicationException;
use Smtm\Base\Application\Exception\ValidationException;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityCreateNonUniqueException;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityNotFoundException;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityUpdateNonUniqueException;
use Smtm\Base\Http\Exception\BadRequestException;
use Smtm\Base\Http\Exception\ConflictException;
use Smtm\Base\Http\Exception\ForbiddenException;
use Smtm\Base\Http\Exception\GatewayTimeoutException;
use Smtm\Base\Http\Exception\InternalServerErrorException;
use Smtm\Base\Http\Exception\MethodNotAllowedException;
use Smtm\Base\Http\Exception\NotFoundException;
use Smtm\Base\Http\Exception\ServiceUnavailableException;
use Smtm\Base\Http\Exception\UnauthorizedException;
use Smtm\Base\Http\Exception\UnprocessableEntityException;
use Smtm\Base\Http\Helper\ErrorResponseHelper;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Helper\IetfHelper;
use Smtm\Base\Infrastructure\Helper\MimeHelper;
use Smtm\Base\Infrastructure\Helper\ThrowableHelper;
use Smtm\Base\Infrastructure\Helper\XmlHelper;
use Smtm\Base\Infrastructure\Service\Log\LoggerCollectionAwareInterface;
use Smtm\Base\Infrastructure\Service\Log\LoggerCollectionAwareTrait;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteMethodNotAllowedResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceBadRequestResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceConflictResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceConnectionException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceForbiddenResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceGatewayTimeoutResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceNotFoundResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceResponseExceptionInterface;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceServiceUnavailableResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceUnauthorizedResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceUnprocessableEntityResponseException;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Mezzio\Middleware\WhoopsErrorResponseGenerator;
use Mezzio\ProblemDetails\Exception\ProblemDetailsExceptionInterface;
use Negotiation\Negotiator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Spatie\ArrayToXml\ArrayToXml;
use Throwable;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ErrorHandler implements MiddlewareInterface, LoggerCollectionAwareInterface
{

    use LoggerCollectionAwareTrait;

    /**
     * Constant value to indicate throwable details (backtrace, previous
     * exceptions, etc.) should be excluded when generating a response from a
     * Throwable.
     *
     * @var bool
     */
    public const EXCLUDE_THROWABLE_DETAILS = false;

    /**
     * Constant value to indicate throwable details (backtrace, previous
     * exceptions, etc.) should be included when generating a response from a
     * Throwable.
     *
     * @var bool
     */
    public const INCLUDE_THROWABLE_DETAILS = true;

    /**
     * @var callable
     */
    protected $responseFactory;

    /**
     * @var callable[]
     */
    protected $listeners = [];
    protected array $responseMutatorListeners = [];

    public function __construct(
        protected array $config,
        callable $responseFactory,
        protected WhoopsErrorResponseGenerator $whoopsErrorResponseGenerator
    ) {
        // Ensures type safety of the composed factory
        $this->responseFactory = function () use ($responseFactory): ResponseInterface {
            return $responseFactory();
        };
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            error_reporting(0);
            set_error_handler(ThrowableHelper::createErrorHandler());

            if ($this->config['debug']) {
                register_shutdown_function([$this, 'handleShutdown'], $request);
            }

            $response = $handler->handle($request);
        } catch (Throwable $e) {
            $response = ErrorResponseHelper::createResponseFromThrowable(
                $request,
                $this->responseFactory,
                $e,
                $this->config,
                $this->getLoggerCollection(),
                $this->whoopsErrorResponseGenerator
            );
            $this->triggerListeners($e, $request, $response);
            $response = $this->triggerResponseMutatorListeners($e, $request, $response);
        } finally {
            restore_error_handler();
        }

        return $response;
    }

    /**
     * Attach an error listener.
     *
     * Each listener receives the following three arguments:
     *
     * - Throwable $error
     * - ServerRequestInterface $request
     * - ResponseInterface $response
     *
     * These instances are all immutable, and the return values of
     * listeners are ignored; use listeners for reporting purposes
     * only.
     */
    public function attachListener(callable $listener): void
    {
        if (in_array($listener, $this->listeners, true)) {
            return;
        }

        $this->listeners[] = $listener;
    }

    /**
     * Trigger all error listeners.
     */
    private function triggerListeners(
        Throwable $error,
        ServerRequestInterface $request,
        ResponseInterface $response
    ): void {
        foreach ($this->listeners as $listener) {
            $listener($error, $request, $response);
        }
    }

    /**
     * Attach an error listener.
     *
     * Each listener receives the following three arguments:
     *
     * - Throwable $error
     * - ServerRequestInterface $request
     * - ResponseInterface $response
     *
     * These instances can mutate the response.
     */
    public function attachResponseMutatorListener(callable $listener): void
    {
        if (in_array($listener, $this->responseMutatorListeners, true)) {
            return;
        }

        $this->responseMutatorListeners[] = $listener;
    }

    protected function triggerResponseMutatorListeners(
        Throwable $error,
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        foreach ($this->responseMutatorListeners as $listener) {
            $response = $listener($error, $request, $response);
        }

        return $response;
    }

    public function handleShutdown(ServerRequestInterface $request)
    {
        $error = error_get_last();

        if ($error && ThrowableHelper::isLevelFatal($error['type'])) {
            // If there was a fatal error, it has not been handled yet.
            $e = new \Exception($error['message'] . ' in (' . $error['file'] . ':' . $error['line']);
            $response = ErrorResponseHelper::createResponseFromThrowable(
                $request,
                $this->responseFactory,
                $e,
                $this->config,
                $this->getLoggerCollection(),
                $this->whoopsErrorResponseGenerator
            );
            $this->triggerListeners($e, $request, $response);
            $response = $this->triggerResponseMutatorListeners($e, $request, $response);

            $emitter = new SapiEmitter();
            $emitter->emit($response);
        }
    }
}
