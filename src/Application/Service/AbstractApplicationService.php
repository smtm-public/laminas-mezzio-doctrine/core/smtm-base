<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service;

use Smtm\Base\Application\Exception\RuntimeException;
use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Hydrator\DomainObjectHydrator;
use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Domain\DomainObjectInterface;
use Smtm\Base\Infrastructure\Collection\Collection;
use Smtm\Base\Infrastructure\Collection\CollectionInterface;
use Smtm\Base\Infrastructure\Collection\PaginatedCollection;
use Smtm\Base\Infrastructure\Collection\PaginatedCollectionInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\Hydrator\HydratorInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * @template T of DomainObjectInterface
 */
abstract class AbstractApplicationService implements ApplicationServiceInterface
{
    public const OPTION_NAME_LOG_EXTRA_DATA =
        'optionNameLogExtraData';
    public const LOG_EXTRA_DATA = [];

    /**
     * @var class-string<T>|null
     */
    protected ?string $domainObjectName = null;
    protected ?string $hydratorName = null;
    protected ?string $extractorName = null;

    public function __construct(
        protected ApplicationServicePluginManager $applicationServicePluginManager,
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected HydratorPluginManager $hydratorPluginManager,
        protected ExtractorPluginManager $extractorPluginManager,
        protected \Laminas\Hydrator\HydratorPluginManager $baseHydratorPluginManager,
    ) {

    }

    public static function extract(
        CollectionInterface|PaginatedCollectionInterface|DomainObjectInterface|array $data,
        HydratorInterface $extractor
    ): ?array {
        if (is_array($data) || $data instanceof CollectionInterface) { // It's a collection of objects
            if ($data instanceof CollectionInterface) {
                $data = $data->getValues();
            }

            $extractedCollection = [];

            foreach ($data as $key => $row) {
                if ($row instanceof DomainObjectInterface) {
                    $extractedCollection[$key] = $extractor->extract($row);
                } else {
                    $extractedCollection[$key] = $row;
                }
            }

            return $extractedCollection;
        }

        return $extractor->extract($data); // It's a single object
    }

    /**
     * @return T
     */
    public function prepareDomainObject(): DomainObjectInterface
    {
        return new $this->domainObjectName;
    }

    public function getHydrator(DomainObjectHydrator | string | null $hydrator = null): DomainObjectHydrator
    {
        if ($hydrator === null && $this->hydratorName === null) {
            throw new RuntimeException(
                'Cannot get hydrator. \''
                . static::class . '::$hydratorName\' has not been set in the application service for domain object \''
                . $this->domainObjectName . ' and no $hydrator argument has been passed to '
                . static::class . '::getHydrator()\''
            );
        }

        $requestedHydrator = $hydrator ?? $this->hydratorName;
        $hydrator = is_object($requestedHydrator)
            ? $requestedHydrator
            : $this->hydratorPluginManager->get($requestedHydrator);

        return $hydrator;
    }

    /**
     * @return T
     */
    public function hydrateDomainObject(
        array $data,
        ?DomainObjectInterface $domainObject = null,
        DomainObjectHydrator | string | null $hydrator = null
    ): DomainObjectInterface {
        if ($this->domainObjectName === null) {
            throw new RuntimeException(
                'Cannot hydrate domain object. \''
                . static::class . '::$domainObjectName\' has not been set in the \''
                . static::class . '\' application service.'
            );
        }

        if ($domainObject === null) {
            $domainObject = $this->prepareDomainObject();
        }

        if ($hydrator === null) {
            $hydrator = $this->getHydrator();
        } elseif (is_string($hydrator)) {
            $hydrator = $this->hydratorPluginManager->get($hydrator);
        }

        $domainObject = $hydrator->hydrate($data, $domainObject);

        return $domainObject;
    }

    public function hydrateCollection(
        CollectionInterface $dataSet,
        ?string $indexBy = null,
        DomainObjectHydrator | string | null $hydrator = null
    ): CollectionInterface {
        $hydratedCollection = [];

        if ($indexBy === null) {
            foreach ($dataSet->getValues() as $data) {
                $hydratedCollection[] = $this->hydrateDomainObject($data, hydrator: $hydrator);
            }
        } else {
            foreach ($dataSet->getValues() as $data) {
                $hydratedCollection[$data[$indexBy]] = $this->hydrateDomainObject($data, hydrator: $hydrator);
            }
        }

        if ($dataSet instanceof PaginatedCollection) {
            $hydratedCollection = new PaginatedCollection($hydratedCollection, $dataSet->getTotalCount());
        } else {
            $hydratedCollection = new Collection($hydratedCollection);
        }

        return $hydratedCollection;
    }

    public function getDomainObjectName(): ?string
    {
        return $this->domainObjectName;
    }
}
