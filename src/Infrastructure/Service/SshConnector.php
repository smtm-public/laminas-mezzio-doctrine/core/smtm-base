<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

use Smtm\Base\Infrastructure\Laminas\Log\LoggerAwareInterface;
use Smtm\Base\Infrastructure\Laminas\Log\LoggerAwareTrait;
use JetBrains\PhpStorm\Pure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class SshConnector extends AbstractInfrastructureService implements LoggerAwareInterface
{

    use LoggerAwareTrait;

    public const AUTHENTICATION_TYPE_PASSWORD = 'password';
    public const OPTION_NAME_LOG_EXTRA_DATA =
        'optionNameLogExtraData';
    public const LOG_EXTRA_DATA = [];

    protected mixed $sshConnection = null;
    protected array $tunnels = [];
    protected array $listeners = [];

    #[Pure] public function __construct(
        InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected ?array $sshConfig = null
    ) {
        parent::__construct($infrastructureServicePluginManager);

        if ($sshConfig !== null) {
            $this->connect($sshConfig);
        }
    }

    public function __destruct()
    {
        $this->disconnect();
    }

    public function connect(array $sshConfig): static
    {
        $this->disconnect();

        $this->sshConfig = $sshConfig;

        try {
            $this->sshConnection = ssh2_connect(
                $sshConfig['host'],
                $sshConfig['port'] ?? 22,
                $sshConfig['methods'] ?? null,
                $sshConfig['callbacks'] ?? null
            );

            if (!empty($sshConfig['authentication'])) {
                if ($sshConfig['authentication']['type'] === static::AUTHENTICATION_TYPE_PASSWORD) {
                    ssh2_auth_password(
                        $this->sshConnection,
                        $sshConfig['authentication']['username'],
                        $sshConfig['authentication']['password']
                    );
                }
            }

            if (!empty($sshConfig['tunnels'])) {
                foreach ($sshConfig['tunnels'] as $tunnelConfig) {
                    $this->tunnels[$tunnelConfig['localPort'] . ':' . $tunnelConfig['host'] . ':' . $tunnelConfig['remotePort']]['stream'] =
                        @ssh2_tunnel(
                            $this->sshConnection,
                            $tunnelConfig['host'],
                            $tunnelConfig['remotePort']
                        );
                    $this->tunnels[$tunnelConfig['localPort'] . ':' . $tunnelConfig['host'] . ':' . $tunnelConfig['remotePort']]['socket'] =
                        socket_import_stream(
                            $this->tunnels[$tunnelConfig['localPort'] . ':' . $tunnelConfig['host'] . ':' . $tunnelConfig['remotePort']]['stream']
                        );
//                    $so = socket_set_option(
//                        $this->tunnels[$tunnelConfig['localPort'] . ':' . $tunnelConfig['host'] . ':' . $tunnelConfig['remotePort']]['socket'],
//                        SOL_TCP,
//                        MCAST_JOIN_GROUP,
//                        [
//                            'group'	=> '224.0.0.23',
//                            'interface' => 'lo',
//                        ]
//                    );
//                    socket_set_option($socket, SOL_SOCKET, SO_KEEPALIVE, 1);
//                    socket_set_option($socket, SOL_TCP, TCP_NODELAY, 1);
                    socket_bind(
                        $this->tunnels[$tunnelConfig['localPort'] . ':' . $tunnelConfig['host'] . ':' . $tunnelConfig['remotePort']]['socket'],
                        '0.0.0.0',
                        $tunnelConfig['localPort']
                    );
                    socket_listen(
                        $this->tunnels[$tunnelConfig['localPort'] . ':' . $tunnelConfig['host'] . ':' . $tunnelConfig['remotePort']]['socket']
                    );
                }
            }

            if (!empty($sshConfig['listeners'])) {
                foreach ($sshConfig['listeners'] as $listenerConfig) {
                    $this->listeners[$listenerConfig['host'] . ':' . $listenerConfig['port']] = @ssh2_forward_listen(
                        $this->sshConnection,
                        $listenerConfig['port'],
                        $listenerConfig['host']
                    );
                }
            }
        } catch (\Throwable $t) {
            $this->disconnect();
        }

        return $this;
    }

    public function disconnect(): ?bool
    {
        if ($this->sshConnection === null) {
            return null;
        }

        return ssh2_disconnect($this->sshConnection);
    }

    public function writeToTunnel(int $localPort, string $host, int $remotePort, string $data): string
    {
        if (empty($this->tunnels[$localPort . ':' . $host . ':' . $remotePort])) {
            $this->tunnels[$localPort . ':' . $host . ':' . $remotePort]['socket'] = socket_create_listen($localPort);
            $this->tunnels[$localPort . ':' . $host . ':' . $remotePort]['stream'] = @ssh2_tunnel(
                $this->sshConnection,
                $host,
                $remotePort
            );
        }

        socket_send(
            $this->tunnels[$localPort . ':' . $host . ':' . $remotePort]['socket'],
            $data,
            strlen($data),
            MSG_EOF
        );
        $output = '';

        while (!feof($this->tunnels[$localPort . ':' . $host . ':' . $remotePort])) {
            $output .= fgets($this->tunnels[$host . ':' . $remotePort]);
        }

        return $output;
    }
}
