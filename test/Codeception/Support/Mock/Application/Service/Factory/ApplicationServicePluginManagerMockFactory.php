<?php

declare(strict_types=1);

namespace SmtmTest\Base\Codeception\Support\Mock\Application\Service\Factory;

use Smtm\Base\Application\Service\Factory\ApplicationServicePluginManagerFactory;
use SmtmTest\Base\Codeception\Support\Mock\Application\Service\ApplicationServicePluginManagerMock;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ApplicationServicePluginManagerMockFactory extends ApplicationServicePluginManagerFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $applicationServicePluginManager = parent::__invoke($container, $requestedName, $options);

        return new ApplicationServicePluginManagerMock(
            null,
            [],
            $applicationServicePluginManager
        );
    }
}
