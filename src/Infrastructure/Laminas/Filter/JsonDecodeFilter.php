<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Laminas\Filter\AbstractFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class JsonDecodeFilter extends AbstractFilter
{
    public const OPTION_NAME_ASSOCIATIVE = 'associative';
    public const OPTION_NAME_DEPTH = 'depth';
    public const OPTION_NAME_FLAGS = 'flags';

    public function __construct(protected $options = null)
    {
        $this->setOptions($options ?? []);
    }

    public function filter($value)
    {
        if ($value === null) {
            return null;
        }

        return json_decode(
            $value,
                $this->options['associative'] ?? false,
                $this->options['depth'] ?? 512,
                $this->options['flags'] ?? 0
        );
    }
}
