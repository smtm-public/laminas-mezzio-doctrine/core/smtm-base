<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MimeHelper
{
    public const CONTENT_TYPE_APPLICATION_PROBLEM_PLUS_XML = 'application/problem+xml';
    public const CONTENT_TYPE_APPLICATION_PROBLEM_PLUS_JSON = 'application/problem+json';
}
