<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ArchivedByIntColumnAndIndexAndForeignKeyMigrationTrait
{
    protected function addArchivedByIntColumnAndIndexAndForeignKey(
        Table $table,
        Table $foreignKeyTable,
        array $foreignKeyColumns = ['id'],
        string $relationIndexName = null,
        string $foreignKeyConstraintName = null
    ): void {
        $table->addColumn(
            'archived_by',
            Types::INTEGER,
            ['notNull' => false]
        );
        $relationIndexName ?: $relationIndexName = 'idx_' . $table->getName() . '_archived_by';
        $table->addIndex(
            ['archived_by'],
            substr($relationIndexName, 0, SqlHelper::IDENTIFIER_LENGTH_INDEX)
        );
        $foreignKeyConstraintName ?: $foreignKeyConstraintName = 'fk_' . $table->getName() . '_archived_by_'
            . $foreignKeyTable->getName() . '_' . implode('_', $foreignKeyColumns);
        $table->addForeignKeyConstraint(
            $foreignKeyTable,
            ['archived_by'],
            $foreignKeyColumns,
            [],
            substr($foreignKeyConstraintName, 0, SqlHelper::IDENTIFIER_LENGTH_INDEX)
        );
    }
}
