<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\Mezzio\Cors\Configuration\Factory;

use Smtm\Base\Http\Middleware\Mezzio\Cors\Configuration\ProjectConfiguration;
use Psr\Container\ContainerInterface;
use Webmozart\Assert\Assert;

class ProjectConfigurationFactory
{
    public function __invoke(ContainerInterface $container): ProjectConfiguration
    {
        $parameters = $container->get('config')[ProjectConfiguration::CONFIGURATION_IDENTIFIER] ?? [];
        Assert::isMap($parameters);
        return new ProjectConfiguration($parameters);
    }
}
