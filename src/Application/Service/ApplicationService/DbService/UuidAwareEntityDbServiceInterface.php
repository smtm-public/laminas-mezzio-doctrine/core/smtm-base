<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\DbService;

use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Domain\UuidAwareEntityInterface;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface UuidAwareEntityDbServiceInterface
{
    public function getOneByUuid(string $uuid, array $options = []): UuidAwareEntityInterface;
    public function getOneOrNullByUuid(string $uuid, array $options = []): ?UuidAwareEntityInterface;
    public function updateByUuid(
        string $uuid,
        array $data,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): UuidAwareEntityInterface;
    public function deleteByUuid(
        string $uuid,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): void;
}
