<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../../../.env.mezzio.mezzio-problem-details')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../../../',
        '.env.mezzio.mezzio-problem-details'
    );
    $dotenv->load();
}

return [
    'debug' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal('MEZZIO_MEZZIO_PROBLEM_DETAILS_DEBUG'),
];
