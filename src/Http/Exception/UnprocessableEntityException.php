<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Exception;

use Smtm\Base\Infrastructure\Helper\HttpHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UnprocessableEntityException extends AbstractHttpException
{
    public const MESSAGE = 'Unprocessable entity';
    public const CODE = 0;
    public const STATUS = HttpHelper::STATUS_CODE_UNPROCESSABLE_ENTITY;
    public const TYPE = 'https://example.com/problems/unprocessable-entity';

    protected int $status = self::STATUS;
    protected string $title = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_UNPROCESSABLE_ENTITY];
    protected string $detail = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_UNPROCESSABLE_ENTITY];
    protected string $type = self::TYPE;
}
