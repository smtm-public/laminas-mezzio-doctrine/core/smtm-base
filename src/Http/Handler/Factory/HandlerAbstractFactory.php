<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler\Factory;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Http\Handler\AbstractHandler;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class HandlerAbstractFactory implements AbstractFactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            applicationServicePluginManager: $container->get(ApplicationServicePluginManager::class),
            infrastructureServicePluginManager: $container->get(InfrastructureServicePluginManager::class),
            extractorPluginManager: $container->get(ExtractorPluginManager::class),
            config: $container->get('config')['base']['http']['handler']
        );
    }

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return is_subclass_of($requestedName, AbstractHandler::class);
    }
}
