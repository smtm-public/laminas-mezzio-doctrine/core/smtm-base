<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityCreateException;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityCreateNonUniqueException;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityDeleteException;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityNotFoundException;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityUpdateException;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityUpdateNonUniqueException;
use Smtm\Base\Application\Service\ApplicationService\Exception\UndefinedEntityException;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Domain\DomainObjectInterface;
use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Domain\MarkedForUpdateInterface;
use Smtm\Base\Infrastructure\Collection\Collection;
use Smtm\Base\Infrastructure\Collection\CollectionInterface;
use Smtm\Base\Infrastructure\Collection\PaginatedCollection;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Helper\ArrayHelper;
use Smtm\Base\Infrastructure\Helper\ReflectionHelper;
use Smtm\Base\Infrastructure\Repository\Exception\DeleteRecordException;
use Smtm\Base\Infrastructure\Repository\Exception\NotFoundRecordException;
use Smtm\Base\Infrastructure\Repository\Exception\SaveNonUniqueRecordException;
use Smtm\Base\Infrastructure\Repository\Exception\SaveRecordException;
use Smtm\Base\Infrastructure\Repository\RepositoryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Doctrine\ORM\Query\Filter\SQLFilter;
use Doctrine\ORM\Query\FilterCollection;
use Doctrine\Persistence\ObjectRepository;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use Laminas\Hydrator\ClassMethodsHydrator;
use Laminas\Hydrator\HydratorInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * @template-covariant T
 * TODO: MD - Add generic at extends AbstractDbService<DomainObjectInterface>
 */
class AbstractDbService extends AbstractApplicationService
{
    public const DEFAULT_PAGE_SIZE = 20;
    public const ORDER_ASC = RepositoryInterface::ORDER_ASC;
    public const ORDER_DESC = RepositoryInterface::ORDER_DESC;

    public const OPTION_KEY_CACHE_RESULT_SET = RepositoryInterface::OPTION_KEY_CACHE_RESULT_SET;
    public const OPTION_KEY_CACHE_RESULT_SET_KEY = RepositoryInterface::OPTION_KEY_CACHE_RESULT_SET_KEY;
    public const OPTION_KEY_CACHE_RESULT_SET_LIFETIME =
        RepositoryInterface::OPTION_KEY_CACHE_RESULT_SET_LIFETIME;
    public const OPTION_KEY_HYDRATION_MODE = RepositoryInterface::OPTION_KEY_HYDRATION_MODE;
    public const OPTION_KEY_EXTRACTOR = 'extractor';
    public const OPTION_KEY_RESULT_COLLECTION_INDEX_BY = RepositoryInterface::OPTION_KEY_RESULT_COLLECTION_INDEX_BY;
    public const OPTION_KEY_RESULT_COLLECTION_INDEX_BY_LETTER_CASE = RepositoryInterface::OPTION_KEY_RESULT_COLLECTION_INDEX_BY_LETTER_CASE;
    public const OPTION_KEY_RESULT_COLLECTION_INDEX_BY_EXTRACTOR = 'indexByExtractor';
    public const OPTION_KEY_FILTERS_ENABLE = RepositoryInterface::OPTION_KEY_FILTERS_ENABLE;
    public const OPTION_KEY_FILTERS_DISABLE = RepositoryInterface::OPTION_KEY_FILTERS_DISABLE;

    protected ?string $entityManagerName = ManagerRegistryInterface::DEFAULT_MANAGER_NAME;
    protected ?string $connectionName = null;

    #[Pure] public function __construct(
        ApplicationServicePluginManager $applicationServicePluginManager,
        InfrastructureServicePluginManager $infrastructureServicePluginManager,
        HydratorPluginManager $hydratorPluginManager,
        ExtractorPluginManager $extractorPluginManager,
        \Laminas\Hydrator\HydratorPluginManager $baseHydratorPluginManager,
        protected ManagerRegistryInterface $entityManagerRegistry
    ) {
        parent::__construct(
            $applicationServicePluginManager,
            $infrastructureServicePluginManager,
            $hydratorPluginManager,
            $extractorPluginManager,
            $baseHydratorPluginManager
        );
    }

    /**
     * @return T|array
     */
    public function getOneBy(
        array $criteria = [],
        array $orderBy = null,
        #[ArrayShape([
            self::OPTION_KEY_CACHE_RESULT_SET => 'bool | null',
            self::OPTION_KEY_CACHE_RESULT_SET_KEY => 'string | null',
            self::OPTION_KEY_CACHE_RESULT_SET_LIFETIME => 'int | null',
            self::OPTION_KEY_HYDRATION_MODE => 'int | null',
            self::OPTION_KEY_EXTRACTOR => HydratorInterface::class . ' | null',
            self::OPTION_KEY_FILTERS_ENABLE => 'array | null',
            self::OPTION_KEY_FILTERS_DISABLE => 'array | null',
        ])] array $options = []
    ): EntityInterface | array {
        $this->enableFilters($options[self::OPTION_KEY_FILTERS_ENABLE] ?? null);
        $this->disableFilters($options[self::OPTION_KEY_FILTERS_DISABLE] ?? null);
        $repository = $this->getRepository();

        try {
            $result = $repository->findOneBy($criteria, $orderBy, $options);

            if (array_key_exists(self::OPTION_KEY_EXTRACTOR, $options) && $options[self::OPTION_KEY_EXTRACTOR] !== null) {
                $result = static::extract($result, $options[self::OPTION_KEY_EXTRACTOR]);
            }

            return $result;
        } catch (NotFoundRecordException $e) {
            throw new EntityNotFoundException(
                EntityNotFoundException::formatMessageForEntity($this->domainObjectName),
                0,
                $e
            );
        } finally {
            $this->disableFilters($options[self::OPTION_KEY_FILTERS_ENABLE] ?? null);
            $this->enableFilters($options[self::OPTION_KEY_FILTERS_DISABLE] ?? null);
        }
    }

    /**
     * @return T|array|null
     */
    public function getOneOrNullBy(
        array $criteria = [],
        array $orderBy = null,
        #[ArrayShape([
            self::OPTION_KEY_CACHE_RESULT_SET => 'bool | null',
            self::OPTION_KEY_CACHE_RESULT_SET_KEY => 'string | null',
            self::OPTION_KEY_CACHE_RESULT_SET_LIFETIME => 'int | null',
            self::OPTION_KEY_HYDRATION_MODE => 'int | null',
            self::OPTION_KEY_EXTRACTOR => HydratorInterface::class . ' | null',
            self::OPTION_KEY_FILTERS_ENABLE => 'array | null',
            self::OPTION_KEY_FILTERS_DISABLE => 'array | null',
        ])] array $options = []
    ): EntityInterface | array | null {
        $this->enableFilters($options[self::OPTION_KEY_FILTERS_ENABLE] ?? null);
        $this->disableFilters($options[self::OPTION_KEY_FILTERS_DISABLE] ?? null);
        $repository = $this->getRepository();

        try {
            $result = $repository->findOneBy($criteria, $orderBy, $options);

            if (array_key_exists(self::OPTION_KEY_EXTRACTOR, $options) && $options[self::OPTION_KEY_EXTRACTOR] !== null) {
                $result = static::extract($result, $options[self::OPTION_KEY_EXTRACTOR]);
            }

            return $result;
        } catch (NotFoundRecordException $e) {
            return null;
        } finally {
            $this->disableFilters($options[self::OPTION_KEY_FILTERS_ENABLE] ?? null);
            $this->enableFilters($options[self::OPTION_KEY_FILTERS_DISABLE] ?? null);
        }
    }

    /**
     * @return T|array
     */
    public function getById(
        array|string|float|int|bool $id,
        #[ArrayShape([
            self::OPTION_KEY_CACHE_RESULT_SET => 'bool | null',
            self::OPTION_KEY_CACHE_RESULT_SET_KEY => 'string | null',
            self::OPTION_KEY_CACHE_RESULT_SET_LIFETIME => 'int | null',
            self::OPTION_KEY_HYDRATION_MODE => 'int | null',
            self::OPTION_KEY_EXTRACTOR => HydratorInterface::class . ' | null',
            self::OPTION_KEY_FILTERS_ENABLE => 'array | null',
            self::OPTION_KEY_FILTERS_DISABLE => 'array | null',
        ])] array $options = []
    ): EntityInterface | array {
        $this->enableFilters($options[self::OPTION_KEY_FILTERS_ENABLE] ?? null);
        $this->disableFilters($options[self::OPTION_KEY_FILTERS_DISABLE] ?? null);
        $repository = $this->getRepository();

        try {
            $result = $repository->find($id);

            if (array_key_exists(self::OPTION_KEY_EXTRACTOR, $options) && $options[self::OPTION_KEY_EXTRACTOR] !== null) {
                $result = static::extract($result, $options[self::OPTION_KEY_EXTRACTOR]);
            }

            return $result;
        } catch (NotFoundRecordException $e) {
            throw new EntityNotFoundException(
                EntityNotFoundException::formatMessageForEntity($this->domainObjectName),
                0,
                $e
            );
        } finally {
            $this->disableFilters($options[self::OPTION_KEY_FILTERS_ENABLE] ?? null);
            $this->enableFilters($options[self::OPTION_KEY_FILTERS_DISABLE] ?? null);
        }
    }

    /**
     * @return T|array|null
     */
    public function getOneOrNullById(
        array|string|float|int|bool $id,
        #[ArrayShape([
            self::OPTION_KEY_CACHE_RESULT_SET => 'bool | null',
            self::OPTION_KEY_CACHE_RESULT_SET_KEY => 'string | null',
            self::OPTION_KEY_CACHE_RESULT_SET_LIFETIME => 'int | null',
            self::OPTION_KEY_HYDRATION_MODE => 'int | null',
            self::OPTION_KEY_EXTRACTOR => HydratorInterface::class . ' | null',
            self::OPTION_KEY_FILTERS_ENABLE => 'array | null',
            self::OPTION_KEY_FILTERS_DISABLE => 'array | null',
        ])] array $options = []
    ): EntityInterface | array | null {
        $this->enableFilters($options[self::OPTION_KEY_FILTERS_ENABLE] ?? null);
        $this->disableFilters($options[self::OPTION_KEY_FILTERS_DISABLE] ?? null);
        $repository = $this->getRepository();

        try {
            $result = $repository->find($id);

            if (array_key_exists(self::OPTION_KEY_EXTRACTOR, $options) && $options[self::OPTION_KEY_EXTRACTOR] !== null) {
                $result = static::extract($result, $options[self::OPTION_KEY_EXTRACTOR]);
            }

            return $result;
        } catch (NotFoundRecordException $e) {
            return null;
        } finally {
            $this->disableFilters($options[self::OPTION_KEY_FILTERS_ENABLE] ?? null);
            $this->enableFilters($options[self::OPTION_KEY_FILTERS_DISABLE] ?? null);
        }
    }

    /**
     * @return CollectionInterface|T[]
     */
    public function getAll(
        array $criteria,
        ?int $page = null,
        ?int $pageSize = null,
        array $orderBy = [],
        #[ArrayShape([
            self::OPTION_KEY_CACHE_RESULT_SET => 'bool | null',
            self::OPTION_KEY_CACHE_RESULT_SET_KEY => 'string | null',
            self::OPTION_KEY_CACHE_RESULT_SET_LIFETIME => 'int | null',
            self::OPTION_KEY_HYDRATION_MODE => 'int | null',
            self::OPTION_KEY_EXTRACTOR => HydratorInterface::class . ' | null',
            self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY => 'string | null',
            self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY_LETTER_CASE => 'string | null',
            self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY_EXTRACTOR => HydratorInterface::class . ' | null',
            self::OPTION_KEY_FILTERS_ENABLE => 'array | null',
            self::OPTION_KEY_FILTERS_DISABLE => 'array | null',
        ])] array $options = []
    ): CollectionInterface | array {
        $this->enableFilters($options[self::OPTION_KEY_FILTERS_ENABLE] ?? null);
        $this->disableFilters($options[self::OPTION_KEY_FILTERS_DISABLE] ?? null);
        $repository = $this->getRepository();

        $resultArray = $repository->findBy(
            $criteria,
            $orderBy,
            $pageSize,
            $page ? ($page - 1) * ($pageSize ?? 1) : null,
            $options
        );

        if (array_key_exists(self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY, $options) && !empty($resultArray)) {
            if (is_array($resultArray[array_key_first($resultArray)])) { // Array hydration mode
                $resultArray = array_column($resultArray, null, $options[self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY]);

                if (!empty($options[self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY_LETTER_CASE])) {
                    $resultArray = ArrayHelper::convertKeyLetterCase(
                        $resultArray,
                        $options[self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY_LETTER_CASE]
                    );
                }
            } else { // Object hydration mode
                $extractor = $options[self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY_EXTRACTOR]
                    ?? $this->baseHydratorPluginManager->get(ClassMethodsHydrator::class);
                $keys = array_column(
                    static::extract($resultArray, $extractor),
                    $options[self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY],
                    $options[self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY]
                );

                if (!empty($options[self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY_LETTER_CASE])) {
                    $keys = ArrayHelper::convertValueLetterCase(
                        $keys,
                        $options[self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY_LETTER_CASE]
                    );
                }

                $resultArray = array_combine(
                    $keys,
                    $resultArray
                );
            }
        }

        $result = $page || $pageSize
            ? new PaginatedCollection($resultArray, $repository->findCountBy($criteria, $options))
            : new Collection($resultArray);

        $this->disableFilters($options[self::OPTION_KEY_FILTERS_ENABLE] ?? null);
        $this->enableFilters($options[self::OPTION_KEY_FILTERS_DISABLE] ?? null);

        if (isset($options[self::OPTION_KEY_EXTRACTOR])) {
            $result = static::extract($result, $options[self::OPTION_KEY_EXTRACTOR]);
        }

        return $result;
    }

    public function getCount(
        array $criteria,
        #[ArrayShape([
            self::OPTION_KEY_CACHE_RESULT_SET => 'bool | null',
            self::OPTION_KEY_CACHE_RESULT_SET_KEY => 'string | null',
            self::OPTION_KEY_CACHE_RESULT_SET_LIFETIME => 'int | null',
            self::OPTION_KEY_FILTERS_ENABLE => 'array | null',
            self::OPTION_KEY_FILTERS_DISABLE => 'array | null',
        ])] array $options = []
    ): int {
        $this->enableFilters($options[self::OPTION_KEY_FILTERS_ENABLE] ?? null);
        $this->disableFilters($options[self::OPTION_KEY_FILTERS_DISABLE] ?? null);
        $repository = $this->getRepository();

        $result = $repository->findCountBy($criteria, $options);

        $this->disableFilters($options[self::OPTION_KEY_FILTERS_ENABLE] ?? null);
        $this->enableFilters($options[self::OPTION_KEY_FILTERS_DISABLE] ?? null);

        return $result;
    }

    public function clearResultSetCache(?string $id = null): bool
    {
        return $this->getRepository()->clearResultSetCache($id);
    }

    /**
     * @return T
     */
    public function createFunc(
        array $data,
        ?EntityInterface $entity = null,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
        return $this->saveFunc($entity, $data, $options);
    }

    /**
     * @return T
     */
    public function create(
        array $data,
        ?EntityInterface $entity = null,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
        if ($this instanceof DbPersistStrategyAwareServiceInterface) {
            return $this->getDbPersistStrategy()->create(
                $this,
                $data,
                $entity,
                $options
            );
        }

//        if ($this->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
//            return $this->createFunc($data, $entity, $options);
//        }

        return $this->transactional(
            function () use ($data, $entity, $options) {
                return call_user_func_array([$this, 'createFunc'], [$data, $entity, $options]);
            }
        );
    }

    /**
     * @return T
     */
    public function updateFunc(
        EntityInterface $entity,
        array $data,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
        return $this->saveFunc($entity, $data, $options);
    }

    /**
     * @return T
     */
    public function updateByIdFunc(
        array|string|float|int|bool $id,
        array $data,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
        $entity = $this->getById($id);

        return $this->saveFunc($entity, $data, $options);
    }

    /**
     * @return T
     */
    public function update(
        EntityInterface $entity,
        array $data,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
        if ($this instanceof DbPersistStrategyAwareServiceInterface) {
            return $this->getDbPersistStrategy()->update(
                $this,
                $entity,
                $data,
                $options
            );
        }

        /**
         * @deprecated due to functional tests already working inside a transaction because of the DataFactory module
         * rollback mechanism
         */
//        if ($this->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
//            return $this->updateFunc($entity, $data, $options);
//        }

        return $this->transactional(
            function () use ($entity, $data, $options) {
                return call_user_func_array([$this, 'updateFunc'], [$entity, $data, $options]);
            }
        );
    }

    /**
     * @return T
     */
    public function updateById(
        array|string|float|int|bool $id,
        array $data,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
        if ($this instanceof DbPersistStrategyAwareServiceInterface) {
            return $this->getDbPersistStrategy()->updateById(
                $this,
                $id,
                $data,
                $options
            );
        }

//        if ($this->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
//            return $this->updateFunc($entity, $data, $options);
//        }

        return $this->transactional(
            function () use ($id, $data, $options) {
                return call_user_func_array([$this, 'updateByIdFunc'], [$id, $data, $options]);
            }
        );
    }

    /**
     * @return T
     */
    public function saveFunc(
        EntityInterface $entity = null,
        array $data = [],
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
        $entity = $this->hydrateDomainObject($data, $entity);

        $repository = $this->getRepository();

        if (!($entity instanceof MarkedForUpdateInterface) || $entity->__getMarkedForUpdate()) {
            try {
                $repository->save($entity);
            } catch (SaveNonUniqueRecordException $e) {
                if (ReflectionHelper::isPropertyInitialized('id', $entity)) {
                    throw new EntityUpdateNonUniqueException(
                        EntityUpdateException::formatMessageForEntity($this->domainObjectName),
                        0,
                        $e
                    );
                } else {
                    throw new EntityCreateNonUniqueException(
                        EntityCreateException::formatMessageForEntity($this->domainObjectName),
                        0,
                        $e
                    );
                }
            } catch (SaveRecordException $e) {
                if (ReflectionHelper::isPropertyInitialized('id', $entity)) {
                    throw new EntityUpdateException(
                        EntityUpdateException::formatMessageForEntity($this->domainObjectName),
                        0,
                        $e
                    );
                } else {
                    throw new EntityCreateException(
                        EntityCreateException::formatMessageForEntity($this->domainObjectName),
                        0,
                        $e
                    );
                }
            }
        }

        if ($entity instanceof MarkedForUpdateInterface) {
            $entity->__setMarkedForUpdate(false);
        }

        return $entity;
    }

    /**
     * @return T
     */
    public function save(
        EntityInterface $entity = null,
        array $data = [],
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
//        if ($this->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
//            return $this->saveFunc($entity);
//        }

        return $this->transactional(
            function () use ($entity, $data, $options) {
                return call_user_func_array([$this, 'saveFunc'], [$entity, $data, $options]);
            }
        );
    }

    public function deleteFunc(
        EntityInterface $entity,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): void {
        $repository = $this->getRepository();

        try {
            $repository->remove($entity);
        } catch (DeleteRecordException $e) {
            throw new EntityDeleteException(
                EntityDeleteException::formatMessageForEntity($this->domainObjectName),
                0,
                $e
            );
        }
    }

    public function delete(
        EntityInterface $entity,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): void {
        if ($this->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
            $this->deleteFunc($entity);

            return;
        }

        $this->transactional(
            function () use ($entity, $options) {
                call_user_func_array([$this, 'deleteFunc'], [$entity, $options]);
            }
        );
    }

    public function transactional(callable $func)
    {
        $repository = $this->getRepository();

        return $repository->transactional($func);
    }

    public function markReadOnly(EntityInterface $entity): void
    {
        $this->entityManagerRegistry
            ->getManager($this->entityManagerName)
            ->getUnitOfWork()
            ->markReadOnly($entity);
    }

    public function attach(EntityInterface $entity): EntityInterface
    {
        return $this->entityManagerRegistry
            ->getManager($this->entityManagerName)
            ->find(get_class($entity), $entity->getId());
    }

    public function detach(EntityInterface $entity): void
    {
        $this->entityManagerRegistry
            ->getManager($this->entityManagerName)
            ->getUnitOfWork()
            ->detach($entity);
    }

    public function refresh(EntityInterface $entity): void
    {
        $this->entityManagerRegistry
            ->getManager($this->entityManagerName)
            ->refresh($entity);
    }

    public function clear(): void
    {
        $this->entityManagerRegistry
            ->getManager($this->entityManagerName)
            ->getUnitOfWork()
            ->clear();
    }

    public function getRepository(): ObjectRepository
    {
        if ($this->domainObjectName === null) {
            throw new UndefinedEntityException(
                sprintf('No entity class has been defined for service %s.', static::class)
            );
        }

        return $this->entityManagerRegistry
            ->getManager($this->entityManagerName)
            ->getRepository($this->domainObjectName);
    }

    public function enableFilter(string $filterName, array $params = []): SQLFilter|null
    {
        $repository = $this->getRepository();

        if (!$this->isFilterEnabled($filterName)) {
            return $repository->enableFilter($filterName, $params);
        }

        $filter = $repository->getFilter($filterName);

        return $repository->setFilterParams($filter, $params);
    }

    public function disableFilter(string $filterName): SQLFilter|null
    {
        $repository = $this->getRepository();

        if ($this->isFilterEnabled($filterName)) {
            return $repository->disableFilter($filterName);
        }

        return null;
    }

    public function isFilterEnabled(string $filterName): bool
    {
        return $this->getRepository()->isFilterEnabled($filterName);
    }

    public function disableAllFilters(): void
    {
        foreach ($this->getRepository()->getEntityManager()->getFilters()->getEnabledFilters() as $filter) {
            $this->disableFilter($filter::NAME);
        }
    }

    public function setEntityManagerName(string $name): static
    {
        $this->entityManagerName = $name;

        return $this;
    }

    public function enableFilters(?array $enableFiltersOptions = null): static
    {
        $repository = $this->getRepository();
        /** @var FilterCollection $filters */
        $filters = $repository->getEntityManager()->getFilters();

        foreach ($enableFiltersOptions ?? [] as $filterDefinition) {
            $filter =
                $filters->isEnabled($filterDefinition['name'])
                    ? $filters->getFilter($filterDefinition['name'])
                    : $filters->enable($filterDefinition['name']);

            if (array_key_exists('params', $filterDefinition)) {
                foreach ($filterDefinition['params'] as $paramName => $paramValue) {
                    $filter->setParameter($paramName, $paramValue);
                }
            }
        }

        return $this;
    }

    public function disableFilters(?array $disableFiltersOptions = null): static
    {
        $repository = $this->getRepository();
        /** @var FilterCollection $filters */
        $filters = $repository->getEntityManager()->getFilters();

        foreach ($disableFiltersOptions ?? [] as $filterDefinition) {
            $filter =
                $filters->isEnabled($filterDefinition['name'])
                    ? $filters->disable($filterDefinition['name'])
                    : $filters->getFilter($filterDefinition['name']);

            if (array_key_exists('params', $filterDefinition)) {
                foreach ($filterDefinition['params'] as $paramName => $paramValue) {
                    $filter->setParameter($paramName, $paramValue);
                }
            }
        }

        return $this;
    }
}
