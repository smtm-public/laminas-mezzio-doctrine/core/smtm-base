<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler\DbServiceEntity;

use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Http\Middleware\ServerParamsAndRequestIdMiddleware;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractReadHandler extends AbstractHandler
{
    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): JsonResponse
    {
        $parsedQueryParams = static::parseQueryParams($this->getMergedFilteredQueryParams($request));
        $entity = $this->applicationService->getById(
            $this->getMergedFilteredAttributes($request)['id'] ?? '',
            array_replace(
                [ApplicationServiceInterface::OPTION_KEY_PARAMS => $parsedQueryParams],
                [ApplicationServiceInterface::OPTION_KEY_METHOD => $request->getMethod()],
                ServerParamsAndRequestIdMiddleware::getRequestParams($request)
            )
        );

        return $this->prepareResponse(
            $entity,
            static::parseQueryParams($this->getMergedFilteredQueryParams($request))
        );
    }
}
