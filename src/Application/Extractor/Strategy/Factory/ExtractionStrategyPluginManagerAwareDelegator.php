<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy\Factory;

use Smtm\Base\Application\Extractor\Strategy\ExtractionStrategyPluginManager;
use Smtm\Base\Application\Extractor\Strategy\ExtractionStrategyPluginManagerAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExtractionStrategyPluginManagerAwareDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var ExtractionStrategyPluginManagerAwareInterface $object */
        $object = $callback();

        $object->setExtractionStrategyPluginManager($container->get(ExtractionStrategyPluginManager::class));

        return $object;
    }
}
