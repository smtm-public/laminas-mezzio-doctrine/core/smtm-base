<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface UnarchivedByAwareEntityInterface
{
    public function getUnarchivedBy();
    public function setUnarchived($unarchivedBy): static;
}
