<?php

declare(strict_types=1);

namespace Smtm\Base\Context\System\Http\Handler;

use Smtm\Base\Http\Handler\AbstractHandler;
use Laminas\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class PhpInfoHandler extends AbstractHandler
{
    public function handle(ServerRequestInterface $request): HtmlResponse
    {
        ob_start();
        phpinfo();
        $output = ob_get_clean();

        return new HtmlResponse($output);
    }
}
