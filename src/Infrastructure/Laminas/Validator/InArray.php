<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator;

use Laminas\Validator\InArray as LaminasInArray;

/**
 * Explodes the value by comma before checking each element
 *
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InArray extends LaminasInArray
{
    protected array $options = [];

    /**
     * Explodes the CSV value before checking each element
     */
    public function isValid($value)
    {
        if (is_string($value)) {
            $value = explode(',', $value);
        } else {
            if (!is_array($value)) {
                $value = [$value];
            }
        }

        $result = true;

        foreach ($value as $valueElement) {
            $result &= parent::isValid($valueElement);
        }

        return $result;
    }
}
