<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Log;

interface LoggerCollectionAwareInterface
{
    /** @return \Psr\Log\LoggerInterface[] */
    public function getLoggerCollection(): array;
    public function setLoggerCollection(array $loggerCollection): static;
    public function addLogger(string $name, \Psr\Log\LoggerInterface $logger): static;
    public function getLoggerByName(string $name): ?\Psr\Log\LoggerInterface;
    public function removeLogger(string $name): static;
}
