<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy\Factory;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExtractorPluginManagerAwareExtractionStrategyFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get(ExtractorPluginManager::class)
        );
    }
}
