<?php

declare(strict_types=1);

namespace Smtm\Base;

return [
    'json_exceptions' => [
        'display'    => true,
        'show_trace' => true,
        'ajax_only'  => true,
    ],
];
