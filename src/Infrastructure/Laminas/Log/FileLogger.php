<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Log;

use Smtm\Base\Infrastructure\Helper\DateTimeHelper;
use Smtm\Base\Infrastructure\LaminasPsrLogger;
use Laminas\Log\Formatter\Simple;
use Laminas\Log\Writer\Stream;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class FileLogger extends LaminasPsrLogger
{
    public const OPTION_KEY_DIR = 'dir';
    public const OPTION_KEY_FILE_NAME_SUFFIX = 'fileNameSuffix';

    protected string $dir;
    protected string $fileNameSuffix;
    protected string $logFileName;

    public function __construct($options = null)
    {
        parent::__construct($options);

        $this->dir = $options[self::OPTION_KEY_DIR];
        $this->fileNameSuffix = $options[self::OPTION_KEY_FILE_NAME_SUFFIX] ?? '';
        $this->logFileName = rtrim($this->dir, '/') . '/' . DateTimeHelper::format(new \DateTime(), DateTimeHelper::FORMAT_FILENAME) . $this->fileNameSuffix . '.log';

        $fileWriter = $this->writerPlugin(
            Stream::class,
            [
                'stream' => $this->logFileName,
            ]
        );
        $fileWriter->setFormatter(
            $fileWriter->formatterPlugin(
                Simple::class,
                $this->getFormatterConfig()
            )
        );
        $this->addWriter($fileWriter);
    }
}
