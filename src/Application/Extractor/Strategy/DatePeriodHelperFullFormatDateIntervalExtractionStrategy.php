<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Smtm\Base\Infrastructure\Helper\DateTimeHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DatePeriodHelperFullFormatDateIntervalExtractionStrategy extends AbstractExtractionStrategy
{
    public const OPTION_KEY_DATE_TIME_START_FORMAT = 'dateTimeStartFormat';
    public const OPTION_KEY_DATE_TIME_END_FORMAT = 'dateTimeEndFormat';

    public const OPTION_KEY_DATE_INTERVAL_FULL_FORMAT_SEPARATOR = 'dateIntervalFullFormatSeparator';
    public const OPTION_KEY_DATE_INTERVAL_FULL_FORMAT_FLAGS = 'dateIntervalFullFormatFlags';
    public const OPTION_KEY_DATE_INTERVAL_FULL_FORMAT_COMPONENT_MAPPING = 'dateIntervalFullFormatComponentMapping';

    public function extract($value, ?object $object = null): array
    {
        if ($value instanceof \DatePeriod) {
            return [
                'start' => $value->getStartDate()->format(
                    $this->options[static::OPTION_KEY_DATE_TIME_START_FORMAT] ?? DateTimeHelper::DEFAULT_FORMAT
                ),
                'interval' => DateTimeHelper::fullFormatDateInterval(
                    $value->getDateInterval(),
                    $this->options[self::OPTION_KEY_DATE_INTERVAL_FULL_FORMAT_SEPARATOR] ?? ',',
                    $this->options[self::OPTION_KEY_DATE_INTERVAL_FULL_FORMAT_FLAGS] ?? DateTimeHelper::FORMAT_DATE_INTERVAL_FLAG_FILTER_EMPTY_VALUES,
                    $this->options[self::OPTION_KEY_DATE_INTERVAL_FULL_FORMAT_COMPONENT_MAPPING] ?? DateTimeHelper::DATE_INTERVAL_FORMAT_COMPONENTS_MAPPING_ISO_8601
                ),
                'end' => $value->getEndDate()->format(
                    $this->options[static::OPTION_KEY_DATE_TIME_END_FORMAT] ?? DateTimeHelper::DEFAULT_FORMAT
                ),
            ];
        }

        return $value;
    }
}
