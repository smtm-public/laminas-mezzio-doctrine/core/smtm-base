<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Persistence\Factory;

use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManagerDecorator;
use Smtm\Base\Infrastructure\Doctrine\Orm\Mapping\EntityListenerResolver;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistry;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Doctrine\Common\EventManager;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Schema\DefaultSchemaManagerFactory;
use Doctrine\DBAL\Types\Type;
use Doctrine\Deprecations\Deprecation;
use Doctrine\Persistence\Proxy;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
final class ManagerRegistryFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): ManagerRegistryInterface {
        /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

        $config = $container->get('config')['doctrine'];

        if (is_array($options)) {
            $config = array_replace_recursive($config, $options);
        }

        if (!empty($config['deprecation']['logger'])) {
            Deprecation::enableWithPsrLogger(
                $infrastructureServicePluginManager->get(
                    $config['deprecation']['logger']['name'],
                    $config['deprecation']['logger']['options'] ?? null
                )
            );
        }

        $middlewares = [];

        if (isset($config['dbal']['middlewares'])) {
            foreach ($config['dbal']['middlewares'] as $middleware) {
                if (isset($middleware['options'])) {
                    $middlewares[] = $infrastructureServicePluginManager->build($middleware['name'], $middleware['options']);
                } else {
                    $middlewares[] = $infrastructureServicePluginManager->get($middleware['name']);
                }
            }
        }

        if (!isset($config['connections'])) {
            throw new RuntimeException('Missing doctrine connection config');
        }

        if (!empty($config['dbal']['types'])) {
            foreach ($config['dbal']['types'] as $name => $class) {
                if (Type::hasType($name)) {
                    Type::overrideType($name, $class);
                } else {
                    Type::addType($name, $class);
                }
            }
        }

        $connections = [];

        foreach ($config['connections'] as $connectionName => $connectionConfig) {
            $connection = null;
            $eventManager = new EventManager();
            $connectionConfigObject = new Configuration();
            $connectionConfigObject->setSchemaManagerFactory(new DefaultSchemaManagerFactory());

            if ($middlewares) {
                $connectionConfigObject->setMiddlewares($middlewares);
            }

            if (is_array($connectionConfig)) {
                $connection = DriverManager::getConnection(
                    $connectionConfig,
                    $connectionConfigObject,
                    $eventManager
                );

                $connection->setNestTransactionsWithSavepoints($connectionConfig['nestTransactionsWithSavepoints'] ?? true);
            }

            $connections[$connectionName] = $connection;
        }

        /** @var EntityManagerDecorator[] $entityManagers */
        $entityManagers = [];

        foreach ($config['orm']['entityManagers'] as $entityManagerName => $entityManagerConfig) {
            $entityManagers[$entityManagerName] = ManagerRegistry::createEntityManager(
                $infrastructureServicePluginManager,
                $config,
                $connections,
                $entityManagerName
            );
        }

        $managerRegistry = new ManagerRegistry(
            'defaultManagerRegistry',
            $connections,
            $entityManagers,
            ManagerRegistryInterface::DEFAULT_CONNECTION_NAME,
            ManagerRegistryInterface::DEFAULT_MANAGER_NAME,
            Proxy::class
        );
        $managerRegistry->setConfig($config);

        /** @var EntityListenerResolver $entityListenerResolver */
        $entityListenerResolver = $infrastructureServicePluginManager->get(EntityListenerResolver::class);
        $entityListenerResolver->setEntityManager($entityManagers[array_key_first($entityManagers)]);

        return $managerRegistry;
    }
}
