<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface IdAwareEntityInterface extends EntityInterface
{
    public function getId(): array|string|float|int;
    public function setId(array|string|float|int $id): static;
}
