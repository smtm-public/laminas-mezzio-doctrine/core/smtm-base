<?php

declare(strict_types=1);

namespace Smtm\Base;

return [
    // System
    'smtm.base.system.throw-exception' => [
        'path' => '/system/throw-exception',
        'method' => 'get',
        'middleware' => Context\System\Http\Handler\ThrowExceptionHandler::class,
        'options' => [
            'authenticationServiceNames' => null,
            'authorizationServiceNames' => null,
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
];
