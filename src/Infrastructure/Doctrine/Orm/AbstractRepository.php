<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm;

use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Infrastructure\Exception\RuntimeException;
use Smtm\Base\Infrastructure\Helper\LetterCaseHelper;
use Smtm\Base\Infrastructure\Repository\Exception\DeleteRecordException;
use Smtm\Base\Infrastructure\Repository\Exception\NotFoundRecordException;
use Smtm\Base\Infrastructure\Repository\Exception\SaveNonUniqueRecordException;
use Smtm\Base\Infrastructure\Repository\Exception\SaveRecordException;
use Smtm\Base\Infrastructure\Repository\RepositoryInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use Throwable;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractRepository extends EntityRepository implements RepositoryInterface
{
    protected string $entityName;

    #[Pure] public function __construct(protected EntityManagerInterface $entityManager, ClassMetadata $class)
    {
        parent::__construct($entityManager, $class);

        $this->entityName = $class->name;
    }

    /**
     * @inheritDoc
     */
    public function getClassName(): string
    {
        return $this->entityName;
    }

    /**
     * Generates an entity alias from its class name
     *
     * @return string
     */
    public function getEntityAlias(): string
    {
        $targetClassShortName = explode('\\', $this->entityName);
        $targetClassShortName = array_pop($targetClassShortName);

        return LetterCaseHelper::format(
            $targetClassShortName,
            [LetterCaseHelper::LETTER_CASE_ACRONYM, LetterCaseHelper::LETTER_CASE_LOWER]
        );
    }

    /**
     * @param object $entity
     *
     * @throws SaveRecordException
     */
    public function save(object $entity): void
    {
        try {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $t) {
            throw new SaveNonUniqueRecordException(
                'Error saving record',
                0,
                $t
            );
        } catch (Throwable $t) {
            throw new SaveRecordException(
                'Error saving record',
                0,
                $t
            );
        }
    }

    /**
     * @param object $entity
     *
     * @throws DeleteRecordException
     */
    public function remove(object $entity): void
    {
        try {
            $this->entityManager->remove($entity);
            $this->entityManager->flush();
        } catch (Throwable $t) {
            throw new DeleteRecordException(
                'Error deleting record',
                0,
                $t
            );
        }
    }

    public function createQueryBuilder($alias, $indexBy = null): QueryBuilder
    {
        if ($alias === null) {
            $alias = $this->getEntityAlias();
        }

        $entityName = $this->getClassName();

        return $this->entityManager->createQueryBuilder()
            ->select($alias)
            ->from($entityName, $alias);
    }

    public function findUniqueValuesInColumn(
        string $column
    ): array {
        $qb = $this->entityManager->createQueryBuilder();

        $entityAlias = $this->getEntityAlias();

        $qb->select($entityAlias . '.' . $column)
            ->from($this->entityName, $entityAlias)
            ->groupBy($entityAlias . '.' . $column);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    /**
     * $id cannot have a type hint because of parent method's signature otherwise a fatal error is thrown
     *
     * @inheritDoc
     *
     * @throws NotFoundRecordException
     */
    public function find($id, $lockMode = null, $lockVersion = null): EntityInterface
    {
        try {
            $entity = $this->entityManager->find($this->entityName, $id);

            if ($entity === null) {
                throw new NotFoundRecordException(
                    'Record not found',
                    0
                );
            }

            return $entity;
        } catch (Throwable $t) {
            throw new NotFoundRecordException(
                'Record not found',
                0,
                $t
            );
        }
    }

    /**
     * Returns a single entity by a set of criteria.
     *
     * @throws NotFoundRecordException
     */
    public function findOneBy(
        array $criteria,
        ?array $orderBy = null,
        #[ArrayShape([
            self::OPTION_KEY_CACHE_RESULT_SET => 'bool | null',
            self::OPTION_KEY_CACHE_RESULT_SET_KEY => 'string | null',
            self::OPTION_KEY_CACHE_RESULT_SET_LIFETIME => 'int | null',
            self::OPTION_KEY_HYDRATION_MODE => 'int | null',
            self::OPTION_KEY_FILTERS_ENABLE => 'array | null',
            self::OPTION_KEY_FILTERS_DISABLE => 'array | null',
        ])] array $options = []
    ): EntityInterface {
        $criteria = $this->beforeFetch($criteria);

        try {
            $entityAlias = $this->getEntityAlias();

            $qb = $this->createQueryBuilder($entityAlias);

            $qb->select($entityAlias)
                ->setFirstResult(0)
                ->setMaxResults(1);

            $joins = [];

            if (!empty($criteria)) {
                $resolvedPredicates = $this->addCriteriaWithJoins(
                    $qb,
                    $criteria,
                    $entityAlias,
                    'and',
                    '0',
                    $joins
                );

                if (!empty($resolvedPredicates)) {
                    $qb->where($resolvedPredicates);
                }
            }

            if ($orderBy !== null) {
                foreach ($orderBy as $field => $order) {
                    $qb->addOrderBy(
                        $this->getFieldIdentifier($field, $entityAlias, $joins),
                        $order
                    );
                }
            }

            if (!empty($joins)) {
                foreach ($joins as $alias => $join) {
                    $qb->addSelect($alias);
                    $qb->leftJoin($join['source'] . '.' . $join['target'], $alias);
                }
            }

            $query = $qb->getQuery();

            if ($options[self::OPTION_KEY_CACHE_RESULT_SET] ?? false) {
                $query->enableResultCache(
                    $options[self::OPTION_KEY_CACHE_RESULT_SET_LIFETIME] ?? null,
                    $options[self::OPTION_KEY_CACHE_RESULT_SET_KEY] ?? null
                );
            }

            $result = $query->getSingleResult(
                $options[self::OPTION_KEY_HYDRATION_MODE] ?? Query::HYDRATE_OBJECT
            );
        } catch (Throwable $t) {
            throw new NotFoundRecordException(
                'Record not found',
                0,
                $t
            );
        }

        $result = $this->afterFetch($result);

        return $result;
    }

    /**
     * @inheritDoc
     *
     * @return EntityInterface[]
     */
    public function findAll(
        #[ArrayShape([
            self::OPTION_KEY_CACHE_RESULT_SET => 'bool | null',
            self::OPTION_KEY_CACHE_RESULT_SET_KEY => 'string | null',
            self::OPTION_KEY_CACHE_RESULT_SET_LIFETIME => 'int | null',
            self::OPTION_KEY_HYDRATION_MODE => 'int | null',
            self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY => 'string | null',
            self::OPTION_KEY_FILTERS_ENABLE => 'array | null',
            self::OPTION_KEY_FILTERS_DISABLE => 'array | null',
        ])] array $options = []
    ): array {
        return $this->findBy([], $options);
    }

    /**
     * @inheritDoc
     *
     * @return EntityInterface[]
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        $limit = null,
        $offset = null,
        #[ArrayShape([
            self::OPTION_KEY_CACHE_RESULT_SET => 'bool | null',
            self::OPTION_KEY_CACHE_RESULT_SET_KEY => 'string | null',
            self::OPTION_KEY_CACHE_RESULT_SET_LIFETIME => 'int | null',
            self::OPTION_KEY_HYDRATION_MODE => 'int | null',
            self::OPTION_KEY_RESULT_COLLECTION_INDEX_BY => 'string | null',
            self::OPTION_KEY_FILTERS_ENABLE => 'array | null',
            self::OPTION_KEY_FILTERS_DISABLE => 'array | null',
        ])] array $options = []
    ): array {
        $criteria = $this->beforeFetch($criteria);

        $entityAlias = $this->getEntityAlias();

        $qb = $this->createQueryBuilder($entityAlias);

        $qb->select($entityAlias);

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $joins = [];

        if (!empty($criteria)) {
            $resolvedPredicates = $this->addCriteriaWithJoins(
                $qb,
                $criteria,
                $entityAlias,
                'and',
                '0',
                $joins
            );

            if (!empty($resolvedPredicates)) {
                $qb->where($resolvedPredicates);
            }
        }

        if ($orderBy !== null) {
            foreach ($orderBy as $field => $order) {
                $qb->addOrderBy(
                    $this->getFieldIdentifier($field, $entityAlias, $joins),
                    $order
                );
            }
        }

        if (!empty($joins)) {
            foreach ($joins as $alias => $join) {
                $qb->addSelect($alias);
                $qb->leftJoin($join['source'] . '.' . $join['target'], $alias);
            }
        }

        $query = $qb->getQuery();

        if ($options[self::OPTION_KEY_CACHE_RESULT_SET] ?? false) {
            $query->enableResultCache(
                $options[self::OPTION_KEY_CACHE_RESULT_SET_LIFETIME] ?? null,
                $options[self::OPTION_KEY_CACHE_RESULT_SET_KEY] ?? null
            );
        }

        $result = $query->getResult(
            $options[self::OPTION_KEY_HYDRATION_MODE] ?? Query::HYDRATE_OBJECT
        );

        $result = $this->afterFetch($result);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function findCountBy(
        array $criteria,
        #[ArrayShape([
            self::OPTION_KEY_CACHE_RESULT_SET => 'bool | null',
            self::OPTION_KEY_CACHE_RESULT_SET_KEY => 'string | null',
            self::OPTION_KEY_CACHE_RESULT_SET_LIFETIME => 'int | null',
            self::OPTION_KEY_FILTERS_ENABLE => 'array | null',
            self::OPTION_KEY_FILTERS_DISABLE => 'array | null',
        ])] array $options = []
    ): int {
        $criteria = $this->beforeFetch($criteria);

        $entityAlias = $this->getEntityAlias();

        $qb = $this->createQueryBuilder($entityAlias);

        $qb->select($entityAlias . ', count(' . $entityAlias . '.id) _c');

        if (!empty($criteria)) {
            $resolvedPredicates = $this->addCriteriaWithSubqueries(
                $qb,
                $criteria,
                $entityAlias,
                'and',
                '0'
            );

            if (!empty($resolvedPredicates)) {
                $qb->where($resolvedPredicates);
            }
        }

        $query = $qb->getQuery();

        if ($options[self::OPTION_KEY_CACHE_RESULT_SET] ?? false) {
            $query->enableResultCache(
                $options[self::OPTION_KEY_CACHE_RESULT_SET_LIFETIME] ?? null,
                $options[self::OPTION_KEY_CACHE_RESULT_SET_KEY] ?? null
            );
        }

        $result = $query->getScalarResult();

        $result = $this->afterFetch($result);

        return (int)($result[0]['_c'] ?? 0);
    }

    public function clearResultSetCache(?string $id = null): bool
    {
        return $id === null
            ? $this->getEntityManager()->getCache()->getQueryCache()->clear()
            : $this->getEntityManager()->getConfiguration()->getQueryCacheImpl()->delete($id);
    }

    /**
     * Parses the given $criteria array and returns a query object
     *
     * @param QueryBuilder $queryBuilder
     * @param array $criteria
     * [
     *      'and' / 'or',
     *      ['=', 'property0', 'value0'],
     *      ['like', 'property1', 'value1'],
     *      ['in', 'property2', ['value21', 'value22']],
     * ]
     *
     * or
     *
     * ['=', 'property', 'value']
     *
     * or
     *
     * ['property'=> 'value']
     *
     * or (in this case the field-value pairs will be combined with "AND"
     *     operator)
     *
     * [
     *      'property0' => 'value0',
     *      'property1' => 'value1'
     * ]
     */
    protected function addCriteriaWithJoins(
        QueryBuilder &$queryBuilder,
        array $criteria,
        string $entityAlias,
        string $currentConjunction = 'and',
        string $ciCarry = '0',
        array &$joins = []
    ): Expr\Comparison|Expr\Func|Expr\Base|array|string|null {
        $criterionCount = count($criteria);
        $resolvedCriteria = [];

        if (isset($criteria[0]) && in_array($criteria[0], ['and', 'or'], true)) {
            $currentConjunction = $criteria[0];

            /**
             * [
             *   'and',
             *   ['property0' => 'value0'],
             *   ['=', 'property1', 'value1'],
             *   [
             *     ['property2' => 'value2'],
             *     ['=', 'property3', 'value3'],
             *     ...
             *   ],
             *   ...
             * ]
             */
            return $this->addCriteriaWithJoins(
                $queryBuilder,
                array_slice($criteria, 1),
                $entityAlias,
                $currentConjunction,
                $ciCarry,
                $joins
            );
        } elseif (
            $criterionCount === 3
            && array_key_exists(0, $criteria)
            && array_key_exists(1, $criteria)
            && array_key_exists(2, $criteria)
            && is_string($criteria[0])
        ) {
            /**
             * ['=', 'property', 'value']
             */
            return $this->buildComparison(
                $queryBuilder,
                $criteria[0],
                $this->getFieldIdentifier($criteria[1], $entityAlias, $joins),
                $criteria[2],
                $ciCarry
            );
        } else {
            /**
             * ['=', 'property0', 'value0'],
             * ['property1' => 'value1']
             * [
             *   ['property2' => 'value2'],
             *   ['=', 'property3', 'value3']
             *   ...
             * ]
             * ...
             */
            $i = 0;

            foreach ($criteria as $childIndex => $childCriterion) {
                $resolvedCriterion = null;

                if (is_array($childCriterion)) {
                    /**
                     * ['=', 'property', 'value']
                     *
                     * - or -
                     *
                     * [
                     *   'and',
                     *   ['=', 'property0', 'value0'],
                     *   ['property1' => 'value1']
                     *   ...
                     * ]
                     */
                    $resolvedCriterion =
                        $this->addCriteriaWithJoins(
                            $queryBuilder,
                            $childCriterion,
                            $entityAlias,
                            $currentConjunction,
                            $ciCarry . $i,
                            $joins
                        );
                } elseif (!is_numeric($childIndex)) {
                    /**
                     * ['property' => 'value']
                     */
                    $resolvedCriterion =
                        $this->addCriteriaWithJoins(
                            $queryBuilder,
                            ['=', $childIndex, $childCriterion],
                            $entityAlias,
                            $currentConjunction,
                            $ciCarry . $i,
                            $joins
                        );
                }

                if ($resolvedCriterion !== null) {
                    $resolvedCriteria[] = $resolvedCriterion;
                }

                $i++;
            }

            return !empty($resolvedCriteria)
                ? $queryBuilder->expr()->{$currentConjunction . 'X'}(...$resolvedCriteria)
                : null;
        }
    }

    /**
     * Similar to the addCriteriaWithJoins method this parses a criteria array and creates a query builder where
     * predicates object. Whenever a relation is referenced, the joining of the related table will happen in a
     * SQL IN(subquery), which the root entity will be filtered against. This method is used in the findCountBy method
     * to prevent counting duplicating rows which can result from left joins.
     */
    protected function addCriteriaWithSubqueries(
        QueryBuilder &$queryBuilder,
        array $criteria,
        string $entityAlias,
        string $currentConjunction = 'and',
        string $ciCarry = '0'
    ): Expr\Comparison|Expr\Base|Expr\Func|array|string|null {
        $criterionCount = count($criteria);
        $resolvedCriteria = [];

        if (isset($criteria[0]) && in_array($criteria[0], ['and', 'or'], true)) {
            $currentConjunction = $criteria[0];

            /**
             * [
             *   'and',
             *   ['property0' => 'value0'],
             *   ['=', 'property1', 'value1'],
             *   [
             *     ['property2' => 'value2'],
             *     ['=', 'property3', 'value3'],
             *     ...
             *   ],
             *   ...
             * ]
             */
            return $this->addCriteriaWithSubqueries(
                $queryBuilder,
                array_slice($criteria, 1),
                $entityAlias,
                $currentConjunction,
                $ciCarry
            );
        } elseif (
            $criterionCount === 3
            && array_key_exists(0, $criteria)
            && array_key_exists(1, $criteria)
            && array_key_exists(2, $criteria)
            && is_string($criteria[0])
        ) {
            if (str_contains($criteria[1], '.')) {
                /**
                 * ['=', 'relation.property', 'value']
                 */
                $joins = [];

                $where = $this->buildComparison(
                    $queryBuilder,
                    $criteria[0],
                    $this->getFieldIdentifier(
                        $criteria[1],
                        $entityAlias . '_sq' . $ciCarry,
                        $joins,
                        '_ex' . $ciCarry
                    ),
                    $criteria[2],
                    $ciCarry
                );

                $qbSubquery =
                    $this->createQueryBuilder($entityAlias . '_sq' . $ciCarry)
                        ->select($entityAlias . '_sq' . $ciCarry)
                        ->where($where);

                foreach ($joins as $alias => $join) {
                    $qbSubquery->leftJoin($join['source'] . '.' . $join['target'], $alias);
                }

                return $queryBuilder->expr()->in($entityAlias, $qbSubquery->getDQL());
            } else {
                /**
                 * ['=', 'property', 'value']
                 */
                $where = $this->buildComparison(
                    $queryBuilder,
                    $criteria[0],
                    $this->getFieldIdentifier(
                        $criteria[1],
                        $entityAlias
                    ),
                    $criteria[2],
                    $ciCarry
                );

                return $where;
            }
        } else {
            /**
             * ['=', 'property0', 'value0'],
             * ['property1' => 'value1']
             * [
             *   ['property2' => 'value2'],
             *   ['=', 'property3', 'value3']
             *   ...
             * ]
             * ...
             */
            $i = 0;

            foreach ($criteria as $childIndex => $childCriterion) {
                $resolvedCriterion = null;

                if (is_array($childCriterion)) {
                    /**
                     * ['=', 'property', 'value']
                     *
                     * - or -
                     *
                     * [
                     *   'and',
                     *   ['=', 'property0', 'value0'],
                     *   ['property1' => 'value1']
                     *   ...
                     * ]
                     */
                    $resolvedCriterion =
                        $this->addCriteriaWithSubqueries(
                            $queryBuilder,
                            $childCriterion,
                            $entityAlias,
                            $currentConjunction,
                            $ciCarry . $i
                        );
                } elseif (!is_numeric($childIndex)) {
                    /**
                     * ['property' => 'value']
                     */
                    $resolvedCriterion =
                        $this->addCriteriaWithSubqueries(
                            $queryBuilder,
                            ['=', $childIndex, $childCriterion],
                            $entityAlias,
                            $currentConjunction,
                            $ciCarry . $i
                        );
                }

                if ($resolvedCriterion !== null) {
                    $resolvedCriteria[] = $resolvedCriterion;
                }

                $i++;
            }

            return !empty($resolvedCriteria)
                ? $queryBuilder->expr()->{$currentConjunction . 'X'}(...$resolvedCriteria)
                : null;
        }
    }

    /**
     * Generates a field identifier which includes its table alias
     * Populates the $joins array with the corresponding required tables/aliases which are to be joined in the query
     */
    protected function getFieldIdentifier(
        string $identifier,
        string $entityAlias,
        array &$joins = [],
        ?string $ciCarry = null
    ): string {
        $identifierElements = explode('.', $identifier);
        $fieldIdentifier = array_pop($identifierElements);
        $tableIdentifiers = $identifierElements;
        $tableAlias = $entityAlias;

        if (!empty($tableIdentifiers)) {
            $tableIdentifiersCount = count($tableIdentifiers);

            for ($i = 0; $i < $tableIdentifiersCount; $i++) {
                $tableName = $tableIdentifiers[$i];
                // check whether the to-be-joined table already exists in the $joins array and return its key or return
                // null otherwise
                $tableAlias = array_reduce(
                    array_keys($joins),
                    function ($carry, $key) use ($joins, $tableIdentifiers, $tableName) {
                        if ($carry !== null) {
                            return $carry;
                        }

                        if ($joins[$key]['target'] === $tableName && $joins[$key]['root'] === $tableIdentifiers[0]) {
                            return $key;
                        }

                        return null;
                    }
                );

                if ($tableAlias === null) {
                    $tableAlias = LetterCaseHelper::format(
                        $tableIdentifiers[$i],
                        LetterCaseHelper::LETTER_CASE_ACRONYM
                    );

                    if (
                        $tableAlias === $entityAlias
                        || (array_key_exists($tableAlias, $joins)
                            && ($tableIdentifiers[$i] !== $joins[$tableAlias]['target']
                                || $tableIdentifiers[0] !== $joins[$tableAlias]['root']))
                    ) {
                        $j = 0;

                        while (array_key_exists($tableAlias . $j, $joins)) {
                            $j++;
                        }

                        $tableAlias .= $j;
                    }

                    if ($ciCarry !== null) {
                        $tableAlias .= $ciCarry;
                    }
                }

                $previousTableAlias = $entityAlias;

                if ($i !== 0) {
                    $previousTableName = $tableIdentifiers[$i - 1];
                    $previousTableAlias = array_reduce(
                        array_keys($joins),
                        function ($carry, $key) use ($joins, $previousTableName) {
                            if ($carry !== null) {
                                return $carry;
                            }

                            if ($joins[$key]['target'] === $previousTableName) {
                                return $key;
                            }

                            return null;
                        }
                    );
                }

                $joins[$tableAlias] = [
                    'root' => $tableIdentifiers[0],
                    'source' => $previousTableAlias,
                    'target' => $tableIdentifiers[$i],
                ];
            }
        }

        return $tableAlias . '.' . $fieldIdentifier;
    }

    /**
     * Before hook for the findOneBy, findAll (inherently as it calls findBy subsequently), findBy, findChunkBy,
     * findCountBy methods which can modify the criteria or trigger implementation or domain-specific logic for
     * centralized refining of the WHERE clause of the query
     */
    protected function beforeFetch(?array $criteria): ?array
    {
        return $criteria;
    }

    /**
     * After hook for the findOneBy, findAll (inherently as it calls findBy subsequently), findBy, findChunkBy,
     * findCountBy methods which can modify the result or trigger implementation or domain-specific logic for cleanup
     */
    protected function afterFetch($result)
    {
        return $result;
    }

    protected function buildComparison(
        QueryBuilder $qb,
        string $comparator,
        string $field,
        /** Callable objects (implementing the __invoke magic method) will be passed as-is */
        object|array|callable|string|int|bool|null $value,
        string $expressionNumber
    ): Expr\Comparison|Expr\Func|Expr\Base|string {
        if (!is_object($value) && is_callable($value)) {
            $value = $value();
        }

        if ($comparator === 'is null'
            || (is_null($value) && $comparator === '=')
        ) {
            $comparison = $qb->expr()->isNull($field);
        } elseif ($comparator === 'is not null'
            || (is_null($value) && $comparator === '!=')
        ) {
            $comparison = $qb->expr()->isNotNull($field);
        } elseif ($comparator === '=') {
            $comparison = $qb->expr()->eq($field, ":p$expressionNumber");
            $qb->setParameter("p$expressionNumber", $value);
        } elseif ($comparator === '!=') {
            $comparison = $qb->expr()->neq($field, ":p$expressionNumber");
            $qb->setParameter("p$expressionNumber", $value);
        } elseif ($comparator === 'like') {
            $comparison = $qb->expr()->like($field, ":p$expressionNumber");
            $qb->setParameter("p$expressionNumber", '%' . $value . '%');
        } elseif ($comparator === 'like%') {
            $comparison = $qb->expr()->like($field, ":p$expressionNumber");
            $qb->setParameter("p$expressionNumber", $value . '%');
        } elseif ($comparator === '%like') {
            $comparison = $qb->expr()->like($field, ":p$expressionNumber");
            $qb->setParameter("p$expressionNumber", '%' . $value);
        } elseif ($comparator === 'not like') {
            $comparison = $qb->expr()->notLike($field, ":p$expressionNumber");
            $qb->setParameter("p$expressionNumber", '%' . $value . '%');
        } elseif ($comparator === 'not like%') {
            $comparison = $qb->expr()->notLike($field, ":p$expressionNumber");
            $qb->setParameter("p$expressionNumber", $value . '%');
        } elseif ($comparator === '%not like') {
            $comparison = $qb->expr()->notLike($field, ":p$expressionNumber");
            $qb->setParameter("p$expressionNumber", '%' . $value);
        } elseif ($comparator === 'in') {
            if (is_object($value)) {
                $comparison = $qb->expr()->isMemberOf(":p$expressionNumber", $field);
            } else {
                $comparison = $qb->expr()->in($field, ":p$expressionNumber");
            }

            $qb->setParameter("p$expressionNumber", $value);
        } elseif ($comparator === 'memberOfAnd') {
            if (is_array($value)) {
                $components = [];
                $expressionSubNumber = 0;

                foreach ($value as $object) {
                    $components[] = $qb->expr()->isMemberOf(":p{$expressionNumber}__{$expressionSubNumber}", $field);
                    $qb->setParameter("p{$expressionNumber}__{$expressionSubNumber}", $object);
                    $expressionSubNumber++;
                }

                $comparison = $qb->expr()->andX(...$components);
            } else {
                $comparison = $qb->expr()->isMemberOf(":p$expressionNumber", $field);
                $qb->setParameter("p$expressionNumber", $value);
            }
        } elseif ($comparator === 'memberOfOr') {
            if (is_array($value)) {
                $components = [];
                $expressionSubNumber = 0;

                foreach ($value as $object) {
                    $components[] = $qb->expr()->isMemberOf(":p{$expressionNumber}__{$expressionSubNumber}", $field);
                    $qb->setParameter("p{$expressionNumber}__{$expressionSubNumber}", $object);
                    $expressionSubNumber++;
                }

                $comparison = $qb->expr()->orX(...$components);
            } else {
                $comparison = $qb->expr()->isMemberOf(":p$expressionNumber", $field);
                $qb->setParameter("p$expressionNumber", $value);
            }
        } elseif ($comparator === 'not in') {
//            if (is_object($value)) {
//                $comparison = $qb->expr()->notMemberOf(":p$expressionNumber", $field);
//            } else {
                $comparison = $qb->expr()->notIn($field, ":p$expressionNumber");
//            }

            $qb->setParameter("p$expressionNumber", $value);
        } elseif ($comparator === '>') {
            $comparison = $qb->expr()->gt($field, ":p$expressionNumber");
            $qb->setParameter("p$expressionNumber", $value);
        } elseif ($comparator === '>=') {
            $comparison = $qb->expr()->orX(
                $qb->expr()->eq($field, ":p$expressionNumber"),
                $qb->expr()->gt($field, ":p$expressionNumber")
            );
            $qb->setParameter("p$expressionNumber", $value);
        } elseif ($comparator === '<') {
            $comparison = $qb->expr()->lt($field, ":p$expressionNumber");
            $qb->setParameter("p$expressionNumber", $value);
        } elseif ($comparator === '<=') {
            $comparison = $qb->expr()->orX(
                $qb->expr()->lt($field, ":p$expressionNumber"),
                $qb->expr()->eq($field, ":p$expressionNumber")
            );
            $qb->setParameter("p$expressionNumber", $value);
        } else {
            throw new RuntimeException('Unsupported operator');
        }

        return $comparison;
    }

    public function transactional(callable $func)
    {
        return $this->entityManager->wrapInTransaction($func);
    }

    public function reConnect(): void
    {
        $this->entityManager->getConnection()->close();
        $this->entityManager->getConnection()->connect();
    }

    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    public function enableFilter(string $filterName, array $params = []): Query\Filter\SQLFilter
    {
        $filter = $this->entityManager->getFilters()->enable($filterName);

        return $this->setFilterParams($filter, $params);
    }

    public function setFilterParams(Query\Filter\SQLFilter $filter, array $params = []): Query\Filter\SQLFilter
    {
        foreach ($params as $paramName => $paramValue) {
            if (is_array($paramValue)) {
                $value = $paramValue['value'];
                $type = $paramValue['type'] ?? null;

                $filter->setParameter($paramName, $value, $type);
            } else {
                $filter->setParameter($paramName, $paramValue);
            }
        }

        return $filter;
    }

    public function disableFilter(string $filterName): Query\Filter\SQLFilter
    {
        return $this->entityManager->getFilters()->disable($filterName);
    }

    public function isFilterEnabled(string $filterName): bool
    {
        return $this->entityManager->getFilters()->isEnabled($filterName);
    }

    public function getFilter(string $filterName): Query\Filter\SQLFilter|null
    {
        return $this->entityManager->getFilters()->getFilter($filterName);
    }
}
