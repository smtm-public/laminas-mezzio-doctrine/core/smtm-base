<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler\DbServiceEntity\UuidAware;

use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Http\Handler\DbServiceEntity\AbstractDeleteHandler as DbServiceEntityAbstractDeleteHandler;
use Smtm\Base\Http\Middleware\ServerParamsAndRequestIdMiddleware;
use Laminas\Diactoros\Response\EmptyResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractDeleteHandler extends DbServiceEntityAbstractDeleteHandler
{
    /** @var AbstractDbService&UuidAwareEntityDbServiceInterface $applicationService */
    protected ?AbstractApplicationService $applicationService = null;

    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $parsedQueryParams = static::parseQueryParams($this->getMergedFilteredQueryParams($request));
        $this->applicationService->deleteByUuid(
            $this->getMergedFilteredAttributes($request)['uuid'] ?? '',
            array_replace(
                [ApplicationServiceInterface::OPTION_KEY_PARAMS => $parsedQueryParams],
                [ApplicationServiceInterface::OPTION_KEY_METHOD => $request->getMethod()],
                ServerParamsAndRequestIdMiddleware::getRequestParams($request)
            )
        );

        return new EmptyResponse();
    }
}
