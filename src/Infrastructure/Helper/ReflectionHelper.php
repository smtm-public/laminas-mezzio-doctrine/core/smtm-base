<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ReflectionHelper
{
    public static function isPropertyInitialized($propertyName, object $object, bool $throwOnNonExisting = true): ?bool
    {
        if (!$throwOnNonExisting) {
            if (!(new \ReflectionClass($object))->hasProperty($propertyName)) {
                return null;
            }
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        $reflectionProperty = new \ReflectionProperty($object, $propertyName);

        if ($reflectionProperty->isPrivate() || $reflectionProperty->isProtected()) {
            $reflectionProperty->setAccessible(true);
        }

        $result = $reflectionProperty->isInitialized($object);

        if ($reflectionProperty->isPrivate() || $reflectionProperty->isProtected()) {
            $reflectionProperty->setAccessible(false);
        }

        return $result;
    }

    public static function getShortName(object $object): string
    {
        return (new \ReflectionObject($object))->getShortName();
    }

    public static function extractObjectPropertyDataUsingGetters(
        object $object,
        bool $recursive = true,
        array $typeFilter = [
            'resource' => true,
            'object' => true,
            'callable' => true,
            'array' => true,
            'string' => true,
            'float' => true,
            'int' => true,
            'bool' => true,
            'null' => true,
        ],
        array $propertyFilter = [],
        array $propertyPathFilter = [],
        array $objectTypeSelfCallbacks = [],
        array $objectTypeCasts = [],
        bool $skipMagicProperties = false,
        array $currentPath = [],
        object $parentObject = null
    ): array {
        $typeFilter = array_replace(
            [
                'resource' => true,
                'object' => true,
                'callable' => true,
                'array' => true,
                'string' => true,
                'float' => true,
                'int' => true,
                'bool' => true,
                'null' => true,
            ],
            $typeFilter
        );
        $reflectionObject = new \ReflectionObject($object);
        $objectProperties = $reflectionObject->getProperties();
        $objectData = [];

        foreach ($objectProperties as $objectProperty) {
            if ((!$skipMagicProperties
                || (!str_starts_with($objectProperty->getName(), '__') && !str_ends_with($objectProperty->getName(), '__')))
                && !in_array($objectProperty->getName(), $propertyFilter)
                && !in_array(array_merge($currentPath, [$objectProperty->getName()]), $propertyPathFilter)
                && (empty($currentPath) || !in_array($currentPath[array_key_last($currentPath)] . '.' . $objectProperty->getName(), $propertyPathFilter))
            ) {
                $methodName = 'get' . ucfirst($objectProperty->getName());

                if (method_exists($object, $methodName)) {
                    $value = $object->{$methodName}();

                    if (is_resource($value) && !$typeFilter['resource']) {
                        continue;
                    } elseif (is_object($value) && !$typeFilter['object']) {
                        continue;
                    } elseif (is_callable($value) && !$typeFilter['callable']) {
                        continue;
                    } elseif (is_array($value) && !$typeFilter['array']) {
                        continue;
                    } elseif (is_string($value) && !$typeFilter['string']) {
                        continue;
                    } elseif (is_float($value) && !$typeFilter['float']) {
                        continue;
                    } elseif (is_int($value) && !$typeFilter['int']) {
                        continue;
                    } elseif (is_bool($value) && !$typeFilter['bool']) {
                        continue;
                    } elseif ($value === null && !$typeFilter['null']) {
                        continue;
                    }

                    if ($recursive && is_object($value)) {
                        if (array_key_exists(get_class($value), $objectTypeSelfCallbacks)) {
                            $value = call_user_func_array([$value, $objectTypeSelfCallbacks[get_class($value)][0]], $objectTypeSelfCallbacks[get_class($value)][1]);
                        } elseif ($object !== $value && ($parentObject !== null || $parentObject !== $value)) { // prevent endless recursion
                            $value = static::extractObjectPropertyDataUsingGetters(
                                $value,
                                $recursive,
                                $typeFilter,
                                $propertyFilter,
                                $propertyPathFilter,
                                $objectTypeSelfCallbacks,
                                $objectTypeCasts,
                                $skipMagicProperties,
                                array_merge($currentPath, [$objectProperty->getName()]),
                                $value
                            );
                        }
                    }

                    $objectData[$objectProperty->getName()] = $value;
                }
            }
        }

        return $objectData;
    }
}
