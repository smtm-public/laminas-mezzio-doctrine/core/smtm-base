<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait InfrastructureServicePluginManagerAwareTrait
{
    protected InfrastructureServicePluginManager $infrastructureServicePluginManager;

    public function getInfrastructureServicePluginManager(): InfrastructureServicePluginManager
    {
        return $this->infrastructureServicePluginManager;
    }

    public function setInfrastructureServicePluginManager(InfrastructureServicePluginManager $infrastructureServicePluginManager): static
    {
        $this->infrastructureServicePluginManager = $infrastructureServicePluginManager;

        return $this;
    }
}
