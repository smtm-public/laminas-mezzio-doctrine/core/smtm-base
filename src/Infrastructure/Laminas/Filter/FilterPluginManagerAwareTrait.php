<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Laminas\Filter\FilterPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait FilterPluginManagerAwareTrait
{
    protected FilterPluginManager $filterPluginManager;

    public function getFilterPluginManager()
    {
        return $this->filterPluginManager;
    }

    public function setFilterPluginManager(FilterPluginManager $pluginManager): static
    {
        $this->filterPluginManager = $pluginManager;

        return $this;
    }
}
