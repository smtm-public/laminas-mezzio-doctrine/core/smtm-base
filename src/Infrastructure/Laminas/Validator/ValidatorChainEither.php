<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator;

use Laminas\ServiceManager\ServiceManager;
use Laminas\Stdlib\PriorityQueue;
use Laminas\Validator\AbstractValidator;
use Laminas\Validator\ValidatorInterface;
use Laminas\Validator\ValidatorPluginManager;

/**
 * OR union validator chain
 *
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ValidatorChainEither extends AbstractValidator implements \Countable, ValidatorInterface
{
    /**
     * Default priority at which validators are added
     */
    const DEFAULT_PRIORITY = 1;

    protected ValidatorPluginManager $plugins;
    protected PriorityQueue $validators;
    protected array $messages = [];

    public function __construct()
    {
        parent::__construct();

        $this->validators = new PriorityQueue();
    }

    /**
     * Return the count of attached validators
     *
     * @return int
     */
    public function count()
    {
        return count($this->validators);
    }

    public function getPluginManager(): ValidatorPluginManager
    {
        if (! $this->plugins) {
            $this->setPluginManager(new ValidatorPluginManager(new ServiceManager));
        }
        return $this->plugins;
    }

    public function setPluginManager(ValidatorPluginManager $plugins): static
    {
        $this->plugins = $plugins;

        return $this;
    }

    /**
     * Retrieve a validator by name
     */
    public function plugin($name, array $options = null)
    {
        $plugins = $this->getPluginManager();

        return $plugins->get($name, $options);
    }

    /**
     * Attach a validator to the end of the chain
     *
     * If $breakChainOnFailure is true, then if the validator fails, the next validator in the chain,
     * if one exists, will not be executed.
     */
    public function attach(
        ValidatorInterface $validator,
        $priority = self::DEFAULT_PRIORITY
    ) {
        $this->validators->insert(
            [
                'instance'            => $validator,
            ],
            $priority
        );

        return $this;
    }

    /**
     * Adds a validator to the beginning of the chain
     */
    public function prependValidator(ValidatorInterface $validator, $breakChainOnFailure = false)
    {
        $priority = self::DEFAULT_PRIORITY;

        if (! $this->validators->isEmpty()) {
            $extractedNodes = $this->validators->toArray(PriorityQueue::EXTR_PRIORITY);
            rsort($extractedNodes, SORT_NUMERIC);
            $priority = $extractedNodes[0] + 1;
        }

        $this->validators->insert(
            [
                'instance'            => $validator,
            ],
            $priority
        );
        return $this;
    }

    /**
     * Use the plugin manager to add a validator by name
     */
    public function attachByName($name, $options = [], $priority = self::DEFAULT_PRIORITY)
    {
        $this->attach($this->plugin($name, $options), $priority);

        return $this;
    }

    /**
     * Use the plugin manager to prepend a validator by name
     */
    public function prependByName($name, $options = []): static
    {
        $validator = $this->plugin($name, $options);
        $this->prependValidator($validator);

        return $this;
    }

    /**
     * Returns true if and only if $value passes one of the validations in the chain
     *
     * Validators are run in the order in which they were added to the chain (FIFO).
     */
    public function isValid($value, $context = null)
    {
        $this->messages = [];
        $messages = [];

        foreach ($this->validators as $element) {
            $validator = $element['instance'];

            if ($validator->isValid($value, $context)) {
                return true;
            } else {
                $messages[]       = $validator->getMessages();
            }
        }

        $this->messages[] = implode(' - OR - ', array_merge(...$messages));

        return false;
    }

    /**
     * Merge the validator chain with the one given in parameter
     *
     * @param ValidatorChainEither $validatorChainEither
     * @return $this
     */
    public function merge(ValidatorChainEither $validatorChainEither)
    {
        foreach ($validatorChainEither->validators->toArray(PriorityQueue::EXTR_BOTH) as $item) {
            $this->attach($item['data']['instance'], $item['priority']);
        }

        return $this;
    }

    /**
     * Returns array of validation failure messages
     *
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    public function getValidators(): array
    {
        return $this->validators->toArray(PriorityQueue::EXTR_DATA);
    }

    /**
     * Invoke chain as command
     *
     * @param  mixed $value
     * @return bool
     */
    public function __invoke($value)
    {
        return $this->isValid($value);
    }

    /**
     * Deep clone handling
     */
    public function __clone()
    {
        $this->validators = clone $this->validators;
    }

    /**
     * Prepare validator chain for serialization
     *
     * Plugin manager (property 'plugins') cannot
     * be serialized. On wakeup the property remains unset
     * and next invocation to getPluginManager() sets
     * the default plugin manager instance (ValidatorPluginManager).
     *
     * @return array
     */
    public function __sleep()
    {
        return ['validators', 'messages'];
    }
}
