<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator;

use Laminas\Validator\ValidatorPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ValidatorPluginManagerAwareTrait
{
    protected ValidatorPluginManager $validatorPluginManager;

    public function getValidatorPluginManager()
    {
        return $this->validatorPluginManager;
    }

    public function setValidatorPluginManager(ValidatorPluginManager $pluginManager): static
    {
        $this->validatorPluginManager = $pluginManager;

        return $this;
    }
}
