<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service;

use Laminas\ServiceManager\AbstractPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ApplicationServicePluginManager extends AbstractPluginManager
{
    protected $instanceOf = ApplicationServiceInterface::class;

    public function __construct($configInstanceOrParentLocator = null, array $config = [])
    {
        parent::__construct($configInstanceOrParentLocator, $config);
    }
}
