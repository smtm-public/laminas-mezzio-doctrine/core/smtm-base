<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Psr\Http\Message\Factory;

use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\ServerRequestFilter\FilterServerRequestInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ServerRequestFactoryDelegator
{
    public function __invoke(
        ContainerInterface $container,
        $name,
        callable $callback,
        array $options = null
    ): callable {
        $filter = $container->has(FilterServerRequestInterface::class)
            ? $container->get(FilterServerRequestInterface::class)
            : null;

        return static fn(): ServerRequestInterface =>
            ServerRequestFactory::fromGlobals(
                query: HttpHelper::parseStr($_SERVER['QUERY_STRING']),
                requestFilter: $filter
            );
    }
}
