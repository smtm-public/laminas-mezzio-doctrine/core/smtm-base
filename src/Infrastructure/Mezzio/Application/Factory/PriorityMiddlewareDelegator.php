<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Mezzio\Application\Factory;

use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Mezzio\Application;
use Psr\Container\ContainerInterface;
use ReflectionProperty;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class PriorityMiddlewareDelegator implements DelegatorFactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $name,
        callable $callback,
        array $options = null
    ): Application {
        /** @var Application $app */
        $app = $callback();
        $reflectionPropertyAppPipeline = new ReflectionProperty($app, 'pipeline');
        $reflectionPropertyAppPipeline->setAccessible(true);

        $config = $container->get('config');

        if (!array_key_exists('middleware_pipeline', $config)) {
            return $app;
        }

        $middlewarePipeline = array_column($config['middleware_pipeline'], null, 'priority');
        ksort($middlewarePipeline);

        foreach ($middlewarePipeline as $middleware) {
            $app->pipe($container->get($middleware['middleware']));
        }

        return $app;
    }
}
