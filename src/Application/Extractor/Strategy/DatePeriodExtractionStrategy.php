<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Smtm\Base\Infrastructure\Helper\DateTimeHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DatePeriodExtractionStrategy extends AbstractExtractionStrategy
{
    public const OPTION_KEY_DATE_TIME_START_FORMAT = 'dateTimeStartFormat';
    public const OPTION_KEY_DATE_TIME_END_FORMAT = 'dateTimeEndFormat';
    public const OPTION_KEY_DATE_INTERVAL_FORMAT = 'dateIntervalFormat';

    public const OPTION_VALUE_DEFAULT_DATE_INTERVAL_FORMAT = '%RR%yy%mm%dd%hh%ii%ss%ff';

    public function extract($value, ?object $object = null): array
    {
        if ($value instanceof \DatePeriod) {
            return [
                'start' => $value->getStartDate()->format(
                    $this->options[static::OPTION_KEY_DATE_TIME_START_FORMAT] ?? DateTimeHelper::DEFAULT_FORMAT
                ),
                'interval' => $value->getDateInterval()->format(
                    $this->options[static::OPTION_KEY_DATE_INTERVAL_FORMAT] ?? static::OPTION_VALUE_DEFAULT_DATE_INTERVAL_FORMAT
                ),
                'end' => $value->getEndDate()->format(
                    $this->options[static::OPTION_KEY_DATE_TIME_END_FORMAT] ?? DateTimeHelper::DEFAULT_FORMAT
                ),
            ];
        }

        return $value;
    }
}
