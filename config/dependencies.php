<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Extractor\Factory\ExtractorPluginManagerFactory;
use Smtm\Base\Application\Extractor\NamingStrategy\ExtractionNamingStrategyPluginManager;
use Smtm\Base\Application\Extractor\NamingStrategy\Factory\ExtractorNamingStrategyPluginManagerFactory;
use Smtm\Base\Application\Extractor\Strategy\ClassNameExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\ExtractionStrategyPluginManager;
use Smtm\Base\Application\Extractor\Strategy\Factory\ExtractorStrategyPluginManagerFactory;
use Smtm\Base\Application\Hydrator\Factory\HydratorPluginManagerFactory;
use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Application\Hydrator\Strategy\Factory\HydratorStrategyPluginManagerFactory;
use Smtm\Base\Application\Hydrator\Strategy\HydrationStrategyPluginManager;
use Smtm\Base\Application\Service\ApplicationService\DbPersistStrategy\DbPersistStrategyPluginManager;
use Smtm\Base\Application\Service\ApplicationService\DbPersistStrategy\Factory\DbPersistStrategyAbstractFactory;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Application\Service\Factory\ApplicationServicePluginManagerAwareDelegator;
use Smtm\Base\Application\Service\Factory\ApplicationServicePluginManagerFactory;
use Smtm\Base\Command\Factory\CommandAbstractFactory;
use Smtm\Base\Http\Handler\Factory\HandlerAbstractFactory;
use Smtm\Base\Http\InputFilter\RequestValidatingInputFilterCollectionPluginManager;
use Smtm\Base\Http\InputFilter\Factory\RequestValidatingInputFilterCollectionPluginManagerFactory;
use Smtm\Base\Http\Mezzio\Container\Factory\ServerRequestErrorResponseGeneratorDelegator;
use Smtm\Base\Http\Mezzio\Router\Middleware\MethodNotAllowedMiddleware;
use Smtm\Base\Http\Mezzio\Router\Middleware\MethodNotAllowedMiddlewareFactory;
use Smtm\Base\Http\Mezzio\Router\Middleware\RouteMiddleware;
use Smtm\Base\Http\Mezzio\Router\Middleware\RouteMiddlewareFactory;
use Smtm\Base\Http\Middleware\ErrorHandler;
use Smtm\Base\Http\Middleware\Factory\ErrorHandlerFactory;
use Smtm\Base\Http\Middleware\Factory\RequestValidatingInputFilterCollectionMiddlewareFactory;
use Smtm\Base\Http\Middleware\Mezzio\Cors\Configuration\Factory\ProjectConfigurationFactory;
use Smtm\Base\Http\Middleware\Mezzio\Cors\Configuration\ProjectConfiguration;
use Smtm\Base\Http\Middleware\Mezzio\Cors\ConfigurationLocator;
use Smtm\Base\Http\Middleware\Mezzio\Cors\CorsMiddleware;
use Smtm\Base\Http\Middleware\Mezzio\Cors\Factory\ConfigurationLocatorFactory;
use Smtm\Base\Http\Middleware\Mezzio\Cors\Factory\CorsMiddlewareFactory;
use Smtm\Base\Http\Middleware\Mezzio\Cors\Service\Cors;
use Smtm\Base\Http\Middleware\Mezzio\Cors\Service\Factory\CorsFactory;
use Smtm\Base\Http\Middleware\Mezzio\Cors\Service\Factory\ResponseFactoryFactory;
use Smtm\Base\Http\Middleware\Mezzio\Cors\Service\ResponseFactory;
use Smtm\Base\Http\Middleware\RequestValidatingInputFilterCollectionMiddleware;
use Smtm\Base\Http\Middleware\ServerParamsAndRequestIdMiddleware;
use Smtm\Base\Http\Psr\Http\Message\Factory\ServerRequestFactoryDelegator;
use Smtm\Base\Infrastructure\Doctrine\Persistence\Factory\ManagerRegistryAwareDelegator;
use Smtm\Base\Infrastructure\Laminas\View\Factory\HelperPluginManagerDelegator;
use Smtm\Base\Infrastructure\Mezzio\Application\Factory\PriorityMiddlewareDelegator;
use Smtm\Base\Infrastructure\Mezzio\Application\Factory\RoutesDelegator;
use Smtm\Base\Infrastructure\Service\Factory\InfrastructureServicePluginManagerAwareDelegator;
use Smtm\Base\Infrastructure\Service\Factory\InfrastructureServicePluginManagerFactory;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Laminas\Log\FilterPluginManager;
use Laminas\Log\FormatterPluginManager;
use Laminas\Log\ProcessorPluginManager;
use Laminas\Log\WriterPluginManager;
use Laminas\ServiceManager\Factory\InvokableFactory;
use Laminas\View\HelperPluginManager;
use Psr\Container\ContainerInterface;

return [
    'abstract_factories' => [
        // ATTENTION: THE ORDER OF THE ABSTRACT FACTORIES IN THE ARRAY IS THE ORDER IN WHICH THEY WILL
        //            ATTEMPT TO CREATE AN INSTANCE. TARGET-SPECIFIC ABSTRACT FACTORIES SHOULD BE
        //            LISTED BEFORE THE MORE GENERAL-PURPOSE ONES.
        CommandAbstractFactory::class,
        HandlerAbstractFactory::class,
        \Laminas\ServiceManager\AbstractFactory\ReflectionBasedAbstractFactory::class,
    ],
    'aliases' => [
        FilterPluginManager::class => 'LogFilterManager',
        FormatterPluginManager::class => 'LogFormatterManager',
        ProcessorPluginManager::class => 'LogProcessorManager',
        WriterPluginManager::class => 'LogWriterManager',
    ],
    'delegators' => [
        \Mezzio\Application::class => [
            // These two are similar to \Mezzio\Container\ApplicationConfigInjectionDelegator::class,
            // (see https://docs.mezzio.dev/mezzio/v3/cookbook/autowiring-routes-and-pipelines/#applicationconfiginjectiondelegator)
            // but the RoutesDelegator expects the routes configuration array to be indexed by route
            // name. This way any misconfiguration of the routes arising from duplicate route names
            // can be caught in the IDE.
            RoutesDelegator::class,
            PriorityMiddlewareDelegator::class,
        ],
        \Psr\Http\Message\ServerRequestInterface::class => [
            ServerRequestFactoryDelegator::class,
        ],
        \Mezzio\Response\ServerRequestErrorResponseGenerator::class => [
            ServerRequestErrorResponseGeneratorDelegator::class,
        ],
        Http\Middleware\ReconfigMiddleware::class => [
            ApplicationServicePluginManagerAwareDelegator::class,
            InfrastructureServicePluginManagerAwareDelegator::class,
        ],
        /*
        \Mezzio\Middleware\ErrorResponseGenerator\ErrorResponseGenerator::class => [
            ErrorResponseGeneratorDelegator::class,
        ],
        */
        Cors::class => [
            InfrastructureServicePluginManagerAwareDelegator::class,
        ],
        ServerParamsAndRequestIdMiddleware::class => [
            InfrastructureServicePluginManagerAwareDelegator::class,
        ],
        Command\Doctrine\GenerateMigration::class => [
            ManagerRegistryAwareDelegator::class,
        ],

        HelperPluginManager::class => [
            HelperPluginManagerDelegator::class,
        ],
    ],

    'factories' => [
        \Mezzio\Middleware\WhoopsErrorResponseGenerator::class =>
            Infrastructure\Mezzio\Container\WhoopsErrorResponseGeneratorFactory::class,
        'Mezzio\Whoops' => \Mezzio\Container\WhoopsFactory::class,
        'Mezzio\WhoopsPageHandler' => \Mezzio\Container\WhoopsPageHandlerFactory::class,

        ResponseFactory::class => ResponseFactoryFactory::class,
        RouteMiddleware::class => RouteMiddlewareFactory::class,
        MethodNotAllowedMiddleware::class => MethodNotAllowedMiddlewareFactory::class,
        CorsMiddleware::class => CorsMiddlewareFactory::class,
        Cors::class => CorsFactory::class,
        ConfigurationLocator::class => ConfigurationLocatorFactory::class,
        ProjectConfiguration::class => ProjectConfigurationFactory::class,
        ErrorHandler::class => ErrorHandlerFactory::class,
        RequestValidatingInputFilterCollectionMiddleware::class =>
            RequestValidatingInputFilterCollectionMiddlewareFactory::class,
        UnderscoreNamingStrategy::class => InvokableFactory::class,
        ClassNameExtractionStrategy::class => InvokableFactory::class,

        ApplicationServicePluginManager::class => ApplicationServicePluginManagerFactory::class,

        InfrastructureServicePluginManager::class => InfrastructureServicePluginManagerFactory::class,

        RequestValidatingInputFilterCollectionPluginManager::class =>
            RequestValidatingInputFilterCollectionPluginManagerFactory::class,

        DbPersistStrategyPluginManager::class => function (ContainerInterface $container, $requestedName) {
            return new DbPersistStrategyPluginManager(
                $container,
                [
                    'abstract_factories' => [
                        DbPersistStrategyAbstractFactory::class,
                    ],
                ]
            );
        },

        HydratorPluginManager::class => HydratorPluginManagerFactory::class,

        HydrationStrategyPluginManager::class => HydratorStrategyPluginManagerFactory::class,

        ExtractorPluginManager::class => ExtractorPluginManagerFactory::class,

        ExtractionStrategyPluginManager::class => ExtractorStrategyPluginManagerFactory::class,

        ExtractionNamingStrategyPluginManager::class => ExtractorNamingStrategyPluginManagerFactory::class,
    ],
];
