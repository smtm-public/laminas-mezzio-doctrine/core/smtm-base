<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Psr\Log\Factory;

use Smtm\Base\Infrastructure\Psr\Log\LoggerAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LoggerAwareDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var LoggerAwareInterface $object */
        $object = $callback();

        $object->setLogger($container->get(LoggerInterface::class));

        return $object;
    }
}
