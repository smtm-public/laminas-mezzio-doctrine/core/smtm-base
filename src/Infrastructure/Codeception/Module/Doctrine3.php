<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Codeception\Module;

use Codeception\Exception\ModuleConfigException;
use Codeception\Lib\Interfaces\DoctrineProvider;
use Codeception\Module\Doctrine2;
use Codeception\Util\ReflectionPropertyAccessor;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Doctrine3 extends Doctrine2
{
    protected ?DoctrineProvider $dependentModule = null;

    public function _depends(): array
    {
        if ($this->config['connection_callback']) {
            return [];
        }

        return [DoctrineProvider::class => $this->dependencyMessage];
    }

    public function _inject(DoctrineProvider $dependentModule = null): void
    {
        $this->dependentModule = $dependentModule;
    }

    public function _getEntityManager(): EntityManagerInterface
    {
        if (is_null($this->em)) {
            $this->retrieveEntityManager();
        }

        return $this->em;
    }

    protected function retrieveEntityManager(): void
    {
        if ($this->dependentModule) {
            $this->em = $this->dependentModule->_getEntityManager();
        } else {
            if (is_callable($this->config['connection_callback'])) {
                $this->em = call_user_func($this->config['connection_callback']);
            }
        }

        if (!$this->em) {
            throw new ModuleConfigException(
                __CLASS__,
                "EntityManager can't be obtained.\n \n"
                . "Please specify either `connection_callback` config option\n"
                . "with callable which will return instance of EntityManager or\n"
                . "pass a dependent module which are Symfony or ZF2\n"
                . "to connect to Doctrine using Dependency Injection Container"
            );
        }


        if (!($this->em instanceof EntityManagerInterface)) {
            throw new ModuleConfigException(
                __CLASS__,
                "Connection object is not an instance of \\Doctrine\\ORM\\EntityManagerInterface.\n"
                . "Use `connection_callback` or dependent framework modules to specify one"
            );
        }

        //$this->em->getConnection()->connect();
    }

    protected function extractPrimaryKey(object $instance)
    {
        $className = get_class($instance);
        $metadata = $this->em->getClassMetadata($className);
        $rpa = new ReflectionPropertyAccessor();
        if ($metadata->isIdentifierComposite) {
            $pk = [];
            foreach ($metadata->identifier as $field) {
                $pk[] = $rpa->getProperty($instance, $field);
            }
        } else {
            $pk = $rpa->getProperty($instance, $metadata->identifier[0]);
        }

        return $pk;
    }

    protected function debugEntityCreation(object $instance, $pks): void
    {
        $message = get_class($instance) . ' entity created with ';

        if (!is_array($pks)) {
            $pks = [$pks];
            $message .= 'primary key ';
        } else {
            $message .= 'composite primary key of ';
        }

        foreach ($pks as $pk) {
            if ($this->isDoctrineEntity($pk)) {
                $message .= get_class($pk) . ': ' . var_export($this->extractPrimaryKey($pk), true) . ', ';
            } else {
                $message .= var_export($pk, true) . ', ';
            }
        }

        $this->debug(trim($message, ' ,'));
    }

    protected function isDoctrineEntity(mixed $pk): bool
    {
        $isEntity = is_object($pk);

        if ($isEntity) {
            try {
                $this->em->getClassMetadata(get_class($pk));
            } catch (\Doctrine\ORM\Mapping\MappingException|\Doctrine\Persistence\Mapping\MappingException $exception) {
                $isEntity = false;
            } catch (\Doctrine\Common\Persistence\Mapping\MappingException $exception) { // @phpstan-ignore-line
                $isEntity = false;
            }
        }

        return $isEntity;
    }

    protected function cleanupEntityManager(): void
    {
        $this->retrieveEntityManager();
        if ($this->config['cleanup']) {
            if ($this->em->getConnection()->isTransactionActive()) {
                try {
                    while ($this->em->getConnection()->getTransactionNestingLevel() > 0) {
                        $this->em->getConnection()->rollback();
                    }
                    $this->debugSection('Database', 'Transaction cancelled; all changes reverted.');
                } catch (\PDOException $e) {
                }
            }

            $this->em->getConnection()->beginTransaction();
            $this->debugSection('Database', 'Transaction started');
        }
    }
}
