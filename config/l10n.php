<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-base.l10n')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.smtm.smtm-base.l10n');
    $dotenv->load();
}

$configL10n = [
    'locale' => [
        'id' => [
            'posix' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_L10N_LOCALE_ID_POSIX'),
            'icu' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_L10N_LOCALE_ID_ICU'),
        ],
    ],
    'localeFallback' => [
        'id' => [
            'posix' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_L10N_LOCALE_FALLBACK_ID_POSIX'),
            'icu' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_L10N_LOCALE_FALLBACK_ID_ICU'),
        ],
    ],
    'dateTime' => [
        'defaultTimeZone' => EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_BASE_L10N_DATETIME_DEFAULT_TIME_ZONE',
            date_default_timezone_get()
        ),
    ],
];

date_default_timezone_set($configL10n['dateTime']['defaultTimeZone']);

return $configL10n;
