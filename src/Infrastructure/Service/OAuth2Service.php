<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class OAuth2Service extends AbstractInfrastructureService
{
    public function generateAuthCode(
        array $data
    ): string {
        /** @var CryptoService $cryptoService */
        $cryptoService = $this->infrastructureServicePluginManager->get(CryptoService::class);
        $expires = $data['expires'];

        if ($expires instanceof \DateTimeInterface) {
            $expires = $expires->getTimestamp();
        }

        return $cryptoService->encrypt(
            json_encode([
                'uuid' => $data['uuid'],
                'expires' => $expires,
                'salt' => CryptoService::generateSalt()
            ])
        );
    }

    public function generateAccessToken(
        array $data
    ): string {
        /** @var CryptoService $cryptoService */
        $cryptoService = $this->infrastructureServicePluginManager->get(CryptoService::class);
        $expires = $data['expires'];

        if ($expires instanceof \DateTimeInterface) {
            $expires = $expires->getTimestamp();
        }

        return $cryptoService->encrypt(
            json_encode([
                'uuid' => $data['uuid'],
                'providerUuid' => $data['providerUuid'],
                'expires' => $expires,
                'salt' => CryptoService::generateSalt()
            ])
        );
    }

    public function generateRefreshToken(
        array $data
    ): string {
        /** @var CryptoService $cryptoService */
        $cryptoService = $this->infrastructureServicePluginManager->get(CryptoService::class);

        return $cryptoService->encrypt(
            json_encode([
                'uuid' => $data['uuid'],
                'providerUuid' => $data['providerUuid'],
                'refreshTokenExpires' => $data['refreshTokenExpires'],
                'accessTokenHash' => hash(
                    'sha256',
                    $data['accessToken']
                ),
                'salt' => CryptoService::generateSalt()
            ])
        );
    }
}
