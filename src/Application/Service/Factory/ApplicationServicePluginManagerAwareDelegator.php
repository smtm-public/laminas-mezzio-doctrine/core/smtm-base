<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\Factory;

use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

class ApplicationServicePluginManagerAwareDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var ApplicationServicePluginManagerAwareInterface $object */
        $object = $callback();

        $object->setApplicationServicePluginManager($container->get(ApplicationServicePluginManager::class));

        return $object;
    }
}
