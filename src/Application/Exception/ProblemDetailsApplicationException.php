<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Exception;

use Smtm\Base\Http\Middleware\Mezzio\ProblemDetails\Exception\CommonProblemDetailsExceptionTrait;
use Mezzio\ProblemDetails\Exception\ProblemDetailsExceptionInterface;
use Throwable;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ProblemDetailsApplicationException extends RuntimeException implements ProblemDetailsExceptionInterface
{
    use CommonProblemDetailsExceptionTrait;

    protected const STATUS = 500;
    protected const TYPE = 'example.com';
    protected const TITLE = 'Application Service Exception';

    public function __construct($message = '', $code = 0, Throwable $previous = null, $additional = [])
    {
        parent::__construct($message, $code, $previous);

        $this->class = static::class;
        $this->status = static::STATUS;
        $this->type   = self::TYPE;
        $this->title  = self::TITLE;
        $this->detail = '';
        $this->additional = $additional;
    }
}
