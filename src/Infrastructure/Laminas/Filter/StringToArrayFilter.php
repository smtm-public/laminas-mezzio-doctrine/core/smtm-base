<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Smtm\Base\Application\Exception\InvalidArgumentException;
use Smtm\Base\Infrastructure\Helper\ReflectionHelper;
use Laminas\Filter\AbstractFilter;

/**
 * @author Nikolay Nikolov <n.nikolov@smtm.bg>
 */
class StringToArrayFilter extends AbstractFilter
{
    protected string $separator;
    protected bool $uniqueValues = true;

    public function __construct(protected $options = null)
    {
        if ($options !== null) {
            $this->setOptions($options);
        }
    }

    public function getSeparator(): string
    {
        return $this->separator;
    }

    public function setSeparator(string $separator): static
    {
        $this->separator = $separator;

        return $this;
    }

    public function isUniqueValues(): bool
    {
        return $this->uniqueValues;
    }

    public function setUniqueValues(bool $uniqueValues): static
    {
        $this->uniqueValues = $uniqueValues;

        return $this;
    }


    public function filter($value)
    {
        if ($value === null || (is_string($value) && strlen($value) === 0)) {
            return null;
        }

        if (!is_string($value)) {
            throw new InvalidArgumentException(
                sprintf('%s expects a string value; received "%s"', __METHOD__, gettype($value))
            );
        }

        if (!ReflectionHelper::isPropertyInitialized('separator', $this)) {
            throw new InvalidArgumentException(sprintf('%s expects a "separator" option; none given', __METHOD__));
        }

        $values = array_filter(explode($this->getSeparator(), $value), fn(string $item) => strlen($item) > 0);

        if ($this->isUniqueValues()) {
            $values = array_unique($values);
        }

        return $values;
    }
}
