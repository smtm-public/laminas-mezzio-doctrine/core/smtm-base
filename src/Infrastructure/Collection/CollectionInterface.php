<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Collection;

use ArrayAccess;
use Countable;
use Iterator;
use Traversable;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * @template T
 */
interface CollectionInterface extends Traversable, Iterator, ArrayAccess, Countable
{
    /**
     * @return T[]
     */
    public function getValues(): array;

    public function keyFirst(): string|int;

    /**
     * @return T
     */
    public function first(): mixed;

    public function keyLast(): string|int;

    /**
     * @return T
     */
    public function last(): mixed;

    /**
     * @param T $value
     */
    public function mergeValue(mixed $value, string $key = null): static;

    /**
     * @param T $value
     */
    public function replaceValue(mixed $value, string $key = null): static;

    /**
     * @return T
     */
    public function shift(): mixed;

    /**
     * @param T $value
     */
    public function unshift(mixed $value): static;

    /**
     * @param T $value
     */
    public function push(mixed $value): static;

    /**
     * @return T
     */
    public function pop(): mixed;

    /**
     * @param T[] $values
     */
    public function merge(array $values): static;

    /**
     * @param T[] $values
     */
    public function mergeRecursive(array $values): static;

    /**
     * @param T[] $values
     */
    public function replace(array $values): static;

    /**
     * @param T[] $values
     */
    public function replaceRecursive(array $values): static;

    /**
     * @param T[] $values
     */
    public function union(array $values): static;
}
