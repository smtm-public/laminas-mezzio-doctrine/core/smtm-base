<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RuntimeException extends \Smtm\Base\Http\Exception\RuntimeException
{

}
