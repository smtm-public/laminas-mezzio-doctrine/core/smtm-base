<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ProblemDetailsApplicationConflictException extends ProblemDetailsApplicationRuntimeException
{

}
