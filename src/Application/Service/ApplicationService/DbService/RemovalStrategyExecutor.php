<?php

namespace Smtm\Base\Application\Service\ApplicationService\DbService;

use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Strategy\AbstractRemovalStrategy;
use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Domain\Helper\EntityHelper;
use Smtm\Base\Domain\IdAwareEntityInterface;
use Smtm\Base\Domain\UuidAwareEntityInterface;
use Smtm\Base\Infrastructure\Helper\ReflectionHelper;
use Smtm\Base\Infrastructure\Laminas\Log\LoggerAwareInterface;
use Smtm\Base\Infrastructure\Laminas\Log\LoggerAwareTrait;

class RemovalStrategyExecutor extends AbstractApplicationService implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    public function execute(IdAwareEntityInterface $entity, AbstractRemovalStrategy $strategy)
    {
        $this->executeUpdates($entity, $strategy->updates());
        $this->executeRemovalMap($entity, $strategy->removalMap());
    }

    private function executeUpdates(IdAwareEntityInterface $removalEntity, array $updates)
    {
        foreach ($updates as $serviceClass => $updateConfig) {
            /** @var AbstractDbService $service */
            $service = $this->applicationServicePluginManager->get($serviceClass);
            $service->disableAllFilters();

            foreach ($updateConfig as $updateConfigEntry) {
                $entities = $service->getAll($this->mapCriteria($updateConfigEntry[1], $removalEntity));

                /** @var EntityInterface $relatedEntity */
                foreach ($entities as $relatedEntity) {
                    foreach ($updateConfigEntry[0] as $field => $value) {
                            if (is_array($value) && array_key_exists('service', $value)) {
                                /** @var AbstractDbService $valueProviderService */
                                $valueProviderService = $this->applicationServicePluginManager->get($value['service']);
                                $value = $valueProviderService->getOneBy($value['value']);
                            }

                            $setter = EntityHelper::generateSetterName($field);
                            $relatedEntity->{$setter}($value);
                    }

                    $service->getRepository()->save($relatedEntity);
                }
            }
        }
    }

    private function executeRemovalMap(IdAwareEntityInterface $removalEntity, array $map)
    {
        $this->log('Executing removal of ' . ReflectionHelper::getShortName($removalEntity));

        foreach ($map as $serviceClass => $removalConfig) {
            /** @var AbstractDbService $service */
            $service = $this->applicationServicePluginManager->get($serviceClass);
            $service->disableAllFilters();
            $primaryKey = $removalConfig['pk'] ?? 'id';

            $this->removeEntities($service, $removalEntity, $removalConfig, $primaryKey);
        }
    }

    private function removeEntities(
        AbstractDbService $service,
        IdAwareEntityInterface $removalEntity,
        array $removalConfig,
        $primaryKey
    )
    {
        $criteria = $this->mapCriteria($removalConfig['criteria'], $removalEntity);

        $entities = $service->getAll(
            $criteria,
            null,
            null,
            [$primaryKey => 'desc']
        );

        foreach ($entities as $entity) {
            if (isset($removalConfig['children'])) {
                $this->processChildren($entity, $removalConfig);
            }

            if ($entity instanceof IdAwareEntityInterface) {
                $this->log('Removing ' . ReflectionHelper::getShortName($entity) . ' #' . $entity->getId());
            } else if ($entity instanceof UuidAwareEntityInterface) {
                $this->log('Removing ' . ReflectionHelper::getShortName($entity) . ' #' . $entity->getUuid());
            }

            $service->getRepository()->remove($entity);
        }
    }

    private function processChildren(EntityInterface $entity, array $removalConfig)
    {
        foreach ($removalConfig['children'] as $childServiceClass => $child) {
            $relations = is_string($child['fk']) ? [$child['fk']] : $child['fk'];
            foreach ($relations as $foreignKey) {
                $child['criteria'] = [$foreignKey => $entity];

                if ($entity instanceof UuidAwareEntityInterface) {
                    $this->log('Searching for ' . $childServiceClass . ' entities by ' . $entity->getUuid());
                }

                $this->executeRemovalMap($entity, [$childServiceClass => $child]);
            }
        }
    }

    private function mapCriteria(array $criteria, IdAwareEntityInterface $removalEntity)
    {
        array_walk_recursive(
            $criteria,
            function (&$value, $key) use ($removalEntity) {
                if ($value === AbstractRemovalStrategy::ENTITY_ID) {
                    $value = $removalEntity->getId();
                }
            }
        );

        return $criteria;
    }

    private function log(string $message)
    {
        if (!is_null($this->logger)) {
            $this->logger->info($message);
        }
    }
}
