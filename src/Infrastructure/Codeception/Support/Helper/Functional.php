<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Codeception\Support\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * all public methods declared in this helper class will be available in $I
 */
class Functional extends \Codeception\Module
{

}
