<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.mezzio')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.mezzio');
    $dotenv->load();
}

$configMezzio = [
    'error_handler' => [
        'template_renderer_enabled' => filter_var(EnvHelper::getEnvFromProcessOrSuperGlobal('MEZZIO_ERROR_HANDLER_TEMPLATE_RENDERER_SERVICE_NAME'), FILTER_VALIDATE_BOOL),
        'template_renderer_service_name' => EnvHelper::getEnvFromProcessOrSuperGlobal('MEZZIO_ERROR_HANDLER_TEMPLATE_RENDERER_SERVICE_NAME'),
        'template_error' => EnvHelper::getEnvFromProcessOrSuperGlobal('MEZZIO_ERROR_HANDLER_TEMPLATE_ERROR'),
    ],
];

return $configMezzio;
