<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait RelationColumnAndIndexAndForeignKeyMigrationTrait
{
    protected function addRelationColumnAndIndexAndForeignKey(
        Table $table,
        string $relationTableName,
        string $relationColumnName = null,
        string $relationColumnType = Types::INTEGER,
        array $relationColumnOptions = [],
        string $relationIndexName = null,
        array $foreignColumnNames = null,
        string $foreignKeyConstraintName = null
    ): void {
        $relationColumnName ?: $relationColumnName = $relationTableName . '_id';
        $table->addColumn($relationColumnName, $relationColumnType, $relationColumnOptions);
        $relationIndexName ?: $relationIndexName = 'idx_' . $table->getName() . '_' . $relationColumnName;
        $table->addIndex(
            [$relationColumnName],
            substr($relationIndexName, 0, SqlHelper::IDENTIFIER_LENGTH_INDEX)
        );
        $foreignColumnNames ?: $foreignColumnNames = ['id'];
        $foreignKeyConstraintName ?: $foreignKeyConstraintName = 'fk_' . $table->getName() . '_' . $relationColumnName;
        $table->addForeignKeyConstraint(
            $relationTableName,
            [$relationColumnName],
            $foreignColumnNames,
            [],
            substr($foreignKeyConstraintName, 0, SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT)
        );
    }
}
