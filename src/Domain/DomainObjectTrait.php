<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

use Smtm\Base\Infrastructure\Helper\ReflectionHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait DomainObjectTrait
{
    public function __setProperty(string $name, mixed $value): static
    {
        if ($this instanceof MarkedForUpdateInterface) {
            $reflectionProperty = new \ReflectionProperty($this, $name);

            if ((!ReflectionHelper::isPropertyInitialized($name, $this) && (!$reflectionProperty->hasDefaultValue() || $reflectionProperty->getDefaultValue() !== $value))
                || ReflectionHelper::isPropertyInitialized($name, $this) && (
                    ($value instanceof IdAwareEntityInterface && !$reflectionProperty->getValue($this) instanceof IdAwareEntityInterface)
                    || (!$value instanceof IdAwareEntityInterface && $reflectionProperty->getValue($this) instanceof IdAwareEntityInterface)
                    || ($value instanceof IdAwareEntityInterface && $reflectionProperty->getValue($this) instanceof IdAwareEntityInterface && !ReflectionHelper::isPropertyInitialized('id', $value) && ReflectionHelper::isPropertyInitialized('id', $reflectionProperty->getValue($this)))
                    || ($value instanceof IdAwareEntityInterface && $reflectionProperty->getValue($this) instanceof IdAwareEntityInterface && ReflectionHelper::isPropertyInitialized('id', $value) && !ReflectionHelper::isPropertyInitialized('id', $reflectionProperty->getValue($this)))
                    || ($value instanceof IdAwareEntityInterface && $reflectionProperty->getValue($this) instanceof IdAwareEntityInterface && ReflectionHelper::isPropertyInitialized('id', $value) && ReflectionHelper::isPropertyInitialized('id', $reflectionProperty->getValue($this)) && $value->getId() !== $reflectionProperty->getValue($this)->getId())
                    || ($value instanceof UuidAwareEntityInterface && !$reflectionProperty->getValue($this) instanceof UuidAwareEntityInterface)
                    || (!$value instanceof UuidAwareEntityInterface && $reflectionProperty->getValue($this) instanceof UuidAwareEntityInterface)
                    || ($value instanceof UuidAwareEntityInterface && $reflectionProperty->getValue($this) instanceof UuidAwareEntityInterface && $value->getUuid() !== $reflectionProperty->getValue($this)->getUuid())
                    || ($value !== $reflectionProperty->getValue($this))
                )
            ) {
                if (!array_key_exists($name, $this->__changeSet)) {
                    if (!ReflectionHelper::isPropertyInitialized($name, $this)) {
                        $this->__changeSet[$name]['old'] = new MarkedForUpdateUninitialzedPropertyValue();
                    } else {
                        $this->__changeSet[$name]['old'] = $reflectionProperty->getValue($this);
                    }
                }

                $this->__changeSet[$name]['new'] = $value;
                $this->__setMarkedForUpdate(true);
            }
        }

        $this->{$name} = $value;

        return $this;
    }
}
