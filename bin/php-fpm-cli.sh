#!/usr/bin/env bash

if nc -z 127.0.0.1 $4; then
  DIR=$(pwd)

  export SCRIPT_NAME="$DIR/$1"
  export SCRIPT_FILENAME="$DIR/$2"
  export REQUEST_METHOD="$3"
  cgi-fcgi -bind -connect "127.0.0.1:$4" || true
fi

exit 0
