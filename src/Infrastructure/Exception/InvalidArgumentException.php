<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InvalidArgumentException extends \InvalidArgumentException
{

}
