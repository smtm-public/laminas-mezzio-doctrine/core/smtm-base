<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface InfrastructureServicePluginManagerAwareInterface
{
    public function getInfrastructureServicePluginManager(): InfrastructureServicePluginManager;
    public function setInfrastructureServicePluginManager(InfrastructureServicePluginManager $infrastructureServicePluginManager): static;
}
