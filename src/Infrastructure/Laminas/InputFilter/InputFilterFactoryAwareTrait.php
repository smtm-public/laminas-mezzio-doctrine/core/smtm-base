<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\InputFilter;

use Laminas\InputFilter\Factory;

trait InputFilterFactoryAwareTrait
{
    protected Factory $inputFilterFactory;

    public function getInputFilterFactory(): Factory
    {
        return $this->inputFilterFactory;
    }

    public function setInputFilterFactory(Factory $inputFilterFactory): static
    {
        $this->inputFilterFactory = $inputFilterFactory;

        return $this;
    }
}
