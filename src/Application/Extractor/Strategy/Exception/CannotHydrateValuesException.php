<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy\Exception;

use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CannotHydrateValuesException extends RuntimeException
{

}
