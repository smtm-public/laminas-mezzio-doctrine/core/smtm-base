<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

use DateTime;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ModifiedDateTimeAwareEntityTrait
{
    protected ?DateTime $modified = null;

    public function getModified(): ?DateTime
    {
        return $this->modified;
    }

    public function setModified(?DateTime $modified): static
    {
        $this->modified = $modified;

        return $this;
    }
}
