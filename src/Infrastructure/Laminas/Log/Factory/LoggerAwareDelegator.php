<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Log\Factory;

use Smtm\Base\Infrastructure\Laminas\Log\LoggerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LoggerAwareDelegator implements DelegatorFactoryInterface
{

    protected ?string $loggerName = null;

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var LoggerAwareInterface $object */
        $object = $callback();

        $this->loggerName !== null &&
        $this->loggerName !== '' &&
        $object->setLogger($container->get(InfrastructureServicePluginManager::class)->get($this->loggerName));

        return $object;
    }
}
