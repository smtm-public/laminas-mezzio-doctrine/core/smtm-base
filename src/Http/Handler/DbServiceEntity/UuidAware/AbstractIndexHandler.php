<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler\DbServiceEntity\UuidAware;

use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Http\Handler\DbServiceEntity\AbstractIndexHandler as DbServiceEntityAbstractIndexHandler;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractIndexHandler extends DbServiceEntityAbstractIndexHandler
{
    /** @var AbstractDbService&UuidAwareEntityDbServiceInterface $applicationService */
    protected ?AbstractApplicationService $applicationService = null;
}
