<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Laminas\Filter\AbstractFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DateIntervalFilter extends AbstractFilter
{
    public const OPTION_KEY_METHOD = 'method';
    public const OPTION_VALUE_METHOD_CONSTRUCTOR = 'constructor';
    public const OPTION_VALUE_METHOD_CREATE_FROM_DATE_STRING = 'createFromDateString';

    public function __construct(protected $options = null)
    {
        $this->setOptions($options ?? []);
    }

    public function filter($value)
    {
        if ($value === null) {
            return null;
        }

        return match ($this->options[self::OPTION_KEY_METHOD] ?? static::OPTION_VALUE_METHOD_CREATE_FROM_DATE_STRING) {
            static::OPTION_VALUE_METHOD_CONSTRUCTOR => new \DateInterval($value),
            default => \DateInterval::createFromDateString($value),
        };
    }
}
