<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\Factory;

use Smtm\Base\Http\InputFilter\RequestValidatingInputFilterCollectionPluginManager;
use Smtm\Base\Http\Middleware\RequestValidatingInputFilterCollectionMiddleware;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RequestValidatingInputFilterCollectionMiddlewareFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): RequestValidatingInputFilterCollectionMiddleware {
        return new RequestValidatingInputFilterCollectionMiddleware(
            $container->get(RequestValidatingInputFilterCollectionPluginManager::class)
        );
    }
}
