<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\WebDriver\Factory;

use Smtm\Base\Infrastructure\WebDriver\WebDriver;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Firefox\FirefoxOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class WebDriverFactory implements FactoryInterface
{
    public const DRIVERS = [
        'chrome' => [
            'capabilities' => [DesiredCapabilities::class, 'chrome'],
            'options' => ChromeOptions::class,
        ],
        'firefox' => [
            'capabilities' => [DesiredCapabilities::class, 'firefox'],
            'options' => FirefoxOptions::class,
        ],
    ];

    public function __invoke(ContainerInterface $container, $name, ?array $options = null)
    {
        $serverUrl = $options['serverUrl'];
        $driverOptions = new (static::DRIVERS[$options['driver']['name']]['options'])();
        $driverOptions->addArguments($options['driver']['capabilities']['options']['arguments']);

        if (method_exists($driverOptions, 'setBinary')) {
            $driverOptions->setBinary($options['driver']['capabilities']['options']['binary']);
        }
        $desiredCapabilities = static::DRIVERS[$options['driver']['name']]['capabilities']();
        $desiredCapabilities->setCapability(
            constant(static::DRIVERS[$options['driver']['name']]['options'] . '::CAPABILITY'),
            $driverOptions
        );
        $desiredCapabilities->setPlatform($options['driver']['capabilities']['platform']);

        return new WebDriver($serverUrl, $desiredCapabilities);
    }
}
