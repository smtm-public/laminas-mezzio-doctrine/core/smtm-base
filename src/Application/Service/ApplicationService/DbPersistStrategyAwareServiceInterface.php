<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService;

use Smtm\Base\Application\Service\ApplicationService\DbPersistStrategy\DbPersistStrategyInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface DbPersistStrategyAwareServiceInterface
{
    public function getDbPersistStrategyDefinition(): array;
    public function getDbPersistStrategy(): DbPersistStrategyInterface;
    public function setDbPersistStrategy(DbPersistStrategyInterface $dbPersistStrategy): static;
}
