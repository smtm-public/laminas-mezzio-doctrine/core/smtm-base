<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait UnarchivedDatetimeMutableColumnMigrationTrait
{
    protected function addUnarchivedDatetimeMutableColumn(Table $table): void
    {
        $table->addColumn(
            'unarchived',
            Types::DATETIME_MUTABLE,
            ['notNull' => false]
        );
    }

    protected function addUnarchivedIndex(Table $table): void
    {
        $table->addIndex(
            ['unarchived'],
            substr(
                'fk_' . $table->getName() . '_unarchived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_INDEX
            )
        );
    }
}
