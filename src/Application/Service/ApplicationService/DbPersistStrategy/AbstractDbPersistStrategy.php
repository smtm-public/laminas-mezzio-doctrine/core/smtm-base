<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\DbPersistStrategy;

use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareTrait;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareTrait;
use Laminas\Stdlib\ArrayUtils;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractDbPersistStrategy implements
    DbPersistStrategyInterface,
    ApplicationServicePluginManagerAwareInterface,
    InfrastructureServicePluginManagerAwareInterface
{

    use ApplicationServicePluginManagerAwareTrait,
        InfrastructureServicePluginManagerAwareTrait;

    protected array $options = [];

    public function __construct(?iterable $options = null)
    {
        $options ??= [];

        // The abstract constructor allows no scalar values
        if ($options instanceof \Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }

        $this->options = array_replace_recursive($this->options, $options);
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): static
    {
        $this->options = $options;

        return $this;
    }
}
