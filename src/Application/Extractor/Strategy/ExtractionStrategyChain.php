<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExtractionStrategyChain extends AbstractExtractionStrategy implements ExtractionStrategyPluginManagerAwareInterface
{

    use ExtractionStrategyPluginManagerAwareTrait;

    public const OPTION_KEY_EXTRACTION_STRATEGY_DEFINITIONS = 'extractionStrategyDefinitions';

    public function extract($value, ?object $object = null): ?string
    {
        $result = $value;

        foreach ($this->options[self::OPTION_KEY_EXTRACTION_STRATEGY_DEFINITIONS] ?? [] as $extractionStrategyDefinition) {
            /** @var AbstractExtractionStrategy $strategy */
            $strategy = $this->extractionStrategyPluginManager->get(
                $extractionStrategyDefinition['name']
            );

            if (!empty($extractionStrategyDefinition['options'])) {
                $strategy->setOptions($extractionStrategyDefinition['options']);
            }

            $result = $strategy->extract($result, $object);
        }

        return $result;
    }
}
