<?php

declare(strict_types=1);

namespace Smtm\DoctrineCliProxy\Exception;

use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class NoLocalConfigurationException extends RuntimeException
{

}
