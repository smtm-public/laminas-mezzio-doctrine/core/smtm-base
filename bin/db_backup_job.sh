#!/bin/bash

# Examples:
#
# With SSH:
# vendor/smtm/smtm-base/bin/db_backup_job.sh "smtm-example-app-envname-db-client-container" "127.0.0.1" "root" "root" "123.123.123.123"
# bin/db_backup_job.sh "smtm-example-app-envname-db-client-container" "127.0.0.1" "root" "root" "123.123.123.123"
#
# Without SSH:
# vendor/smtm/smtm-base/bin/db_backup_job.sh "smtm-example-app-envname-db-client-container" "127.0.0.1" "root"
# bin/db_backup_job.sh "smtm-example-app-envname-db-client-container" "127.0.0.1" "root"

DB_CONTAINER_NAME="$1"
DB_HOST_SOURCE="$2"
DB_USER_SOURCE="$3"
SSH_USER="$4"
SSH_HOST="$5"

echo "Enter MySQL password: ";
read -r -s DB_PASS_SOURCE;

export DB_DUMP_DATE_INITIATED;
export DB_DUMP_DATE_COMPLETED;

MYSQL_QUERY="SHOW DATABASES  WHERE \`Database\` NOT IN ('information_schema', 'mysql', 'performance_schema', 'sys');"
MYSQL_COMMAND="mysql -h${DB_HOST_SOURCE} -u${DB_USER_SOURCE} -p${DB_PASS_SOURCE} -e ${MYSQL_QUERY@Q}"
DOCKER_EXEC_COMMAND="docker exec -i ${DB_CONTAINER_NAME} bash -c ${MYSQL_COMMAND@Q}"

#eval "${DOCKER_EXEC_COMMAND}" | tail +2 | while read -r DB_NAME_SOURCE; do
#  echo ${DB_NAME_SOURCE}
#done
#
#exit 0

eval "${DOCKER_EXEC_COMMAND}" | tail +2 | while read -r DB_NAME_SOURCE; do
    DB_DUMP_DATE_INITIATED="$(date +%Y-%m-%dT%H-%M-%S)"
    DB_DUMP_FILE_NAME_INITIATED="${DB_CONTAINER_NAME}.${DB_NAME_SOURCE}.${DB_DUMP_DATE_INITIATED}"
    if [[ -n ${SSH_HOST} ]]
    then
        DB_DUMP_FILE_NAME_INITIATED="${SSH_HOST}.${DB_CONTAINER_NAME}.${DB_NAME_SOURCE}.${DB_DUMP_DATE_INITIATED}"
    fi

    # MySQL
    #MYSQLDUMP_COMMAND="mysqldump --single-transaction --set-gtid-purged=OFF --column-statistics=0 --hex-blob --no-data --host=${DB_HOST_SOURCE} -u ${DB_USER_SOURCE} -p${DB_PASS_SOURCE} ${DB_NAME_SOURCE}"
    # MariaDB
    MYSQLDUMP_COMMAND="mysqldump --single-transaction --column-statistics=0 --hex-blob --no-data --host=${DB_HOST_SOURCE} -u ${DB_USER_SOURCE} -p${DB_PASS_SOURCE} ${DB_NAME_SOURCE}"

    DOCKER_EXEC_COMMAND="docker exec ${DB_CONTAINER_NAME} bash -c "${MYSQLDUMP_COMMAND@Q}

    SSH_COMMAND="${DOCKER_EXEC_COMMAND} > ${DB_DUMP_FILE_NAME_INITIATED}"
    if [[ -n ${SSH_HOST} ]]
    then
        SSH_COMMAND="ssh -n -t ${SSH_USER}@${SSH_HOST} ${DOCKER_EXEC_COMMAND@Q} > ${DB_DUMP_FILE_NAME_INITIATED}"
    fi
    eval "${SSH_COMMAND}"

    # MySQL
    #MYSQLDUMP_COMMAND="mysqldump --single-transaction --set-gtid-purged=OFF --column-statistics=0 --hex-blob --no-create-info --skip-triggers --ignore-table=${DB_NAME_SOURCE}.auth_auth_failure --ignore-table=${DB_NAME_SOURCE}.auth_consumer_token --ignore-table=${DB_NAME_SOURCE}.auth_provider_auth_code --ignore-table=${DB_NAME_SOURCE}.auth_provider_token --host=${DB_HOST_SOURCE} -u ${DB_USER_SOURCE} -p${DB_PASS_SOURCE} ${DB_NAME_SOURCE}"
    # MariaDB
    MYSQLDUMP_COMMAND="mysqldump --single-transaction --column-statistics=0 --hex-blob --no-create-info --skip-triggers --ignore-table=${DB_NAME_SOURCE}.auth_auth_failure --ignore-table=${DB_NAME_SOURCE}.auth_consumer_token --ignore-table=${DB_NAME_SOURCE}.auth_provider_auth_code --ignore-table=${DB_NAME_SOURCE}.auth_provider_token --host=${DB_HOST_SOURCE} -u ${DB_USER_SOURCE} -p${DB_PASS_SOURCE} ${DB_NAME_SOURCE}"

    DOCKER_EXEC_COMMAND="docker exec ${DB_CONTAINER_NAME} bash -c "${MYSQLDUMP_COMMAND@Q}

    SSH_COMMAND="${DOCKER_EXEC_COMMAND} >> ${DB_DUMP_FILE_NAME_INITIATED}"
    if [[ -n ${SSH_HOST} ]]
    then
        SSH_COMMAND="ssh -n -t ${SSH_USER}@${SSH_HOST} ${DOCKER_EXEC_COMMAND@Q} > ${DB_DUMP_FILE_NAME_INITIATED}"
    fi
    eval "${SSH_COMMAND}"

    DB_DUMP_DATE_COMPLETED="$(date +%Y-%m-%dT%H-%M-%S)"
    DB_DUMP_FILE_NAME_COMPLETED="${DB_DUMP_FILE_NAME_INITIATED}.completed${DB_DUMP_DATE_COMPLETED}"
    mv "${DB_DUMP_FILE_NAME_INITIATED}" "${DB_DUMP_FILE_NAME_COMPLETED}.sql"
    sed -i -e s/utf8mb4_0900_ai_ci/utf8mb4_unicode_ci/ -e s/utf8mb4_general_ci/utf8mb4_unicode_ci/ "${DB_DUMP_FILE_NAME_COMPLETED}.sql"
    DB_DUMP_FILE_NAME_COMPLETED_LOCALIZED="${DB_DUMP_FILE_NAME_COMPLETED}-localized"
    mv "${DB_DUMP_FILE_NAME_COMPLETED}.sql" "${DB_DUMP_FILE_NAME_COMPLETED_LOCALIZED}.sql"

    echo "${DB_DUMP_FILE_NAME_COMPLETED_LOCALIZED}.sql"
done

exit 0
