<?php

declare(strict_types=1);

namespace SmtmTest\Base\Codeception\Support\Mock\Application\Service;

use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use SmtmTest\Base\Codeception\Support\Mock\MockPluginManagerTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ApplicationServicePluginManagerMock extends ApplicationServicePluginManager
{
    use MockPluginManagerTrait;
}
