<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface RemoteServiceResponseExceptionInterface
{
    public function getResponseStatus(): int;
    public function setResponseStatus(int $responseStatus): static;
    public function getResponseBody(): string;
    public function setResponseBody(string $responseBody): static;
    public function getResponseBodyParsed(): array|string|null;
    public function setResponseBodyParsed(array|string|null $responseBodyParsed): static;
}
