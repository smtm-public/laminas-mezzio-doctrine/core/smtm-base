<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\Exception;

class InvalidConfigException extends InvalidArgumentException
{

}
