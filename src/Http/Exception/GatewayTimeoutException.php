<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Exception;

use Smtm\Base\Infrastructure\Helper\HttpHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class GatewayTimeoutException extends AbstractHttpException
{
    public const MESSAGE = 'Gateway timeout';
    public const CODE = 0;
    public const STATUS = HttpHelper::STATUS_CODE_GATEWAY_TIMEOUT;
    public const TYPE = 'https://example.com/problems/gateway-timeout';

    protected int $status = self::STATUS;
    protected string $title = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_GATEWAY_TIMEOUT];
    protected string $detail = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_GATEWAY_TIMEOUT];
    protected string $type = self::TYPE;
}
