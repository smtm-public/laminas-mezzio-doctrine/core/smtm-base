<?php

declare(strict_types=1);

namespace Smtm\Base\Domain\Helper;

use Smtm\Base\Domain\EntityInterface;

class EntityHelper
{
    public static function accessField(EntityInterface $entity, string $field)
    {
        $fieldNames = explode('.', $field);
        $value = $entity;

        foreach ($fieldNames as $fieldName) {
            $getter = 'get' . ucfirst($fieldName);

            if (!method_exists(get_class($value), $getter)) {
                return null;
            }

            $value = $value->$getter();
        }

        return $value;
    }

    public static function generateGetterName(string $property)
    {
        return 'get' . ucfirst($property);
    }

    public static function generateSetterName(string $property)
    {
        return 'set' . ucfirst($property);
    }
}
