<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler\DbServiceEntity;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Domain\DomainObjectInterface;
use Smtm\Base\Http\Middleware\ServerParamsAndRequestIdMiddleware;
use Smtm\Base\Infrastructure\Collection\CollectionInterface;
use Smtm\Base\Infrastructure\Collection\PaginatedCollectionInterface;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractCreateHandler extends AbstractHandler
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $parsedQueryParams = static::parseQueryParams($this->getMergedFilteredQueryParams($request));
        $entity = $this->applicationService->create(
            $this->getMergedFilteredParsedBody($request),
            null,
            array_replace(
                [ApplicationServiceInterface::OPTION_KEY_PARAMS => $parsedQueryParams],
                [ApplicationServiceInterface::OPTION_KEY_METHOD => $request->getMethod()],
                ServerParamsAndRequestIdMiddleware::getRequestParams($request)
            )
        );

        return $this->prepareResponse(
            $entity,
            $parsedQueryParams,
            HttpHelper::STATUS_CODE_CREATED
        );
    }

    protected function prepareResponse(
        array|DomainObjectInterface|CollectionInterface|PaginatedCollectionInterface $data,
        array $parsedQueryParams = [],
        int $statusCode = null,
        AbstractDomainObjectExtractor | string | null $extractor = null
    ): ResponseInterface {
        return parent::prepareResponse($data, $parsedQueryParams, $statusCode ?? HttpHelper::STATUS_CODE_CREATED, $extractor);
    }
}
