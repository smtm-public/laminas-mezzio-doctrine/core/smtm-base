<?php

declare(strict_types=1);

namespace Smtm\Base\Http\InputFilter;

use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterSpecificationInterface;
use Smtm\Base\Infrastructure\Laminas\Validator\IsString;
use Laminas\Validator\Uuid;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UuidAndSha256RouteParamRequestValidatingInputFilterCollection extends AbstractRequestValidatingInputFilterCollection
{
    protected array $pathParamsInputFilterSpecification = [
        InputFilterSpecificationInterface::INPUT_COLLECTION => [
            [
                'name' => 'uuid',
                'required' => true,
                'validators' => [
                    [
                        'name' => Uuid::class,
                    ],
                ],
            ],
            [
                'name' => 'sha256',
                'required' => true,
                'validators' => [
                    [
                        'name' => IsString::class,
                        'options' => [
                            'minLength' => 64,
                            'maxLength' => 64,
                            'hexadecimal' => IsString::OPTION_HEXADECIMAL_CASE_INSENSITIVE,
                        ],
                    ],
                ],
            ],
        ],
    ];
}
