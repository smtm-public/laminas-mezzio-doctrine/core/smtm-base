<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator\Factory;

use Smtm\Base\Infrastructure\Laminas\Validator\ValidatorChainEither;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\Validator\ValidatorPluginManager;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ValidatorChainEitherFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var ValidatorChainEither $validatorChainEither */
        $validatorChainEither = new $requestedName();
        /** @var ValidatorPluginManager $validatorPluginManager */
        $validatorPluginManager = $container->get(ValidatorPluginManager::class);

        foreach ($options['validators'] as $validatorDefinition) {
            $validator = $validatorPluginManager->get(
                $validatorDefinition['name'],
                $validatorDefinition['options'] ?? []
            );
            $validatorChainEither->attach($validator);
        }

        return $validatorChainEither;
    }
}
