<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Laminas\Filter\FilterPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface FilterPluginManagerAwareInterface
{
    public function getFilterPluginManager();
    public function setFilterPluginManager(FilterPluginManager $pluginManager): static;
}
