<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface EntityManagerInterface extends \Doctrine\ORM\EntityManagerInterface
{

}
