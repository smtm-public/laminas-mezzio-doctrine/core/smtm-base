<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait CreatedByIntColumnAndIndexAndForeignKeyMigrationTrait
{
    protected function addCreatedByIntColumnAndIndexAndForeignKey(
        Table $table,
        Table $foreignKeyTable,
        array $foreignKeyColumns = ['id'],
        string $relationIndexName = null,
        string $foreignKeyConstraintName = null,
        array $columnOptions = ['notNull' => true]
    ): void {
        $table->addColumn(
            'created_by',
            Types::INTEGER,
            $columnOptions
        );
        $relationIndexName ?: $relationIndexName = 'idx_' . $table->getName() . '_created_by';
        $table->addIndex(
            ['created_by'],
            substr($relationIndexName, 0, SqlHelper::IDENTIFIER_LENGTH_INDEX)
        );
        $foreignKeyConstraintName ?: $foreignKeyConstraintName = 'fk_' . $table->getName() . '_created_by_'
            . $foreignKeyTable->getName() . '_' . implode('_', $foreignKeyColumns);
        $table->addForeignKeyConstraint(
            $foreignKeyTable,
            ['created_by'],
            $foreignKeyColumns,
            [],
            substr($foreignKeyConstraintName, 0, SqlHelper::IDENTIFIER_LENGTH_INDEX)
        );
    }
}
