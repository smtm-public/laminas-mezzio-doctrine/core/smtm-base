<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Enum;

/**
 * Class containing constants of application errors and their corresponding messages
 *
 * @author Grigor Milchev <grigor@smtm.bg>
 */
class Error
{
    public const NOT_ALLOWED_ORIGIN = 1000;
    public const MALFORMED_REQUEST_BODY = 1001;

    protected static array $codesMessages = [
        self::NOT_ALLOWED_ORIGIN => 'Not allowed origin',
        self::MALFORMED_REQUEST_BODY => 'Malformed request body',
    ];

    /**
     * Returns the corresponding message of the given error code
     *
     * @param int $errorCode
     * @param array $sprintfArgs
     *
     * @return string|null
     */
    public static function getMessage(int $errorCode, array $sprintfArgs = []): ?string
    {
        return isset(self::$codesMessages[$errorCode])
            ? sprintf(self::$codesMessages[$errorCode], ...$sprintfArgs)
            : null;
    }

    /**
     * @return array
     */
    public static function getAllErrorCodes(): array
    {
        return array_keys(self::$codesMessages);
    }
}
