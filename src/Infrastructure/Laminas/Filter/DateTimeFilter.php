<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Laminas\Filter\AbstractFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DateTimeFilter extends AbstractFilter implements ConfigAwareInterface
{

    use ConfigAwareTrait;

    public const OPTION_NAME_FORMAT = 'format';

    protected ?string $format = null;

    public function __construct(protected $options = null)
    {
        $this->setOptions($options ?? []);
    }

    public function filter($value)
    {
        if ($value === '' || $value === null) {
            return $value;
        }

        if ($this->format === null) {
            return new \DateTime($value);
        }

        return \DateTime::createFromFormat(
            $this->format,
            $value,
            new \DateTimeZone($this->config['dateTime']['defaultTimeZone'])
        );
    }

    public function getFormat(): string
    {
        return $this->format;
    }

    public function setFormat(string $format): static
    {
        $this->format = $format;

        return $this;
    }
}
