<?php

namespace Smtm\Base\Infrastructure\Helper\Exception;

use Smtm\Base\Infrastructure\Exception\RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UriFormatException extends RuntimeException
{
    const MESSAGE_INVALID_URI_FORMAT = 'invalid_uri_format';
    const CODE_INVALID_URI_FORMAT = 0x01;
}
