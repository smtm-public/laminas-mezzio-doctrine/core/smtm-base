<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../../.env.smtm.smtm-base.infrastructure.remote-service-connector')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../../',
        '.env.smtm.smtm-base.infrastructure.remote-service-connector'
    );
    $dotenv->load();
}

$loggers = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_INFRASTRUCTURE_REMOTE_SERVICE_CONNECTOR_CLIENT_LOGGING_LOGGERS'),
    true,
    flags: JSON_THROW_ON_ERROR
);

return [
    'client' => [
        'verifyRemoteCertificate' => filter_var(
            EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_INFRASTRUCTURE_REMOTE_SERVICE_CONNECTOR_CLIENT_VERIFY_REMOTE_CERTIFICATE'),
            FILTER_VALIDATE_BOOLEAN
        ),
        'logging' => [
            'defaultMessageFormatterTemplate' => EnvHelper::getEnvFromProcessOrSuperGlobal(
                'SMTM_BASE_INFRASTRUCTURE_REMOTE_SERVICE_CONNECTOR_CLIENT_LOGGING_DEFAULT_MESSAGE_FORMATTER_TEMPLATE'
            ),
            'loggers' => $loggers,
        ],
    ],
];
