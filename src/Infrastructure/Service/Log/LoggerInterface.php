<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Log;

interface LoggerInterface extends \Laminas\Log\LoggerInterface, \Psr\Log\LoggerInterface
{

}
