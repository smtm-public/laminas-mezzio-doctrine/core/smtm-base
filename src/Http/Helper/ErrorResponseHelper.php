<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Helper;

use Smtm\Base\Application\Exception\ProblemDetailsApplicationException;
use Smtm\Base\Application\Exception\ValidationException;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityCreateNonUniqueException;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityNotFoundException;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityUpdateNonUniqueException;
use Smtm\Base\Http\Exception\BadRequestException;
use Smtm\Base\Http\Exception\ConflictException;
use Smtm\Base\Http\Exception\ForbiddenException;
use Smtm\Base\Http\Exception\GatewayTimeoutException;
use Smtm\Base\Http\Exception\InternalServerErrorException;
use Smtm\Base\Http\Exception\MethodNotAllowedException;
use Smtm\Base\Http\Exception\NotFoundException;
use Smtm\Base\Http\Exception\ServiceUnavailableException;
use Smtm\Base\Http\Exception\UnauthorizedException;
use Smtm\Base\Http\Exception\UnprocessableEntityException;
use Smtm\Base\Http\Middleware\ServerParamsAndRequestIdMiddleware;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Helper\IetfHelper;
use Smtm\Base\Infrastructure\Helper\MimeHelper;
use Smtm\Base\Infrastructure\Helper\ThrowableHelper;
use Smtm\Base\Infrastructure\Helper\XmlHelper;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteMethodNotAllowedResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceBadRequestResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceConflictResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceConnectionException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceForbiddenResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceGatewayTimeoutResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceNotFoundResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceResponseExceptionInterface;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceServiceUnavailableResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceUnauthorizedResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceUnprocessableEntityResponseException;
use Mezzio\Middleware\WhoopsErrorResponseGenerator;
use Mezzio\ProblemDetails\Exception\ProblemDetailsExceptionInterface;
use Negotiation\Negotiator;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Spatie\ArrayToXml\ArrayToXml;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ErrorResponseHelper
{
    /**
     * @var string Default detail message to use for exceptions when the
     *     $exceptionDetailsInResponse flag is false.
     */
    public const DEFAULT_DETAIL_MESSAGE = 'An unknown error occurred.';

    /**
     * @var string[] Accept header types to match.
     */
    public const NEGOTIATION_PRIORITIES = [
        'application/json',
        'application/*+json',
        'application/xml',
        'application/*+xml',
    ];

    public static function createResponseFromThrowable(
        ServerRequestInterface $request,
        ResponseFactoryInterface|callable $responseFactory,
        \Throwable $e,
        array $config,
        array $loggerCollection = [],
        ?WhoopsErrorResponseGenerator $whoopsErrorResponseGenerator = null
    ): ResponseInterface {
        if ($e instanceof BadRequestException) {
            $e->setAdditional(['details' => $e->getErrors()]);

            $httpStatusCode = HttpHelper::STATUS_CODE_BAD_REQUEST;
        } elseif ($e instanceof ValidationException) {
            $httpStatusCode = HttpHelper::STATUS_CODE_BAD_REQUEST;
            $e = new BadRequestException($e->getMessage(), 400, $e, $e->getErrors());
            $e->setAdditional($e->getErrors());
        } elseif ($e instanceof ForbiddenException) {
            $httpStatusCode = HttpHelper::STATUS_CODE_FORBIDDEN;
        } elseif ($e instanceof UnauthorizedException) {
            $httpStatusCode = HttpHelper::STATUS_CODE_UNAUTHORIZED;
        } elseif ($e instanceof EntityNotFoundException) {
            $e = new NotFoundException('Not Found', 0, $e);

            $httpStatusCode = HttpHelper::STATUS_CODE_NOT_FOUND;
        } elseif ($e instanceof EntityCreateNonUniqueException) {
            $e = new ConflictException('The entity is not unique', 0, $e);

            $httpStatusCode = HttpHelper::STATUS_CODE_CONFLICT;
        } elseif ($e instanceof EntityUpdateNonUniqueException) {
            $e = new ConflictException('The entity is not unique', 0, $e);

            $httpStatusCode = HttpHelper::STATUS_CODE_CONFLICT;
        } elseif ($e instanceof RemoteServiceBadRequestResponseException) {
            $e = new BadRequestException('Bad Request', 0, $e);

            $httpStatusCode = HttpHelper::STATUS_CODE_BAD_REQUEST;
        } elseif ($e instanceof RemoteServiceUnauthorizedResponseException) {
            $e = new UnauthorizedException('Unauthorized', 0, $e);

            $httpStatusCode = HttpHelper::STATUS_CODE_UNAUTHORIZED;
        } elseif ($e instanceof RemoteServiceForbiddenResponseException) {
            $e = new ForbiddenException('Forbidden', 0, $e);

            $httpStatusCode = HttpHelper::STATUS_CODE_FORBIDDEN;
        } elseif ($e instanceof RemoteServiceNotFoundResponseException) {
            $e = new NotFoundException('Not Found', 0, $e);

            $httpStatusCode = HttpHelper::STATUS_CODE_NOT_FOUND;
        } elseif ($e instanceof RemoteMethodNotAllowedResponseException) {
            $e = new MethodNotAllowedException('Method Not Allowed', 0, $e);

            $httpStatusCode = HttpHelper::STATUS_CODE_METHOD_NOT_ALLOWED;
        } elseif ($e instanceof RemoteServiceConflictResponseException) {
            $e = new ConflictException('Conflict', 0, $e);

            $httpStatusCode = HttpHelper::STATUS_CODE_CONFLICT;
        } elseif ($e instanceof RemoteServiceUnprocessableEntityResponseException) {
            $e = new UnprocessableEntityException('Unprocessable Entity', 0, $e);

            $httpStatusCode = HttpHelper::STATUS_CODE_UNPROCESSABLE_ENTITY;
        } elseif ($e instanceof RemoteServiceGatewayTimeoutResponseException) {
            $e = new GatewayTimeoutException('Gateway Timeout', 0, $e);

            $httpStatusCode = HttpHelper::STATUS_CODE_GATEWAY_TIMEOUT;
        } elseif ($e instanceof RemoteServiceServiceUnavailableResponseException) {
            $e = new ServiceUnavailableException('Service Unavailable', 0, $e);

            $httpStatusCode = HttpHelper::STATUS_CODE_SERVICE_UNAVAILABLE;
        } elseif ($e instanceof RemoteServiceResponseException) {
            $httpStatusCode = HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR;
        } elseif ($e instanceof RemoteServiceConnectionException) {
            $e = new ServiceUnavailableException('Service Unavailable', 0, $e);

            $httpStatusCode = HttpHelper::STATUS_CODE_SERVICE_UNAVAILABLE;
        } elseif ($e instanceof ProblemDetailsExceptionInterface) {
            if ($e instanceof ProblemDetailsApplicationException) {
                if (array_key_exists(get_class($e), $config['errorMapping']['application'])) {
                    $e = new $config['errorMapping']['application'][get_class($e)](
                        'Problem Details Application Exception',
                        0,
                        $e,
                        $e->getAdditionalData()
                    );
                }
            }

            $httpStatusCode = $e->getStatus();
        } else {
            if ($config['whoops']['errorResponseGenerator']['enableForInternalServerErrors'] ?? false) {
                return ($whoopsErrorResponseGenerator)($e, $request, static::createEmptyResponse($responseFactory));
            }

            $e = new InternalServerErrorException('Internal Server Error', 0, $e);
            $httpStatusCode = HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR;
        }

        $debug =
            $config['debug']
                ? ThrowableHelper::formatAsArray($e, $httpStatusCode, $config)
                : null;
        $payload = [];

        if ($e instanceof ProblemDetailsExceptionInterface) {
            if (is_subclass_of($e, ProblemDetailsApplicationException::class)) {
                $payload['class'] = $e->getClass();
            }

            $payload += [
                'title' => $e->getTitle() ?: HttpHelper::STATUS_CODES_DESCRIPTIONS[$httpStatusCode],
                'type' => $e->getType() ?: /*$this->defaultTypesMap[$httpStatusCode]
                    ?? */
                    sprintf('https://httpstatus.es/%s', $httpStatusCode),
                'status' => $httpStatusCode,
                'detail' => $e->getDetail(),
            ];

            if ($e->getAdditionalData()) {
                $additional = $e->getAdditionalData();
                // ensure payload can be json_encoded
                array_walk_recursive(
                    $additional,
                    function (&$value) {
                        if (is_resource($value)) {
                            $value = print_r($value, true) . ' of type ' . get_resource_type($value);
                        }
                    }
                );
                $payload = array_merge($additional, $payload);
            }

            if ($debug !== null) {
                $payload['debug'] = $debug;
            }
        } else {
            $detail = $config['debug'] ? $e->getMessage() : static::DEFAULT_DETAIL_MESSAGE;

            $payload = [
                'title' => HttpHelper::STATUS_CODES_DESCRIPTIONS[$httpStatusCode],
                'type' => /*$this->defaultTypesMap[$httpStatusCode]?? */
                    sprintf('https://httpstatus.es/%s', $httpStatusCode),
                'status' => $httpStatusCode,
                'detail' => $detail,
            ];

            if ($debug !== null) {
                if ($e instanceof RemoteServiceResponseExceptionInterface) {
                    $payload['responseStatus'] = $e->getResponseStatus();
                    $payload['responseBody'] = $e->getResponseBody();
                    $payload['responseBodyParsed'] = $e->getResponseBodyParsed();
                }

                $payload['debug'] = $debug;
            }
        }

        foreach ($loggerCollection as $loggerName => $logger) {
            try {
                $logger->error(
                    $e,
                    [
                        'request' => [
                            'id' => $request->getAttribute(ServerParamsAndRequestIdMiddleware::REQUEST_ATTRIBUTE_NAME_ID),
                        ],
                        'throwable' => $e,
                        'loggerName' => $loggerName,
                        'logger' => $logger,
                    ]
                );
            } catch (\Throwable $t) {
                file_put_contents('php://stderr', $t);
            }
        }

        return static::getResponseGenerator($responseFactory, $request)($payload, $config['debug']);
    }

    public static function getResponseGenerator(
        ResponseFactoryInterface|callable $responseFactory,
        ServerRequestInterface $request
    ): callable {
        $accept = $request->getHeaderLine('Accept') ?: '*/*';
        $mediaType = (new Negotiator())->getBest($accept, self::NEGOTIATION_PRIORITIES);

        return !$mediaType || !str_contains($mediaType->getValue(), 'json')
            ? fn (array $payload, bool $debug = false) => ErrorResponseHelper::generateXmlResponse($responseFactory, $payload, $debug)
            : fn (array $payload, bool $debug = false) => ErrorResponseHelper::generateJsonResponse($responseFactory, $payload, $debug);
    }

    public static function generateJsonResponse(
        ResponseFactoryInterface|callable $responseFactory,
        array $payload,
        bool $debug = false
    ): ResponseInterface {
        $jsonFlags = JSON_UNESCAPED_SLASHES
            | JSON_UNESCAPED_UNICODE
            | JSON_PRESERVE_ZERO_FRACTION
            | JSON_PARTIAL_OUTPUT_ON_ERROR;

        if ($debug) {
            $jsonFlags = JSON_PRETTY_PRINT | $jsonFlags;
        }

        return ErrorResponseHelper::generateResponse(
            $responseFactory,
            $payload['status'],
            MimeHelper::CONTENT_TYPE_APPLICATION_PROBLEM_PLUS_JSON,
            json_encode($payload, $jsonFlags)
        );
    }

    public static function generateXmlResponse(
        ResponseFactoryInterface|callable $responseFactory,
        array $payload,
        bool $debug = false
    ): ResponseInterface {
        $converter = new ArrayToXml(XmlHelper::sanitizeData($payload), 'problem');
        $dom = $converter->toDom();
        $root = $dom->firstChild;
        $root->setAttribute('xmlns', IetfHelper::RFC_7807_URN);

        return ErrorResponseHelper::generateResponse(
            $responseFactory,
            $payload['status'],
            MimeHelper::CONTENT_TYPE_APPLICATION_PROBLEM_PLUS_XML,
            $dom->saveXML()
        );
    }

    public static function generateResponse(
        ResponseFactoryInterface|callable $responseFactory,
        int $status,
        string $contentType,
        string $payload
    ): ResponseInterface {
        $response = static::createEmptyResponse($responseFactory);
        $response->getBody()->write($payload);

        return $response
            ->withStatus($status)
            ->withHeader('Content-Type', $contentType);
    }

    public static function createEmptyResponse(ResponseFactoryInterface|callable $responseFactory): ResponseInterface
    {
        return $responseFactory instanceof ResponseFactoryInterface
            ? $responseFactory->createResponse()
            : $responseFactory();
    }
}
