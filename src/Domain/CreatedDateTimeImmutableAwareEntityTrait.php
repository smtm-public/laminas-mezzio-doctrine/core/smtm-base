<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

use DateTimeImmutable;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait CreatedDateTimeImmutableAwareEntityTrait
{
    protected DateTimeImmutable $created;

    public function getCreated(): DateTimeImmutable
    {
        return $this->created;
    }

    public function setCreated(DateTimeImmutable $created): static
    {
        $this->created = $created;

        return $this;
    }
}
