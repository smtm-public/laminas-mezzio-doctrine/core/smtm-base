<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator;

use Laminas\Validator\AbstractValidator;
use Laminas\Validator\Exception\ExtensionNotLoadedException;
use Laminas\Validator\Exception\InvalidArgumentException;
use Traversable;

/**
 * Bidirectional (is|is not) validation for expected value type: number
 *
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IsNumeric extends AbstractValidator
{
    const OPTION_KEY_IS = 'is';
    const OPTION_KEY_LT = 'lt';
    const OPTION_KEY_GT = 'gt';

    const INVALID_TRUE = 'invalidValueIs';
    const INVALID_FALSE = 'invalidValueIsNot';
    const INVALID_NOT_LESSER_THAN = 'invalidValueNotLesserThan';
    const INVALID_NOT_GREATER_THAN = 'invalidValueNotGreaterThan';

    protected array $messageTemplates = [
        self::INVALID_TRUE => 'Invalid value given. Number expected.',
        self::INVALID_FALSE => 'Invalid value given. Cannot be a number.',
        self::INVALID_NOT_LESSER_THAN => 'Invalid value given. Cannot be lesser than %s.',
        self::INVALID_NOT_GREATER_THAN => 'Invalid value given. Cannot be greater than %s.',
    ];

    protected array $options = [];

    /**
     * $options can have the following keys:
     * - is - true|false - whether it should be a number (true) or not (false)
     * - lt - int - whether it should be lesser than (<) <value>
     * - gt - int - whether it should be greater than (>) <value>
     *
     * @param array|Traversable $options
     *
     * @throws ExtensionNotLoadedException if ext/intl is not present
     */
    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    /**
     * @param $value
     *
     * @return bool
     *
     * @throws InvalidArgumentException
     */
    public function isValid($value)
    {
        $is = $this->options[self::OPTION_KEY_IS] ?? true;

        if (!$is && is_numeric($value)) {
            $this->error(self::INVALID_FALSE);

            return false;
        } elseif ($is) {
            if (!is_numeric($value)) {
                $this->error(self::INVALID_TRUE);

                return false;
            } else {
                $lt  = $this->options[self::OPTION_KEY_LT] ?? null;

                if ($lt !== null && (int) $value > $lt) {
                    $this->error(self::INVALID_NOT_LESSER_THAN);

                    return false;
                }

                $gt  = $this->getOptions()[self::OPTION_KEY_GT] ?? null;

                if ($gt !== null && (int) $value < $gt) {
                    $this->error(self::INVALID_NOT_GREATER_THAN);

                    return false;
                }
            }
        }

        return true;
    }
}
