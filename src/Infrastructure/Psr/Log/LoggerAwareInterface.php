<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Psr\Log;

use Psr\Log\LoggerInterface;

interface LoggerAwareInterface
{
    public function getLogger(): LoggerInterface;
    public function setLogger(LoggerInterface $logger): static;
}
