<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../../.env.smtm.smtm-base.infrastructure.webdriver')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../../', '.env.smtm.smtm-base.infrastructure.webdriver');
    $dotenv->load();
}

return [
    'services' => json_decode(
        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_INFRASTRUCTURE_WEBDRIVER_SERVICES', '[]'),
        true
    ),
];
