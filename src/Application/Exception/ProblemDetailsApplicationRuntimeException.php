<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Exception;

/**
 * @author Alek Nikolov <a.nikolov@smtm.com>
 */
class ProblemDetailsApplicationRuntimeException
    extends ProblemDetailsApplicationException
{
}
