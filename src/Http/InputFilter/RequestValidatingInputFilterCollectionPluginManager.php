<?php

declare(strict_types=1);

namespace Smtm\Base\Http\InputFilter;

use Laminas\ServiceManager\AbstractPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RequestValidatingInputFilterCollectionPluginManager extends AbstractPluginManager
{
    protected $instanceOf = AbstractRequestValidatingInputFilterCollection::class;
}
