<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\Mezzio\Cors\Factory;

use Smtm\Base\Http\Middleware\Mezzio\Cors\ConfigurationLocator;
use Smtm\Base\Http\Middleware\Mezzio\Cors\CorsMiddleware;
use Smtm\Base\Http\Middleware\Mezzio\Cors\Service\Cors;
use Smtm\Base\Http\Middleware\Mezzio\Cors\Service\ResponseFactory;
use Mezzio\Cors\Service\ConfigurationLocatorInterface;
use Mezzio\Cors\Service\CorsInterface;
use Psr\Container\ContainerInterface;
use Webmozart\Assert\Assert;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CorsMiddlewareFactory
{
    public function __invoke(ContainerInterface $container): CorsMiddleware
    {
        $cors = $container->get(Cors::class);
        Assert::isInstanceOf($cors, CorsInterface::class);
        $configurationLocator = $container->get(ConfigurationLocator::class);
        Assert::isInstanceOf($configurationLocator, ConfigurationLocatorInterface::class);
        $responseFactory = $container->get(ResponseFactory::class);
        Assert::isInstanceOf($responseFactory, ResponseFactory::class);
        return new CorsMiddleware(
            $cors,
            $configurationLocator,
            $responseFactory
        );
    }
}
