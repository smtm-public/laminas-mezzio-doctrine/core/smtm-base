<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Laminas\Filter\AbstractFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class JsonEncodeFilter extends AbstractFilter
{
    public const OPTION_NAME_FLAGS = 'flags';
    public const OPTION_NAME_DEPTH = 'depth';

    public function __construct(protected $options = null)
    {
        $this->setOptions($options ?? []);
    }

    public function filter($value)
    {
        return json_encode($value, $this->options['flags'] ?? 0, $this->options['depth'] ?? 512);
    }
}
