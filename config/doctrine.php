<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Doctrine\DBAL\Types\DateTimeMicrosecondsImmutableType;
use Smtm\Base\Infrastructure\Doctrine\DBAL\Types\DateTimeMicrosecondsType;
use Smtm\Base\Infrastructure\Helper\EnvHelper;
use PDO;

if (file_exists(__DIR__ . '/../../../../.env.doctrine')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.doctrine');
    $dotenv->load();
}

if (defined('APP_ENV') && APP_ENV ==='testing' && file_exists(__DIR__ . '/../../../../test/environment/.env.doctrine')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../test/environment/', '.env.doctrine');
    $dotenv->load();
}

$dbConnections =
    EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_DB_CONNECTIONS');
$dbDriver = EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_DB_DRIVER', null);
$dbPath = EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_DB_PATH', null);
$dbHost = EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_DB_HOST', null);
$dbPort = EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_DB_PORT', null);
$dbUser = EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_DB_USER', null);
$dbPassword = EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_DB_PASSWORD', null);
$dbName = EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_DB_NAME', null);
$dbNestTransactionsWithSavepoints = filter_var(
    EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_DB_NEST_TRANSACTIONS_WITH_SAVEPOINTS', 'true'),
    FILTER_VALIDATE_BOOLEAN
);
$dbalMiddlewares = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_DBAL_MIDDLEWARES'),
    true,
    flags: JSON_THROW_ON_ERROR
);
$deprecationLogger = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_DEPRECATION_LOGGER'),
    true,
    flags: JSON_THROW_ON_ERROR
);
$ormEntityManagers =
    EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_ORM_ENTITY_MANAGERS');
$ormAutoGenerateProxyClasses =
    EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_ORM_AUTO_GENERATE_PROXY_CLASSES');
$ormMetadataCache = EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_ORM_METADATA_CACHE') ?: null;
$ormQueryCache = EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_ORM_QUERY_CACHE') ?: null;
$ormSecondLevelCache = EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_ORM_SECOND_LEVEL_CACHE') ?: null;

$entityManagers = [];

if (!empty($ormEntityManagers)) {
    $entityManagers = json_decode($ormEntityManagers, true);

    foreach ($entityManagers as &$entityManager) {
        $entityManager['autoGenerateProxyClasses'] ??= $ormAutoGenerateProxyClasses;
        $entityManager['metadata']['cache'] ??= $ormMetadataCache;
        $entityManager['query']['cache'] ??= $ormQueryCache;
    }
}

$connections = [];

if (!empty($dbConnections) && !empty($connections = json_decode($dbConnections, true))) {
    foreach ($connections as &$connection) {
        $connection['host'] ??= $dbHost;
        $connection['port'] ??= $dbPort;
        $connection['user'] ??= $dbUser;
        $connection['password'] ??= $dbPassword;
        $connection['nestTransactionsWithSavepoints'] ??= $dbNestTransactionsWithSavepoints;
        $connection['charset'] = 'utf8mb4';

        if ($connection['driverClass'] === \Doctrine\DBAL\Driver\PDO\MySQL\Driver::class) {
            $connection['driverOptions'] = [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8MB4'",
            ];
        }
    }
} else {
    $connections = [
        'default' => [
            'driverClass' => $dbDriver,
            'path' => $dbPath,
            'host' => $dbHost,
            'port' => $dbPort,
            'user' => $dbUser,
            'password' => $dbPassword,
            'dbname' => $dbName,
            'nestTransactionsWithSavepoints' => $dbNestTransactionsWithSavepoints,
            'charset' => 'utf8mb4',
            'driverOptions' => [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8MB4'"
            ],
        ],
        'default-reader' => [
            'driverClass' => $dbDriver,
            'path' => $dbPath,
            'host' => $dbHost,
            'port' => $dbPort,
            'user' => $dbUser,
            'password' => $dbPassword,
            'dbname' => $dbName,
            'nestTransactionsWithSavepoints' => $dbNestTransactionsWithSavepoints,
            'charset' => 'utf8mb4',
            'driverOptions' => [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8MB4'"
            ],
        ],
    ];

    if ($dbDriver === \Doctrine\DBAL\Driver\PDO\MySQL\Driver::class) {
        $connection['default']['driverOptions'] = $connection['default-reader']['driverOptions'] = [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8MB4'",
        ];
    }
}

return [
    'connections' => $connections,

    'logging' => [
        'enabled' => false,
    ],

    'dbal' => [
        'middlewares' => $dbalMiddlewares,

        'result' => [
            'cache' =>
                EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_DBAL_RESULT_CACHE'),
        ],

        'types' => array_merge(
            [
                DateTimeMicrosecondsType::DATETIME_MICROSECONDS_MUTABLE => DateTimeMicrosecondsType::class,
                DateTimeMicrosecondsImmutableType::DATETIME_MICROSECONDS_IMMUTABLE => DateTimeMicrosecondsImmutableType::class,
            ],
            json_decode(EnvHelper::getEnvFromProcessOrSuperGlobal('DOCTRINE_DBAL_TYPES'), true)
        ),
    ],

    'orm' => [
        /**
         * Here we can specify project-wide ORM mappings paths using:
         * 'mapping' => [
         *     'paths' => [
         *         'EntityClassName0' => 'path/to/mappings0',
         *         'EntityClassName1' => 'path/to/mappings1',
         *         etc...
         *     ],
         * ],
         *
         * ORM mappings paths should be specified in the ConfigProvider for each module.
         */
        'autoGenerateProxyClasses' => $ormAutoGenerateProxyClasses,
        'metadata' => [
            'cache' => $ormMetadataCache,
        ],
        'query' => [
            'cache' => $ormQueryCache,
        ],
        'secondLevelCache' => $ormSecondLevelCache,
        'entityManagers' => $entityManagers,
    ],

    'deprecation' => [
        'logger' => $deprecationLogger,
    ],
];
