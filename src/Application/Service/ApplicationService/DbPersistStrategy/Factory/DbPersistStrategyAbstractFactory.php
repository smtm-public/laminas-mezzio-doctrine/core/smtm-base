<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\DbPersistStrategy\Factory;

use Smtm\Base\Application\Service\ApplicationService\DbPersistStrategy\AbstractDbPersistStrategy;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DbPersistStrategyAbstractFactory implements AbstractFactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var AbstractDbPersistStrategy $strategy */
        $strategy = new $requestedName($options);
        $strategy->setApplicationServicePluginManager($container->get(ApplicationServicePluginManager::class));
        $strategy->setInfrastructureServicePluginManager($container->get(InfrastructureServicePluginManager::class));

        return $strategy;
    }

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return is_subclass_of($requestedName, AbstractDbPersistStrategy::class);
    }
}
