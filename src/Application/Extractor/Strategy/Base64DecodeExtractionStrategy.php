<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Base64DecodeExtractionStrategy extends AbstractExtractionStrategy
{
    public const OPTION_KEY_STRICT = 'strict';

    public function extract($value, ?object $object = null): ?string
    {
        return base64_decode($value, $this->options[static::OPTION_KEY_STRICT] ?? false);
    }
}
