<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

use Smtm\Base\Application\Exception\ProblemDetailsApplicationException;
use Smtm\Base\Infrastructure\Collection\Collection;
use Smtm\Base\Infrastructure\Collection\CollectionInterface;
use Smtm\Base\Infrastructure\Collection\PaginatedCollection;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Laminas\Log\LoggerAwareInterface;
use Smtm\Base\Infrastructure\Laminas\Log\LoggerAwareTrait;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteMethodNotAllowedResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceBadRequestResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceConflictResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceConnectionException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceForbiddenResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceGatewayTimeoutResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceInternalServerErrorResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceNotFoundResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceServiceUnavailableResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceUnauthorizedResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\RemoteServiceConnectorAuth;
use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Utils;
use GuzzleHttp\RequestOptions;
use JetBrains\PhpStorm\Pure;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;

/**
 * @author Ivan Chepanov <ivan.chepanov@smtm.bg>
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractRemoteServiceConnector extends AbstractInfrastructureService implements LoggerAwareInterface
{

    use LoggerAwareTrait;

    public const OPTION_NAME_REMOTE_HEADER_NAME_PAGINATION_COUNT =
        'optionNameRemoteHeaderNamePaginationCount';
    public const REMOTE_HEADER_NAME_PAGINATION_COUNT = 'x-pagination-count';
    public const OPTION_NAME_REMOTE_HEADER_NAME_PAGINATION_TOTAL_COUNT =
        'optionNameRemoteHeaderNamePaginationTotalCount';
    public const REMOTE_HEADER_NAME_PAGINATION_TOTAL_COUNT = 'x-pagination-total-count';
    public const OPTION_NAME_LOG_EXTRA_DATA =
        'optionNameLogExtraData';
    public const LOG_EXTRA_DATA = [];
    public const OPTION_NAME_THROW_ON_ERROR =
        'optionNameThrowOnError';
    public const THROW_ON_ERROR = false;

    #[Pure] public function __construct(
        InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected ClientInterface $client,
        protected ?array $options = null
    ) {
        parent::__construct($infrastructureServicePluginManager);
    }

    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    public function setClient(ClientInterface $client): static
    {
        $this->client = $client;

        return $this;
    }

    protected function createRequest(string $method, string|UriInterface $uri): RequestInterface
    {
        return new Request($method, $uri);
    }

    protected function processAuth(
        RequestInterface $request,
        array &$options,
        RemoteServiceConnectorAuth $auth
    ): RequestInterface {
        switch ($auth->getType()) {
            case RemoteServiceConnectorAuth::TYPE_BASIC:
                $request = $this->setAuthenticationBasicOnRequest(
                    $request,
                    $auth->getParams()['username'],
                    $auth->getParams()['password']
                );
                break;
            case RemoteServiceConnectorAuth::TYPE_TOKEN:
                $request = $this->setAuthenticationTokenOnRequest(
                    $request,
                    $auth->getParams()['token']
                );
                break;
            case RemoteServiceConnectorAuth::TYPE_BEARER:
                $request = $this->setAuthenticationBearerOnRequest(
                    $request,
                    $auth->getParams()['token']
                );
                break;
            case RemoteServiceConnectorAuth::TYPE_OAUTH:
                $request = $this->setAuthenticationOAuthOnRequest(
                    $request,
                    $auth->getParams()['consumerKey'],
                    $auth->getParams()['consumerSecret'],
                    $auth->getParams()['signatureMethod'],
                    $auth->getParams()['httpMethod'],
                    $auth->getParams()['url'],
                    $auth->getParams()['callback'] ?? null,
                    $auth->getParams()['verifier'] ?? null,
                    $auth->getParams()['realm'] ?? null
                );
                break;
            case RemoteServiceConnectorAuth::TYPE_DIGEST:
                $options['auth'] = [
                    $auth->getParams()['username'],
                    $auth->getParams()['password'],
                    'digest'
                ];
                break;
            case RemoteServiceConnectorAuth::TYPE_NTLM:
                $options['auth'] = [
                    $auth->getParams()['username'],
                    $auth->getParams()['password'],
                    'ntlm'
                ];
                break;
        }

        return $request;
    }

    protected function setAuthenticationBasicOnRequest(
        RequestInterface $request,
        string $username,
        ?string $password = null
    ): RequestInterface {
        $request = $request->withHeader(
            'Authorization',
            HttpHelper::buildHeaderAuthorizationBasicValue($username, $password)
        );

        return $request;
    }

    protected function setAuthenticationOAuthOnRequest(
        RequestInterface $request,
        string $consumerKey,
        string $consumerSecret,
        string $signatureMethod,
        string $httpMethod,
        string $url,
        ?string $callback = null,
        ?string $verifier = null,
        ?string $realm = null
    ): RequestInterface {
        $request = $request->withHeader(
            'Authorization',
            HttpHelper::buildHeaderAuthorizationOAuthValue(
                $consumerKey,
                $consumerSecret,
                $signatureMethod,
                $httpMethod,
                $url,
                $callback,
                $verifier,
                $realm
            )
        );

        return $request;
    }

    protected function setAuthenticationTokenOnRequest(RequestInterface $request, string $token): RequestInterface
    {
        $request = $request->withHeader(
            'Authorization',
            HttpHelper::buildHeaderAuthorizationTokenValue($token)
        );

        return $request;
    }

    protected function setAuthenticationBearerOnRequest(RequestInterface $request, string $token): RequestInterface
    {
        $request = $request->withHeader(
            'Authorization',
            HttpHelper::buildHeaderAuthorizationBearerValue($token)
        );

        return $request;
    }

    protected function setAcceptHeaderOnRequest(
        RequestInterface $request,
        string $accept
    ): RequestInterface {
        return $request->withHeader(
            'Accept',
            $accept
        );
    }

    protected function setTextBodyOnRequest(
        RequestInterface $request,
        string $body
    ): RequestInterface {
        $request = $request->withHeader(
            'Content-Type',
            'text/plain'
        );
        $stream = Utils::streamFor($body);

        return $request->withBody($stream);
    }

    protected function setJsonBodyOnRequest(
        RequestInterface $request,
        array|object|string|int|float|null $body
    ): RequestInterface {
        $request = $request->withHeader(
            'Content-Type',
            'application/json'
        );
        $stream = Utils::streamFor(json_encode($body));

        return $request->withBody($stream);
    }

    protected function setXmlBodyOnRequest(
        RequestInterface $request,
        string $body,
        /**
         * https://www.rfc-editor.org/rfc/rfc3023#section-3
         * https://www.rfc-editor.org/rfc/rfc7303#section-9.2
         */
        string $typeName = 'text'
    ): RequestInterface {
        $request = $request->withHeader(
            'Content-Type',
            $typeName . '/xml'
        );
        $stream = Utils::streamFor($body);

        return $request->withBody($stream);
    }

    protected function setHtmlBodyOnRequest(
        RequestInterface $request,
        string $body,
        string $typeName = 'text'
    ): RequestInterface {
        $request = $request->withHeader(
            'Content-Type',
            $typeName . '/html'
        );
        $stream = Utils::streamFor($body);

        return $request->withBody($stream);
    }

    protected function setBinaryBodyOnRequest(
        RequestInterface $request,
        string $body
    ): RequestInterface {
        $request = $request->withHeader(
            'Content-Type',
            'application/octet-stream'
        );
        $stream = Utils::streamFor($body);

        return $request->withBody($stream);
    }

    protected function setFormUrlEncodedBodyOnRequest(
        RequestInterface $request,
        array $formData
    ): RequestInterface {
        $request = $request->withHeader(
            'Content-Type',
            'application/x-www-form-urlencoded'
        );
        $stream = Utils::streamFor(http_build_query($formData));

        return $request->withBody($stream);
    }

    protected function sendRequest(
        RequestInterface $request,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA =>
                self::LOG_EXTRA_DATA,
            self::OPTION_NAME_THROW_ON_ERROR =>
                self::THROW_ON_ERROR,
        ]
    ): ResponseInterface {
        $logExtraData = $options[self::OPTION_NAME_LOG_EXTRA_DATA] ?? [];
        unset($options[self::OPTION_NAME_LOG_EXTRA_DATA]);

        $requestOptions = array_merge(
            $this->options ?? [],
            [
                RequestOptions::HEADERS => $request->getHeaders(),
                RequestOptions::BODY => $request->getBody(),
            ],
            $options
        );

        try {
            $response = $this->getClient()->request(
                $request->getMethod(),
                $request->getUri(),
                $requestOptions
            );
        } catch (ClientException | ServerException $e) {
            if ($options[self::OPTION_NAME_THROW_ON_ERROR] ?? false) {
                throw new RemoteServiceConnectionException(
                    'HTTP connection error',
                    previous: $e
                );
            } else {
                $response = $e->getResponse();
            }
        } catch (Exception $e) {
            throw new RemoteServiceConnectionException(
                'HTTP connection error',
                HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR,
                $e
            );
        }

        $request->getBody()->rewind();

        $this->getLogger() !== null
        && $this->getLogger()->info(
            '',
            array_merge(
                $logExtraData,
                [
                    'request_method' => $request->getMethod(),
                    'request_headers' => HttpHelper::formatHeadersArrayAsString($request->getHeaders()),
                    'request_uri' => (string) $request->getUri(),
                    'request_body' => $request->getBody()->getContents(),
                    'response_status_code' => $response->getStatusCode(),
                    'response_headers' => HttpHelper::formatHeadersArrayAsString($response->getHeaders()),
                    'response_body' => $response->getBody()->getContents(),
                ]
            )
        );

        $request->getBody()->rewind();
        $response->getBody()->rewind();

        return $response;
    }

    protected function parseResponse(
        ResponseInterface $response,
        int $expectedStatusCode = HttpHelper::STATUS_CODE_OK
    ): array|string|int|float|bool|null {
        return $this->validateParseResponse($response, $expectedStatusCode);
    }

    protected function parseCollectionResponse(
        ResponseInterface $response,
        int $expectedStatusCode = HttpHelper::STATUS_CODE_OK,
        array $options = [
            self::OPTION_NAME_REMOTE_HEADER_NAME_PAGINATION_TOTAL_COUNT =>
                self::REMOTE_HEADER_NAME_PAGINATION_TOTAL_COUNT,
        ]
    ): CollectionInterface {
        $parsedResponse = $this->validateParseResponse($response, $expectedStatusCode);

        if ($response->hasHeader($options[self::OPTION_NAME_REMOTE_HEADER_NAME_PAGINATION_TOTAL_COUNT])) {
            $parsedResponse = new PaginatedCollection(
                $parsedResponse,
                (int) $response->getHeaderLine($options[self::OPTION_NAME_REMOTE_HEADER_NAME_PAGINATION_TOTAL_COUNT])
            );
        } else {
            $parsedResponse = new Collection($parsedResponse);
        }

        return $parsedResponse;
    }

    protected function validateParseResponse(
        ResponseInterface $response,
        int $expectedStatusCode = HttpHelper::STATUS_CODE_OK
    ): array|string|float|int|bool|null {
        $responseStatusCode = $response->getStatusCode();
        $responseBody = $response->getBody()->getContents();

        $nonJsonResponseBody = false;
        $e = null;

        try {
            $parsedResponseBody =
                $responseBody !== ''
                    ? json_decode($responseBody, true, flags: JSON_THROW_ON_ERROR)
                    : null;
        } catch (\JsonException $e) {
            $nonJsonResponseBody = true;
            $parsedResponseBody = $responseBody;
        }

        if ($responseStatusCode !== $expectedStatusCode || $nonJsonResponseBody) {
            $errorMessage = $this->buildErrorMessage(
                $expectedStatusCode,
                $responseStatusCode,
                $responseBody,
                $nonJsonResponseBody
            );

            if (is_subclass_of($parsedResponseBody['class'] ?? '', ProblemDetailsApplicationException::class)) { // TODO: FIX LAYER MIXING
                throw new $parsedResponseBody['class'](
                    '',
                    0,
                    $e,
                    [
                        'code' => $parsedResponseBody['code'],
                        'additional' => $parsedResponseBody['additional']
                    ]
                );
            } else {
                $e = match ($responseStatusCode) {
                    HttpHelper::STATUS_CODE_BAD_REQUEST =>
                    new RemoteServiceBadRequestResponseException($errorMessage, 0, $e),
                    HttpHelper::STATUS_CODE_UNAUTHORIZED =>
                    new RemoteServiceUnauthorizedResponseException($errorMessage, 0, $e),
                    HttpHelper::STATUS_CODE_FORBIDDEN =>
                    new RemoteServiceForbiddenResponseException($errorMessage, 0, $e),
                    HttpHelper::STATUS_CODE_NOT_FOUND =>
                    new RemoteServiceNotFoundResponseException($errorMessage, 0, $e),
                    HttpHelper::STATUS_CODE_METHOD_NOT_ALLOWED =>
                    new RemoteMethodNotAllowedResponseException($errorMessage, 0, $e),
                    HttpHelper::STATUS_CODE_CONFLICT =>
                    new RemoteServiceConflictResponseException($errorMessage, 0, $e),
                    HttpHelper::STATUS_CODE_GATEWAY_TIMEOUT =>
                    new RemoteServiceGatewayTimeoutResponseException($errorMessage, 0, $e),
                    HttpHelper::STATUS_CODE_SERVICE_UNAVAILABLE =>
                    new RemoteServiceServiceUnavailableResponseException($errorMessage, 0, $e),
                    HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR =>
                    new RemoteServiceInternalServerErrorResponseException($errorMessage, 0, $e),
                    default => new RemoteServiceResponseException($errorMessage, 0, $e),
                };
                $e
                    ->setResponseStatus($responseStatusCode)
                    ->setResponseBody($responseBody)
                    ->setResponseBodyParsed($parsedResponseBody);

                throw $e;
            }
        }

        return $parsedResponseBody;
    }

    protected function buildErrorMessage(
        int $expectedStatusCode,
        int $responseStatusCode,
        string $responseBody,
        bool $nonJsonResponseBody
    ): string {
        $message = 'Remote service response exception. ';

        if ($nonJsonResponseBody) {
            $message .= 'The response body is not a valid JSON string. ';
        }

        $message .= 'Expected status: ' . $expectedStatusCode . "\n"
            . 'Got status: ' . $responseStatusCode . "\n"
            . 'Response body: ' . $responseBody;

        return $message;
    }
}
