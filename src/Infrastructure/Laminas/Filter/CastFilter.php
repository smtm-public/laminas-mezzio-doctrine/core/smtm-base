<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Laminas\Filter\AbstractFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CastFilter extends AbstractFilter
{
    public const OPTION_NAME_CAST_TO = 'castTo';
    public const OPTION_VALUE_CAST_TO_STRING = 'string';
    public const OPTION_VALUE_CAST_TO_FLOAT = 'float';
    public const OPTION_VALUE_CAST_TO_INT = 'int';
    public const OPTION_VALUE_CAST_TO_BOOL = 'bool';
    public const OPTION_VALUE_CAST_TO_FILTER_BOOL = 'filterBool';
    public const OPTION_VALUE_CAST_TO_EMPTY_TO_NULL = 'emptyToNull';

    protected string $castTo;

    public function __construct(protected $options = null)
    {
        $this->setOptions($options ?? []);
    }

    public function filter($value)
    {
        return match ($this->castTo) {
            'string' => (string) $value,
            'float' => (float) $value,
            'int' => (int) $value,
            'bool' => (bool) $value,
            'filterBool' => filter_var($value, FILTER_VALIDATE_BOOL),
            'emptyToNull' => empty($value) ? null : $value,
        };
    }

    public function getCastTo(): string
    {
        return $this->castTo;
    }

    public function setCastTo(string $castTo): static
    {
        $this->castTo = $castTo;

        return $this;
    }
}
