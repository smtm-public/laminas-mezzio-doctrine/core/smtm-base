<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ExtractorPluginManagerAwareInterface
{
    public function getExtractorPluginManager(): ExtractorPluginManager;
    public function setExtractorPluginManager(ExtractorPluginManager $extractorPluginManager): static;
}
