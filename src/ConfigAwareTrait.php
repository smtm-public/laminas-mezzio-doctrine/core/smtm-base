<?php

declare(strict_types=1);

namespace Smtm\Base;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ConfigAwareTrait
{
    protected array $config;

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setConfig(array $config): static
    {
        $this->config = $config;

        return $this;
    }
}
