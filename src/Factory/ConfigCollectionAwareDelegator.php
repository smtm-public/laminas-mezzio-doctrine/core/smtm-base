<?php

declare(strict_types=1);

namespace Smtm\Base\Factory;

use Smtm\Base\ConfigCollectionAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigCollectionAwareDelegator implements DelegatorFactoryInterface
{

    protected array $configCollection = [];

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var ConfigCollectionAwareInterface $object */
        $object = $callback();

        foreach ($this->configCollection as $configName => $config) {
            if ($configName !== null && $config !== '') {
                $currentConfigElement = $container->get('config');

                if (is_array($config)) {
                    foreach ($config as $configKey) {
                        $currentConfigElement = $currentConfigElement[$configKey];
                    }
                } else {
                    $currentConfigElement = $currentConfigElement[$config];
                }

                $object->addConfig(
                    $configName,
                    $currentConfigElement
                );
            }
        }

        return $object;
    }

    public function getConfigCollection(): array
    {
        return $this->configCollection;
    }

    public function setConfigCollection(array $configCollection): static
    {
        $this->configCollection = $configCollection;

        return $this;
    }
}
