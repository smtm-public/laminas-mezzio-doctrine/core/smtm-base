<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Codeception\Support;

use Codeception\Test\Unit;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IntegrationTestCase extends Unit
{
    protected IntegrationTester $tester;
}
