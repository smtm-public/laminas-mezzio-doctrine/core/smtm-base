<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ModifiedByIntColumnAndIndexAndForeignKeyMigrationTrait
{
    protected function addModifiedByIntColumnAndIndexAndForeignKey(
        Table $table,
        Table $foreignKeyTable,
        array $foreignKeyColumns = ['id'],
        string $relationIndexName = null,
        string $foreignKeyConstraintName = null,
        array $columnOptions = ['notNull' => false]
    ): void {
        $table->addColumn(
            'modified_by',
            Types::INTEGER,
            $columnOptions
        );
        $relationIndexName ?: $relationIndexName = 'idx_' . $table->getName() . '_modified_by';
        $table->addIndex(
            ['modified_by'],
            substr($relationIndexName, 0, SqlHelper::IDENTIFIER_LENGTH_INDEX)
        );
        $foreignKeyConstraintName ?: $foreignKeyConstraintName = 'fk_' . $table->getName() . '_modified_by_'
            . $foreignKeyTable->getName() . '_' . implode('_', $foreignKeyColumns);
        $table->addForeignKeyConstraint(
            $foreignKeyTable,
            ['modified_by'],
            $foreignKeyColumns,
            [],
            substr($foreignKeyConstraintName, 0, SqlHelper::IDENTIFIER_LENGTH_INDEX)
        );
    }
}
