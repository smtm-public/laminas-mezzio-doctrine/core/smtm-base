<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Factory;

use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class HydratorPluginManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new HydratorPluginManager(
            $container,
            [
                'abstract_factories' => [
                    EntityHydratorAbstractFactory::class,
                ],
            ]
        );
    }
}
