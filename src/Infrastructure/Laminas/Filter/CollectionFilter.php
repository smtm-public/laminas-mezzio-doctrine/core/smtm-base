<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Smtm\Base\Infrastructure\Exception\RuntimeException;
use Laminas\Filter\AbstractFilter;
use Laminas\Filter\FilterInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CollectionFilter extends AbstractFilter implements FilterPluginManagerAwareInterface
{

    use FilterPluginManagerAwareTrait;

    public const OPTION_NAME_FILTER = 'filter';

    protected array $filter;

    public function __construct(protected $options = null)
    {
        $this->setOptions($options ?? []);
    }

    public function filter($value)
    {
        if ($value === null) {
            return null;
        }

        if (empty($this->getOptions()['filter']['name'])) {
            throw new RuntimeException('The CollectionFilter filter name option is not set.');
        }

        if (!is_array($value)) {
            throw new RuntimeException('Expected value to be array. \'' . gettype($value) . '\' given.');
        }

        /** @var FilterInterface $filter */
        $filter = $this->filterPluginManager->build(
            $this->getOptions()['filter']['name'],
            $this->getOptions()['filter']['options'] ?? []
        );
        $filteredValue = [];

        foreach ($value as $key => $valueElement) {
            $filteredValue[$key] = $filter->filter($valueElement);
        }

        return $filteredValue;
    }
}
