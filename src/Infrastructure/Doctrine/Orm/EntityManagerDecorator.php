<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EntityManagerDecorator extends \Doctrine\ORM\Decorator\EntityManagerDecorator implements EntityManagerInterface
{
    public function __construct(EntityManagerInterface $wrapped)
    {
        parent::__construct($wrapped);
    }

    public function getReaderConnection()
    {
        return $this->wrapped->getReaderConnection();
    }

    public function getWriterConnection()
    {
        return $this->wrapped->getWriterConnection();
    }

    public function transactional($func)
    {
        if (! is_callable($func)) {
            throw new \InvalidArgumentException('Expected argument of type "callable", got "' . gettype($func) . '"');
        }

        $this->beginTransaction();

        try {
            $return = call_user_func($func, $this);

            $this->flush();
            $this->commit();

            return $return ?: true;
        } catch (\Throwable $e) {
            $this->rollBack();

            throw $e;
        }
    }

    public function reopen(): static
    {
        $this->wrapped->reopen();

        return $this;
    }
}
