<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RuntimeException extends \RuntimeException
{

}
