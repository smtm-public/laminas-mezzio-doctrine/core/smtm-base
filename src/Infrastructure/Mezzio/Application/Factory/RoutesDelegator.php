<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Mezzio\Application\Factory;

use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Mezzio\Application;
use Mezzio\Router\Route;
use Psr\Container\ContainerInterface;

class RoutesDelegator implements DelegatorFactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $name,
        callable $callback,
        array $options = null
    ): Application {
        /** @var Application $app */
        $app = $callback();

        $config = $container->get('config');

        foreach ($config['routes'] ?? [] as $routeName => $routeDefinition) {
            if ($routeDefinition['enabled'] ?? true) {
                /** @var Route $route */
                $route = $app->{$routeDefinition['method']}(
                    $routeDefinition['path'],
                    $routeDefinition['middleware'],
                    $routeName
                );
                !empty($routeDefinition['options']) && $route->setOptions($routeDefinition['options']);
            }
        }

        return $app;
    }
}
