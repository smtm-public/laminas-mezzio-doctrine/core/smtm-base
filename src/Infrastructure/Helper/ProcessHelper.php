<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ProcessHelper
{
    public const GET_MEMORY_STATS_RESULT_CONSOLE_LOGGER_HEADERS = 0b0001;

    public static function getStatsMemoryGetUsage(int $flags = 0b000): array
    {
        if ($flags & self::GET_MEMORY_STATS_RESULT_CONSOLE_LOGGER_HEADERS) {
            return [
                'memory_get_usage(): ' .  memory_get_usage(),
                'memory_get_usage() (formatted): ' .  number_format(memory_get_usage()),
            ];
        }

        return [
            'memory_get_usage' => memory_get_usage(),
            'memory_get_usage_formatted' => number_format(memory_get_usage()),
        ];
    }

    public static function getStatsMemoryGetUsageReal(int $flags = 0b000): array
    {
        if ($flags & self::GET_MEMORY_STATS_RESULT_CONSOLE_LOGGER_HEADERS) {
            return [
                'memory_get_usage(true): ' . memory_get_usage(true),
                'memory_get_usage(true) (formatted): ' . number_format(memory_get_usage(true)),
            ];
        }

        return [
            'memory_get_usage_real' => memory_get_usage(true),
            'memory_get_usage_real_formatted' => number_format(memory_get_usage(true)),
        ];
    }

    public static function getStatsMemoryGetPeakUsage(int $flags = 0b000): array
    {
        if ($flags & self::GET_MEMORY_STATS_RESULT_CONSOLE_LOGGER_HEADERS) {
            return [
                'memory_get_peak_usage(): ' . memory_get_peak_usage(),
                'memory_get_peak_usage() (formatted): ' . number_format(memory_get_peak_usage()),
            ];
        }

        return [
            'memory_get_peak_usage' => memory_get_peak_usage(),
            'memory_get_peak_usage_formatted' => number_format(memory_get_peak_usage()),
        ];
    }

    public static function getStatsMemoryGetPeakUsageReal(int $flags = 0b000): array
    {
        if ($flags & self::GET_MEMORY_STATS_RESULT_CONSOLE_LOGGER_HEADERS) {
            return [
                'memory_get_peak_usage(true): ' . memory_get_peak_usage(true),
                'memory_get_peak_usage(true) (formatted): ' . number_format(memory_get_peak_usage(true)),
            ];
        }

        return [
            'memory_get_peak_usage_real' => memory_get_peak_usage(true),
            'memory_get_peak_usage_real_formatted' => number_format(memory_get_peak_usage(true)),
        ];
    }
}
