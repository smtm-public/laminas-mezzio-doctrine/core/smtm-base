<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Diactoros\Factory;

use Smtm\Base\Infrastructure\Laminas\Diactoros\Uri;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UriFactory implements FactoryInterface, UriFactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $options ??= [];

        if (array_key_exists('allowedSchemes', $options)) {
            $uri = new Uri();
            $uri->setAllowedSchemes($options['allowedSchemes']);
            $uri->parseUri($options['uri'] ?? '');

            return $uri;
        }

        return $this->createUri($options['uri'] ?? '');
    }

    public static function createUriFromString(string $uri = ''): UriInterface
    {
        return new Uri($uri);
    }

    public function createUri(string $uri = ''): UriInterface
    {
        return static::createUriFromString($uri);
    }
}
