{
    "name": "smtm/smtm-base",
    "description": "smtm - library - base components",
    "license": "BSD-3-Clause",
    "keywords": [
        "smtm",
        "library",
        "base"
    ],
    "homepage": "https://smtm.com",
    "config": {
        "sort-packages": true
    },
    "minimum-stability": "dev",
    "prefer-stable": true,
    "conflict": {
        "doctrine/migrations": "<3.0",
        "guzzlehttp/guzzle": "<7.1"
    },
    "require": {
        "php": "^8.1.0",
        "ext-bcmath": "*",
        "ext-http": "*",
        "ext-imagick": "*",
        "ext-json": "*",
        "ext-mbstring": "*",
        "ext-oauth": "*",
        "ext-openssl": "*",
        "ext-pdo": "*",
        "ext-pdo_mysql": "*",
        "ext-pdo_sqlite": "*",
        "ext-pspell": "*",
        "ext-sockets": "*",
        "ext-ssh2": "*",
        "ext-yaml": "*",
        "ext-zip": "*",

        "defuse/php-encryption": "^2.0.0",
        "smtm/smtm-composer-plugin-run-package-scripts": "^1.0.0",
        "doctrine/dbal": "^3.3.2",
        "doctrine/doctrine-laminas-hydrator": "^3.0.0",
        "doctrine/doctrine-module": "^6.0.2",
        "doctrine/event-manager": "^1.1.1",
        "doctrine/orm": "^3.1.2",
        "doctrine/persistence": "^3.0.0",
        "doctrine/migrations": "^3.0",
        "gettext/gettext": "^v5.7.0",
        "guzzlehttp/guzzle": "^7.0.0",
        "laminas/laminas-cache": "^3.0.0",
        "laminas/laminas-component-installer": "^3.0.0",
        "laminas/laminas-config-aggregator": "^1.0.0",
        "laminas/laminas-diactoros": "^3.0.0",
        "laminas/laminas-development-mode": "^3.0.0",
        "laminas/laminas-eventmanager": "^3.0.0",
        "laminas/laminas-hydrator": "^4.0.0",
        "laminas/laminas-i18n": "^2.0.0",
        "laminas/laminas-inputfilter": "^2.0.0",
        "laminas/laminas-log": "^2.0.0",
        "laminas/laminas-serializer": "^2.0.0",
        "laminas/laminas-stdlib": "^3.0.0",
        "mezzio/mezzio": "^3.0.0",
        "mezzio/mezzio-helpers": "^5.0.0",
        "mezzio/mezzio-fastroute": "^3.0.0",
        "lcobucci/jwt": "^4.1.2",
        "mezzio/mezzio-cors": "^1.0.0",
        "mezzio/mezzio-problem-details": "^1.0.0",
        "phpoffice/phpspreadsheet": "^1.16.0",
        "php-http/mock-client": "^1.5.0",
        "php-webdriver/webdriver": "^1.13.1",
        "ramsey/uuid": "^4.0.0",
        "symfony/console": "^5.2",
        "vlucas/phpdotenv": "^5.2",
        "bjeavons/zxcvbn-php": "^1.0",
        "symfony/process": "^6.0.11",

        "filp/whoops": "^2.0.0",

        "slevomat/coding-standard": "^8.15.0",
        "squizlabs/php_codesniffer": "^3.10.1",
        "vimeo/psalm": "@stable",

        "codeception/codeception": "^5.1.2",
        "codeception/module-asserts": "^3.0.0",
        "codeception/module-cli": "^2.0.1",
        "codeception/module-datafactory": "^3.0.0",
        "codeception/module-doctrine2": "^3.0.4",
        "codeception/module-rest": "^3.3.2",
        "codeception/module-mezzio": "^4.0.2",
        "codeception/mockery-module": "^0.5.0",
        "league/factory-muffin": "^3.3.0",
        "league/factory-muffin-faker": "^2.3.0",
        "mockery/mockery": "^1.6.12"
    },


    "require-dev": {

    },
    "extra": {
        "laminas": {
            "config-provider": "Smtm\\Base\\ConfigProvider"
        },
        "run-package-scripts": true
    },
    "autoload": {
        "psr-4": {
            "Smtm\\Base\\": "src/",
            "SmtmTest\\Base\\": "test/"
        }
    },
    "autoload-dev": {
        "psr-4": {
            "SmtmTest\\Base\\": "test/"
        }
    },
    "bin": [
        "bin/generate-defuse-key.php",
        "bin/generate-openssl-key-pair",
        "bin/php-fpm-cli.sh",
        "bin/doctrine-console",
        "bin/symfony-console",
        "bin/xplatform-copy"
    ],
    "scripts": {
        "pre-package-install": [
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.bcmath.example .env.smtm.smtm-base.bcmath",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.l10n.example .env.smtm.smtm-base.l10n",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.i18n.example .env.smtm.smtm-base.i18n",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.doctrine.example .env.doctrine",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.laminas.laminas-cache.example .env.laminas.laminas-cache",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.laminas.laminas-diactoros.example .env.laminas.laminas-diactoros",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.mezzio.example .env.mezzio",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.mezzio.mezzio-problem-details.example .env.mezzio.mezzio-problem-details",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.mezzio.mezzio-cors.example .env.mezzio.mezzio-cors",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.http.error-handling.example .env.smtm.smtm-base.http.error-handling",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.http.handler.example .env.smtm.smtm-base.http.handler",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.infrastructure.crypto-service.example .env.smtm.smtm-base.infrastructure.crypto-service",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.infrastructure.jwt.example .env.smtm.smtm-base.infrastructure.jwt",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.infrastructure.laminas.log.example .env.smtm.smtm-base.infrastructure.laminas.log",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.infrastructure.remote-service-connector.example .env.smtm.smtm-base.infrastructure.remote-service-connector",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.infrastructure.webdriver.example .env.smtm.smtm-base.infrastructure.webdriver",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.infrastructure.application-cache-service.example .env.smtm.smtm-base.infrastructure.application-cache-service",
            "generate-defuse-key.php -n data/keys/defuse-key"
        ],
        "pre-package-update": [
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.bcmath.example .env.smtm.smtm-base.bcmath",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.l10n.example .env.smtm.smtm-base.l10n",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.i18n.example .env.smtm.smtm-base.i18n",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.doctrine.example .env.doctrine",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.laminas.laminas-cache.example .env.laminas.laminas-cache",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.laminas.laminas-diactoros.example .env.laminas.laminas-diactoros",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.mezzio.example .env.mezzio",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.mezzio.mezzio-problem-details.example .env.mezzio.mezzio-problem-details",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.mezzio.mezzio-cors.example .env.mezzio.mezzio-cors",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.http.error-handling.example .env.smtm.smtm-base.http.error-handling",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.http.handler.example .env.smtm.smtm-base.http.handler",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.infrastructure.crypto-service.example .env.smtm.smtm-base.infrastructure.crypto-service",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.infrastructure.jwt.example .env.smtm.smtm-base.infrastructure.jwt",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.infrastructure.laminas.log.example .env.smtm.smtm-base.infrastructure.laminas.log",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.infrastructure.remote-service-connector.example .env.smtm.smtm-base.infrastructure.remote-service-connector",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.infrastructure.webdriver.example .env.smtm.smtm-base.infrastructure.webdriver",
            "xplatform-copy -n vendor/smtm/smtm-base/.env.smtm.smtm-base.infrastructure.application-cache-service.example .env.smtm.smtm-base.infrastructure.application-cache-service",
            "generate-defuse-key.php -n data/keys/defuse-key"
        ],
        "phpcs-smtm-smtm-base": "phpcs --standard=\"vendor/smtm/smtm-base/phpcs.xml\"",
        "phpinfo-html": "php-fpm-cli.sh vendor/smtm/smtm-base/bin/phpinfo.php vendor/smtm/smtm-base/bin/phpinfo.php GET $(netstat -pantu | grep php-fpm.conf | awk '{print $4}' | sed 's/::://' | sed 's/127.0.0.1://' | sed 's/0.0.0.0://')",
        "phpinfo-html-file": "php-fpm-cli.sh vendor/smtm/smtm-base/bin/phpinfo.php vendor/smtm/smtm-base/bin/phpinfo.php GET $(netstat -pantu | grep php-fpm.conf | awk '{print $4}' | sed 's/::://' | sed 's/127.0.0.1://' | sed 's/0.0.0.0://') > data/reports/phpinfo/html/PhpInfo_$(date +'%Y-%m-%dT%H_%M_%SZ').html",
        "phpinfo-cli": "symfony-console base:phpinfo -vvv",
        "phpinfo-cli-file": "symfony-console base:phpinfo -vvv > data/reports/phpinfo/text/PhpInfo_$(date +'%Y-%m-%dT%H_%M_%SZ').txt"
    }
}
