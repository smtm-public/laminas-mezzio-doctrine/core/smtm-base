<?php

namespace Smtm\Base\Infrastructure\Service\ApplicationCache\Adapter\Redis;

use Smtm\Base\Infrastructure\Service\InfrastructureServiceInterface;
use Laminas\Serializer\Adapter\AbstractAdapter;
use Predis\Client;
use Psr\SimpleCache\CacheInterface;

class RedisAdapter implements CacheInterface, InfrastructureServiceInterface
{
    public function __construct(
        protected Client $redis,
        protected AbstractAdapter $serializer
    ) {}

    public function get($key, $default = null)
    {
        if ($serialized = $this->redis->get($key)) {
            return $this->serializer->unserialize($serialized);
        }

        return $default;
    }

    public function getMultiple($keys = array(), $default = null)
    {
        return array_map([$this, 'getItem'], $keys);
    }

    public function has($key)
    {
        return $this->redis->exists($key) > 0;
    }


    public function clear()
    {
        // no-op
    }

    public function delete($key): bool
    {
        return $this->redis->del($key) > 0;
    }


    public function deleteMultiple($keys)
    {
        $result = array_map([$this, 'deleteItem'], $keys);

        return array_sum($result) === count($keys);
    }


    public function set($key, $value, $ttl = null)
    {
        if ($ttl > 0) {
            $this->redis->setex($key, $ttl, $this->serializer->serialize($value));
        } else {
            $this->redis->set($key, $this->serializer->serialize($value));
        }
    }

    public function setMultiple($values, $ttl = null)
    {
        foreach ($values as $key => $value) {
            $this->set($key, $value, $ttl);
        }
    }
}
