<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Smtm\Base\Application\Extractor\ExtractorPluginManagerAwareInterface;
use Smtm\Base\Application\Extractor\ExtractorPluginManagerAwareTrait;
use Smtm\Base\OptionsAwareInterface;
use Smtm\Base\OptionsAwareTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractExtractorPluginManagerAwareExtractionStrategy extends AbstractExtractionStrategy
    implements OptionsAwareInterface, ExtractorPluginManagerAwareInterface
{
    use OptionsAwareTrait, ExtractorPluginManagerAwareTrait;

    public const OPTION_KEY_EXTRACTOR_NAME = 'extractorName';
    public const OPTION_KEY_EXTRACTOR_OPTIONS = 'extractorOptions';
    public const OPTION_KEY_EXTRACTOR_OPTIONS_KEY_INCLUDES = 'includes';

    public function __construct()
    {
        $this->options = [
            self::OPTION_KEY_EXTRACTOR_NAME => null,
            self::OPTION_KEY_EXTRACTOR_OPTIONS => [
                self::OPTION_KEY_EXTRACTOR_OPTIONS_KEY_INCLUDES => [],
            ],
        ];
    }
}
