<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Diactoros;

use Psr\Http\Message\UriInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * This class bypasses some of the limitations of laminas-diactoros being strictly bound by the PSR-7 rules and more
 * specifically, it allows non-standard scheme URLs to be handled. For more info see:
 * https://github.com/laminas/laminas-diactoros/issues/179
 */
class Uri extends \Laminas\Diactoros\Uri
{
    protected \Laminas\Diactoros\Uri $wrapped;

    public function __construct(string $uri = '')
    {
        $this->wrapped = new \Laminas\Diactoros\Uri($uri);
    }

    /**
     * {@inheritdoc}
     */
    public function __clone()
    {
        $this->wrapped = clone $this->wrapped;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString() : string
    {
        return $this->wrapped->__toString();
    }

    public function getScheme(): string
    {
        return $this->wrapped->getScheme();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthority(): string
    {
        return $this->wrapped->getAuthority();
    }

    /**
     * {@inheritdoc}
     */
    public function getUserInfo(): string
    {
        return $this->wrapped->getUserInfo();
    }

    /**
     * {@inheritdoc}
     */
    public function getHost(): string
    {
        return $this->wrapped->getHost();
    }

    /**
     * {@inheritdoc}
     */
    public function getPort(): ?int
    {
        return $this->wrapped->getPort();
    }

    /**
     * {@inheritdoc}
     */
    public function getQuery(): string
    {
        return $this->wrapped->getQuery();
    }

    /**
     * {@inheritdoc}
     */
    public function getFragment(): string
    {
        return $this->wrapped->getFragment();
    }

    /**
     * {@inheritdoc}
     */
    public function withScheme($scheme): UriInterface
    {
        $new = clone $this;
        $new->wrapped = clone $this->wrapped->withScheme($scheme);

        return $new;
    }

    /**
     * {@inheritdoc}
     */
    public function withUserInfo(
        $user,
        #[\SensitiveParameter]
        $password = null
    ): UriInterface {
        $new = clone $this;
        $new->wrapped = clone $this->wrapped->withUserInfo($user, $password);

        return $new;
    }

    /**
     * {@inheritdoc}
     */
    public function withHost($host): UriInterface
    {
        $new = clone $this;
        $new->wrapped = clone $this->wrapped->withHost($host);

        return $new;
    }

    /**
     * {@inheritdoc}
     */
    public function withPort($port): UriInterface
    {
        $new = clone $this;
        $new->wrapped = clone $this->wrapped->withPort($port);

        return $new;
    }

    /**
     * {@inheritdoc}
     */
    public function withPath($path): UriInterface
    {
        $new = clone $this;
        $new->wrapped = clone $this->wrapped->withPath($path);

        return $new;
    }

    /**
     * {@inheritdoc}
     */
    public function withQuery($query): UriInterface
    {
        $new = clone $this;
        $new->wrapped = clone $this->wrapped->withPath($query);

        return $new;
    }

    /**
     * {@inheritdoc}
     */
    public function withFragment($fragment): UriInterface
    {
        $new = clone $this;
        $new->wrapped = clone $this->wrapped->withPath($fragment);

        return $new;
    }

    public function parseUri($uri = '')
    {
        $parseUriMethod = new \ReflectionMethod($this->wrapped, 'parseUri');
        $parseUriMethod->setAccessible(true);
        $parseUriMethod->invoke($this->wrapped, $uri);
        $parseUriMethod->setAccessible(false);
    }

    public function getAllowedSchemes(): array
    {
        $wrappedAllowedSchemes = new \ReflectionProperty($this->wrapped, 'allowedSchemes');

        return $wrappedAllowedSchemes->getValue();
    }

    public function setAllowedSchemes(array $allowedSchemes): static
    {
        $wrappedAllowedSchemes = new \ReflectionProperty($this->wrapped, 'allowedSchemes');
        $wrappedAllowedSchemes->setValue($this->wrapped, $allowedSchemes);

        return $this;
    }
}
