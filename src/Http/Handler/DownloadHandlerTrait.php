<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler;

use Smtm\Base\Infrastructure\Laminas\Diactoros\Response\DownloadResponseDecorator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait DownloadHandlerTrait
{
    public function prepareDownloadResponse(
        string $contentType,
        string $fileName,
        mixed $body = 'php://temp',
        int $status = 200,
        array $headers = []
    ): DownloadResponseDecorator {
        return new DownloadResponseDecorator(
            $contentType,
            $fileName,
            $body,
            $status,
            $headers
        );
    }
}
