<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Guzzle\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use GuzzleHttp\TransferStats;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
final class ClientFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): ClientInterface
    {
        $config = $container->get('config')['base']['infrastructure']['remote_service_connector']['client'];

        $handler = HandlerStack::create();

        if (!empty($config['logging']['loggers'])) {
            /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
            $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

            foreach ($config['logging']['loggers'] as $logger) {
                if (is_string($logger)) {
                    $handler->push(
                        Middleware::log(
                            $infrastructureServicePluginManager->get($logger),
                            new MessageFormatter($config['logging']['defaultMessageFormatterTemplate'])
                        )
                    );
                } elseif (is_object($logger)) {
                    $handler->push(
                        Middleware::log(
                            $logger,
                            new MessageFormatter($config['logging']['defaultMessageFormatterTemplate'])
                        )
                    );
                } elseif (is_array($logger)) {
                    $handler->push(
                        Middleware::log(
                            $infrastructureServicePluginManager->build(
                                $logger['name'],
                                $logger['options'] ?? null
                            ),
                            new MessageFormatter(
                                $logger['messageFormatterTemplate']
                                    ?? $config['logging']['defaultMessageFormatterTemplate']
                            )
                        )
                    );
                }
            }
        }

        return new Client([
            'handler' => $handler,
            'verify' => $config['verifyRemoteCertificate'],
            'on_stats' => function (TransferStats $stats) {
                $stats;
            }
        ]);
    }
}
