<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Codeception\Module;

use Codeception\Event\FailEvent;
use Codeception\Lib\Interfaces\DataMapper;
use Codeception\TestInterface;
use Smtm\Base\Infrastructure\League\FactoryMuffin\Stores\RepositoryStore;
use League\FactoryMuffin\Stores\StoreInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DataFactory extends \Codeception\Module\DataFactory
{
    public function _after(TestInterface $test)
    {
        $time = time();

        try {
            parent::_after($test);
        } catch (\Throwable $t) {
            $test->getResultAggregator()->addError(new FailEvent($test, $t, time() - $time));

            //throw $t;
        }
    }

    protected function getStore(): ?StoreInterface
    {
        if (!empty($this->config['customStore'])) {
            $store = new $this->config['customStore'];
            if (method_exists($store, 'create')) {
                return $store->create();
            }

            return $store;
        }

        return $this->ormModule instanceof DataMapper
            ? new RepositoryStore($this->ormModule->_getEntityManager()) // for Doctrine
            : null;
    }
}
