<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler\DbServiceEntity;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Http\Middleware\ServerParamsAndRequestIdMiddleware;
use Smtm\Base\Infrastructure\Collection\CollectionInterface;
use Smtm\Base\Infrastructure\Collection\PaginatedCollectionInterface;
use JetBrains\PhpStorm\ArrayShape;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractIndexHandler extends AbstractHandler
{
    public const PAGINATION_DEFAULT_PAGE = 1;
    public const PAGINATION_DEFAULT_PAGE_SIZE = AbstractDbService::DEFAULT_PAGE_SIZE;
    public const PAGINATION_DEFAULT_BUTTONS_COUNT = 10;
    public const ORDER_NONE = 0;
    public const ORDER_ASC = 1;
    public const ORDER_DESC = -1;

    protected array $criteria = [];

    protected static function getPage(?array $parsedQueryParams = []): int
    {
        return $parsedQueryParams['page'] ?? static::PAGINATION_DEFAULT_PAGE;
    }

    protected static function getPageSize(?array $parsedQueryParams = []): int
    {
        return $parsedQueryParams['pageSize'] ?? static::PAGINATION_DEFAULT_PAGE_SIZE;
    }

    protected static function getPages(PaginatedCollectionInterface $items, int $pageSize): int
    {
        return (int) ceil($items->getTotalCount() / $pageSize);
    }

    protected static function getPaginationDefaultButtonsCount(): int
    {
        return static::PAGINATION_DEFAULT_BUTTONS_COUNT;
    }

    protected static function getPaginationButtonsLeadingCount(int $buttonsCount): int
    {
        return ceil($buttonsCount / 2) - 1;
    }

    protected static function getPaginationButtonsTrailingCount(int $buttonsCount): int
    {
        return (int) floor($buttonsCount / 2);
    }

    protected static function getPaginationButtonsLeadingIndexMin(
        int $page,
        int $pages,
        int $buttonsLeadingCount,
        int $buttonsTrailingCount
    ): int {
        return max(
            1,
            $page - $buttonsLeadingCount - abs(min(0, $pages - ($page + $buttonsTrailingCount)))
        );
    }

    protected static function getPaginationButtonsTrailingIndexMax(
        int $page,
        int $pages,
        int $buttonsTrailingCount
    ): int {
        return min(
            $page + $buttonsTrailingCount + abs(min(0, $page - $buttonsTrailingCount)),
            $pages
        );
    }

    #[ArrayShape([
        'page' => 'int',
        'pages' => 'int',
        'pageSize' => 'int',
        'total' => 'int',
        'buttonLeadingIndexMin' => 'int',
        'buttonTrailingIndexMax' => 'int'
    ])] protected static function getPagination(
        PaginatedCollectionInterface $items,
        int $page,
        int $pageSize,
        ?int $buttonsCount = null
    ): array {
        $pages = static::getPages($items, $pageSize);

        if ($buttonsCount === null) {
            $buttonsCount = static::getPaginationDefaultButtonsCount();
        }

        return [
            'page' => $page,
            'pages' => $pages,
            'pageSize' => $pageSize,
            'total' => $items->getTotalCount(),
            'buttonLeadingIndexMin' => static::getPaginationButtonsLeadingIndexMin(
                $page,
                $pages,
                static::getPaginationButtonsLeadingCount($buttonsCount),
                static::getPaginationButtonsTrailingCount($buttonsCount)
            ),
            'buttonTrailingIndexMax' => static::getPaginationButtonsTrailingIndexMax(
                $page,
                $pages,
                static::getPaginationButtonsTrailingCount($buttonsCount)
            ),
        ];
    }

    protected static function getOrderBy(?array $parsedQueryParams = [], ?array $appendOrderBy = null): array
    {
        if ($appendOrderBy) {
            $parsedQueryParams['orderBy'] += $appendOrderBy;
        }

        return $parsedQueryParams['orderBy'];
    }

    protected static function getOrderIndex(string $field, ?array $parsedQueryParams = []): int
    {
        $value = $parsedQueryParams['orderBy'][$field] ?? null;

        return $value === 'ASC' ? static::ORDER_ASC : ($value === 'DESC' ? static::ORDER_DESC : static::ORDER_NONE);
    }

    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->prepareResponse(
            $this->getAll($request),
            static::parseQueryParams($this->getMergedFilteredQueryParams($request))
        );
    }

    public function getAll(
        ServerRequestInterface $request,
        ?array $overrideMergedFilteredQueryParams = null
    ): CollectionInterface | array {
        $mergedFilteredQueryParams = $overrideMergedFilteredQueryParams ?? $this->getMergedFilteredQueryParams($request);
        $parsedQueryParams = static::parseQueryParams($mergedFilteredQueryParams);

        if (isset($parsedQueryParams['criteria'])) {
            if ($this->getCriteria()) {
                $this->setCriteria(
                    array_merge(
                        $parsedQueryParams['criteria'],
                        [$this->getCriteria()]
                    )
                );
            } else {
                $this->setCriteria($parsedQueryParams['criteria']);
            }
        }

        if ($this->config['index']['criteria']['translateUnknownQueryParamsAsCriteria']) {
            if ($this->config['index']['criteria']['treatStringValueAsLooseMatch']) {
                switch ($this->config['index']['criteria']['treatStringValueAsLooseMatch']) {
                    case 1:
                        foreach ($mergedFilteredQueryParams as $name => $value) {
                            if (!array_key_exists($name, static::DEFAULT_PARSED_QUERY_PARAMS) && !in_array($name, static::IGNORE_QUERY_PARAMS)) {
                                if (is_string($value)) {
                                    $this->setCriteria(
                                        array_merge(
                                            $this->getCriteria(),
                                            [['like', $name, '%' . $value . '%']]
                                        )
                                    );
                                } else {
                                    $this->setCriteria(
                                        array_merge(
                                            $this->getCriteria(),
                                            [$name => $value]
                                        )
                                    );
                                }
                            }
                        }

                        break;
                    case 2:
                        foreach ($mergedFilteredQueryParams as $name => $value) {
                            if (!array_key_exists($name, static::DEFAULT_PARSED_QUERY_PARAMS) && !in_array($name, static::IGNORE_QUERY_PARAMS)) {
                                if (is_string($value)) {
                                    $this->setCriteria(
                                        array_merge(
                                            $this->getCriteria(),
                                            [['like', $name, $value . '%']]
                                        )
                                    );
                                } else {
                                    $this->setCriteria(
                                        array_merge(
                                            $this->getCriteria(),
                                            [$name => $value]
                                        )
                                    );
                                }
                            }
                        }

                        break;
                    case 3:
                        foreach ($mergedFilteredQueryParams as $name => $value) {
                            if (!array_key_exists($name, static::DEFAULT_PARSED_QUERY_PARAMS) && !in_array($name, static::IGNORE_QUERY_PARAMS)) {
                                if (is_string($value)) {
                                    $this->setCriteria(
                                        array_merge(
                                            $this->getCriteria(),
                                            [['like', $name, '%' . $value]]
                                        )
                                    );
                                } else {
                                    $this->setCriteria(
                                        array_merge(
                                            $this->getCriteria(),
                                            [$name => $value]
                                        )
                                    );
                                }
                            }
                        }

                        break;
                }
            } else {
                foreach ($mergedFilteredQueryParams as $name => $value) {
                    if (!array_key_exists($name, static::DEFAULT_PARSED_QUERY_PARAMS) && !in_array($name, static::IGNORE_QUERY_PARAMS)) {
                        $this->setCriteria(
                            array_merge(
                                $this->getCriteria(),
                                [$name => $value]
                            )
                        );
                    }
                }
            }
        }

        return $this->applicationService->getAll(
            $this->getCriteria(),
            static::getPage($parsedQueryParams),
            static::getPageSize($parsedQueryParams),
            static::getOrderBy($parsedQueryParams, ['id' => 'ASC']),
            array_replace(
                [ApplicationServiceInterface::OPTION_KEY_PARAMS => $parsedQueryParams],
                [ApplicationServiceInterface::OPTION_KEY_METHOD => $request->getMethod()],
                ServerParamsAndRequestIdMiddleware::getRequestParams($request)
            )
        );
    }

    public function getCriteria(): array
    {
        return $this->criteria;
    }

    public function setCriteria(array $criteria): static
    {
        $this->criteria = $criteria;

        return $this;
    }
}
