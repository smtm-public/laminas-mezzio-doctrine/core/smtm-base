<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Factory;

use Defuse\Crypto\Key;
use Smtm\Base\Infrastructure\Service\CryptoService;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CryptoServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CryptoService
    {
        $config = $container->get('config')['base']['infrastructure']['crypto_service'];
        $key = Key::loadFromAsciiSafeString(
            file_get_contents($config['keyPath'])
        );

        return new CryptoService(
            $container->get(InfrastructureServicePluginManager::class),
            $key
        );
    }
}
