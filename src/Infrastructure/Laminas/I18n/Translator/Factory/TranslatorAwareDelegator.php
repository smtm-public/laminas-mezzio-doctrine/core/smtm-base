<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\I18n\Translator\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\I18n\Translator\TranslatorAwareInterface;
use Laminas\I18n\Translator\TranslatorInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TranslatorAwareDelegator implements DelegatorFactoryInterface
{
    protected ?string $translatorName = null;

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var TranslatorAwareInterface $object */
        $object = $callback();
        /** @var TranslatorInterface $translator */
        $translator = $container->get(InfrastructureServicePluginManager::class)->get(
            $this->translatorName ?? TranslatorInterface::class
        );
        $object->setTranslator($translator);

        return $object;
    }

    public function getTranslatorName(): ?string
    {
        return $this->translatorName;
    }

    public function setTranslatorName(?string $translatorName): static
    {
        $this->translatorName = $translatorName;

        return $this;
    }
}
