<?php
/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * Based on
 *     https://gist.github.com/superbrothers/3431198
 *
 * Example:
 ```
 echo AnsiEscapeSequenceHelper::styleString(
     'Test test test',
     [
         AnsiEscapeSequenceHelper::STYLE_OFF,
         AnsiEscapeSequenceHelper::STYLE_BOLD,
         AnsiEscapeSequenceHelper::STYLE_FAINT,
         AnsiEscapeSequenceHelper::STYLE_ITALIC,
         AnsiEscapeSequenceHelper::STYLE_UNDERLINE,
         AnsiEscapeSequenceHelper::STYLE_SLOW_BLINK,
         AnsiEscapeSequenceHelper::STYLE_RAPID_BLINK,
         AnsiEscapeSequenceHelper::STYLE_INVERT,
         AnsiEscapeSequenceHelper::STYLE_HIDE,
         AnsiEscapeSequenceHelper::STYLE_STRIKETHROUGH,
         AnsiEscapeSequenceHelper::STYLE_DEFAULT_FONT,
         AnsiEscapeSequenceHelper::STYLE_ALT_FONT0,
         AnsiEscapeSequenceHelper::STYLE_ALT_FONT1,
         AnsiEscapeSequenceHelper::STYLE_ALT_FONT2,
         AnsiEscapeSequenceHelper::STYLE_ALT_FONT3,
         AnsiEscapeSequenceHelper::STYLE_ALT_FONT4,
         AnsiEscapeSequenceHelper::STYLE_ALT_FONT5,
         AnsiEscapeSequenceHelper::STYLE_ALT_FONT6,
         AnsiEscapeSequenceHelper::STYLE_ALT_FONT7,
         AnsiEscapeSequenceHelper::STYLE_ALT_FONT8,
         AnsiEscapeSequenceHelper::STYLE_FRAKTUR,
         AnsiEscapeSequenceHelper::STYLE_DOUBLY_UNDERLINED,
         AnsiEscapeSequenceHelper::STYLE_NO_BOLD_NO_FAINT,
         AnsiEscapeSequenceHelper::STYLE_NO_ITALIC_NO_BLACKLETTER,
         AnsiEscapeSequenceHelper::STYLE_NO_UNDERLINE,
         AnsiEscapeSequenceHelper::STYLE_NO_BLINK,
         AnsiEscapeSequenceHelper::STYLE_PROPORTIONAL_SPACING,
         AnsiEscapeSequenceHelper::STYLE_NO_INVERT,
         AnsiEscapeSequenceHelper::STYLE_NO_HIDE,
         AnsiEscapeSequenceHelper::STYLE_NO_STRIKETHROUGH,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_BLACK,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_RED,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_GREEN,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_YELLOW,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_BLUE,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_MAGENTA,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_CYAN,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_WHITE,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_WITH_ARGS,
         AnsiEscapeSequenceHelper::STYLE_DEFAULT_FOREGROUND_COLOR,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_BLACK,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_RED,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_GREEN,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_YELLOW,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_BLUE,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_MAGENTA,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_CYAN,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_WHITE,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_WITH_ARGS,
         AnsiEscapeSequenceHelper::STYLE_DEFAULT_BACKGROUND_COLOR,
         AnsiEscapeSequenceHelper::STYLE_NO_PROPORTIONAL_SPACING,
         AnsiEscapeSequenceHelper::STYLE_FRAME,
         AnsiEscapeSequenceHelper::STYLE_ENCIRCLE,
         AnsiEscapeSequenceHelper::STYLE_OVERLINE,
         AnsiEscapeSequenceHelper::STYLE_NO_FRAME_NO_ENCIRCLE,
         AnsiEscapeSequenceHelper::STYLE_NO_OVERLINE,
         AnsiEscapeSequenceHelper::STYLE_UNDERLINE_COLOR_WITH_ARGS,
         AnsiEscapeSequenceHelper::STYLE_DEFAULT_UNDERLINE_COLOR,
         AnsiEscapeSequenceHelper::STYLE_IDEOGRAM_UNDERLINE_OR_RIGHT_SIDE_LINE,
         AnsiEscapeSequenceHelper::STYLE_DOUBLE_IDEOGRAM_UNDERLINE_OR_RIGHT_SIDE_LINE,
         AnsiEscapeSequenceHelper::STYLE_IDEOGRAM_OVERLINE_OR_LEFT_SIDE_LINE,
         AnsiEscapeSequenceHelper::STYLE_DOUBLE_IDEOGRAM_OVERLINE_OR_LEFT_SIDE_LINE,
         AnsiEscapeSequenceHelper::STYLE_IDEOGRAM_STRESS_MARKING,
         AnsiEscapeSequenceHelper::STYLE_NO_IDEOGRAM_STYLING,
         AnsiEscapeSequenceHelper::STYLE_SUPERSCRIPT,
         AnsiEscapeSequenceHelper::STYLE_SUBSCRIPT,
         AnsiEscapeSequenceHelper::STYLE_NO_SUPERSCRIPT_NO_SUBSCRIPT,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_BRIGHT_BLACK,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_BRIGHT_RED,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_BRIGHT_GREEN,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_BRIGHT_YELLOW,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_BRIGHT_BLUE,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_BRIGHT_MAGENTA,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_BRIGHT_CYAN,
         AnsiEscapeSequenceHelper::STYLE_FOREGROUND_COLOR_BRIGHT_WHITE,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_BRIGHT_BLACK,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_BRIGHT_RED,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_BRIGHT_GREEN,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_BRIGHT_YELLOW,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_BRIGHT_BLUE,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_BRIGHT_MAGENTA,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_BRIGHT_CYAN,
         AnsiEscapeSequenceHelper::STYLE_BACKGROUND_COLOR_BRIGHT_WHITE,
     ]
 );
 ```
 *
 */
namespace Smtm\Base\Infrastructure\Helper;

class AnsiEscapeSequenceHelper
{
    public const STYLE_OFF = 'off';
    public const STYLE_BOLD = 'bold';
    public const STYLE_FAINT = 'faint';
    public const STYLE_ITALIC = 'italic';
    public const STYLE_UNDERLINE = 'underline';
    public const STYLE_SLOW_BLINK = 'slow_blink';
    public const STYLE_RAPID_BLINK = 'rapid_blink';
    public const STYLE_INVERT = 'invert';
    public const STYLE_HIDE = 'hide';
    public const STYLE_STRIKETHROUGH = 'strikethrough';
    public const STYLE_DEFAULT_FONT = 'default_font';
    public const STYLE_ALT_FONT0 = 'alt_font0';
    public const STYLE_ALT_FONT1 = 'alt_font1';
    public const STYLE_ALT_FONT2 = 'alt_font2';
    public const STYLE_ALT_FONT3 = 'alt_font3';
    public const STYLE_ALT_FONT4 = 'alt_font4';
    public const STYLE_ALT_FONT5 = 'alt_font5';
    public const STYLE_ALT_FONT6 = 'alt_font6';
    public const STYLE_ALT_FONT7 = 'alt_font7';
    public const STYLE_ALT_FONT8 = 'alt_font8';
    public const STYLE_FRAKTUR = 'fraktur';
    public const STYLE_DOUBLY_UNDERLINED = 'doubly_underlined'; // Disables bold intensity on some terminals. See: https://en.wikipedia.org/wiki/ANSI_escape_code
    public const STYLE_NO_BOLD_NO_FAINT = 'no_bold_no_faint';
    public const STYLE_NO_ITALIC_NO_BLACKLETTER = 'no_italic_no_blackletter';
    public const STYLE_NO_UNDERLINE = 'no_underline';
    public const STYLE_NO_BLINK = 'no_blink';
    public const STYLE_PROPORTIONAL_SPACING = 'proportional_spacing';
    public const STYLE_NO_INVERT = 'no_invert';
    public const STYLE_NO_HIDE = 'no_hide';
    public const STYLE_NO_STRIKETHROUGH = 'no_strikethrough';
    public const STYLE_FOREGROUND_COLOR_BLACK = 'foreground_color_black';
    public const STYLE_FOREGROUND_COLOR_RED = 'foreground_color_red';
    public const STYLE_FOREGROUND_COLOR_GREEN = 'foreground_color_green';
    public const STYLE_FOREGROUND_COLOR_YELLOW = 'foreground_color_yellow';
    public const STYLE_FOREGROUND_COLOR_BLUE = 'foreground_color_blue';
    public const STYLE_FOREGROUND_COLOR_MAGENTA = 'foreground_color_magenta';
    public const STYLE_FOREGROUND_COLOR_CYAN = 'foreground_color_cyan';
    public const STYLE_FOREGROUND_COLOR_WHITE = 'foreground_color_white';
    public const STYLE_FOREGROUND_COLOR_WITH_ARGS = 'foreground_color_with_args';
    public const STYLE_DEFAULT_FOREGROUND_COLOR = 'default_foreground_color';
    public const STYLE_BACKGROUND_COLOR_BLACK = 'background_color_black';
    public const STYLE_BACKGROUND_COLOR_RED = 'background_color_red';
    public const STYLE_BACKGROUND_COLOR_GREEN = 'background_color_green';
    public const STYLE_BACKGROUND_COLOR_YELLOW = 'background_color_yellow';
    public const STYLE_BACKGROUND_COLOR_BLUE = 'background_color_blue';
    public const STYLE_BACKGROUND_COLOR_MAGENTA = 'background_color_magenta';
    public const STYLE_BACKGROUND_COLOR_CYAN = 'background_color_cyan';
    public const STYLE_BACKGROUND_COLOR_WHITE = 'background_color_white';
    public const STYLE_BACKGROUND_COLOR_WITH_ARGS = 'background_color_with_args';
    public const STYLE_DEFAULT_BACKGROUND_COLOR = 'default_background_color';
    public const STYLE_NO_PROPORTIONAL_SPACING = 'no_proportional_spacing';
    public const STYLE_FRAME = 'frame';
    public const STYLE_ENCIRCLE = 'encircle';
    public const STYLE_OVERLINE = 'overline';
    public const STYLE_NO_FRAME_NO_ENCIRCLE = 'no_frame_no_encircle';
    public const STYLE_NO_OVERLINE = 'no_overline';
    public const STYLE_UNDERLINE_COLOR_WITH_ARGS = 'underline_color_with_args';
    public const STYLE_DEFAULT_UNDERLINE_COLOR = 'default_underline_color';
    public const STYLE_IDEOGRAM_UNDERLINE_OR_RIGHT_SIDE_LINE = 'ideogram_underline_or_right_side_line';
    public const STYLE_DOUBLE_IDEOGRAM_UNDERLINE_OR_RIGHT_SIDE_LINE = 'double_ideogram_underline_or_right_side_line';
    public const STYLE_IDEOGRAM_OVERLINE_OR_LEFT_SIDE_LINE = 'ideogram_overline_or_left_side_line';
    public const STYLE_DOUBLE_IDEOGRAM_OVERLINE_OR_LEFT_SIDE_LINE = 'double_ideogram_overline_or_left_side_line';
    public const STYLE_IDEOGRAM_STRESS_MARKING = 'ideogram_stress_marking';
    public const STYLE_NO_IDEOGRAM_STYLING = 'no_ideogram_styling';
    public const STYLE_SUPERSCRIPT = 'superscript';
    public const STYLE_SUBSCRIPT = 'subscript';
    public const STYLE_NO_SUPERSCRIPT_NO_SUBSCRIPT = 'no_superscript_no_subscript';
    public const STYLE_FOREGROUND_COLOR_BRIGHT_BLACK = 'foreground_color_bright_black';
    public const STYLE_FOREGROUND_COLOR_BRIGHT_RED = 'foreground_color_bright_red';
    public const STYLE_FOREGROUND_COLOR_BRIGHT_GREEN = 'foreground_color_bright_green';
    public const STYLE_FOREGROUND_COLOR_BRIGHT_YELLOW = 'foreground_color_bright_yellow';
    public const STYLE_FOREGROUND_COLOR_BRIGHT_BLUE = 'foreground_color_bright_blue';
    public const STYLE_FOREGROUND_COLOR_BRIGHT_MAGENTA = 'foreground_color_bright_magenta';
    public const STYLE_FOREGROUND_COLOR_BRIGHT_CYAN = 'foreground_color_bright_cyan';
    public const STYLE_FOREGROUND_COLOR_BRIGHT_WHITE = 'foreground_color_bright_white';
    public const STYLE_BACKGROUND_COLOR_BRIGHT_BLACK = 'background_color_bright_black';
    public const STYLE_BACKGROUND_COLOR_BRIGHT_RED = 'background_color_bright_red';
    public const STYLE_BACKGROUND_COLOR_BRIGHT_GREEN = 'background_color_bright_green';
    public const STYLE_BACKGROUND_COLOR_BRIGHT_YELLOW = 'background_color_bright_yellow';
    public const STYLE_BACKGROUND_COLOR_BRIGHT_BLUE = 'background_color_bright_blue';
    public const STYLE_BACKGROUND_COLOR_BRIGHT_MAGENTA = 'background_color_bright_magenta';
    public const STYLE_BACKGROUND_COLOR_BRIGHT_CYAN = 'background_color_bright_cyan';
    public const STYLE_BACKGROUND_COLOR_BRIGHT_WHITE = 'background_color_bright_white';

    public const ANSI_CODES = [
        self::STYLE_OFF                                             => 0,
        self::STYLE_BOLD                                            => 1,
        self::STYLE_FAINT                                           => 2,
        self::STYLE_ITALIC                                          => 3,
        self::STYLE_UNDERLINE                                       => 4,
        self::STYLE_SLOW_BLINK                                      => 5,
        self::STYLE_RAPID_BLINK                                     => 6,
        self::STYLE_INVERT                                          => 7,
        self::STYLE_HIDE                                            => 8,
        self::STYLE_STRIKETHROUGH                                   => 9,
        self::STYLE_DEFAULT_FONT                                    => 10,
        self::STYLE_ALT_FONT0                                       => 11,
        self::STYLE_ALT_FONT1                                       => 12,
        self::STYLE_ALT_FONT2                                       => 13,
        self::STYLE_ALT_FONT3                                       => 14,
        self::STYLE_ALT_FONT4                                       => 15,
        self::STYLE_ALT_FONT5                                       => 16,
        self::STYLE_ALT_FONT6                                       => 17,
        self::STYLE_ALT_FONT7                                       => 18,
        self::STYLE_ALT_FONT8                                       => 19,
        self::STYLE_FRAKTUR                                         => 20,
        self::STYLE_DOUBLY_UNDERLINED                               => 21,
        self::STYLE_NO_BOLD_NO_FAINT                                => 22,
        self::STYLE_NO_ITALIC_NO_BLACKLETTER                        => 23,
        self::STYLE_NO_UNDERLINE                                    => 24,
        self::STYLE_NO_BLINK                                        => 25,
        self::STYLE_PROPORTIONAL_SPACING                            => 26,
        self::STYLE_NO_INVERT                                       => 27,
        self::STYLE_NO_HIDE                                         => 28,
        self::STYLE_NO_STRIKETHROUGH                                => 29,
        self::STYLE_FOREGROUND_COLOR_BLACK                          => 30,
        self::STYLE_FOREGROUND_COLOR_RED                            => 31,
        self::STYLE_FOREGROUND_COLOR_GREEN                          => 32,
        self::STYLE_FOREGROUND_COLOR_YELLOW                         => 33,
        self::STYLE_FOREGROUND_COLOR_BLUE                           => 34,
        self::STYLE_FOREGROUND_COLOR_MAGENTA                        => 35,
        self::STYLE_FOREGROUND_COLOR_CYAN                           => 36,
        self::STYLE_FOREGROUND_COLOR_WHITE                          => 37,
        self::STYLE_FOREGROUND_COLOR_WITH_ARGS                      => 38,
        self::STYLE_DEFAULT_FOREGROUND_COLOR                        => 39,
        self::STYLE_BACKGROUND_COLOR_BLACK                          => 40,
        self::STYLE_BACKGROUND_COLOR_RED                            => 41,
        self::STYLE_BACKGROUND_COLOR_GREEN                          => 42,
        self::STYLE_BACKGROUND_COLOR_YELLOW                         => 43,
        self::STYLE_BACKGROUND_COLOR_BLUE                           => 44,
        self::STYLE_BACKGROUND_COLOR_MAGENTA                        => 45,
        self::STYLE_BACKGROUND_COLOR_CYAN                           => 46,
        self::STYLE_BACKGROUND_COLOR_WHITE                          => 47,
        self::STYLE_BACKGROUND_COLOR_WITH_ARGS                      => 48,
        self::STYLE_DEFAULT_BACKGROUND_COLOR                        => 49,
        self::STYLE_NO_PROPORTIONAL_SPACING                         => 50,
        self::STYLE_FRAME                                           => 51,
        self::STYLE_ENCIRCLE                                        => 52,
        self::STYLE_OVERLINE                                        => 53,
        self::STYLE_NO_FRAME_NO_ENCIRCLE                            => 54,
        self::STYLE_NO_OVERLINE                                     => 55,
        self::STYLE_UNDERLINE_COLOR_WITH_ARGS                       => 58,
        self::STYLE_DEFAULT_UNDERLINE_COLOR                         => 59,
        self::STYLE_IDEOGRAM_UNDERLINE_OR_RIGHT_SIDE_LINE           => 60,
        self::STYLE_DOUBLE_IDEOGRAM_UNDERLINE_OR_RIGHT_SIDE_LINE    => 61,
        self::STYLE_IDEOGRAM_OVERLINE_OR_LEFT_SIDE_LINE             => 62,
        self::STYLE_DOUBLE_IDEOGRAM_OVERLINE_OR_LEFT_SIDE_LINE      => 63,
        self::STYLE_IDEOGRAM_STRESS_MARKING                         => 64,
        self::STYLE_NO_IDEOGRAM_STYLING                             => 65,
        self::STYLE_SUPERSCRIPT                                     => 73,
        self::STYLE_SUBSCRIPT                                       => 74,
        self::STYLE_NO_SUPERSCRIPT_NO_SUBSCRIPT                     => 75,
        self::STYLE_FOREGROUND_COLOR_BRIGHT_BLACK                   => 90,
        self::STYLE_FOREGROUND_COLOR_BRIGHT_RED                     => 91,
        self::STYLE_FOREGROUND_COLOR_BRIGHT_GREEN                   => 92,
        self::STYLE_FOREGROUND_COLOR_BRIGHT_YELLOW                  => 93,
        self::STYLE_FOREGROUND_COLOR_BRIGHT_BLUE                    => 94,
        self::STYLE_FOREGROUND_COLOR_BRIGHT_MAGENTA                 => 95,
        self::STYLE_FOREGROUND_COLOR_BRIGHT_CYAN                    => 96,
        self::STYLE_FOREGROUND_COLOR_BRIGHT_WHITE                   => 97,
        self::STYLE_BACKGROUND_COLOR_BRIGHT_BLACK                   => 100,
        self::STYLE_BACKGROUND_COLOR_BRIGHT_RED                     => 101,
        self::STYLE_BACKGROUND_COLOR_BRIGHT_GREEN                   => 102,
        self::STYLE_BACKGROUND_COLOR_BRIGHT_YELLOW                  => 103,
        self::STYLE_BACKGROUND_COLOR_BRIGHT_BLUE                    => 104,
        self::STYLE_BACKGROUND_COLOR_BRIGHT_MAGENTA                 => 105,
        self::STYLE_BACKGROUND_COLOR_BRIGHT_CYAN                    => 106,
        self::STYLE_BACKGROUND_COLOR_BRIGHT_WHITE                   => 107,
    ];

    /**
     * Use '+' to combine styles/color
     */
    public static function styleString(string $string, array $styles): string
    {
        $result = '';

        foreach ($styles as $style) {
            $result .= "\033[" . static::ANSI_CODES[$style] . 'm';
        }

        $result .= $string . "\033[" . static::ANSI_CODES[self::STYLE_OFF] . 'm';

        return $result;
    }

    public static function styleSubstringByRegexPattern(
        string $string,
        string $substringRegexPattern,
        array $styles
    ): string {
        $result = preg_replace_callback(
            "/($substringRegexPattern)/",
            function ($matches) use ($styles) {
                return static::styleString($matches[1], $styles);
            },
            $string
        );

        return $result ?? $string;
    }
}
