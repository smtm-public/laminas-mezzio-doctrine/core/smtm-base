<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class StringHelper
{
    public static function lineSeparatorsLfToCrLf(string $string, bool $mixed = true): string
    {
        if ($mixed) {
            return mb_ereg_replace("(?:(?<!\r)\n)", "\r\n", $string);
        }

        return str_replace("\n", "\r\n", $string);
    }

    public static function lineSeparatorsCrToCrLf(string $string, bool $mixed = true): string
    {
        if ($mixed) {
            return mb_ereg_replace("(?:\r(?!\n))", "\r\n", $string);
        }

        return str_replace("\r", "\r\n", $string);
    }

    public static function lineSeparatorsAnyToCrLf(string $string, bool $mixed = true): string
    {
        if ($mixed) {
            return mb_ereg_replace("(?:(?<!\r)\n)", "\r\n", mb_ereg_replace("(?:\r(?!\n))", "\r\n", $string));
        }

        return str_replace(["\r\n", "\r", "\n"], "\r\n", $string);
    }

    public static function lineSeparatorsCrLfToLf(string $string): string
    {
        return str_replace("\r\n", "\n", $string);
    }

    public static function lineSeparatorsCrToLf(string $string): string
    {
        return str_replace("\r", "\n", $string);
    }

    public static function lineSeparatorsAnyToLf(string $string): string
    {
        return str_replace(["\r\n", "\r", "\n"], "\n", $string);
    }

    public static function lineSeparatorsCrLfToCr(string $string): string
    {
        return str_replace("\r\n", "\r", $string);
    }

    public static function lineSeparatorsLfToCr(string $string): string
    {
        return str_replace("\n", "\r", $string);
    }

    public static function lineSeparatorsAnyToCr(string $string): string
    {
        return str_replace(["\r\n", "\n"], "\r", $string);
    }
}
