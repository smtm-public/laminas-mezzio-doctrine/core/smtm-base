<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Smtm\Base\Infrastructure\Helper\DateTimeHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DateIntervalHelperFullFormatExtractionStrategy extends AbstractExtractionStrategy
{
    public const OPTION_KEY_SEPARATOR = 'separator';
    public const OPTION_KEY_FLAGS = 'flags';
    public const OPTION_KEY_COMPONENT_MAPPING = 'componentMapping';

    public function extract($value, ?object $object = null): ?string
    {
        return DateTimeHelper::fullFormatDateInterval(
            $value,
            $this->options[self::OPTION_KEY_SEPARATOR] ?? ',',
            $this->options[self::OPTION_KEY_FLAGS] ?? DateTimeHelper::FORMAT_DATE_INTERVAL_FLAG_FILTER_EMPTY_VALUES,
            $this->options[self::OPTION_KEY_COMPONENT_MAPPING] ?? DateTimeHelper::DATE_INTERVAL_FORMAT_COMPONENTS_MAPPING_FULL
        );
    }
}
