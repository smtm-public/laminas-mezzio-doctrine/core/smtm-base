<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Log;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait LoggerCollectionAwareTrait
{

    /* use LoggerAwareTrait; */

    /** @var \Psr\Log\LoggerInterface[] $loggerCollection */
    protected array $loggerCollection = [];

    /** @return \Psr\Log\LoggerInterface[] */
    public function getLoggerCollection(): array
    {
        return $this->loggerCollection;
    }

    public function setLoggerCollection(array $loggerCollection): static
    {
        $this->loggerCollection = $loggerCollection;

        return $this;
    }

    public function addLogger(string $name, \Psr\Log\LoggerInterface $logger): static
    {
        $this->loggerCollection[$name] = $logger;

        return $this;
    }

    public function getLoggerByName(string $name): ?\Psr\Log\LoggerInterface
    {
        return $this->loggerCollection[$name] ?? null;
    }

    public function removeLogger(string $name): static
    {
        unset($this->loggerCollection[$name]);

        return $this;
    }
}
