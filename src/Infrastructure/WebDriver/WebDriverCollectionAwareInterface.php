<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\WebDriver;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface WebDriverCollectionAwareInterface
{
    public function getWebDriverCollection(): array;
    public function setWebDriverCollection(array $webDriverCollection): static;
    public function addWebDriver(string $name, WebDriver $webDriver): static;
    public function getWebDriverByName(string $name): ?WebDriver;
    public function removeWebDriver(string $name): static;
}
