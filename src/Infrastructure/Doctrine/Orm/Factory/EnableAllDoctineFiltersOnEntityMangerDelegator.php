<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm\Factory;

use Doctrine\ORM\EntityManager;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

class EnableAllDoctineFiltersOnEntityMangerDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var EntityManager $object */
        $object = $callback();

        $config = $container->get('config')['doctrine'];

        foreach ($config['orm']['query']['filters'] as $name => $filter) {
            $object->getFilters()->enable($name);
        }

        return $object;
    }
}
