<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\Factory;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbPersistStrategy\DbPersistStrategyPluginManager;
use Smtm\Base\Application\Service\ApplicationService\DbPersistStrategyAwareServiceInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DbServiceAbstractFactory implements AbstractFactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var AbstractDbService $service */
        $service = new $requestedName(
            applicationServicePluginManager: $container->get(ApplicationServicePluginManager::class),
            infrastructureServicePluginManager: $container->get(InfrastructureServicePluginManager::class),
            hydratorPluginManager: $container->get(HydratorPluginManager::class),
            extractorPluginManager: $container->get(ExtractorPluginManager::class),
            baseHydratorPluginManager: $container->get(\Laminas\Hydrator\HydratorPluginManager::class),
            entityManagerRegistry: $container
                ->get(InfrastructureServicePluginManager::class)
                ->get(ManagerRegistryInterface::class)
        );

        if ($service instanceof DbPersistStrategyAwareServiceInterface) {
            $service->setDbPersistStrategy(
                $container->get(DbPersistStrategyPluginManager::class)->build(
                    $service->getDbPersistStrategyDefinition()['name'],
                    $service->getDbPersistStrategyDefinition()['options']
                )
            );
        }

        return $service;
    }

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return is_subclass_of($requestedName, AbstractDbService::class);
    }
}
