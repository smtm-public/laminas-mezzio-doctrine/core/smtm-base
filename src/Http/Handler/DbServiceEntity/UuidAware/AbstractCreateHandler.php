<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler\DbServiceEntity\UuidAware;

use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Http\Handler\DbServiceEntity\AbstractCreateHandler as DbServiceEntityAbstractCreateHandler;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractCreateHandler extends DbServiceEntityAbstractCreateHandler
{
    /** @var AbstractDbService&UuidAwareEntityDbServiceInterface $applicationService */
    protected ?AbstractApplicationService $applicationService = null;
}
