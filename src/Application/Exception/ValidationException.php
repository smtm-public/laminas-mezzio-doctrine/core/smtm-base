<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Exception;

use JetBrains\PhpStorm\Pure;
use Throwable;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ValidationException extends RuntimeException
{
    #[Pure] public function __construct(
        $message = "",
        $code = 0,
        Throwable $previous = null,
        protected ?array $errors = [])
    {
        parent::__construct($message, $code, $previous);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
