<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait NotArchivedAndArchivedAtAwareEntityTrait
{
    use NotArchivedAwareEntityTrait,
        ArchivedAtAwareEntityTrait;
}
