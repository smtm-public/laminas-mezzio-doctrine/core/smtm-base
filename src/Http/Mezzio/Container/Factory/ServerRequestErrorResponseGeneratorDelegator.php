<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Mezzio\Container\Factory;

use Smtm\Base\Http\Mezzio\Response\ServerRequestErrorResponseGenerator;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Mezzio\Container\ResponseFactoryFactory;
use Mezzio\Response\CallableResponseFactoryDecorator;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Webmozart\Assert\Assert;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ServerRequestErrorResponseGeneratorDelegator
{
    public function __invoke(
        ContainerInterface $container,
        $name,
        callable $callback,
        array $options = null
    ): callable {
        $config = $container->has('config') ? $container->get('config') : [];
        Assert::isArrayAccessible($config);

        $debug = $config['base']['http']['error_handling']['debug'] ?? false;
        $mezzioConfiguration = $config['mezzio'] ?? [];
        Assert::isMap($mezzioConfiguration);
        $errorHandlerConfiguration = $mezzioConfiguration['error_handler'] ?? [];
        Assert::isMap($errorHandlerConfiguration);

        $renderer =
            (
                $errorHandlerConfiguration['template_renderer_enabled']
                && !empty($errorHandlerConfiguration['template_renderer_service_name'])
                && $container->has($errorHandlerConfiguration['template_renderer_service_name'])
            )
                ? $container->get($errorHandlerConfiguration['template_renderer_service_name'])
                : null;

        $template = $errorHandlerConfiguration['template_error']
            ?? ServerRequestErrorResponseGenerator::TEMPLATE_DEFAULT;

        $responseFactory = $this->detectResponseFactory($container);

        $serverRequestErrorResponseGenerator = new ServerRequestErrorResponseGenerator(
            $responseFactory,
            $debug,
            $renderer,
            $template
        );
        $serverRequestErrorResponseGenerator->setConfig($config['base']['http']['error_handling']);
        /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

        foreach ($container->get('config')['base']['http']['error_handling']['loggers'] as $loggerName => $loggerServiceName) {
            $serverRequestErrorResponseGenerator->addLogger(
                $loggerName,
                $infrastructureServicePluginManager->get($loggerServiceName)
            );
        }

        return $serverRequestErrorResponseGenerator;
    }

    private function detectResponseFactory(ContainerInterface $container): ResponseFactoryInterface
    {
        $psr17FactoryAvailable = $container->has(ResponseFactoryInterface::class);

        if (! $psr17FactoryAvailable) {
            return $this->createResponseFactoryFromDeprecatedCallable($container);
        }

        if ($this->doesConfigurationProvidesDedicatedResponseFactory($container)) {
            return $this->createResponseFactoryFromDeprecatedCallable($container);
        }

        $responseFactory = $container->get(ResponseFactoryInterface::class);
        Assert::isInstanceOf($responseFactory, ResponseFactoryInterface::class);
        return $responseFactory;
    }

    private function createResponseFactoryFromDeprecatedCallable(
        ContainerInterface $container
    ): ResponseFactoryInterface {
        /** @var callable():ResponseInterface $responseFactory */
        $responseFactory = $container->get(ResponseInterface::class);

        return new CallableResponseFactoryDecorator($responseFactory);
    }

    private function doesConfigurationProvidesDedicatedResponseFactory(ContainerInterface $container): bool
    {
        if (! $container->has('config')) {
            return false;
        }

        $config = $container->get('config');
        Assert::isArrayAccessible($config);
        $dependencies = $config['dependencies'] ?? [];
        Assert::isMap($dependencies);

        $delegators = $dependencies['delegators'] ?? [];
        $aliases    = $dependencies['aliases'] ?? [];
        Assert::isArrayAccessible($delegators);
        Assert::isArrayAccessible($aliases);

        if (isset($delegators[ResponseInterface::class]) || isset($aliases[ResponseInterface::class])) {
            // Even tho, aliases could point to a different service, we assume that there is a dedicated factory
            // available. The alias resolving is not worth it.
            return true;
        }

        /** @psalm-suppress MixedAssignment */
        $deprecatedResponseFactory = $dependencies['factories'][ResponseInterface::class] ?? null;

        return $deprecatedResponseFactory !== null && $deprecatedResponseFactory !== ResponseFactoryFactory::class;
    }
}
