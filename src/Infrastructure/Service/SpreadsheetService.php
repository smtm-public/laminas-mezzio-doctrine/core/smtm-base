<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

use Smtm\Base\Infrastructure\Exception\RuntimeException;
use Smtm\Base\Infrastructure\PhpOffice\PhpSpreadsheet\IOFactory;
use Exception;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Row;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv as WriterCsv;
use PhpOffice\PhpSpreadsheet\Writer\Xls as WriterXls;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as WriterXlsx;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class SpreadsheetService extends AbstractInfrastructureService
{
    public const FORMAT_CSV = 'csv';
    public const FORMAT_XLS = 'xls';
    public const FORMAT_XLSX = 'xlsx';

    public const WRITER_OPTION_KEY_CSV_DELIMITER = 'delimiter';
    public const WRITER_OPTION_KEY_CSV_ENCLOSURE = 'enclosure';
    public const WRITER_OPTION_KEY_CSV_LINE_ENDING = 'lineEnding';
    public const WRITER_OPTION_KEY_CSV_USE_BOM = 'useBom';

    public static function getColumnIndexFromString(string $column): int
    {
        return Coordinate::columnIndexFromString($column);
    }

    public function createBlankSpreadsheet(): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        // remove the initial worksheet for a start-from-scratch experience
        $spreadsheet->removeSheetByIndex(0);

        return $spreadsheet;
    }

    public function createWorksheet(
        Spreadsheet $spreadsheet,
        string $title,
        array $headings,
        array ...$dataRows
    ): Worksheet {
        $worksheet = new Worksheet($spreadsheet, $title);
        $headerStartColumnIndex = 1;
        $rowIndex = 1;

        if (!empty($headings)) {
            foreach ($headings as $heading) {
                $worksheet->setCellValue([$headerStartColumnIndex, 1], $heading);
                $headerStartColumnIndex++;
            }

            $rowIndex = 2;
        }

        foreach ($dataRows as $dataRow) {
            $columnIndex = 1;

            foreach ($dataRow as $cellValue) {
                $worksheet->setCellValue([$columnIndex, $rowIndex], $cellValue);
                $columnIndex++;
            }

            $rowIndex++;
        }

        return $worksheet;
    }

    public function getSpreadsheetWriter(
        Spreadsheet $spreadsheet,
        string $format = self::FORMAT_XLS,
        array $options = [
            self::WRITER_OPTION_KEY_CSV_DELIMITER => ',',
            self::WRITER_OPTION_KEY_CSV_ENCLOSURE => '"',
            self::WRITER_OPTION_KEY_CSV_LINE_ENDING => "\n",
            self::WRITER_OPTION_KEY_CSV_USE_BOM => true,
        ]
    ): ?BaseWriter {
        switch (strtolower($format)) {
            case self::FORMAT_XLS:
                return new WriterXls($spreadsheet);
            case self::FORMAT_XLSX:
                return new WriterXlsx($spreadsheet);
            case self::FORMAT_CSV:
                $writer = new WriterCsv($spreadsheet);
                $writer->setDelimiter($options[self::WRITER_OPTION_KEY_CSV_DELIMITER] ?? ',');
                $writer->setEnclosure($options[self::WRITER_OPTION_KEY_CSV_ENCLOSURE] ?? '"');
                $writer->setLineEnding($options[self::WRITER_OPTION_KEY_CSV_LINE_ENDING] ?? "\n");
                $writer->setUseBOM($options[self::WRITER_OPTION_KEY_CSV_USE_BOM] ?? true);

                return $writer;
            default:
                throw new RuntimeException('Unrecognized spreadsheet writer format `' . $format . '`');
        }
    }

    public function getSpreadsheetData(
        Spreadsheet $spreadsheet,
        string $format = self::FORMAT_XLS,
        array $options = [
            self::WRITER_OPTION_KEY_CSV_DELIMITER => ',',
            self::WRITER_OPTION_KEY_CSV_ENCLOSURE => '"',
        ]
    ): ?string {
        $writer = null;
        $output = null;

        if (strtolower($format) === self::FORMAT_XLS) {
            $writer = new WriterXls($spreadsheet);
        } elseif (strtolower($format) === self::FORMAT_XLSX) {
            $writer = new WriterXlsx($spreadsheet);
        } elseif (strtolower($format) === self::FORMAT_CSV) {
            $writer = new WriterCsv($spreadsheet);
            $writer->setDelimiter($options[self::WRITER_OPTION_KEY_CSV_DELIMITER] ?? ',');
            $writer->setEnclosure($options[self::WRITER_OPTION_KEY_CSV_ENCLOSURE] ?? '"');
            $writer->setLineEnding("\n");
            $writer->setUseBOM(true);
        }

        if ($writer) {
            $handle = fopen('php://temp', 'wb+');

            if ($handle === false) {
                throw new Exception('Error creating file handle.');
            }

            try {
                $writer->save($handle);
                fseek($handle, 0);
                $output = stream_get_contents($handle);
            } finally {
                fclose($handle);
            }
        }

        return $output;
    }

    public function createReaderFromFile(string $path): Spreadsheet
    {
        return IOFactory::load($path);
    }

    public function createReaderFromString(string $data): Spreadsheet
    {
        return (new IOFactory)->loadFromString($data);
    }

    public function getWorksheetHeadingsRowCellValuesAsArray(Worksheet $worksheet, int $rowNumber = 1): array
    {
        $headingsRow = $worksheet->getRowIterator($rowNumber, $rowNumber)->current();

        return $this->getRowCellValuesAsArray($headingsRow);
    }

    public function getRowCellValuesAsArray(Row $row): array
    {
        $cellIterator = $row->getCellIterator();
        $cells = [];

        foreach($cellIterator as $cell) {
            $cells[] = $cell->getValue();
        }

        return $cells;
    }

    public function getRowCellCalculatedValuesAsArray(Row $row): array
    {
        $cellIterator = $row->getCellIterator();
        $cells = [];

        foreach($cellIterator as $cell) {
            $cells[] = $cell->getCalculatedValue();
        }

        return $cells;
    }

    public function getRowCellFormattedValuesAsArray(Row $row): array
    {
        $cellIterator = $row->getCellIterator();
        $cells = [];

        foreach($cellIterator as $cell) {
            $cells[] = $cell->getFormattedValue();
        }

        return $cells;
    }
}
