<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Base\Infrastructure\Service\JwtService;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lcobucci\JWT;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
final class JwtServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): JwtService
    {
        $privateKeyPath = $options['privateKeyPath'];
        $publicKeyPath = $options['publicKeyPath'];

        $privateKeySigner =
            $privateKeyPath !== null
                ? JWT\Signer\Key\InMemory::file($privateKeyPath)
                : JWT\Signer\Key\InMemory::plainText('dummy');
        $publicKeySigner =
            $publicKeyPath !== null
                ? JWT\Signer\Key\InMemory::file($publicKeyPath)
                : JWT\Signer\Key\InMemory::plainText('dummy');

        $configuration = JWT\Configuration::forAsymmetricSigner(
            new Jwt\Signer\Rsa\Sha256(),
            $privateKeySigner,
            $publicKeySigner
        );

        $configuration->setValidationConstraints(
            new JWT\Validation\Constraint\SignedWith(
                new JWT\Signer\Rsa\Sha256(),
                $configuration->verificationKey()
            )
        );

        return new JwtService(
            $container->get(InfrastructureServicePluginManager::class),
            $configuration
        );
    }
}
