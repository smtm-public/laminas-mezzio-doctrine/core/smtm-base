<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface LaminasPsrLoggerInterface extends \Laminas\Log\LoggerInterface, \Psr\Log\LoggerInterface
{

}
