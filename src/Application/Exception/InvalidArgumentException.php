<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InvalidArgumentException extends \InvalidArgumentException
{

}
