<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * This strategy must be defined in the constructor of the respective hydrator because class property initializers can
 * not contain expressions (such as lambda functions). Otherwise a "Constant expression contains invalid operations"
 * error is thrown. Note that parent::__construct() must be called after the property definitions.
 *
 * Class member variables are called "properties". You may also see them referred to using other terms such as
 * "attributes" or "fields", but for the purposes of this reference we will use "properties". They are defined by using
 * one of the keywords public, protected, or private, followed by a normal variable declaration. This declaration may
 * include an initialization, but this initialization must be a constant value--that is, it must be able to be evaluated
 * at compile time and must not depend on run-time information in order to be evaluated.
 * @link http://php.net/manual/en/language.oop5.properties.php
 *
 * <code>
 * public function __construct(
 *     ApplicationServicePluginManager $applicationServicePluginManager,
 *     InfrastructureServicePluginManager $infrastructureServicePluginManager,
 *     HydrationStrategyPluginManager $hydrationStrategyPluginManager
 * ) {
 *     $this->properties['role'] = [
 *         'strategy' => [
 *             'name' => ClosureStrategy::class,
 *             'options' => [
 *                 ClosureStrategy::OPTION_KEY_CALLBACK => function ($value) {
 *                     $roleMembershipService = $this->applicationServicePluginManager->get(
 *                         RoleMembershipService::class
 *                     );
 *
 *                     return $roleMembershipService->getOneByUuid($value['uuid']);
 *                 },
 *             ],
 *         ]
 *     ];
 *
 *     parent::__construct(
 *         $applicationServicePluginManager,
 *         $infrastructureServicePluginManager,
 *         $hydrationStrategyPluginManager
 *     );
 * }
 * </code>
 */
class ClosureHydrationStrategy extends AbstractServicePluginManagerAwareHydrationStrategy
{
    public const OPTION_KEY_CALLBACK = 'callback';

    public function hydrate($value, ?array $data = null)
    {
        return $this->options[static::OPTION_KEY_CALLBACK]
            ? $this->options[static::OPTION_KEY_CALLBACK]($value, $data)
            : $value;
    }
}
