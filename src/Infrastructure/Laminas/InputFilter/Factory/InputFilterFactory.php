<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\InputFilter\Factory;

use Smtm\Base\Infrastructure\Laminas\InputFilter\VariableInput;
use Laminas\InputFilter\CollectionInputFilter;
use Laminas\InputFilter\Factory;
use Laminas\InputFilter\InputFilter;
use Laminas\InputFilter\InputFilterInterface;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\InputFilter\InputInterface;
use Laminas\Stdlib\ArrayUtils;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InputFilterFactory extends Factory
{
    public function createInputFilter($inputFilterSpecification)
    {
        if ($inputFilterSpecification instanceof InputFilterProviderInterface) {
            $inputFilterSpecification = $inputFilterSpecification->getInputFilterSpecification();
        }

        if ($inputFilterSpecification instanceof \Traversable) {
            $inputFilterSpecification = ArrayUtils::iteratorToArray($inputFilterSpecification);
        }

        /** @psalm-suppress DocblockTypeContradiction */
        if (! is_array($inputFilterSpecification)) {
            throw new \Laminas\InputFilter\Exception\InvalidArgumentException(sprintf(
                '%s expects an array or Traversable; received "%s"',
                __METHOD__,
                is_object($inputFilterSpecification)
                    ? $inputFilterSpecification::class
                    : gettype($inputFilterSpecification)
            ));
        }

        $type = InputFilter::class;

        if (isset($inputFilterSpecification['type']) && is_string($inputFilterSpecification['type'])) {
            $type = $inputFilterSpecification['type'];
            unset($inputFilterSpecification['type']);
        }

        $inputFilter = $this->getInputFilterManager()->get($type);

        if ($inputFilter instanceof CollectionInputFilter) {
            $inputFilter->setFactory($this);
            if (isset($inputFilterSpecification['input_filter'])) {
                $inputFilter->setInputFilter($inputFilterSpecification['input_filter']);
            }
            if (isset($inputFilterSpecification['count'])) {
                $inputFilter->setCount($inputFilterSpecification['count']);
            }
            if (isset($inputFilterSpecification['required'])) {
                $inputFilter->setIsRequired($inputFilterSpecification['required']);
            }
            if (isset($inputFilterSpecification['required_message'])) {
                $inputFilter->getNotEmptyValidator()->setMessage($inputFilterSpecification['required_message']);
            }
            return $inputFilter;
        }

        foreach ($inputFilterSpecification['input_collection'] ?? [] as $key => $value) {
            if (null === $value) {
                continue;
            }

            if (
                $value instanceof InputInterface
                || $value instanceof InputFilterInterface
            ) {
                $inputFilter->add($value, $key);
                continue;
            }

            // Patch to enable nested, integer indexed input_filter_specs.
            // Check type and name are in spec, and that composed type is
            // an input filter...
            if (
                (isset($value['type']) && is_string($value['type']))
                && (isset($value['name']) && is_string($value['name']))
                && $this->getInputFilterManager()->get($value['type']) instanceof InputFilter
            ) {
                // If $key is an integer, reset it to the specified name.
                if (is_int($key)) {
                    $key = $value['name'];
                }

                // Remove name from specification. InputFilter doesn't have a
                // name property!
                unset($value['name']);
            }

            $inputFilter->add($this->createInput($value), $key);
        }

        return $inputFilter;
    }

    public function createInput($inputSpecification)
    {
        $input = parent::createInput($inputSpecification);

        if ($input instanceof VariableInput) {
            foreach ($inputSpecification['inputs'] ?? [] as $index => $inputSpec) {
                $input->addInput(
                    $index,
                    $this->createInput(
                        array_replace(
                            $inputSpec,
                            [
                                'name' => $input->getName(),
                            ]
                        )
                    )
                );
            }
        }

        return $input;
    }
}
