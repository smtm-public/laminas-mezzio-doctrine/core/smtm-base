#!/usr/bin/env bash

set -evx

# Examples:
#
# With SSH:
# vendor/smtm/smtm-base/bin/db_import.sh "smtm-example-app-envname-db-client-container" "127.0.0.1" "root" "DB_NAME_SOURCE" "mariadb" "root" "DB_NAME_TARGET" "root" "123.123.123.123"
# bin/db_import.sh "smtm-example-app-envname-db-client-container" "127.0.0.1" "root" "DB_NAME_SOURCE" "mariadb" "root" "DB_NAME_TARGET" "root" "123.123.123.123"
#
# Without SSH:
# vendor/smtm/smtm-base/bin/db_import.sh "smtm-example-app-envname-db-client-container" "127.0.0.1" "root" "DB_NAME_SOURCE" "mariadb" "root" "DB_NAME_TARGET"
# bin/db_import.sh "smtm-example-app-envname-db-client-container" "127.0.0.1" "root" "DB_NAME_SOURCE" "mariadb" "root" "DB_NAME_TARGET"

DB_CONTAINER_NAME="$1"
DB_HOST_SOURCE="$2"
DB_USER_SOURCE="$3"
DB_NAME_SOURCE="$4"
DB_HOST_TARGET="$5"
DB_USER_TARGET="$6"
DB_NAME_TARGET="$7"
SSH_USER="$8"
SSH_HOST="$9"

DB_DUMP_FILE_NAME_COMPLETED_LOCALIZED=$(./db_backup "${DB_CONTAINER_NAME}" "${DB_HOST_SOURCE}" "${DB_USER_SOURCE}" "${DB_NAME_SOURCE}" "${SSH_USER}" "${SSH_HOST}")

mysql -h "${DB_HOST_TARGET}" --port "3306" -u "${DB_USER_TARGET}" -p -e "DROP DATABASE IF EXISTS "'`'"${DB_NAME_TARGET}"'`'
mysql -h "${DB_HOST_TARGET}" --port "3306" -u "${DB_USER_TARGET}" -p -e "CREATE DATABASE "'`'"${DB_NAME_TARGET}"'`'
mysql -h "${DB_HOST_TARGET}" --port "3306" -u "${DB_USER_TARGET}" -p "${DB_NAME_TARGET}" < "${DB_DUMP_FILE_NAME_COMPLETED_LOCALIZED}"
