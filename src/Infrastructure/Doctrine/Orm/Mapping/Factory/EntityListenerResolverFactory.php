<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm\Mapping\Factory;

use Smtm\Base\Infrastructure\Doctrine\Orm\Mapping\EntityListenerResolver;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EntityListenerResolverFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): EntityListenerResolver {
        return (new EntityListenerResolver())->setInfrastructureServicePluginManager(
            $container->get(InfrastructureServicePluginManager::class)
        );
    }
}
