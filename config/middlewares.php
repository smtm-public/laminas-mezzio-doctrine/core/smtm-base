<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Http\Mezzio\Router\Middleware\RouteMiddleware;
use Smtm\Base\Http\Middleware\BodyParamsMiddlware\BodyParamsMiddleware;
use Smtm\Base\Http\Middleware\ErrorHandler;
use Smtm\Base\Http\Middleware\Mezzio\Cors\CorsMiddleware;
use Smtm\Base\Http\Middleware\RequestValidatingInputFilterCollectionMiddleware;
use Smtm\Base\Http\Middleware\ServerParamsAndRequestIdMiddleware;
use Mezzio\Handler\NotFoundHandler;
use Mezzio\Helper\UrlHelperMiddleware;
use Mezzio\Router\Middleware\DispatchMiddleware;
use Mezzio\Router\Middleware\ImplicitHeadMiddleware;
use Mezzio\Router\Middleware\ImplicitOptionsMiddleware;

return [
    // Request attribute initializing middleware
    [
        'priority' => 0,
        'middleware' => ServerParamsAndRequestIdMiddleware::class,
    ],

    [
        'priority' => 100,
        'middleware' => ErrorHandler::class,
    ],

    // Pipe more middleware here that you want to execute on every request:
    // - bootstrapping
    // - pre-conditions
    // - modifications to outgoing responses
    //
    // Piped Middleware may be either callables or service names. Middleware may
    // also be passed as an array; each item in the array must resolve to
    // middleware eventually (i.e., callable or service name).
    //
    // Middleware can be attached to specific paths, allowing you to mix and match
    // applications under a common domain.  The handlers in each middleware
    // attached this way will see a URI with the matched path segment removed.
    //
    // i.e., path of "/api/member/profile" only passes "/member/profile" to $apiMiddleware
    // - $app->pipe('/api', $apiMiddleware);
    // - $app->pipe('/docs', $apiDocMiddleware);
    // - $app->pipe('/files', $filesMiddleware);

    // Register the routing middleware in the middleware pipeline.
    // This middleware registers the Mezzio\Router\RouteResult request attribute.
    [
        'priority' => 200,
        'middleware' => CorsMiddleware::class,
    ],

    [
        'priority' => 300,
        'middleware' => RouteMiddleware::class,
    ],

    [
        'priority' => 400,
        'middleware' => BodyParamsMiddleware::class,
    ],

    // The following handle routing failures for common conditions:
    // - HEAD request but no routes answer that method
    // - OPTIONS request but no routes answer that method
    // - method not allowed
    // Order here matters; the MethodNotAllowedMiddleware should be placed
    // after the Implicit*Middleware.
    [
        'priority' => 500,
        'middleware' => ImplicitHeadMiddleware::class,
    ],
    [
        'priority' => 600,
        'middleware' => ImplicitOptionsMiddleware::class,
    ],

    /**
     * Currently handled by the custom \Smtm\Base\Http\Mezzio\Router\Middleware\RouteMiddleware
     * [
     *     'priority' => 700,
     *     'middleware' => MethodNotAllowedMiddleware::class,
     * ],
     */

    // Seed the UrlHelper with the routing results:
    [
        'priority' => 800,
        'middleware' => UrlHelperMiddleware::class,
    ],

    // Add more middleware here that needs to introspect the routing results; this
    // might include:
    //
    // - route-based authentication
    // - route-based validation
    // - etc.
    [
        'priority' => 900,
        'middleware' => RequestValidatingInputFilterCollectionMiddleware::class,
    ],

    // Register the dispatch middleware in the middleware pipeline
    [
        'priority' => 1000,
        'middleware' => DispatchMiddleware::class,
    ],

    // At this point, if no Response is returned by any middleware, the
    // NotFoundHandler kicks in; alternately, you can provide other fallback
    // middleware to execute.
    [
        'priority' => 1100,
        'middleware' => NotFoundHandler::class,
    ],
];
