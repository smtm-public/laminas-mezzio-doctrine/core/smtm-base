<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DateIntervalExtractionStrategy extends AbstractExtractionStrategy
{
    public const OPTION_KEY_FORMAT = 'format';

    public function extract($value, ?object $object = null): ?string
    {
        if ($value instanceof \DateInterval) {
            return $value->format($this->options[static::OPTION_KEY_FORMAT]);
        }

        return $value;
    }
}
