<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ExtractionStrategyPluginManagerAwareInterface
{
    public function getExtractionStrategyPluginManager(): ExtractionStrategyPluginManager;
    public function setExtractionStrategyPluginManager(ExtractionStrategyPluginManager $extractionStrategyPluginManager): static;
}
