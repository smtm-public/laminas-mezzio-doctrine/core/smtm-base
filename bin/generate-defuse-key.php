#!/usr/bin/env php
<?php

declare(strict_types=1);

chdir(__DIR__ . '/../../../../');

$sopt = [];
$sopt[] = 'n';
$lopt = [];
$lopt[] = 'no-clobber';
$opts = getopt(implode($sopt), $lopt, $optInd);

$argumentsWithoutOptions = array_slice($argv, $optInd);

/**
 * Self-called anonymous function that creates its own scope and keep the global namespace clean.
 */
(function (string $destination, array $options) {
    if (file_exists($destination)
        && (array_key_exists('n', $options) || array_key_exists('no-clobber', $options))
    ) {
        echo 'The destination path "'
            . $destination . '" exists and the -n/--no-clobber option has been specified. Exiting.' . PHP_EOL;
        exit(0);
    }

    exec('generate-defuse-key > ' . $destination);

    exit(0);
})($argumentsWithoutOptions[0], $opts);
