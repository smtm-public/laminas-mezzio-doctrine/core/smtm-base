<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ArchivedByIpAddressAwareEntityInterface
{
    public function getArchivedByIpAddress(): ?string;
    public function setArchivedByIpAddress(?string $archivedByIpAddress): static;
}
