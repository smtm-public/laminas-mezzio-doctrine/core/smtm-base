<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception;

use RuntimeException;

/**
 * @author Ivan Chepanov <ivan.chepanov@smtm.bg>
 */
class RemoteServiceConnectionException extends RuntimeException
{

}
