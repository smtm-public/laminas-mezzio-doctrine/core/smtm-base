<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator;

use Laminas\Validator\AbstractValidator;

/**
 * Bidirectional (is|is not) validation for expected value type: instance of \DateTimeImmutable
 *
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IsDateTimeImmutable extends AbstractValidator
{
    public const INVALID_TRUE = 'invalidValueIs';
    public const INVALID_FALSE = 'invalidValueIsNot';

    protected array $messageTemplates = [
        self::INVALID_TRUE => 'Invalid value given. Instance of ' . \DateTimeImmutable::class . ' expected.',
        self::INVALID_FALSE => 'Invalid value given. Cannot be an instance of ' . \DateTimeImmutable::class . '.',
    ];

    protected array $options = [];

    /**
     * $options can have the following keys:
     * - is - true(default)|false - whether it should be of type \DateTime (true) or not (false)
     */
    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    public function isValid($value)
    {
        $is = (bool) ($this->options['is'] ?? true);
        $nullable = $this->options['nullable'] ?? true;

        if (($is && !$value instanceof \DateTimeImmutable) || (!$is && $value instanceof \DateTimeImmutable) || (!$nullable && $value === null)) {
            $this->error(constant('static::INVALID_' . strtoupper(var_export($is, true))));

            return false;
        }

        return true;
    }
}
