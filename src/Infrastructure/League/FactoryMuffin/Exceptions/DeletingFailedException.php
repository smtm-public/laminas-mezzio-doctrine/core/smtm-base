<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\League\FactoryMuffin\Exceptions;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DeletingFailedException extends \League\FactoryMuffin\Exceptions\DeletingFailedException implements \Stringable
{
    public function __toString(): string
    {
        return json_encode($this->getExceptions());
    }
}
