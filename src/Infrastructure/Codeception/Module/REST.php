<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Codeception\Module;

use Codeception\Exception\ModuleException;
use PHPUnit\Framework\Assert;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class REST extends \Codeception\Module\REST
{
    protected array $config = [
        'url' => '',
        'aws' => '',
        'prettyJsonResponse' => false,
    ];

    protected function execute($method, $url, $parameters = [], $files = [])
    {
        // allow full url to be requested
        if (!$url) {
            $url = $this->config['url'];
        } elseif (!is_string($url)) {
            throw new ModuleException(__CLASS__, 'URL must be string');
        } elseif (!str_contains($url, '://') && $this->config['url']) {
            $url = rtrim($this->config['url'], '/') . '/' . ltrim($url, '/');
        }

        $this->params = $parameters;

        $isQueryParamsAwareMethod = in_array($method, self::QUERY_PARAMS_AWARE_METHODS, true);

        if ($isQueryParamsAwareMethod) {
            if (!is_array($parameters)) {
                throw new ModuleException(__CLASS__, $method . ' parameters must be passed in array format');
            }
        } else {
            $parameters = $this->encodeApplicationJson($method, $parameters);
        }

        if (is_array($parameters) || $isQueryParamsAwareMethod) {
            if ($isQueryParamsAwareMethod) {
                if (!empty($parameters)) {
                    if (str_contains($url, '?')) {
                        $url .= '&';
                    } else {
                        $url .= '?';
                    }

                    $url .= http_build_query($parameters);
                }

                $this->debugSection("Request", sprintf('%s %s', $method, $url));
                $files = [];
            } else {
                $this->debugSection("Request",
                    sprintf('%s %s ', $method, $url) . json_encode($parameters, JSON_PRESERVE_ZERO_FRACTION | JSON_THROW_ON_ERROR)
                );
                $files = $this->formatFilesArray($files);
            }

            $this->response = $this->connectionModule->_request($method, $url, $parameters, $files);
        } else {
            $requestData = $parameters;
            if ($this->isBinaryData($requestData)) {
                $requestData = $this->binaryToDebugString($requestData);
            }

            $this->debugSection("Request", sprintf('%s %s ', $method, $url) . $requestData);
            $this->response = $this->connectionModule->_request($method, $url, [], $files, [], $parameters);
        }

        $printedResponse = $this->response;
        if ($this->isBinaryData((string)$printedResponse)) {
            $printedResponse = $this->binaryToDebugString($printedResponse);
        }

        $short = $this->_getConfig('shortDebugResponse');

        if (!is_null($short)) {
            $printedResponse = $this->shortenMessage($printedResponse, $short);
            $this->debugSection("Shortened Response", $printedResponse);
        } else {
            if ($this->getRunningClient()->getResponse()->getHeader('content-type') === 'application/json'
                && $this->config['prettyJsonResponse']
            ) {
                try {
                    $decodedResponse = json_decode($printedResponse, true, flags: JSON_THROW_ON_ERROR);
                    $this->debugSection("Response", json_encode($decodedResponse, JSON_PRETTY_PRINT, JSON_UNESCAPED_UNICODE));
                } catch (\JsonException $e) {
                    $this->debugSection("Response (INVALID JSON!)", $printedResponse);
                }
            } else {
                $this->debugSection("Response", $printedResponse);
            }
        }

        return $this->response;
    }

    private function setHeaderLink(array $linkEntries): void
    {
        $values = [];
        foreach ($linkEntries as $linkEntry) {
            Assert::assertArrayHasKey(
                'uri',
                $linkEntry,
                'linkEntry should contain property "uri"'
            );
            Assert::assertArrayHasKey(
                'link-param',
                $linkEntry,
                'linkEntry should contain property "link-param"'
            );
            $values[] = $linkEntry['uri'] . '; ' . $linkEntry['link-param'];
        }

        $this->haveHttpHeader('Link', implode(', ', $values));
    }

    private function formatFilesArray(array $files): array
    {
        foreach ($files as $name => $value) {
            if (is_string($value)) {
                $this->checkFileBeforeUpload($value);
                $files[$name] = [
                    'name' => basename($value),
                    'tmp_name' => $value,
                    'size' => filesize($value),
                    'type' => $this->getFileType($value),
                    'error' => 0,
                ];
                continue;
            }

            if (is_array($value)) {
                if (isset($value['tmp_name'])) {
                    $this->checkFileBeforeUpload($value['tmp_name']);
                    if (!isset($value['name'])) {
                        $value['name'] = basename($value['tmp_name']);
                    }

                    if (!isset($value['size'])) {
                        $value['size'] = filesize($value['tmp_name']);
                    }

                    if (!isset($value['type'])) {
                        $value['type'] = $this->getFileType($value['tmp_name']);
                    }

                    if (!isset($value['error'])) {
                        $value['error'] = 0;
                    }
                } else {
                    $files[$name] = $this->formatFilesArray($value);
                }
            } elseif (is_object($value)) {
                /**
                 * do nothing, probably the user knows what he is doing
                 * @issue https://github.com/Codeception/Codeception/issues/3298
                 */
            } else {
                throw new ModuleException(__CLASS__, sprintf('Invalid value of key %s in files array', $name));
            }
        }

        return $files;
    }

    private function getFileType($file): string
    {
        if (function_exists('mime_content_type') && mime_content_type($file)) {
            return mime_content_type($file);
        }

        return 'application/octet-stream';
    }

    private function checkFileBeforeUpload(string $file): void
    {
        if (!file_exists($file)) {
            throw new ModuleException(__CLASS__, sprintf('File %s does not exist', $file));
        }

        if (!is_readable($file)) {
            throw new ModuleException(__CLASS__, sprintf('File %s is not readable', $file));
        }

        if (!is_file($file)) {
            throw new ModuleException(__CLASS__, sprintf('File %s is not a regular file', $file));
        }
    }
}
