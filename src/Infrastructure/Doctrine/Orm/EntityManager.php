<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\Cache;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Exception\EntityManagerClosed;
use Doctrine\ORM\Exception\InvalidHydrationMode;
use Doctrine\ORM\Exception\MissingIdentifierField;
use Doctrine\ORM\Exception\MissingMappingDriverImplementation;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\Exception\UnrecognizedIdentifierFields;
use Doctrine\ORM\Internal\Hydration\AbstractHydrator;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadataFactory;
use Doctrine\ORM\NativeQuery;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Doctrine\ORM\Proxy\DefaultProxyClassNameResolver;
use Doctrine\ORM\Proxy\ProxyFactory;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\FilterCollection;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Repository\RepositoryFactory;
use Doctrine\ORM\TransactionRequiredException;
use Doctrine\ORM\UnitOfWork;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EntityManager extends \Doctrine\ORM\EntityManager implements EntityManagerInterface
{
    protected ClassMetadataFactory $metadataFactory;
    protected UnitOfWork $unitOfWork;
    protected EventManager $eventManager;
    protected ProxyFactory $proxyFactory;
    protected RepositoryFactory $repositoryFactory;
    protected Expr|null $expressionBuilder = null;
    protected bool $closed = false;
    private FilterCollection|null $filterCollection = null;
    protected Cache|null $cache = null;
    protected array $connections = [
        'writer' => null,
        'reader' => null,
    ];

    public function __construct(
        array $connections,
        protected Configuration $config,
        EventManager|null $eventManager = null,
    ) {
        $this->connections  = $connections;

        if (! $config->getMetadataDriverImpl()) {
            throw MissingMappingDriverImplementation::create();
        }

        $this->eventManager = $eventManager
            ?? (method_exists($connections['writer'], 'getEventManager')
                ? $connections['writer']->getEventManager()
                : new EventManager()
            );

        $metadataFactoryClassName = $config->getClassMetadataFactoryName();

        $this->metadataFactory = new $metadataFactoryClassName();
        $this->metadataFactory->setEntityManager($this);

        $this->configureMetadataCache();

        $this->repositoryFactory = $config->getRepositoryFactory();
        $this->unitOfWork        = new UnitOfWork($this);
        $this->proxyFactory      = new ProxyFactory(
            $this,
            $config->getProxyDir(),
            $config->getProxyNamespace(),
            $config->getAutoGenerateProxyClasses(),
        );

        if ($config->isSecondLevelCacheEnabled()) {
            $cacheConfig  = $config->getSecondLevelCacheConfiguration();
            $cacheFactory = $cacheConfig->getCacheFactory();
            $this->cache  = $cacheFactory->createCache($this);
        }

        //parent::__construct($connections['writer'], $config, $eventManager);
    }

    public static function create($connections, Configuration $config, ?EventManager $eventManager = null)
    {
        $eventManager = $eventManager ?: new EventManager();

        if (! $config->getMetadataDriverImpl()) {
            throw MissingMappingDriverImplementation::create();
        }

        return new static(
            [
                'reader' => $connections['reader'],
                'writer' => $connections['writer'],
            ],
            $config,
            $eventManager
        );
    }

    protected static function createConnection($connection, Configuration $config, ?EventManager $eventManager = null)
    {
        if (is_array($connection)) {
            return DriverManager::getConnection($connection, $config, $eventManager ?: new EventManager());
        }

        if (! $connection instanceof Connection) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Invalid $connection argument of type %s given%s.',
                    is_object($connection) ? get_class($connection) : gettype($connection),
                    is_object($connection) ? '' : ': "' . $connection . '"'
                )
            );
        }

        if ($eventManager !== null && $connection->getEventManager() !== $eventManager) {
            throw MismatchedEventManager::create();
        }

        return $connection;
    }

    public function getReaderConnection()
    {
        return $this->connections['reader'] ?? null;
    }

    public function getWriterConnection()
    {
        return $this->connections['writer'] ?? null;
    }

    public function getRepositoryFactory(): RepositoryFactory
    {
        return $this->repositoryFactory;
    }

    public function getClosed(): bool
    {
        return $this->closed;
    }

    public function getConnection(): Connection
    {
        return $this->connections['writer'];
    }

    public function getMetadataFactory(): ClassMetadataFactory
    {
        return $this->metadataFactory;
    }

    public function getExpressionBuilder(): Expr
    {
        return $this->expressionBuilder ??= new Expr();
    }

    public function beginTransaction(): void
    {
        $this->getConnection()->beginTransaction();
    }

    public function getCache(): Cache|null
    {
        return $this->cache;
    }

    public function wrapInTransaction(callable $func): mixed
    {
        $this->getConnection()->beginTransaction();

        try {
            $return = $func($this);

            $this->flush();
            $this->getConnection()->commit();

            return $return;
        } catch (\Throwable $e) {
            $this->close();
            $this->getConnection()->rollBack();

            throw $e;
        }
    }

    public function commit(): void
    {
        $this->getConnection()->commit();
    }

    public function rollback(): void
    {
        $this->getConnection()->rollBack();
    }

    /**
     * Returns the ORM metadata descriptor for a class.
     *
     * Internal note: Performance-sensitive method.
     *
     * {@inheritDoc}
     */
    public function getClassMetadata(string $className): \Doctrine\ORM\Mapping\ClassMetadata
    {
        return $this->metadataFactory->getMetadataFor($className);
    }

    public function createQuery(string $dql = ''): Query
    {
        $query = new Query($this);

        if (! empty($dql)) {
            $query->setDQL($dql);
        }

        return $query;
    }

    public function createNativeQuery(string $sql, ResultSetMapping $rsm): NativeQuery
    {
        $query = new NativeQuery($this);

        $query->setSQL($sql);
        $query->setResultSetMapping($rsm);

        return $query;
    }

    public function createQueryBuilder(): QueryBuilder
    {
        return new QueryBuilder($this);
    }

    /**
     * Flushes all changes to objects that have been queued up to now to the database.
     * This effectively synchronizes the in-memory state of managed objects with the
     * database.
     *
     * If an entity is explicitly passed to this method only this entity and
     * the cascade-persist semantics + scheduled inserts/removals are synchronized.
     *
     * @throws OptimisticLockException If a version check on an entity that
     * makes use of optimistic locking fails.
     * @throws ORMException
     */
    public function flush(): void
    {
        $this->errorIfClosed();
        $this->unitOfWork->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function find($className, mixed $id, LockMode|int|null $lockMode = null, int|null $lockVersion = null): object|null
    {
        $class = $this->metadataFactory->getMetadataFor(ltrim($className, '\\'));

        if ($lockMode !== null) {
            $this->checkLockRequirements($lockMode, $class);
        }

        if (! is_array($id)) {
            if ($class->isIdentifierComposite) {
                throw ORMInvalidArgumentException::invalidCompositeIdentifier();
            }

            $id = [$class->identifier[0] => $id];
        }

        foreach ($id as $i => $value) {
            if (is_object($value)) {
                $className = DefaultProxyClassNameResolver::getClass($value);
                if ($this->metadataFactory->hasMetadataFor($className)) {
                    $id[$i] = $this->unitOfWork->getSingleIdentifierValue($value);

                    if ($id[$i] === null) {
                        throw ORMInvalidArgumentException::invalidIdentifierBindingEntity($className);
                    }
                }
            }
        }

        $sortedId = [];

        foreach ($class->identifier as $identifier) {
            if (! isset($id[$identifier])) {
                throw MissingIdentifierField::fromFieldAndClass($identifier, $class->name);
            }

            if ($id[$identifier] instanceof \BackedEnum) {
                $sortedId[$identifier] = $id[$identifier]->value;
            } else {
                $sortedId[$identifier] = $id[$identifier];
            }

            unset($id[$identifier]);
        }

        if ($id) {
            throw UnrecognizedIdentifierFields::fromClassAndFieldNames($class->name, array_keys($id));
        }

        $unitOfWork = $this->getUnitOfWork();

        $entity = $unitOfWork->tryGetById($sortedId, $class->rootEntityName);

        // Check identity map first
        if ($entity !== false) {
            if (! ($entity instanceof $class->name)) {
                return null;
            }

            switch (true) {
                case $lockMode === LockMode::OPTIMISTIC:
                    $this->lock($entity, $lockMode, $lockVersion);
                    break;

                case $lockMode === LockMode::NONE:
                case $lockMode === LockMode::PESSIMISTIC_READ:
                case $lockMode === LockMode::PESSIMISTIC_WRITE:
                    $persister = $unitOfWork->getEntityPersister($class->name);
                    $persister->refresh($sortedId, $entity, $lockMode);
                    break;
            }

            return $entity; // Hit!
        }

        $persister = $unitOfWork->getEntityPersister($class->name);

        switch (true) {
            case $lockMode === LockMode::OPTIMISTIC:
                $entity = $persister->load($sortedId);

                if ($entity !== null) {
                    $unitOfWork->lock($entity, $lockMode, $lockVersion);
                }

                return $entity;

            case $lockMode === LockMode::PESSIMISTIC_READ:
            case $lockMode === LockMode::PESSIMISTIC_WRITE:
                return $persister->load($sortedId, null, null, [], $lockMode);

            default:
                return $persister->loadById($sortedId);
        }
    }

    public function getReference(string $entityName, mixed $id): object|null
    {
        $class = $this->metadataFactory->getMetadataFor(ltrim($entityName, '\\'));

        if (! is_array($id)) {
            $id = [$class->identifier[0] => $id];
        }

        $sortedId = [];

        foreach ($class->identifier as $identifier) {
            if (! isset($id[$identifier])) {
                throw MissingIdentifierField::fromFieldAndClass($identifier, $class->name);
            }

            $sortedId[$identifier] = $id[$identifier];
            unset($id[$identifier]);
        }

        if ($id) {
            throw UnrecognizedIdentifierFields::fromClassAndFieldNames($class->name, array_keys($id));
        }

        $entity = $this->unitOfWork->tryGetById($sortedId, $class->rootEntityName);

        // Check identity map first, if its already in there just return it.
        if ($entity !== false) {
            return $entity instanceof $class->name ? $entity : null;
        }

        if ($class->subClasses) {
            return $this->find($entityName, $sortedId);
        }

        $entity = $this->proxyFactory->getProxy($class->name, $sortedId);

        $this->unitOfWork->registerManaged($entity, $sortedId, []);

        return $entity;
    }

    /**
     * Clears the EntityManager. All entities that are currently managed
     * by this EntityManager become detached.
     */
    public function clear(): void
    {
        $this->unitOfWork->clear();
    }

    public function close(): void
    {
        //$this->clear();

        $this->closed = true;
    }

    public function isOpen(): bool
    {
        return ! $this->closed;
    }

    public function reopen(): static
    {
//        $oldUnitOfWork = $this->getUnitOfWork();
//        $this->unitOfWork = new UnitOfWork($this);
        $unitOfWorkPersistersProperty = new \ReflectionProperty($this->unitOfWork, 'persisters');
        $unitOfWorkPersistersProperty->setValue($this->unitOfWork, []);
        $this->closed = false;

        return $this;
    }

    /**
     * Tells the EntityManager to make an instance managed and persistent.
     *
     * The entity will be entered into the database at or before transaction
     * commit or as a result of the flush operation.
     *
     * NOTE: The persist operation always considers entities that are not yet known to
     * this EntityManager as NEW. Do not pass detached entities to the persist operation.
     *
     * @throws ORMInvalidArgumentException
     * @throws ORMException
     */
    public function persist(object $object): void
    {
        $this->errorIfClosed();

        $this->unitOfWork->persist($object);
    }

    /**
     * Removes an entity instance.
     *
     * A removed entity will be removed from the database at or before transaction commit
     * or as a result of the flush operation.
     *
     * @throws ORMInvalidArgumentException
     * @throws ORMException
     */
    public function remove(object $object): void
    {
        $this->errorIfClosed();

        $this->unitOfWork->remove($object);
    }

    public function refresh(object $object, LockMode|int|null $lockMode = null): void
    {
        $this->errorIfClosed();

        $this->unitOfWork->refresh($object, $lockMode);
    }

    /**
     * Detaches an entity from the EntityManager, causing a managed entity to
     * become detached.  Unflushed changes made to the entity if any
     * (including removal of the entity), will not be synchronized to the database.
     * Entities which previously referenced the detached entity will continue to
     * reference it.
     *
     * @throws ORMInvalidArgumentException
     */
    public function detach(object $object): void
    {
        $this->unitOfWork->detach($object);
    }

    public function lock(object $entity, LockMode|int $lockMode, \DateTimeInterface|int|null $lockVersion = null): void
    {
        $this->unitOfWork->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * Gets the repository for an entity class.
     *
     * @psalm-param class-string<T> $className
     *
     * @psalm-return EntityRepository<T>
     *
     * @template T of object
     */
    public function getRepository(string $className): EntityRepository
    {
        return $this->repositoryFactory->getRepository($this, $className);
    }

    /**
     * Determines whether an entity instance is managed in this EntityManager.
     *
     * @return bool TRUE if this EntityManager currently manages the given entity, FALSE otherwise.
     */
    public function contains(object $object): bool
    {
        return $this->unitOfWork->isScheduledForInsert($object)
            || $this->unitOfWork->isInIdentityMap($object)
            && ! $this->unitOfWork->isScheduledForDelete($object);
    }

    public function getEventManager(): EventManager
    {
        return $this->eventManager;
    }

    public function getConfiguration(): Configuration
    {
        return $this->config;
    }

    /**
     * Throws an exception if the EntityManager is closed or currently not active.
     *
     * @throws EntityManagerClosed If the EntityManager is closed.
     */
    private function errorIfClosed(): void
    {
        if ($this->closed) {
            throw EntityManagerClosed::create();
        }
    }

    public function getUnitOfWork(): UnitOfWork
    {
        return $this->unitOfWork;
    }

    public function newHydrator(string|int $hydrationMode): AbstractHydrator
    {
        return match ($hydrationMode) {
            Query::HYDRATE_OBJECT => new \Doctrine\ORM\Internal\Hydration\ObjectHydrator($this),
            Query::HYDRATE_ARRAY => new \Doctrine\ORM\Internal\Hydration\ArrayHydrator($this),
            Query::HYDRATE_SCALAR => new \Doctrine\ORM\Internal\Hydration\ScalarHydrator($this),
            Query::HYDRATE_SINGLE_SCALAR => new \Doctrine\ORM\Internal\Hydration\SingleScalarHydrator($this),
            Query::HYDRATE_SIMPLEOBJECT => new \Doctrine\ORM\Internal\Hydration\SimpleObjectHydrator($this),
            Query::HYDRATE_SCALAR_COLUMN => new \Doctrine\ORM\Internal\Hydration\ScalarColumnHydrator($this),
            default => $this->createCustomHydrator((string) $hydrationMode),
        };
    }

    public function getProxyFactory(): ProxyFactory
    {
        return $this->proxyFactory;
    }

    public function initializeObject(object $obj): void
    {
        $this->unitOfWork->initializeObject($obj);
    }

    /**
     * {@inheritDoc}
     */
    public function isUninitializedObject($obj): bool
    {
        return $this->unitOfWork->isUninitializedObject($obj);
    }

    public function getFilters(): FilterCollection
    {
        return $this->filterCollection ??= new FilterCollection($this);
    }

    public function isFiltersStateClean(): bool
    {
        return $this->filterCollection === null || $this->filterCollection->isClean();
    }

    public function hasFilters(): bool
    {
        return $this->filterCollection !== null;
    }

    /**
     * @psalm-param LockMode::* $lockMode
     *
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    private function checkLockRequirements(LockMode|int $lockMode, ClassMetadata $class): void
    {
        switch ($lockMode) {
            case LockMode::OPTIMISTIC:
                if (! $class->isVersioned) {
                    throw OptimisticLockException::notVersioned($class->name);
                }

                break;
            case LockMode::PESSIMISTIC_READ:
            case LockMode::PESSIMISTIC_WRITE:
                if (! $this->getConnection()->isTransactionActive()) {
                    throw TransactionRequiredException::transactionRequired();
                }
        }
    }

    private function configureMetadataCache(): void
    {
        $metadataCache = $this->config->getMetadataCache();
        if (! $metadataCache) {
            return;
        }

        $this->metadataFactory->setCache($metadataCache);
    }

    private function createCustomHydrator(string $hydrationMode): AbstractHydrator
    {
        $class = $this->config->getCustomHydrationMode($hydrationMode);

        if ($class !== null) {
            return new $class($this);
        }

        throw InvalidHydrationMode::fromMode($hydrationMode);
    }
}
