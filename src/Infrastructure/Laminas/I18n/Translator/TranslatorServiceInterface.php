<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\I18n\Translator;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\I18n\Translator\TranslatorAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface TranslatorServiceInterface
{
    public const TRANSLATOR_SERVICE_NAME_PREFIX = 'translator-';
}
