<?php

namespace Smtm\Base\Application\Strategy;

abstract class AbstractRemovalStrategy
{
    public const ENTITY_ID = 'entity_id';

    public abstract function updates(): array;
    public abstract function removalMap(): array;
}
