<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Laminas\Filter\AbstractFilter;
use Traversable;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TrimFilter extends AbstractFilter
{
    /**
     * @param string|array|Traversable $options (Optional) Options to set
     */
    /*
    public function __construct($options = null)
    {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }
    }
    */

    public function filter($value)
    {
        if (! is_string($value) && ! is_numeric($value)) {
            return $value;
        }

        return trim($value);
    }
}
