<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Exception;

use Smtm\Base\Infrastructure\Helper\HttpHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UnauthorizedException extends AbstractHttpException
{
    public const MESSAGE = 'Unauthorized';
    public const CODE = 0;
    public const STATUS = HttpHelper::STATUS_CODE_UNAUTHORIZED;
    public const TYPE = 'https://example.com/problems/unauthorized';

    protected int $status = self::STATUS;
    protected string $title = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_UNAUTHORIZED];
    protected string $detail = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_UNAUTHORIZED];
    protected string $type = self::TYPE;
}
