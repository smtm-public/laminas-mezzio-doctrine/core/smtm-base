<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Log;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait LoggerAwareTrait
{

    /* use LoggerAwareTrait; */

    protected \Psr\Log\LoggerInterface | \Laminas\Log\LoggerInterface | null $logger = null;

    public function getLogger(): \Psr\Log\LoggerInterface | \Laminas\Log\LoggerInterface | null
    {
        return $this->logger;
    }

    public function setLogger($logger): static
    {
        $this->logger = $logger;

        return $this;
    }
}
