<?php

declare(strict_types=1);

namespace Smtm\Base;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface OptionsAwareInterface
{
    public const OPTION_MERGE_STRATEGY_UNION = 'union';
    public const OPTION_MERGE_STRATEGY_MERGE = 'merge';
    public const OPTION_MERGE_STRATEGY_MERGE_RECURSIVE = 'merge-recursive';
    public const OPTION_MERGE_STRATEGY_MERGE_RECURSIVE_IF_NOT_NULL = 'merge-recursive-if-not-null';
    public const OPTION_MERGE_STRATEGY_REPLACE = 'replace';
    public const OPTION_MERGE_STRATEGY_REPLACE_RECURSIVE = 'replace-recursive';
    public const OPTION_MERGE_STRATEGY_REPLACE_RECURSIVE_IF_NOT_NULL = 'replace-recursive-if-not-null';

    public function getOptions(): array;
    public function setOption(string $key, mixed $value): static;
    public function setOptions(array $options): static;
}
