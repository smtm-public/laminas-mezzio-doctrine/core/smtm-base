<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../../.env.smtm.smtm-base.http.error-handling')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../../', '.env.smtm.smtm-base.http.error-handling'
    );
    $dotenv->load();
}

$loggers = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_HTTP_ERROR_HANDLING_LOGGERS'),
    true,
    flags: JSON_THROW_ON_ERROR
);

return [
    'debug' =>
        filter_var(
            EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_HTTP_ERROR_HANDLING_DEBUG'),
            FILTER_VALIDATE_BOOLEAN
        ),
    'loggers' => $loggers,
    'whoops' => [
        'phpErrorHandler' => [
            'register' => filter_var(
                EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_HTTP_ERROR_HANDLING_WHOOPS_PHP_ERROR_HANDLER_REGISTER'),
                FILTER_VALIDATE_BOOLEAN
            ),
        ],
        'errorResponseGenerator' => [
            'enableForInternalServerErrors' => filter_var(
                EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_HTTP_ERROR_HANDLING_WHOOPS_ERROR_RESPONSE_GENERATOR_ENABLE_FOR_INTERNAL_SERVER_ERRORS'),
                FILTER_VALIDATE_BOOLEAN
            ),
        ],
    ],
    'traceLevel' => (int) EnvHelper::getEnvFromProcessOrSuperGlobal(
        'SMTM_BASE_HTTP_ERROR_HANDLING_TRACE_LEVEL',
        30,
        EnvHelper::TYPE_INT
    ),
    'traceArgumentValueLength' => (int) EnvHelper::getEnvFromProcessOrSuperGlobal(
        'SMTM_BASE_HTTP_ERROR_HANDLING_TRACE_ARGUMENT_VALUE_LENGTH',
        65535,
        EnvHelper::TYPE_INT
    ),
    'traceArgumentArrayValueElementCount' =>
        (int) EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_BASE_HTTP_ERROR_HANDLING_TRACE_ARGUMENT_ARRAY_VALUE_ELEMENT_COUNT',
            10
        ),
    'errorMapping' => [
        'application' => [],
        'infrastructure' => [],
    ],
];
