#!/usr/bin/env bash

set -evx

# Examples:
#
# With SSH:
# vendor/smtm/smtm-base/bin/db_backup.sh "smtm-example-app-envname-db-client-container" "127.0.0.1" "root" "DB_NAME" "root" "123.123.123.123"
# bin/db_backup.sh "smtm-example-app-envname-db-client-container" "127.0.0.1" "root" "DB_NAME" "root" "123.123.123.123"
#
# Without SSH:
# vendor/smtm/smtm-base/bin/db_backup.sh "smtm-example-app-envname-db-client-container" "127.0.0.1" "root" "DB_NAME"
# bin/db_backup.sh "smtm-example-app-envname-db-client-container" "127.0.0.1" "root" "DB_NAME"

DB_CONTAINER_NAME="$1"
DB_HOST_SOURCE="$2"
DB_USER_SOURCE="$3"
DB_NAME_SOURCE="$4"
SSH_USER="$5"
SSH_HOST="$6"

echo "Enter MySQL password: ";
read -r -s DB_PASS_SOURCE;

export DB_DUMP_DATE_INITIATED="$(date +%Y-%m-%dT%H-%M-%S)"
DB_DUMP_FILE_NAME_INITIATED="${DB_CONTAINER_NAME}.${DB_NAME_SOURCE}.${DB_DUMP_DATE_INITIATED}"
if [[ -n ${SSH_HOST} ]]
then
    DB_DUMP_FILE_NAME_INITIATED="${SSH_HOST}.${DB_CONTAINER_NAME}.${DB_NAME_SOURCE}.${DB_DUMP_DATE_INITIATED}"
fi
DOCKER_EXEC_COMMAND="docker exec -it ${DB_CONTAINER_NAME} mysqldump --hex-blob --host=${DB_HOST_SOURCE} -u ${DB_USER_SOURCE} -p${DB_PASS_SOURCE} ${DB_NAME_SOURCE}"
COMMAND="${DOCKER_EXEC_COMMAND}"' > '"${DB_DUMP_FILE_NAME_INITIATED}"
if [[ -n ${SSH_HOST} ]]
then
    COMMAND='ssh -t "'"${SSH_USER}@${SSH_HOST}"'" "'"${DOCKER_EXEC_COMMAND}"'" > '"${DB_DUMP_FILE_NAME_INITIATED}"
fi
eval "${COMMAND}"
export DB_DUMP_DATE_COMPLETED="$(date +%Y-%m-%dT%H-%M-%S)"
DB_DUMP_FILE_NAME_COMPLETED="${DB_DUMP_FILE_NAME_INITIATED}.completed${DB_DUMP_DATE_COMPLETED}"
mv "${DB_DUMP_FILE_NAME_INITIATED}" "${DB_DUMP_FILE_NAME_COMPLETED}.sql"
sed -i -e s/utf8mb4_0900_ai_ci/utf8mb4_unicode_ci/ -e s/utf8mb4_general_ci/utf8mb4_unicode_ci/ "${DB_DUMP_FILE_NAME_COMPLETED}.sql"
DB_DUMP_FILE_NAME_COMPLETED_LOCALIZED="${DB_DUMP_FILE_NAME_COMPLETED}-localized"
mv "${DB_DUMP_FILE_NAME_COMPLETED}.sql" "${DB_DUMP_FILE_NAME_COMPLETED_LOCALIZED}.sql"

echo "${DB_DUMP_FILE_NAME_COMPLETED_LOCALIZED}.sql"
