<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Laminas\Validator\Factory\InputFilterFactoryAwareValidatorDelegator;
use Smtm\Base\Infrastructure\Laminas\Validator\Factory\PasswordValidatorFactory;
use Smtm\Base\Infrastructure\Laminas\Validator\Factory\ValidatorChainEitherFactory;
use Smtm\Base\Infrastructure\Laminas\Validator\Factory\ValidatorChainFactory;
use Smtm\Base\Infrastructure\Laminas\Validator\Factory\ValidatorPluginManagerAwareDelegator;
use Smtm\Base\Infrastructure\Laminas\Validator\FileExists;
use Smtm\Base\Infrastructure\Laminas\Validator\InArray;
use Smtm\Base\Infrastructure\Laminas\Validator\IsArray;
use Smtm\Base\Infrastructure\Laminas\Validator\IsBackedEnum;
use Smtm\Base\Infrastructure\Laminas\Validator\IsBoolean;
use Smtm\Base\Infrastructure\Laminas\Validator\IsDirectory;
use Smtm\Base\Infrastructure\Laminas\Validator\IsFile;
use Smtm\Base\Infrastructure\Laminas\Validator\IsFloat;
use Smtm\Base\Infrastructure\Laminas\Validator\IsInteger;
use Smtm\Base\Infrastructure\Laminas\Validator\IsNumeric;
use Smtm\Base\Infrastructure\Laminas\Validator\IsString;
use Smtm\Base\Infrastructure\Laminas\Validator\PasswordValidator;
use Smtm\Base\Infrastructure\Laminas\Validator\ValidatorChainEither;
use Laminas\ServiceManager\Factory\InvokableFactory;
use Laminas\Validator\ValidatorChain;

return [
    'factories' => [
        InArray::class => InvokableFactory::class,
        IsArray::class => InvokableFactory::class,
        IsBackedEnum::class => InvokableFactory::class,
        IsBoolean::class => InvokableFactory::class,
        IsFloat::class => InvokableFactory::class,
        IsInteger::class => InvokableFactory::class,
        IsNumeric::class => InvokableFactory::class,
        IsString::class => InvokableFactory::class,
        IsFile::class => InvokableFactory::class,
        IsDirectory::class => InvokableFactory::class,
        FileExists::class => InvokableFactory::class,
        ValidatorChain::class => ValidatorChainFactory::class,
        ValidatorChainEither::class => ValidatorChainEitherFactory::class,
        PasswordValidator::class => PasswordValidatorFactory::class,
    ],
    'delegators' => [
        IsArray::class => [
            ValidatorPluginManagerAwareDelegator::class,
            InputFilterFactoryAwareValidatorDelegator::class,
        ],
        IsString::class => [
            ValidatorPluginManagerAwareDelegator::class,
        ],
    ],
];
