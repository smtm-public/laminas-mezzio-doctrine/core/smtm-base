<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy\Factory;

use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ServicePluginManagerAwareHydrationStrategyFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get(ApplicationServicePluginManager::class),
            $container->get(InfrastructureServicePluginManager::class)
        );
    }
}
