<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy\Factory;

use Smtm\Base\Application\Hydrator\Strategy\Base64DecodeHydrationStrategy;
use Smtm\Base\Application\Hydrator\Strategy\Base64EncodeHydrationStrategy;
use Smtm\Base\Application\Hydrator\Strategy\ClosureHydrationStrategy;
use Smtm\Base\Application\Hydrator\Strategy\CollectionHydrationStrategy;
use Smtm\Base\Application\Hydrator\Strategy\DateTimeHydrationStrategy;
use Smtm\Base\Application\Hydrator\Strategy\DateTimeImmutableHydrationStrategy;
use Smtm\Base\Application\Hydrator\Strategy\DomainObjectHydrationStrategy;
use Smtm\Base\Application\Hydrator\Strategy\HydrationStrategyPluginManager;
use Smtm\Base\Application\Hydrator\Strategy\JsonDecodeHydrationStrategy;
use Smtm\Base\Application\Hydrator\Strategy\JsonEncodeHydrationStrategy;
use Smtm\Base\Application\Service\Factory\ApplicationServicePluginManagerAwareDelegator;
use Smtm\Base\Infrastructure\Service\Factory\InfrastructureServicePluginManagerAwareDelegator;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class HydratorStrategyPluginManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new HydrationStrategyPluginManager(
            $container,
            [
                'delegators' => [
                    CollectionHydrationStrategy::class => [
                        ApplicationServicePluginManagerAwareDelegator::class,
                        InfrastructureServicePluginManagerAwareDelegator::class,
                    ],
                    ClosureHydrationStrategy::class => [
                        ApplicationServicePluginManagerAwareDelegator::class,
                        InfrastructureServicePluginManagerAwareDelegator::class,
                    ],
                    DomainObjectHydrationStrategy::class => [
                        ApplicationServicePluginManagerAwareDelegator::class,
                        InfrastructureServicePluginManagerAwareDelegator::class,
                    ],
                ],
                'shared' => [
                    Base64DecodeHydrationStrategy::class => false,
                    Base64EncodeHydrationStrategy::class => false,
                    ClosureHydrationStrategy::class => false,
                    CollectionHydrationStrategy::class => false,
                    DateTimeHydrationStrategy::class => false,
                    DateTimeImmutableHydrationStrategy::class => false,
                    DomainObjectHydrationStrategy::class => false,
                    JsonEncodeHydrationStrategy::class => false,
                    JsonDecodeHydrationStrategy::class => false,
                ],
            ]
        );
    }
}
