<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface DomainObjectInterface
{
    public function __setProperty(string $name, mixed $value): static;
}
