<?php

declare(strict_types=1);

namespace Smtm\Base\Http\InputFilter;

use Smtm\Base\Infrastructure\Laminas\Filter\CastFilter;
use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterSpecificationInterface;
use Smtm\Base\Infrastructure\Laminas\Validator\IsInteger;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IdRouteParamRequestValidatingInputFilterCollection extends AbstractRequestValidatingInputFilterCollection
{
    protected array $pathParamsInputFilterSpecification = [
        InputFilterSpecificationInterface::INPUT_COLLECTION => [
            [
                'name' => 'id',
                'required' => true,
                'filters' => [
                    [
                        'name' => CastFilter::class,
                        'options' => [
                            CastFilter::OPTION_NAME_CAST_TO => CastFilter::OPTION_VALUE_CAST_TO_INT,
                        ]
                    ],
                ],
                'validators' => [
                    [
                        'name' => IsInteger::class,
                        'options' => [
                            IsInteger::OPTION_KEY_GT => 0,
                            IsInteger::OPTION_KEY_INCLUSIVE => false,
                        ]
                    ],
                ],
            ],
        ],
    ];
}
