<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-base.bcmath')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.smtm.smtm-base.bcmath');
    $dotenv->load();
}

$configBCMath = [
    'defaultScale' => (int) EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_BCMATH_DEFAULT_SCALE'),
];

bcscale($configBCMath['defaultScale']);

return $configBCMath;
