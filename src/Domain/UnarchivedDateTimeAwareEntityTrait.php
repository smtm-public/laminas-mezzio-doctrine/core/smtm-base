<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

use DateTime;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait UnarchivedDateTimeAwareEntityTrait
{
    protected ?DateTime $unarchived = null;

    public function getUnarchived(): ?DateTime
    {
        return $this->unarchived;
    }

    public function setUnarchived(?DateTime $unarchived): static
    {
        $this->unarchived = $unarchived;

        return $this;
    }
}
