<?php

declare(strict_types=1);

namespace Smtm\Base\Factory;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DoctrineConfigAwareDelegator extends ConfigAwareDelegator
{
    protected array|string|null $configKey = 'doctrine';
}
