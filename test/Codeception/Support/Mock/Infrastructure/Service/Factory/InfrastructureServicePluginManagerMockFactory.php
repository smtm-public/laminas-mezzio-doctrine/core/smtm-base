<?php

declare(strict_types=1);

namespace SmtmTest\Base\Codeception\Support\Mock\Infrastructure\Service\Factory;

use Smtm\Base\Infrastructure\Service\Factory\InfrastructureServicePluginManagerFactory;
use SmtmTest\Base\Codeception\Support\Mock\Infrastructure\Service\InfrastructureServicePluginManagerMock;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InfrastructureServicePluginManagerMockFactory extends InfrastructureServicePluginManagerFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $infrastructurePluginManager = parent::__invoke($container, $requestedName, $options);

        return new InfrastructureServicePluginManagerMock(
            null,
            [],
            $infrastructurePluginManager
        );
    }
}
