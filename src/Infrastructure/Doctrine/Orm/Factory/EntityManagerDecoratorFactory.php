<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm\Factory;

use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManager;
use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManagerDecorator;
use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManagerInterface;
use Smtm\Base\Infrastructure\Doctrine\Orm\Mapping\EntityListenerResolver;
use Smtm\Base\Infrastructure\Doctrine\Orm\Query\Filter\ExcludeArchivedFilter;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Doctrine\ORM\Cache\DefaultCacheFactory;
use Doctrine\ORM\Cache\RegionsConfiguration;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Doctrine\ORM\Proxy\ProxyFactory;
use Laminas\Cache\Psr\CacheItemPool\CacheItemPoolDecorator;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
final class EntityManagerDecoratorFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): EntityManagerInterface {
        if (!isset($options['connections'])) {
            throw new \RuntimeException('Missing doctrine connection config');
        }

        $options['connections']['reader'] ??= $options['connections']['writer'];

        /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

        $config = new Configuration();
        !isset($options['dbal']['middlewares']) || $config->setMiddlewares($options['dbal']['middlewares']);
        $filters = array_merge(
            $options['entityManager']['query']['filters'] ?? [],
            $options['orm']['query']['filters'] ?? []
        );

        foreach ($filters as $filterName => $filterClass) {
            $config->addFilter($filterName, $filterClass);
        }

        $config->setAutoGenerateProxyClasses(
            isset($entityManagerConfig['autoGenerateProxyClasses'])
                ? (int) $entityManagerConfig['autoGenerateProxyClasses']
                : ProxyFactory::AUTOGENERATE_EVAL
        );
        $config->setProxyDir(__DIR__ . '/../../../../../../../../data/cache');
        $config->setProxyNamespace('Doctrine\Entities');

        $config->setMetadataDriverImpl(
            new XmlDriver(
                array_merge(
                    $options['orm']['mapping']['paths'] ?? [],
                    $options['entityManager']['mapping']['paths'] ?? []
                )
            )
        );
//        $config->setMetadataDriverImpl(
//            new YamlDriver(
//                array_merge(
//                    $options['orm']['mapping']['paths'] ?? [],
//                    $options['entityManager']['mapping']['paths'] ?? []
//                )
//            )
//        );
        $config->setNamingStrategy(new UnderscoreNamingStrategy(CASE_LOWER, true));

        if (isset($options['orm']['metadata']['cache']) && $options['orm']['metadata']['cache'] !== '') {
            $config->setMetadataCache(
                new CacheItemPoolDecorator(
                    $infrastructureServicePluginManager->get($options['orm']['metadata']['cache'])
                )
            );
        }

        if (isset($options['orm']['query']['cache']) && $options['orm']['query']['cache'] !== '') {
            $config->setQueryCache(
                new CacheItemPoolDecorator(
                    $infrastructureServicePluginManager->get($options['orm']['query']['cache'])
                )
            );
        }

        if (isset($options['dbal']['result']['cache']) && $options['dbal']['result']['cache'] !== '') {
            $config->setResultCache(
                new CacheItemPoolDecorator(
                    $infrastructureServicePluginManager->get($options['dbal']['result']['cache'])
                )
            );
        }

        if (isset($options['secondLevelCache']) && $options['secondLevelCache'] !== '') {
            $factory = new DefaultCacheFactory(
                new RegionsConfiguration(),
                new CacheItemPoolDecorator(
                    $infrastructureServicePluginManager->get($options['secondLevelCache'])
                )
            );
            $config->setSecondLevelCacheEnabled(true);
            $config->getSecondLevelCacheConfiguration()->setCacheFactory($factory);
        }

//        if ($options['logging']['enabled'] ?? false) {
//            $logger = new DebugStack();
//            $logger->enabled = true;
//            $config->setSQLLogger($logger);
//        }

        /** @var EntityListenerResolver $entityListenerResolver */
        $entityListenerResolver = $infrastructureServicePluginManager->get(EntityListenerResolver::class);
        $config->setEntityListenerResolver($entityListenerResolver);

        /** @var EntityManagerDecorator $entityManager */
        $entityManager = EntityManager::create(
            $options['connections'],
            $config
        );

        if ($entityManager->getFilters()->has(ExcludeArchivedFilter::NAME)) {
            $entityManager->getFilters()->enable(ExcludeArchivedFilter::NAME);
        }

        $enabledFilters = array_merge(
            $options['entityManager']['query']['enabledFilters'] ?? [],
            $options['orm']['query']['enabledFilters'] ?? []
        );

        foreach ($enabledFilters as $enabledFilterName) {
            $entityManager->getFilters()->enable($enabledFilterName);
        }

        return new EntityManagerDecorator($entityManager);
    }
}
