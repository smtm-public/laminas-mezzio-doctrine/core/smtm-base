<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler\DbServiceEntity\UuidAware;

use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Http\Handler\DbServiceEntity\AbstractReadHandler as DbServiceEntityAbstractViewHandler;
use Smtm\Base\Http\Middleware\ServerParamsAndRequestIdMiddleware;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractReadHandler extends DbServiceEntityAbstractViewHandler
{
    /** @var AbstractDbService&UuidAwareEntityDbServiceInterface $applicationService */
    protected ?AbstractApplicationService $applicationService = null;

    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): JsonResponse
    {
        $parsedQueryParams = static::parseQueryParams($this->getMergedFilteredQueryParams($request));
        $entity = $this->applicationService->getOneByUuid(
            $this->getMergedFilteredAttributes($request)['uuid'] ?? '',
            array_replace(
                [ApplicationServiceInterface::OPTION_KEY_PARAMS => $parsedQueryParams],
                [ApplicationServiceInterface::OPTION_KEY_METHOD => $request->getMethod()],
                ServerParamsAndRequestIdMiddleware::getRequestParams($request)
            )
        );

        return $this->prepareResponse(
            $entity,
            static::parseQueryParams($this->getMergedFilteredQueryParams($request))
        );
    }
}
