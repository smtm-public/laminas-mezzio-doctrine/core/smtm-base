<?php

declare(strict_types=1);

namespace Smtm\Base;


/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    /**
     * By using a generator function the config provider is able to pass multiple arrays to the config aggregator
     * avoiding the need to handle the aggregation logic within the config provider itself.
     * See: https://docs.laminas.dev/laminas-config-aggregator/config-providers/#generators
     */
    public function __invoke(): \Generator
    {
        $config = [
            [
                'bcmath' => include __DIR__ . '/../config/bcmath.php',
                \Mezzio\Cors\Configuration\ConfigurationInterface::CONFIGURATION_IDENTIFIER =>
                    include __DIR__ . '/../config/mezzio/mezzio-cors/cors.php',
                'mezzio' => array_merge(
                    [
                        'mezzio-problem-details' =>
                            include __DIR__ . '/../config/mezzio/mezzio-problem-details/problem_details.php',
                    ],
                    include __DIR__ . '/../config/mezzio.php',
                ),
                'laminas' => [
                    'cache' => include __DIR__ . '/../config/laminas/laminas-cache/cache.php',
                    'diactoros' => include __DIR__ . '/../config/laminas/laminas-diactoros/diactoros.php',
                ],
                'whoops' => include __DIR__ . '/../config/whoops.php',
                'base' => [
                    'http' => [
                        'error_handling' => include __DIR__ . '/../config/http/error_handling.php',
                        'handler' => include __DIR__ . '/../config/http/handler.php',
                    ],
                    'infrastructure' => [
                        'crypto_service' => include __DIR__ . '/../config/infrastructure/crypto_service.php',
                        'jwt' => include __DIR__ . '/../config/infrastructure/jwt.php',
                        'remote_service_connector' =>
                            include __DIR__ . '/../config/infrastructure/remote_service_connector.php',
                        'webdriver' => include __DIR__ . '/../config/infrastructure/webdriver.php',
                        'application_cache' => include __DIR__ . '/../config/infrastructure/application_cache.php',
                    ],
                    'l10n' => include __DIR__ . '/../config/l10n.php',
                    'i18n' => include __DIR__ . '/../config/i18n.php',
                ],
                'commands' => include __DIR__ . '/../config/commands.php',
                'dependencies' => include __DIR__ . '/../config/dependencies.php',
                'doctrine' => include __DIR__ . '/../config/doctrine.php',
                'middleware_pipeline' => include __DIR__ . '/../config/middlewares.php',
                'filters' => include __DIR__ . '/../config/filters.php',
                'validators' => include __DIR__ . '/../config/validators.php',
                'routes' => include __DIR__ . '/../config/routes.php',
            ]
            + include __DIR__ . '/../config/laminas/laminas-log/log.php',
        ];

        foreach ($config as $configComponent) {
            yield $configComponent;
        }
    }
}
