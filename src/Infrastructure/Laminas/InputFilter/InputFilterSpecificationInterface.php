<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\InputFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface InputFilterSpecificationInterface
{
    public const TYPE = 'type';
    public const INPUT_COLLECTION = 'input_collection';
}
