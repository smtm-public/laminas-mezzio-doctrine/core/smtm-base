<?php

declare(strict_types=1);

namespace Smtm\Base\Command;

use Smtm\Base\Application\Extractor\ExtractorPluginManagerAwareInterface;
use Smtm\Base\Application\Extractor\ExtractorPluginManagerAwareTrait;
use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareTrait;
use Smtm\Base\Infrastructure\Helper\LetterCaseHelper;
use Smtm\Base\Infrastructure\Helper\ProcessHelper;
use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterFactoryAwareInterface;
use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterFactoryAwareTrait;
use Smtm\Base\Infrastructure\LaminasPsrLogger;
use Smtm\Base\Infrastructure\LaminasPsrLoggerInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServiceInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareTrait;
use Smtm\Base\Infrastructure\Service\Log\LoggerAwareInterface;
use Smtm\Base\Infrastructure\Service\Log\LoggerAwareTrait;
use Laminas\InputFilter\InputFilterAwareInterface;
use Laminas\InputFilter\InputFilterAwareTrait;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Log\Formatter\Simple;
use Laminas\Log\Writer\Stream;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Command extends \Symfony\Component\Console\Command\Command implements
    ApplicationServicePluginManagerAwareInterface,
    InfrastructureServicePluginManagerAwareInterface,
    ExtractorPluginManagerAwareInterface,
    LoggerAwareInterface,
    InputFilterAwareInterface,
    InputFilterProviderInterface,
    InputFilterFactoryAwareInterface
{

    use ApplicationServicePluginManagerAwareTrait,
        InfrastructureServicePluginManagerAwareTrait,
        ExtractorPluginManagerAwareTrait,
        LoggerAwareTrait,
        InputFilterAwareTrait,
        InputFilterFactoryAwareTrait;

    protected ?\Closure $deferredConstructor = null;
    protected array $initializerCallbacks = [];
    protected array $inputFilterSpecification = [];

    public function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        foreach ($this->initializerCallbacks as $callback) {
            $callback($this, $input, $output);
        }

        if (is_callable($this->deferredConstructor)) {
            $this->deferredConstructor->call($this);
        }

        $this->setLogger($this->infrastructureServicePluginManager->get('logger-console'));
        $this->setInputFilter(
            $this->getInputFilterFactory()->createInputFilter($this->getInputFilterSpecification())
        );
    }

    public function getInputFilterSpecification()
    {
        return $this->inputFilterSpecification;
    }

    public static function parseOptionValueAsInt(string | int | bool | null $intOrBool): int
    {
        if (is_null($intOrBool)) {
            return 0;
        }

        if (is_bool($intOrBool)) {
            return (int) $intOrBool;
        }

        if (is_int($intOrBool)) {
            return $intOrBool;
        }

        return (int) filter_var($intOrBool, FILTER_VALIDATE_BOOLEAN);
    }

    protected function validate(array $options, OutputInterface $output): bool
    {
        $this->inputFilter->setData($options);
        $valid = $this->inputFilter->isValid();

        if (!$valid) {
            $validationMessages = array_map(
                fn (string $inputName, array $inputMessages) =>
                    '    ' . $inputName . ': ' . "\n" . implode("\n", array_map(fn (string $name, string $message) => '        ' . $name . ': ' . $message, array_keys($inputMessages), $inputMessages)),
                array_keys($this->inputFilter->getMessages()),
                $this->inputFilter->getMessages()
            );
            $output->write(
                array_merge(['Validation error(s):'], $validationMessages),
                true,
                OutputInterface::OUTPUT_RAW
            );
        }

        return $valid;
    }

    protected function getCommandNameLastPartPascalCase(): string
    {
        $commandNameParts = explode(':', $this->getName());
        $commandNameLastPart = $commandNameParts[array_key_last($commandNameParts)];

        return LetterCaseHelper::format($commandNameLastPart, LetterCaseHelper::LETTER_CASE_PASCAL);
    }

    protected function prepareFileLogging(
        InputInterface $input,
        OutputInterface $output,
        string $logSubDir,
        string $logFileNamePrefix = '',
        string $logFileNameSuffix = '',
        ?array $options = null,
        \Psr\Log\LoggerInterface | \Laminas\Log\LoggerInterface | null $loggerFull = null,
        \Psr\Log\LoggerInterface | \Laminas\Log\LoggerInterface | null $loggerErrors = null
    ): array {
        $logFileNameCurrentDate = date('Y-m-d');
        $logFileNameCurrentTime = date('H-i-s');
        $logFileNameCurrentTimestamp = $logFileNameCurrentDate . 'T' . $logFileNameCurrentTime . 'Z';

        $logFileNameFull = $logFileNamePrefix . '_' . $logFileNameCurrentTimestamp . '_' . $logFileNameSuffix;

        $output->writeln('Writing full log to:   ' . getcwd() . '/data/logs/' . $logSubDir . '/' . $logFileNameFull);
        $return = [];

        if ($loggerFull instanceof LaminasPsrLogger) {
            $fileWriterFull = $loggerFull->writerPlugin(
                Stream::class,
                [
                    'stream' => 'data/logs/' . $logSubDir . '/' . $logFileNameFull,
                ]
            );
            $fileWriterFull->setFormatter(
                $fileWriterFull->formatterPlugin(
                    Simple::class,
                    $loggerFull->getFormatterConfig()
                )
            );
            $loggerFull->addWriter($fileWriterFull);
            $return['logFileName'] = $logFileNameFull;
        } else {
            $return['logFileName'] = null;
        }

        $logFileNameErrors = $logFileNamePrefix . '_ERRORS_' . $logFileNameCurrentTimestamp . '_' . $logFileNameSuffix;
        $output->writeln('Writing errors log to: ' . getcwd() . '/data/logs/' . $logSubDir . '/' . $logFileNameErrors);

        if ($loggerErrors instanceof LaminasPsrLogger) {
            $fileWriterErrors = $loggerErrors->writerPlugin(
                Stream::class,
                [
                    'stream' => 'data/logs/' . $logSubDir . '/' . $logFileNameErrors,
                ]
            );
            $fileWriterErrors->setFormatter(
                $fileWriterErrors->formatterPlugin(
                    Simple::class,
                    $loggerErrors->getFormatterConfig()
                )
            );
            $loggerErrors->addWriter($fileWriterErrors);
            $return['logFileNameErrors'] = $logFileNameErrors;
        } else {
            $return['logFileNameErrors'] = null;
        }

        $return['logSubDir'] = $logSubDir;
        $return['logFileNameCurrentDate'] = $logFileNameCurrentDate;

        return $return;
    }

    protected function getLogHeader(
        InputInterface $input
    ): array {
        return array_merge(
            [
                'php_uname(): ' . php_uname(),
                'exec(\'whoami\'): ' . exec('whoami'),
                'getmyuid(): ' . getmyuid(),
                'getmygid(): ' . getmygid(),
                'filemtime(__FILE__): ' . filemtime(__FILE__),
                'getmyinode(): ' . getmyinode(),
                'cli_get_process_title(): ' . cli_get_process_title(),
                'getmypid(): ' . getmypid(),
                'PHP ini file: ' . php_ini_loaded_file(),
                'Loaded extensions: ',
                get_loaded_extensions(),
                'Loaded Zend extensions: ',
                get_loaded_extensions(true),
                'App command name: ' . $this->getName(),
                'Script: ' . $_SERVER['argv'][array_key_first($_SERVER['argv'])],
                'Command arguments: ',
                $input->__toString(),
                'Command:',
                $_SERVER['argv'][array_key_first($_SERVER['argv'])] . ' ' . $input->__toString(),
                '$_SERVER:',
                '    \'PHP_VERSION\' => ' . $_SERVER['PHP_VERSION'] ?? '',
                '    \'HOSTNAME\' => ' . $_SERVER['HOSTNAME'] ?? '',
                '    \'PWD\' => ' . $_SERVER['PWD'] ?? '',
                '    \'PHP_SELF\' => ' . $_SERVER['PHP_SELF'] ?? '',
                '    \'SCRIPT_NAME\' => ' . $_SERVER['SCRIPT_NAME'] ?? '',
                '    \'SCRIPT_FILENAME\' => ' . $_SERVER['SCRIPT_FILENAME'] ?? '',
                '    \'PATH_TRANSLATED\' => ' . $_SERVER['PATH_TRANSLATED'] ?? '',
                '    \'REQUEST_TIME_FLOAT\' => ' . $_SERVER['REQUEST_TIME_FLOAT'] ?? '',
                '    \'REQUEST_TIME\' => ' . $_SERVER['REQUEST_TIME'] ?? '',
                '    \'argv\' => ',
                var_export($_SERVER['argv'], true),
                '    \'argc\' => ' . $_SERVER['argc'],
                'ini_get(\'memory_limit\'): ' . ini_get('memory_limit'),
            ],
            ProcessHelper::getStatsMemoryGetUsage(ProcessHelper::GET_MEMORY_STATS_RESULT_CONSOLE_LOGGER_HEADERS),
            ProcessHelper::getStatsMemoryGetUsageReal(ProcessHelper::GET_MEMORY_STATS_RESULT_CONSOLE_LOGGER_HEADERS),
            ProcessHelper::getStatsMemoryGetPeakUsage(ProcessHelper::GET_MEMORY_STATS_RESULT_CONSOLE_LOGGER_HEADERS),
            ProcessHelper::getStatsMemoryGetPeakUsageReal(ProcessHelper::GET_MEMORY_STATS_RESULT_CONSOLE_LOGGER_HEADERS),
        );
    }

    protected function getLogFooter(
        InputInterface $input
    ): array {
        return array_merge(
            ProcessHelper::getStatsMemoryGetUsage(ProcessHelper::GET_MEMORY_STATS_RESULT_CONSOLE_LOGGER_HEADERS),
            ProcessHelper::getStatsMemoryGetUsageReal(ProcessHelper::GET_MEMORY_STATS_RESULT_CONSOLE_LOGGER_HEADERS),
            ProcessHelper::getStatsMemoryGetPeakUsage(ProcessHelper::GET_MEMORY_STATS_RESULT_CONSOLE_LOGGER_HEADERS),
            ProcessHelper::getStatsMemoryGetPeakUsageReal(ProcessHelper::GET_MEMORY_STATS_RESULT_CONSOLE_LOGGER_HEADERS),
        );
    }

    protected function logHeader(
        InputInterface $input,
        OutputInterface $output,
        \Psr\Log\LoggerInterface | \Laminas\Log\LoggerInterface | null $loggerFull = null,
        \Psr\Log\LoggerInterface | \Laminas\Log\LoggerInterface | null $loggerErrors = null
    ): void {
        $logHeaders = $this->getLogHeader($input);

        if ($loggerFull !== null) {
            foreach ($logHeaders as $logHeader) {
                $loggerFull->info($logHeader);
            }
        } else {
            foreach ($logHeaders as $logHeader) {
                $output->writeln($logHeader);
            }
        }

        if ($loggerErrors !== null) {
            foreach ($logHeaders as $logHeader) {
                $loggerErrors->info($logHeader);
            }
        }
    }

    protected function logFooter(
        InputInterface $input,
        OutputInterface $output,
        \Psr\Log\LoggerInterface | \Laminas\Log\LoggerInterface | null $loggerFull = null,
        \Psr\Log\LoggerInterface | \Laminas\Log\LoggerInterface | null $loggerErrors = null
    ): void {
        $logHeaders = $this->getLogFooter($input);

        if ($loggerFull !== null) {
            foreach ($logHeaders as $logHeader) {
                $loggerFull->info($logHeader);
            }
        } else {
            foreach ($logHeaders as $logHeader) {
                $output->writeln($logHeader);
            }
        }

        if ($loggerErrors !== null) {
            foreach ($logHeaders as $logHeader) {
                $loggerErrors->info($logHeader);
            }
        }
    }

    /**
     * TODO: Implement a StorageServiceInterface for $storageService
     */
    protected function renameUploadAndDeleteLogFile(
        InputInterface $input,
        OutputInterface $output,
        array $logFileData,
        string $logFileNameIndex,
        string $endTimestamp,
        InfrastructureServiceInterface $storageService,
        array $config,
        ?array $options = null
    ): void {
        $logFileNameWithExtension = $logFileData[$logFileNameIndex] . '_completed' . $endTimestamp . '.log';
        $output->writeln('Renaming log file: ' . getcwd() . '/data/logs/' . $logFileData['logSubDir'] . '/' . $logFileData[$logFileNameIndex]);
        $output->writeln('               to: ' . getcwd() . '/data/logs/' . $logFileData['logSubDir'] . '/' . $logFileNameWithExtension);
        rename('data/logs/' . $logFileData['logSubDir'] . '/' . $logFileData[$logFileNameIndex],
            'data/logs/' . $logFileData['logSubDir'] . '/' . $logFileNameWithExtension);
        $output->writeln('                   Done!');

        if ($options && ($options['uploadLogFiles'] ?? false)) {
            $logFilesDirectory = $config['storage']['location'];
            $logFilesDirectory = rtrim($logFilesDirectory, '/') . '/';
            $key = $logFilesDirectory . $logFileData['logFileNameCurrentDate'] . '/' . $logFileNameWithExtension;
            $fhandle = fopen('data/logs/' . $logFileData['logSubDir'] . '/' . $logFileNameWithExtension, 'r');

            try {
                $output->writeln('Uploading log file: ' . getcwd() . '/data/logs/' . $logFileData['logSubDir'] . '/' . $logFileNameWithExtension);
                $output->writeln('          to cloud: ' . $key);
                $storageService->writeStream($key, $fhandle);
                $output->writeln('                             Done!');
            } finally {
                fclose($fhandle);
            }
        }

        if ($options && $options['unlinkLocalLogFiles'] ?? false) {
            $output->writeln('Unlinking log file: ' . getcwd() . '/data/logs/' . $logFileData['logSubDir'] . '/' . $logFileNameWithExtension);
            unlink('data/logs/' . $logFileData['logSubDir'] . '/' . $logFileNameWithExtension);
            $output->writeln('                    Done!');
        }
    }

    protected function deleteLogFile(
        InputInterface $input,
        OutputInterface $output,
        array $logFileData,
        string $logFileNameIndex,
        string $endTimestamp,
        array $config,
        ?array $options = null
    ): void {
        $logFileNameWithExtension = $logFileData[$logFileNameIndex] . '_completed' . $endTimestamp . '.log';
        $output->writeln('Renaming log file: ' . getcwd() . '/data/logs/' . $logFileData['logSubDir'] . '/' . $logFileData[$logFileNameIndex]);
        $output->writeln('               to: ' . getcwd() . '/data/logs/' . $logFileData['logSubDir'] . '/' . $logFileNameWithExtension);
        rename('data/logs/' . $logFileData['logSubDir'] . '/' . $logFileData[$logFileNameIndex],
            'data/logs/' . $logFileData['logSubDir'] . '/' . $logFileNameWithExtension);
        $output->writeln('                   Done!');

        if ($options['unlinkLocalLogFiles'] ?? false) {
            $output->writeln('Unlinking log file: ' . getcwd() . '/data/logs/' . $logFileData['logSubDir'] . '/' . $logFileNameWithExtension);
            unlink('data/logs/' . $logFileData['logSubDir'] . '/' . $logFileNameWithExtension);
            $output->writeln('                    Done!');
        }
    }

    public function getDeferredConstructor(): ?\Closure
    {
        return $this->deferredConstructor;
    }

    public function setDeferredConstructor(?\Closure $deferredConstructor): static
    {
        $this->deferredConstructor = $deferredConstructor;

        return $this;
    }

    public function getInitializerCallbacks(): array
    {
        return $this->initializerCallbacks;
    }

    public function setInitializerCallbacks(array $initializerCallbacks): static
    {
        $this->initializerCallbacks = $initializerCallbacks;

        return $this;
    }

    public function addInitializerCallback(callable $initializerCallback): static
    {
        $this->initializerCallbacks[] = $initializerCallback;

        return $this;
    }
}
