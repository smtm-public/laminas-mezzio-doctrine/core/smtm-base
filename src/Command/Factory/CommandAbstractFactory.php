<?php

declare(strict_types=1);

namespace Smtm\Base\Command\Factory;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Command\Command;
use Smtm\Base\Infrastructure\Laminas\InputFilter\Factory\InputFilterFactory;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CommandAbstractFactory implements AbstractFactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var Command $command */
        $command = new $requestedName(is_array($options) ? ($options['name'] ?? null) : null);
        $command->setApplicationServicePluginManager($container->get(ApplicationServicePluginManager::class));
        $command->setInfrastructureServicePluginManager($container->get(InfrastructureServicePluginManager::class));
        $command->setExtractorPluginManager($container->get(ExtractorPluginManager::class));
        $command->setInputFilterFactory($container->get(InputFilterFactory::class));

        return $command;
    }

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return
            is_subclass_of($requestedName, Command::class)
            && (
                !class_exists(\Smtm\Auth\Command\Command::class)
                || !is_subclass_of($requestedName, \Smtm\Auth\Command\Command::class)
            );
    }
}
