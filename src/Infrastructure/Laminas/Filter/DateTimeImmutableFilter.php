<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Laminas\Filter\AbstractFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DateTimeImmutableFilter extends AbstractFilter
{
    public const OPTION_NAME_FORMAT = 'format';

    protected ?string $format = null;

    public function __construct(protected $options = null)
    {
        $this->setOptions($options ?? []);
    }

    public function filter($value)
    {
        if ($value === null) {
            return null;
        }

        if ($this->format === null) {
            return new \DateTimeImmutable($value);
        }

        return \DateTimeImmutable::createFromFormat($this->format, $value);
    }

    public function getFormat(): string
    {
        return $this->format;
    }

    public function setFormat(string $format): static
    {
        $this->format = $format;

        return $this;
    }
}
