<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy;

use Laminas\ServiceManager\AbstractPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class HydrationStrategyPluginManager extends AbstractPluginManager
{
    protected $instanceOf = AbstractHydrationStrategy::class;
}
