<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Laminas\ServiceManager\AbstractPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExtractionStrategyPluginManager extends AbstractPluginManager
{
    protected $instanceOf = AbstractExtractionStrategy::class;
}
