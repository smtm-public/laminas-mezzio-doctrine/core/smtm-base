<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class JsonEncodeHydrationStrategy extends AbstractHydrationStrategy
{
    public const OPTION_KEY_FLAGS = 'flags';

    public function hydrate($value, ?array $data)
    {
        return json_encode($value, $this->options[static::OPTION_KEY_FLAGS] ?? JSON_UNESCAPED_UNICODE);
    }
}
