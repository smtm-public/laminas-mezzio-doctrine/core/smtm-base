<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../../.env.smtm.smtm-base.infrastructure.application-cache-service')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../../',
        '.env.smtm.smtm-base.infrastructure.application-cache-service'
    );
    $dotenv->load();
}

return [
    'storage' => [
        'adapter' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_INFRASTRUCTURE_APPLICATION_CACHE_DRIVER_CLASS')
    ]
];
