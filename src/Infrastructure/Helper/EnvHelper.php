<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EnvHelper
{
    public const TYPE_STRING = 'string';
    public const TYPE_FLOAT = 'float';
    public const TYPE_INT = 'int';

    public static function getEnvFromSuperGlobalOrProcess(
        $name,
        string|float|int|null $default = '',
        string $type = self::TYPE_STRING
    ): string|float|int|null {
        if (!is_array($name)) {
            $name = [$name];
        }

        $env = null;

        foreach ($name as $altName) {
            $env = $_ENV[$altName] ?? (getenv($altName) ?: null);

            if ($env !== null && $env !== false) {
                break;
            }
        }

        if ($env === false || $env === null) {
            $env = $default;
        }

        if ($env !== null || $default !== null) {
            $env = match ($type) {
                self::TYPE_STRING => (string) $env,
                self::TYPE_FLOAT => (float) $env,
                self::TYPE_INT => (int) $env,
            };
        }

        return $env;
    }

    public static function getEnvFromProcessOrSuperGlobal(
        $name,
        string|float|int|null $default = '',
        string $type = self::TYPE_STRING
    ): string|float|int|null {
        if (!is_array($name)) {
            $name = [$name];
        }

        $env = null;

        foreach ($name as $altName) {
            $env = (getenv($altName) ?: null) ?? $_ENV[$altName] ?? null;

            if ($env !== null && $env !== false) {
                break;
            }
        }

        if ($env === false || $env === null) {
            $env = $default;
        }

        if ($env !== null || $default !== null) {
            $env = match ($type) {
                self::TYPE_STRING => (string) $env,
                self::TYPE_FLOAT => (float) $env,
                self::TYPE_INT => (int) $env,
            };
        }

        return $env;
    }
}
