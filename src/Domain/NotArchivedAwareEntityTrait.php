<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait NotArchivedAwareEntityTrait
{
    protected ?bool $notArchived = true;

    public function getNotArchived(): ?bool
    {
        return $this->notArchived;
    }

    public function isArchived(): bool
    {
        return $this->notArchived === null;
    }

    public function setNotArchived(?bool $notArchived): static
    {
        $this->notArchived = $notArchived;

        return $this;
    }
}
