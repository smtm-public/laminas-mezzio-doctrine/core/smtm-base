<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RemoteServiceNotFoundResponseException extends RemoteServiceResponseException
{

}
