<?php

namespace Smtm\Base\Infrastructure\Doctrine\DBAL\Types;

use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\DateTimeType;
use function date_create;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DateTimeMicrosecondsType extends DateTimeType implements
    DateTimeMicrosecondsTypeInterface
{
    public const DATETIME_MICROSECONDS_MUTABLE     = 'datetime_microseconds';

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return static::DATETIME_MICROSECONDS_MUTABLE;
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return $value;
        }

        if ($value instanceof DateTimeInterface) {
            return $value->format(static::FORMAT);
        }

        throw ConversionException::conversionFailedInvalidType($value, $this->getName(), ['null', 'DateTime']);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value instanceof DateTimeInterface) {
            return $value;
        }

        $val = DateTime::createFromFormat(static::FORMAT, $value);

        if ($val === false) {
            $val = date_create($value);
        }

        if ($val === false) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                static::FORMAT
            );
        }

        return $val;
    }
}
