<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Factory;

use Smtm\Base\Infrastructure\Doctrine\DBAL\Logger\Factory\MiddlewareFactory;
use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManagerInterface;
use Smtm\Base\Infrastructure\Doctrine\Orm\Factory\EntityManagerDecoratorFactory;
use Smtm\Base\Infrastructure\Doctrine\Orm\Factory\EntityManagerFactory;
use Smtm\Base\Infrastructure\Doctrine\Orm\Mapping\EntityListenerResolver;
use Smtm\Base\Infrastructure\Doctrine\Orm\Mapping\Factory\EntityListenerResolverFactory;
use Smtm\Base\Infrastructure\Doctrine\Persistence\Factory\DoctrineManagerRegistryFactory;
use Smtm\Base\Infrastructure\Doctrine\Persistence\Factory\ManagerRegistryFactory;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Guzzle\Factory\ClientFactory;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;
use Smtm\Base\Infrastructure\Laminas\Cache\Storage\Adapter\Factory\RedisFactory;
use Smtm\Base\Infrastructure\Laminas\Diactoros\Factory\UriFactory;
use Smtm\Base\Infrastructure\Laminas\Diactoros\Uri;
use Smtm\Base\Infrastructure\Laminas\Log\ConsoleAndFileLogger;
use Smtm\Base\Infrastructure\Laminas\Log\Factory\LoggerFactory;
use Smtm\Base\Infrastructure\Laminas\Log\FileLogger;
use Smtm\Base\Infrastructure\Laminas\Log\Writer\Doctrine;
use Smtm\Base\Infrastructure\LaminasPsrLogger;
use Smtm\Base\Infrastructure\Psr\Log\Factory\LaminasPsrLoggerFactory;
use Smtm\Base\Infrastructure\Service\ApplicationCache\Adapter\Redis\Factory\RedisAdapterFactory;
use Smtm\Base\Infrastructure\Service\ApplicationCache\Adapter\Redis\RedisAdapter;
use Smtm\Base\Infrastructure\Service\ApplicationCacheService;
use Smtm\Base\Infrastructure\Service\CryptoService;
use Smtm\Base\Infrastructure\Service\GettextService;
use Smtm\Base\Infrastructure\Service\ImageProcessor;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Base\Infrastructure\Service\JwtService;
use Smtm\Base\Infrastructure\Service\SshConnector;
use Smtm\Base\Infrastructure\Symfony\Process\Process;
use Smtm\Base\Infrastructure\Symfony\Process\ProcessFactory;
use Smtm\Base\Infrastructure\WebDriver\Factory\WebDriverFactory;
use Smtm\Base\Infrastructure\WebDriver\WebDriver;
use Doctrine\DBAL\Logging\Middleware;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry as DoctrineManagerRegistryInterface;
use GuzzleHttp\ClientInterface;
use Laminas\I18n\Translator\Loader\Gettext;
use Laminas\I18n\Translator\Translator;
use Laminas\I18n\Translator\TranslatorInterface;
use Laminas\Log\Filter\Priority;
use Laminas\Log\Formatter\Db as DbFormatter;
use Laminas\Log\Logger;
use Laminas\Log\Writer\AbstractWriter;
use Laminas\Log\Writer\Stream;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\ServiceManager\Factory\InvokableFactory;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InfrastructureServicePluginManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $infrastructureServicePluginManager = new InfrastructureServicePluginManager(
            $container,
            [
                'aliases' => [
                    // required by the Doctrine2 Codeception module
                    EntityManager::class => EntityManagerInterface::class,
                ],
                'abstract_factories' => [
                    RemoteServiceConnectorAbstractFactory::class,
                    WebConnectorAbstractFactory::class,
                    InfrastructureServiceAbstractFactory::class,
                ],
                'factories' => [
                    SshConnector::class => SshConnectorFactory::class,
                    \Doctrine\ORM\EntityManagerInterface::class => EntityManagerFactory::class,
                    EntityManagerInterface::class => EntityManagerDecoratorFactory::class,
                    DoctrineManagerRegistryInterface::class => DoctrineManagerRegistryFactory::class,
                    ManagerRegistryInterface::class => ManagerRegistryFactory::class,
                    EntityListenerResolver::class => EntityListenerResolverFactory::class,
                    Middleware::class => MiddlewareFactory::class,
                    ClientInterface::class => ClientFactory::class,
                    CryptoService::class => CryptoServiceFactory::class,
                    JwtService::class => JwtServiceFactory::class,
                    ImageProcessor::class => ImageProcessorServiceFactory::class,
                    ApplicationCacheService::class => ApplicationCacheServiceFactory::class,
                    Uri::class => UriFactory::class,
                    TranslatorInterface::class => function (
                        ContainerInterface $container,
                        $name,
                        array $options = null
                    ) {
                        /** @var GettextService $gettextService */
                        $gettextService = $container->get(InfrastructureServicePluginManager::class)->get(GettextService::class);
                        $translationFiles = [];

                        if (is_dir($container->get('config')['base']['i18n']['translation']['baseDir']['po'])) {
                            $localeDirectoryIterator = new \DirectoryIterator($container->get('config')['base']['i18n']['translation']['baseDir']['po']);

                            /** @var \DirectoryIterator $localeDirInfo */
                            foreach ($localeDirectoryIterator as $localeDirInfo) {
                                if (!$localeDirInfo->isDir() || $localeDirInfo->isDot()) {
                                    continue;
                                }

                                $locale = $localeDirInfo->getFilename();
                                $poFileDirectoryIterator = new \DirectoryIterator($localeDirInfo->getPathname());

                                foreach ($poFileDirectoryIterator as $poFileInfo) {
                                    if (!$poFileInfo->isFile() || $poFileInfo->getExtension() !== 'po') {
                                        continue;
                                    }

                                    $fileName = $poFileInfo->getBasename('.po');
                                    $moFileName = $container->get('config')['base']['i18n']['translation']['baseDir']['mo'] . '/' . $locale . '/' . $fileName . '.mo';
                                    $poFileName = $poFileInfo->getPathname();

                                    $gettextService->generateMoFromPo(
                                        $moFileName,
                                        $poFileName,
                                        $container->get('config')['base']['i18n']['gettext']['generateMoOnlyIfNotExists']
                                    );

                                    $translationFiles[] = [
                                        'type' => Gettext::class,
                                        'filename' => $moFileName,
                                        'locale' => $locale,
                                        'text_domain' => $fileName,
                                    ];
                                }
                            }
                        }

                        return Translator::factory([
                            'locale' => [
                                $container->get('config')['base']['l10n']['locale']['id']['icu'],
                                $container->get('config')['base']['l10n']['localeFallback']['id']['icu'],
                            ],
                            'translation_files' => $translationFiles,
                        ]);
                    },
                    Logger::class => LoggerFactory::class,
                    LaminasPsrLogger::class => LaminasPsrLoggerFactory::class,
                    ConsoleAndFileLogger::class => LaminasPsrLoggerFactory::class,
                    Process::class => ProcessFactory::class,
                    RedisAdapter::class => RedisAdapterFactory::class,

                    WebDriver::class => WebDriverFactory::class,

                    'logger-console' => function (
                        ContainerInterface $container,
                        $name,
                        array $options = null
                    ) {
                        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

                        /** @var LaminasPsrLogger $logger */
                        $logger = null;

                        try {
                            /** @var LaminasPsrLogger $logger */
                            $logger = $infrastructureServicePluginManager->build(LaminasPsrLogger::class, $options);

                            /** @var AbstractWriter $writerStdout */
                            $writerStdout = $logger->writerPlugin(
                                Stream::class,
                                ['stream' => 'php://stdout']
                            );
                            $writerStdout->addFilter(
                                $writerStdout->filterPlugin(
                                    Priority::class,
                                    [
                                        'operator' => '>=',
                                        'priority' => Logger::INFO,
                                    ]
                                )
                            );
                            $logger->addWriter($writerStdout);

                            /** @var AbstractWriter $writerStderr */
                            $writerStderr = $logger->writerPlugin(
                                Stream::class,
                                ['stream' => 'php://stderr']
                            );
                            $writerStderr->addFilter(
                                $writerStderr->filterPlugin(
                                    Priority::class,
                                    [
                                        'operator' => '<=',
                                        'priority' => Logger::NOTICE,
                                    ]
                                )
                            );
                            $logger->addWriter($writerStderr);
                        } catch (\Throwable $t) {
                            echo $t;
                            // Skip throwing of exceptions to avoid userd login problems.
                        }

                        return $logger;
                    },

                    'logger-console-file-full' => function (
                        ContainerInterface $container,
                        $name,
                        array $options = null
                    ) {
                        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

                        try {
                            /** @var LaminasPsrLogger $logger */
                            $logger = $infrastructureServicePluginManager->get(ConsoleAndFileLogger::class, $options);
                        } catch (\Throwable $t) {
                            echo $t;
                            // Skip throwing of exceptions to avoid user login problems.
                        }

                        return $logger;
                    },

                    'logger-console-file-errors' => function (
                        ContainerInterface $container,
                        $name,
                        array $options = null
                    ) {
                        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

                        /** @var LaminasPsrLogger $logger */
                        $logger = null;

                        try {
                            $logger = $infrastructureServicePluginManager->get(
                                LaminasPsrLogger::class,
                                $options
                            /*
                            [
                                'writers' => [
                                    'echo' => [
                                        'name' => Stream::class,
                                        'options' =>[
                                            'stream' => 'php://stderr',
                                        ],
                                    ],
                                ],
                            ]
                            */
                            );
                        } catch (\Throwable $t) {
                            echo $t;
                            // Skip throwing of exceptions to avoid user login problems.
                        }

                        return $logger;
                    },

                    'logger-file-full' => function (
                        ContainerInterface $container,
                        $name,
                        array $options = null
                    ) {
                        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

                        try {
                            /** @var LaminasPsrLogger $logger */
                            $logger = $infrastructureServicePluginManager->get(FileLogger::class, $options);
                        } catch (\Throwable $t) {
                            echo $t;
                            // Skip throwing of exceptions to avoid user login problems.
                        }

                        return $logger;
                    },
                    'logger-file-errors' => function (
                        ContainerInterface $container,
                        $name,
                        array $options = null
                    ) {
                        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

                        /** @var LaminasPsrLogger $logger */
                        $logger = null;

                        try {
                            $logger = $infrastructureServicePluginManager->get(
                                LaminasPsrLogger::class,
                                $options
                            );
                        } catch (\Throwable $t) {
                            echo $t;
                            // Skip throwing of exceptions to avoid user login problems.
                        }

                        return $logger;
                    },

                    'logger-output' => function (
                        ContainerInterface $container,
                        $name,
                        array $options = null
                    ) {
                        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

                        /** @var LaminasPsrLogger $logger */
                        $logger = null;

                        try {
                            $logger = $infrastructureServicePluginManager->build(
                                LaminasPsrLogger::class,
                                array_replace(
                                    [
                                        'writers' => [
                                            'echo' => [
                                                'name' => Stream::class,
                                                'options' => [
                                                    'stream' => 'php://output',
                                                    'log_separator' => "\n\n",
                                                ],
                                            ],
                                        ],
                                    ],
                                    $options ?? []
                                )
                            );
                        } catch (\Throwable $t) {
                            echo $t;
                            // Skip throwing of exceptions to avoid userd login problems.
                        }

                        return $logger;
                    },

                    'logger-stdout' => function (
                        ContainerInterface $container,
                        $name,
                        array $options = null
                    ) {
                        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

                        /** @var LaminasPsrLogger $logger */
                        $logger = null;

                        try {
                            $logger = $infrastructureServicePluginManager->build(
                                LaminasPsrLogger::class,
                                array_replace(
                                    [
                                        'writers' => [
                                            'echo' => [
                                                'name' => Stream::class,
                                                'options' => [
                                                    'stream' => 'php://stdout',
                                                    'log_separator' => "\n\n",
                                                ],
                                            ],
                                        ],
                                    ],
                                    $options ?? []
                                )
                            );
                        } catch (\Throwable $t) {
                            echo $t;
                            // Skip throwing of exceptions to avoid userd login problems.
                        }

                        return $logger;
                    },

                    'logger-db-remote-service-connector' => function (
                        ContainerInterface $container,
                        $name,
                        array $options = null
                    ) {
                        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

                        /** @var LaminasPsrLogger $logger */
                        $logger = null;

                        try {
                            $logger = $infrastructureServicePluginManager->build(
                                LaminasPsrLogger::class,
                                array_replace(
                                    [
                                        'writers' => [
                                            [
                                                'name' => Doctrine::class,
                                                'options' => [
                                                    'entityManagerName' => 'default',
                                                    'tableName' => 'log_remote_service_connector',
                                                    'formatter' => [
                                                        'name' => DbFormatter::class,
                                                        'options' => [
                                                            'dateTimeFormat' => DateTimeHelper::FORMAT_SQL_DATETIME,
                                                        ],
                                                    ],
                                                    'columnMap' => [
                                                        'timestamp' => 'created',
                                                        'extra' => [
                                                            'script' => 'script',
                                                            'request_method' => 'request_method',
                                                            'request_headers' => 'request_headers',
                                                            'request_uri' => 'request_uri',
                                                            'request_body' => 'request_body',
                                                            'response_status_code' => 'response_status_code',
                                                            'response_headers' => 'response_headers',
                                                            'response_body' => 'response_body',
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                    $options ?? []
                                )
                            );
                        } catch (\Throwable) {
                            // Skip throwing of exceptions to avoid userd login problems.
                        }

                        return $logger;
                    }
                ],

                'shared' => [
                    Uri::class => false,
                ],
            ]
        );

        foreach ($container->get('config')['laminas']['diactoros']['uri']['services'] ?? [] as $laminasDiactorosUriServiceName => $laminasDiactorosUriServiceConfig) {
            $infrastructureServicePluginManager->setFactory(
                $laminasDiactorosUriServiceName,
                function (
                    ContainerInterface $container,
                    $requestedName,
                    ?array $options = null
                ) use ($laminasDiactorosUriServiceConfig) {
                    /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                    $infrastructureServicePluginManager = $container->get(
                        InfrastructureServicePluginManager::class
                    );

                    if ($options !== null) {
                        $laminasDiactorosUriServiceConfig = array_merge(
                            $laminasDiactorosUriServiceConfig,
                            $options
                        );
                    }

                    return $infrastructureServicePluginManager->build(
                        Uri::class,
                        $laminasDiactorosUriServiceConfig
                    );
                }
            );
            $infrastructureServicePluginManager->setShared($laminasDiactorosUriServiceName, false);
        }

        foreach ($container->get('config')['i18n']['translators'] ?? [] as $translatorName => $translatorConfig) {
            $infrastructureServicePluginManager->setFactory(
                $translatorName,
                function (
                    ContainerInterface $container,
                    $requestedName,
                    ?array $options = null
                ) use ($infrastructureServicePluginManager, $translatorConfig) {
                    if ($options !== null) {
                        $translatorConfig = array_replace_recursive(
                            $translatorConfig,
                            $options
                        );
                    }

                    return Translator::factory($translatorConfig);
                }
            );
        }

        if (class_exists(\Laminas\Cache\Storage\Adapter\Apcu::class)) {
            $infrastructureServicePluginManager->setFactory(
                \Laminas\Cache\Storage\Adapter\Apcu::class,
                InvokableFactory::class
            );
        }

        if (class_exists(\Laminas\Cache\Storage\Adapter\Redis::class)) {
            $infrastructureServicePluginManager->setFactory(
                \Laminas\Cache\Storage\Adapter\Redis::class,
                RedisFactory::class
            );
        }

        foreach ($container->get('config')['base']['infrastructure']['jwt']['services'] ?? [] as $jwtServiceName => $jwtServiceConfig) {
            $infrastructureServicePluginManager->setFactory(
                $jwtServiceName,
                function (
                    ContainerInterface $container,
                    $requestedName,
                    ?array $options = null
                ) use ($jwtServiceConfig) {
                    /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                    $infrastructureServicePluginManager = $container->get(
                        InfrastructureServicePluginManager::class
                    );

                    if ($options !== null) {
                        $jwtServiceConfig = array_replace_recursive(
                            $jwtServiceConfig,
                            $options
                        );
                    }

                    return $infrastructureServicePluginManager->build(
                        JwtService::class,
                        $jwtServiceConfig
                    );
                }
            );
        }

        foreach ($container->get('config')['base']['infrastructure']['webdriver']['services'] ?? [] as $webDriverServiceName => $webDriverServiceConfig) {
            $infrastructureServicePluginManager->setFactory(
                $webDriverServiceName,
                function (
                    ContainerInterface $container,
                    $requestedName,
                    ?array $options = null
                ) use ($webDriverServiceConfig) {
                    /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                    $infrastructureServicePluginManager = $container->get(
                        InfrastructureServicePluginManager::class
                    );

                    if ($options !== null) {
                        $webDriverServiceConfig = array_replace_recursive(
                            $webDriverServiceConfig,
                            $options
                        );
                    }

                    return $infrastructureServicePluginManager->build(
                        WebDriver::class,
                        $webDriverServiceConfig
                    );
                }
            );
        }

        return $infrastructureServicePluginManager;
    }
}
