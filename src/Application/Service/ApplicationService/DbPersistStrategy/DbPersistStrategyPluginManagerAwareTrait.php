<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\DbPersistStrategy\DbPersistStrategyPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait DbPersistStrategyPluginManagerAwareTrait
{
    protected DbPersistStrategyPluginManager $dbPersistStrategyPluginManager;

    public function getDbPersistStrategyPluginManager(): DbPersistStrategyPluginManager
    {
        return $this->dbPersistStrategyPluginManager;
    }

    public function setDbPersistStrategyPluginManager(DbPersistStrategyPluginManager $dbPersistStrategyPluginManager): static
    {
        $this->dbPersistStrategyPluginManager = $dbPersistStrategyPluginManager;

        return $this;
    }
}
