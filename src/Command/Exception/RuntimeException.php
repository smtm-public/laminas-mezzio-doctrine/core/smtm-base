<?php

declare(strict_types=1);

namespace Smtm\Base\Command\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RuntimeException extends \RuntimeException
{

}
