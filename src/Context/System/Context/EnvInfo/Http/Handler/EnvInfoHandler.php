<?php

declare(strict_types=1);

namespace Smtm\Base\Context\System\Context\EnvInfo\Http\Handler;

use Smtm\Base\Context\System\Context\EnvInfo\Application\Extractor\EnvInfoExtractor;
use Smtm\Base\Context\System\Context\EnvInfo\Application\Service\EnvInfoService;
use Smtm\Base\Http\Handler\AbstractHandler;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EnvInfoHandler extends AbstractHandler
{
    public ?string $domainObjectExtractorName = EnvInfoExtractor::class;
    public ?string $applicationServiceName = EnvInfoService::class;

    public function handle(ServerRequestInterface $request): JsonResponse
    {
        return $this->prepareResponse($this->applicationService->hydrateDomainObject([]));
    }
}
