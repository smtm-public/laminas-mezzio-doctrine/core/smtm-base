<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter\Factory;

use Smtm\Base\Infrastructure\Laminas\Filter\FilterPluginManagerAwareInterface;
use Laminas\Filter\FilterPluginManager;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class FilterPluginManagerAwareDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var FilterPluginManagerAwareInterface $filter */
        $filter = $callback();
        $filter->setFilterPluginManager($container->get(FilterPluginManager::class));

        return $filter;
    }
}
