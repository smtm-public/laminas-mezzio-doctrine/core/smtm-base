<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

use Smtm\Base\Infrastructure\Laminas\Diactoros\Uri;
use Smtm\Base\Infrastructure\WebDriver\WebDriver;
use Doctrine\DBAL\Driver\Middleware;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use GuzzleHttp\ClientInterface;
use Laminas\Cache\Storage\Adapter\AbstractAdapter;
use Laminas\I18n\Translator\TranslatorInterface;
use Laminas\ServiceManager\AbstractPluginManager;
use Laminas\ServiceManager\Exception\InvalidServiceException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InfrastructureServicePluginManager extends AbstractPluginManager
{
    public function validate($instance)
    {
        if ($instance instanceof InfrastructureServiceInterface
            // Doctrine
            || $instance instanceof ManagerRegistry
            || $instance instanceof EntityManagerInterface
            || $instance instanceof Middleware

            // Guzzle
            || $instance instanceof ClientInterface

            // laminas-cache/Storage
            || $instance instanceof AbstractAdapter

            // smtm-base->laminas-diactoros
            || $instance instanceof Uri

            // laminas-i18n
            || $instance instanceof TranslatorInterface

            // Log
            || $instance instanceof \Laminas\Log\LoggerInterface
            || $instance instanceof \Psr\Log\LoggerInterface

            || $instance instanceof WebDriver
        ) {
            return;
        }

        throw new InvalidServiceException(sprintf(
            'Plugin manager "%s" expected an instance of type '
            . '"%s" or '
            . '"%s" or '
            . '"%s" or '
            . '"%s" or '
            . '"%s" or '
            . '"%s" or '
            . '"%s" or '
            . '"%s" or '
            . '"%s" '
            . ', but "%s" was received',
            static::class,
            InfrastructureServiceInterface::class,
            ManagerRegistry::class,
            EntityManagerInterface::class,
            AbstractAdapter::class,
            Uri::class,
            \Laminas\Log\LoggerInterface::class,
            \Psr\Log\LoggerInterface::class,
            \Smtm\Base\Infrastructure\Service\Log\LoggerInterface::class,
            WebDriver::class,
            is_object($instance) ? get_class($instance) : gettype($instance)
        ));
    }
}
