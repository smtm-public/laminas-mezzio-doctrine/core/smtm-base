<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class JsonDecodeHydrationStrategy extends AbstractHydrationStrategy
{
    public const OPTION_KEY_ASSOCIATIVE = 'associative';

    public function hydrate($value, ?array $data)
    {
        return json_decode($value, $this->options[static::OPTION_KEY_ASSOCIATIVE] ?? false);
    }
}
