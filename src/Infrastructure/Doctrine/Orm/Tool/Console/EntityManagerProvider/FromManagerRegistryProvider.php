<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm\Tool\Console\EntityManagerProvider;

use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistry;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class FromManagerRegistryProvider implements
    \Doctrine\ORM\Tools\Console\EntityManagerProvider,
    \Doctrine\DBAL\Tools\Console\ConnectionProvider
{
    public function __construct(
        protected ManagerRegistry $managerRegistry
    ) {

    }

    public function getDefaultConnection(): Connection
    {
        return $this->managerRegistry->getConnection(ManagerRegistryInterface::DEFAULT_CONNECTION_NAME);
    }

    public function getConnection(string $name): Connection
    {
        return $this->managerRegistry->getConnection($name);
    }

    public function getDefaultManager(): EntityManagerInterface
    {
        return $this->managerRegistry->getManager(ManagerRegistryInterface::DEFAULT_MANAGER_NAME);
    }

    public function getManager(string $name): EntityManagerInterface
    {
        return $this->managerRegistry->getManager($name);
    }

    public function getManagerRegistry(): ManagerRegistry
    {
        return $this->managerRegistry;
    }
}
