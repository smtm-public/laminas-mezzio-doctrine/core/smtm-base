<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Log;

interface LoggerAwareInterface extends \Laminas\Log\LoggerAwareInterface, \Psr\Log\LoggerAwareInterface
{
    public function getLogger();
    public function setLogger($logger): static;
}
