<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\WebDriver\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Base\Infrastructure\WebDriver\WebDriverCollectionAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class WebDriverCollectionAwareDelegator implements DelegatorFactoryInterface
{

    protected array $webDriverCollection = [];

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var WebDriverCollectionAwareInterface $object */
        $object = $callback();

        foreach ($this->webDriverCollection as $webDriverName => $webDriver) {
            if ($webDriverName !== null && $webDriver !== '') {
                $object->addWebDriver(
                    $webDriverName,
                    $container->get(InfrastructureServicePluginManager::class)->get($webDriver)
                );
            }
        }

        return $object;
    }
}
