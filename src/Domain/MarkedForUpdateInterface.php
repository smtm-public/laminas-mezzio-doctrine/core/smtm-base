<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface MarkedForUpdateInterface extends DomainObjectInterface
{
    public function __getMarkedForUpdate(): bool;
    public function __setMarkedForUpdate(bool $markedForUpdate): static;
    public function __setProperty(string $name, mixed $value): static;
    public function __getChangeSet(): array;
}
