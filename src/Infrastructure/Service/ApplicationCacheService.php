<?php

namespace Smtm\Base\Infrastructure\Service;

use Smtm\Base\Domain\IdAwareEntityInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * @author a.dobrev@smtm.bg
 */
class ApplicationCacheService extends AbstractInfrastructureService implements CacheInterface
{
    private const CACHE_PREFIX = 'app_cache_';

    public function __construct(
        protected InfrastructureServicePluginManager $infrastructureServiceManager,
        private CacheInterface $adapter
    )
    {
    }

    public function get($key, $default = null)
    {
        return $this->adapter->get($this->prefixKey($key));
    }

    public function getMultiple($keys = array(), $default = null)
    {
        $keys = array_map([$this, 'prefixKey'], $keys);

        return $this->adapter->getMultiple($keys);
    }

    public function has($key): bool
    {
        return $this->adapter->has($this->prefixKey($key));
    }

    public function clear(): bool
    {
        return $this->adapter->clear();
    }

    public function delete($key): bool
    {
        return $this->adapter->delete($this->prefixKey($key));
    }


    public function deleteMultiple($keys): bool
    {
        $keys = array_map([$this, 'prefixKey'], $keys);

        return $this->adapter->deleteMultiple($keys);
    }

    public function set($key, $value, $ttl = null)
    {
        return $this->adapter->set($this->prefixKey($key), $value, $ttl);
    }

    public function setMultiple($values, $ttl = null)
    {
        foreach ($values as $key => $value) {
            $this->set($this->prefixKey($key), $value, $ttl);
        }
    }

    public function generateDomainObjectKey(
        IdAwareEntityInterface $entity,
        string $prefix = ''
    ): string
    {
        return sha1(get_class($entity) . '_' . $entity->getId() . $prefix);
    }

    private function prefixKey(string $key): string
    {
        return self::CACHE_PREFIX . $key;
    }
}
