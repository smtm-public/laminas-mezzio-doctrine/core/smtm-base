<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine;

use Composer\InstalledVersions;
use Smtm\Base\Infrastructure\Doctrine\Orm\Tool\Console\EntityManagerProvider\FromManagerRegistryProvider;
use Doctrine\DBAL\Tools\Console as DBALConsole;
use Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager;
use Doctrine\Migrations\Configuration\EntityManager\ManagerRegistryEntityManager;
use Doctrine\Migrations\Configuration\Migration\ConfigurationArray;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\ORM\Tools\Console\EntityManagerProvider;
use Doctrine\ORM\Tools\Console\EntityManagerProvider\ConnectionFromManagerProvider;
use Doctrine\ORM\Tools\Console\EntityManagerProvider\SingleManagerProvider;
use OutOfBoundsException;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command as SymfonyCommand;

use Symfony\Component\Console\Input\ArgvInput;
use function assert;
use function class_exists;

class ConsoleRunner
{
    /**
     * @param SymfonyCommand[] $commands
     */
    public static function run(
        EntityManagerProvider $entityManagerProvider,
        DependencyFactory|null $dependencyFactory = null,
        array $commands = []
    ): void {
        $input = new ArgvInput();
        $cli = self::createApplication(
            $input,
            $entityManagerProvider,
            $dependencyFactory,
            $commands
        );
        $cli->run($input);
    }

    /**
     * @param SymfonyCommand[] $commands
     *
     * @throws OutOfBoundsException
     */
    public static function createApplication(
        ArgvInput $input,
        EntityManagerProvider $entityManagerProvider,
        DependencyFactory|null $dependencyFactory = null,
        array $commands = [],
    ): Application {
        $version = InstalledVersions::getVersion('doctrine/orm');
        assert($version !== null);

        $version = InstalledVersions::getVersion('doctrine/migrations');
        assert($version !== null);

        $cli = new Application('Doctrine CLI/Migrations', $version);
        $cli->setCatchExceptions(true);

        self::addCommands(
            $cli,
            $input,
            $entityManagerProvider,
            $dependencyFactory
        );
        $cli->addCommands($commands);

        return $cli;
    }

    public static function addCommands(
        Application $cli,
        ArgvInput $input,
        EntityManagerProvider $entityManagerProvider,
        DependencyFactory|null $dependencyFactory = null
    ): void {
        $connectionProvider = new ConnectionFromManagerProvider($entityManagerProvider);

        if ($dependencyFactory === null) {
            if ($input->hasOption('cli-config')) {
                $dependencyFactory = require $input->getOption('cli-config');
            } else {
                if ($entityManagerProvider instanceof FromManagerRegistryProvider) {
                    $dependencyFactory = DependencyFactory::fromEntityManager(
                        new ConfigurationArray([]),
                        ManagerRegistryEntityManager::withSimpleDefault($entityManagerProvider->getManagerRegistry())
                    );
                } elseif ($entityManagerProvider instanceof SingleManagerProvider) {
                    $dependencyFactory = DependencyFactory::fromEntityManager(
                        new ConfigurationArray([]),
                        new ExistingEntityManager($entityManagerProvider->getDefaultManager())
                    );
                }
            }
        }

        $cli->addCommands(
            [
                // DBAL Commands
                new DBALConsole\Command\RunSqlCommand($connectionProvider),

                // ORM Commands
                new \Doctrine\ORM\Tools\Console\Command\ClearCache\CollectionRegionCommand($entityManagerProvider),
                new \Doctrine\ORM\Tools\Console\Command\ClearCache\EntityRegionCommand($entityManagerProvider),
                new \Doctrine\ORM\Tools\Console\Command\ClearCache\MetadataCommand($entityManagerProvider),
                new \Doctrine\ORM\Tools\Console\Command\ClearCache\QueryCommand($entityManagerProvider),
                new \Doctrine\ORM\Tools\Console\Command\ClearCache\QueryRegionCommand($entityManagerProvider),
                new \Doctrine\ORM\Tools\Console\Command\ClearCache\ResultCommand($entityManagerProvider),
                new \Doctrine\ORM\Tools\Console\Command\SchemaTool\CreateCommand($entityManagerProvider),
                new \Doctrine\ORM\Tools\Console\Command\SchemaTool\UpdateCommand($entityManagerProvider),
                new \Doctrine\ORM\Tools\Console\Command\SchemaTool\DropCommand($entityManagerProvider),
                new \Doctrine\ORM\Tools\Console\Command\GenerateProxiesCommand($entityManagerProvider),
                new \Doctrine\ORM\Tools\Console\Command\RunDqlCommand($entityManagerProvider),
                new \Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand($entityManagerProvider),
                new \Doctrine\ORM\Tools\Console\Command\InfoCommand($entityManagerProvider),
                new \Doctrine\ORM\Tools\Console\Command\MappingDescribeCommand($entityManagerProvider),

                // Migrations Commands
                new \Doctrine\Migrations\Tools\Console\Command\CurrentCommand($dependencyFactory),
                new \Doctrine\Migrations\Tools\Console\Command\DumpSchemaCommand($dependencyFactory),
                new \Doctrine\Migrations\Tools\Console\Command\ExecuteCommand($dependencyFactory),
                new \Doctrine\Migrations\Tools\Console\Command\GenerateCommand($dependencyFactory),
                new \Doctrine\Migrations\Tools\Console\Command\LatestCommand($dependencyFactory),
                new \Doctrine\Migrations\Tools\Console\Command\MigrateCommand($dependencyFactory),
                new \Doctrine\Migrations\Tools\Console\Command\RollupCommand($dependencyFactory),
                new \Doctrine\Migrations\Tools\Console\Command\StatusCommand($dependencyFactory),
                new \Doctrine\Migrations\Tools\Console\Command\VersionCommand($dependencyFactory),
                new \Doctrine\Migrations\Tools\Console\Command\UpToDateCommand($dependencyFactory),
                new \Doctrine\Migrations\Tools\Console\Command\SyncMetadataCommand($dependencyFactory),
                new \Doctrine\Migrations\Tools\Console\Command\ListCommand($dependencyFactory),
            ],
        );
    }
}
