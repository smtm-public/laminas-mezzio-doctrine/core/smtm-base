<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Exception;

use Smtm\Base\Http\Middleware\Mezzio\ProblemDetails\Exception\CommonProblemDetailsExceptionTrait;
use Mezzio\ProblemDetails\Exception\ProblemDetailsExceptionInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractHttpException extends RuntimeException implements ProblemDetailsExceptionInterface
{

    use CommonProblemDetailsExceptionTrait;

    protected const MESSAGE = 'General Exception';
    protected const CODE = 0;
    protected const STATUS = 500;
    protected const TYPE = 'example.com';
    protected const TITLE = 'Http Exception';

    public function __construct($message = null, $code = null, \Throwable $previous = null, $additional = [])
    {
        parent::__construct($message ?? static::MESSAGE, $code ?? static::CODE, $previous);

        $this->status = static::STATUS;
        $this->type   = self::TYPE;
        $this->title  = self::TITLE;
        $this->detail = '';
        $this->additional = $additional;
    }
}
