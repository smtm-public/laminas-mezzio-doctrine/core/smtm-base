<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DateTimeHydrationStrategy extends AbstractHydrationStrategy
{
    public const OPTION_KEY_FORMAT = 'format';

    public function hydrate($value, ?array $data)
    {
        if ($value === null) {
            return null;
        }

        return \DateTime::createFromFormat($this->options[static::OPTION_KEY_FORMAT], $value);
    }
}
