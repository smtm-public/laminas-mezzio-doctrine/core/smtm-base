<?php

declare(strict_types=1);

namespace Smtm\Base\Context\System\Context\EnvInfo\Application\Service;

use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Context\System\Context\EnvInfo\Application\Hydrator\EnvInfoHydrator;
use Smtm\Base\Context\System\Context\EnvInfo\Domain\EnvInfo;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EnvInfoService extends AbstractApplicationService
{
    protected ?string $domainObjectName = EnvInfo::class;
    protected ?string $hydratorName = EnvInfoHydrator::class;
}
