<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ArchivedByAwareEntityInterface
{
    public function getArchivedBy();
    public function setArchived($createdBy): static;
}
