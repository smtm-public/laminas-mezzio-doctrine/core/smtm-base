<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm\Factory;

use Smtm\Base\Infrastructure\Doctrine\Orm\Query\Filter\ExcludeArchivedFilter;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Doctrine\DBAL\Logging\DebugStack;
use Doctrine\ORM\Cache\DefaultCacheFactory;
use Doctrine\ORM\Cache\RegionsConfiguration;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Doctrine\ORM\Proxy\ProxyFactory;
use Laminas\Cache\Psr\CacheItemPool\CacheItemPoolDecorator;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
final class EntityManagerFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): EntityManagerInterface {
        if (!isset($options['connection'])) {
            throw new RuntimeException('Missing doctrine connection config');
        }

        $config = new Configuration();
        $filters = [ExcludeArchivedFilter::NAME => ExcludeArchivedFilter::class] + ($options['orm']['query']['filters'] ?? []);

        foreach ($filters as $filterName => $filterClass) {
            $config->addFilter($filterName, $filterClass);
        }

        $config->setAutoGenerateProxyClasses(
            isset($entityManagerConfig['autoGenerateProxyClasses'])
                ? (int) $entityManagerConfig['autoGenerateProxyClasses']
                : ProxyFactory::AUTOGENERATE_EVAL
        );
        $config->setProxyDir(__DIR__ . '/../../../../../../../../data/cache');
        $config->setProxyNamespace('Doctrine\Entities');

        $config->setMetadataDriverImpl(
            new XmlDriver($options['orm']['mapping']['paths'] ?? [])
        );
        $config->setNamingStrategy(new UnderscoreNamingStrategy(CASE_LOWER, true));

        if (isset($options['orm']['metadata']['cache']) && $options['orm']['metadata']['cache'] !== '') {
            $metadataCache = new CacheItemPoolDecorator(
                $container->get(InfrastructureServicePluginManager::class)->get($options['orm']['metadata']['cache'])
            );
            $config->setMetadataCache($metadataCache);
        }

        if (isset($options['orm']['query']['cache']) && $options['orm']['query']['cache'] !== '') {
            $config->setQueryCache(
                new CacheItemPoolDecorator(
                    $container->get(InfrastructureServicePluginManager::class)->get($options['orm']['query']['cache'])
                )
            );
        }

        if (isset($options['dbal']['result']['cache']) && $options['dbal']['result']['cache'] !== '') {
            $config->setResultCache(
                new CacheItemPoolDecorator(
                    $container->get(InfrastructureServicePluginManager::class)->get($options['dbal']['result']['cache'])
                )
            );
        }

        if (isset($options['secondLevelCache']) && $options['secondLevelCache'] !== '') {
            $factory = new DefaultCacheFactory(
                new RegionsConfiguration(),
                new CacheItemPoolDecorator(
                    $container->get(InfrastructureServicePluginManager::class)->get($options['secondLevelCache'])
                )
            );
            $config->setSecondLevelCacheEnabled(true);
            $config->getSecondLevelCacheConfiguration()->setCacheFactory($factory);
        }

        if ($options['logging']['enabled'] ?? false) {
            $logger = new DebugStack();
            $logger->enabled = true;
            $config->setSQLLogger($logger);
        }

        $entityManager = new EntityManager(
            $options['connection'],
            $config
        );

        $entityManager->getFilters()->enable(ExcludeArchivedFilter::NAME);

        return $entityManager;
    }
}
