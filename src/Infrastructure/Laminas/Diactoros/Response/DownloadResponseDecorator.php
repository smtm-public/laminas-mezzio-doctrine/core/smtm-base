<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Diactoros\Response;

use Laminas\Diactoros\Response;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DownloadResponseDecorator implements ResponseInterface
{
    protected Response $wrapped;

    public function __construct(
        string $contentType,
        string $fileName,
        mixed $body = 'php://temp',
        int $status = 200,
        array $headers = []
    ) {
        $this->wrapped = new Response(
            $body,
            $status,
            array_merge(
                [
                    'Content-Type' => $contentType,
                    'Content-Disposition' => 'attachment; filename="' . rawurlencode($fileName) . '"; ' .
                        'filename*=utf-8\'\'' . rawurlencode($fileName),
                    'Cache-Control' => 'max-age=0',
                    'Pragma' => 'public',
                    'Content-Transfer-Encoding' => 'Binary',
                    'Content-Description' => 'File Transfer'
                ],
                $headers
            )
        );
    }

    public function getBody(): StreamInterface
    {
        return $this->wrapped->getBody();
    }

    public function getHeader($name): array
    {
        return $this->wrapped->getHeader($name);
    }

    public function getHeaders(): array
    {
        return $this->wrapped->getHeaders();
    }

    public function getHeaderLine($name): string
    {
        return $this->wrapped->getHeaderLine($name);
    }

    public function getProtocolVersion(): string
    {
        return $this->wrapped->getProtocolVersion();
    }

    public function getReasonPhrase(): string
    {
        return $this->wrapped->getReasonPhrase();
    }

    public function getStatusCode(): int
    {
        return $this->wrapped->getStatusCode();
    }

    public function withProtocolVersion($version): MessageInterface
    {
        return $this->wrapped->withProtocolVersion($version);
    }

    public function hasHeader($name): bool
    {
        return $this->wrapped->hasHeader($name);
    }

    public function withHeader($name, $value): MessageInterface
    {
        return $this->wrapped->withHeader($name, $value);
    }

    public function withAddedHeader($name, $value): MessageInterface
    {
        return $this->wrapped->withAddedHeader($name, $value);
    }

    public function withoutHeader($name): MessageInterface
    {
        return $this->wrapped->withoutHeader($name);
    }

    public function withBody(StreamInterface $body): MessageInterface
    {
        return $this->wrapped->withBody($body);
    }

    public function withStatus($code, $reasonPhrase = ''): Response
    {
        return $this->wrapped->withStatus($code, $reasonPhrase);
    }
}
