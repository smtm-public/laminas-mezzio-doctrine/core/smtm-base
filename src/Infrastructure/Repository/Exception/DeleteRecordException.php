<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Repository\Exception;

use Smtm\Base\Infrastructure\Exception\RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DeleteRecordException extends RuntimeException
{

}
