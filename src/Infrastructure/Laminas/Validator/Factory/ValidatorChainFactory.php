<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\Validator\ValidatorChain;
use Laminas\Validator\ValidatorPluginManager;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ValidatorChainFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var ValidatorChain $validatorChain */
        $validatorChain = new $requestedName();
        /** @var ValidatorPluginManager $validatorPluginManager */
        $validatorPluginManager = $container->get(ValidatorPluginManager::class);

        foreach ($options['validators'] as $validatorDefinition) {
            $validator = $validatorPluginManager->get(
                $validatorDefinition['name'],
                $validatorDefinition['options'] ?? []
            );
            $validatorChain->attach(
                $validator,
                $validatorDefinition['breakChainOnFailure'] ?? false,
                $validatorDefinition['priority'] ?? ValidatorChain::DEFAULT_PRIORITY
            );
        }

        return $validatorChain;
    }
}
