<?php

declare(strict_types=1);

namespace Smtm\Base\Http\InputFilter;

use Smtm\Base\Infrastructure\Laminas\Validator\IsInteger;
use Laminas\InputFilter\Factory;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractIndexRequestValidatingInputFilterCollection extends AbstractRequestValidatingInputFilterCollection // phpcs:ignore
{
    protected array $indexInputs = [
        self::KEY_QUERY_PARAMS => [
            [
                'name' => 'page',
                'required' => false,
                'validators' => [
                    [
                        'name' => IsInteger::class,
                        'options' => [
                            'strict' => false,
                        ],
                    ],
                ],
            ],
            [
                'name' => 'pageSize',
                'required' => false,
                'validators' => [
                    [
                        'name' => IsInteger::class,
                        'options' => [
                            'strict' => false,
                        ],
                    ],
                ],
            ],
        ],
    ];

    public function __construct(Factory $inputFilterFactory)
    {
        parent::__construct($inputFilterFactory);

        foreach ($this->indexInputs[static::KEY_QUERY_PARAMS] as $inputSpecification) {
            $this->queryParamsInputFilter->add($this->inputFilterFactory->createInput($inputSpecification));
        }
    }
}
