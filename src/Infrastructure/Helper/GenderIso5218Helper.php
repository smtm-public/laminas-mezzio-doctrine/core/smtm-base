<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class GenderIso5218Helper
{
    public const MALE = 1;
    public const FEMALE = 2;
    public const NOT_SPECIFIED = 3;
    public const GENDER_NAME = [
        self::MALE => 'male',
        self::FEMALE => 'female',
        self::NOT_SPECIFIED => 'not specified',
    ];
}
