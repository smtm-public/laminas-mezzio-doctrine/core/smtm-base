<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Factory\ConfigAwareDelegator;
use Smtm\Base\Infrastructure\Laminas\Filter\Base64DecodeFilter;
use Smtm\Base\Infrastructure\Laminas\Filter\Base64EncodeFilter;
use Smtm\Base\Infrastructure\Laminas\Filter\CastFilter;
use Smtm\Base\Infrastructure\Laminas\Filter\CollectionFilter;
use Smtm\Base\Infrastructure\Laminas\Filter\DateIntervalFilter;
use Smtm\Base\Infrastructure\Laminas\Filter\DateTimeFilter;
use Smtm\Base\Infrastructure\Laminas\Filter\DateTimeImmutableFilter;
use Smtm\Base\Infrastructure\Laminas\Filter\Factory\FilterPluginManagerAwareDelegator;
use Smtm\Base\Infrastructure\Laminas\Filter\Factory\OptionsAwareFilterFactory;
use Smtm\Base\Infrastructure\Laminas\Filter\StringToArrayFilter;
use Smtm\Base\Infrastructure\Laminas\Filter\TrimFilter;
use Laminas\ServiceManager\Factory\InvokableFactory;

return [
    'factories' => [
        CollectionFilter::class => InvokableFactory::class,
        CastFilter::class => InvokableFactory::class,
        Base64DecodeFilter::class => InvokableFactory::class,
        Base64EncodeFilter::class => InvokableFactory::class,
        DateTimeFilter::class => OptionsAwareFilterFactory::class,
        DateTimeImmutableFilter::class => OptionsAwareFilterFactory::class,
        DateIntervalFilter::class => InvokableFactory::class,
        TrimFilter::class => InvokableFactory::class,
        StringToArrayFilter::class => InvokableFactory::class,
    ],

    'delegators' => [
        CollectionFilter::class => [
            FilterPluginManagerAwareDelegator::class,
        ],
        DateTimeFilter::class => [
            (new ConfigAwareDelegator())->setConfigKey(['base', 'l10n'])
        ],
    ],
];
