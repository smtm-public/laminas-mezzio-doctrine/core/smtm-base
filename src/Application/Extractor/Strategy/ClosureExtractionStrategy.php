<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ClosureExtractionStrategy extends AbstractServiceAndExtractorPluginManagersAwareExtractionStrategy
{
    public const OPTION_KEY_CALLBACK = 'callback';

    public function extract($value, ?object $object = null): object|array|string|float|int|bool|null
    {
        /** @var AbstractDomainObjectExtractor|null $extractor */
        $extractor = null;

        if (isset($this->options[static::OPTION_KEY_EXTRACTOR_NAME])) {
            $extractor = $this->extractorPluginManager->get(
                $this->options[static::OPTION_KEY_EXTRACTOR_NAME],
                $this->options[static::OPTION_KEY_EXTRACTOR_OPTIONS] ?? []
            );
        }

        if (!isset($this->options[static::OPTION_KEY_CALLBACK])) {
            return $extractor ? $extractor->extract($value) : $value;
        } else {
            return $extractor
                ? $extractor->extract($this->options[static::OPTION_KEY_CALLBACK]($value, $object))
                : $this->options[static::OPTION_KEY_CALLBACK]($value, $object) ;
        }
    }
}
