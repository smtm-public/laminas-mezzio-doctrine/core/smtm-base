<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface PspellDictionaryCollectionAwareInterface
{
    public function getPspellDictionaryCollection(): array;
    public function setPspellDictionaryCollection(array $pspellDictionaryCollection): static;
    public function addPspellDictionary(string $name, \Pspell\Dictionary $pspellDictionary): static;
    public function getPspellDictionaryByName(string $name): ?\Pspell\Dictionary;
    public function removePspellDictionary(string $name): static;
}
