<?php

declare(strict_types=1);

namespace Smtm\Base\Command\Factory;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Command\Command;
use Smtm\Base\Infrastructure\Laminas\InputFilter\Factory\InputFilterFactory;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CommandFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $name = null;
        $args = [];

        if (is_array($options)) {
            $name = $options['name'] ?? null;
            $args = $options['args'] ?? [];
        }

        /** @var Command $command */
        $command = new $requestedName($name, ...$args);
        $command->setApplicationServicePluginManager($container->get(ApplicationServicePluginManager::class));
        $command->setInfrastructureServicePluginManager($container->get(InfrastructureServicePluginManager::class));
        $command->setExtractorPluginManager($container->get(ExtractorPluginManager::class));
        $command->setInputFilterFactory($container->get(InputFilterFactory::class));

        return $command;
    }
}
