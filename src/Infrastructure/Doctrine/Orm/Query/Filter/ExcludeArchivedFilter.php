<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm\Query\Filter;

use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Infrastructure\Helper\OopHelper;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * Handles the automatic WHERE clause addition of entity_table.archived = 0 for all entities which use
 * the ArchivedAndArchivedAtAwareEntityTrait and ON clause addition of the same condition for all
 * joined tables/related entities
 */
class ExcludeArchivedFilter extends SQLFilter
{
    public const NAME = 'excludeSmtmBaseArchivableEntityInterfaceArchived';

    /**
     * @inheritDoc
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, string $targetTableAlias): string
    {
        if (OopHelper::classImplements($targetEntity->getReflectionClass()->name, NotArchivedAwareEntityInterface::class)) {
            return sprintf(
                '%s.not_archived = 1',
                $targetTableAlias
            );
        }

        return '';
    }
}
