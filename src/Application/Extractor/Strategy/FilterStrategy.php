<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Laminas\Filter\AbstractFilter;
use Laminas\Filter\FilterPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class FilterStrategy extends AbstractExtractionStrategy
{
    public const OPTION_KEY_FILTER_NAME = 'filterName';

    protected FilterPluginManager $filterManager;

    /**
     * @param FilterPluginManager $filterManager
     */
    public function __construct(FilterPluginManager $filterManager)
    {
        $this->filterManager = $filterManager;
    }

    public function extract($value, ?object $object = null)
    {
        /** @var AbstractFilter $filter */
        $filter = $this->filterManager->get($this->options[static::OPTION_KEY_FILTER_NAME]);
        $filter->setOptions($this->getOptions()['filterOptions'] ?? []);

        return $filter->filter($value);
    }
}
