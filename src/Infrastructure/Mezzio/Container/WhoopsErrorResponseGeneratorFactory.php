<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Mezzio\Container;

use Smtm\Base\Infrastructure\Mezzio\Whoops\WhoopsDummyRunner;
use Mezzio\Middleware\WhoopsErrorResponseGenerator;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class WhoopsErrorResponseGeneratorFactory
{
    public function __invoke(ContainerInterface $container) : WhoopsErrorResponseGenerator
    {
        $runner =
            (
                $container->get('config')['base']['http']['error_handling']['whoops']['phpErrorHandler']['register']
                    ?? false
            )
                ? $container->get('Mezzio\Whoops')
                : $container->get(WhoopsDummyRunner::class);

        return new WhoopsErrorResponseGenerator($runner);
    }
}
