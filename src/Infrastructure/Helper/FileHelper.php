<?php

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class FileHelper
{
    public static function isImage(string $filename): bool
    {
        $supported_image = array('gif', 'jpg', 'jpeg', 'png');
        $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

        return (in_array($ext, $supported_image)) ? true : false;
    }

    public static function sanitizeFileName(string $fileName): string
    {
        return preg_replace('#\W#', '_', $fileName);
    }
}
