<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;
use Smtm\Base\Infrastructure\Collection\CollectionInterface;
use Laminas\Hydrator\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CollectionExtractionStrategy extends AbstractExtractorPluginManagerAwareExtractionStrategy
{
    public function extract($value, ?object $object = null)
    {
        if (! is_array($value) && ! $value instanceof CollectionInterface) {
            throw new Exception\InvalidArgumentException(sprintf(
                'Value needs to be an array or an instance of CollectionInterface, got "%s" instead.',
                is_object($value) ? get_class($value) : gettype($value)
            ));
        }

        if ($value instanceof CollectionInterface) {
            $value = $value->getValues();
        }

        /** @var AbstractDomainObjectExtractor $extractor */
        $extractor = $this->extractorPluginManager->get(
            $this->options[self::OPTION_KEY_EXTRACTOR_NAME],
            $this->options[self::OPTION_KEY_EXTRACTOR_OPTIONS] ?? []
        );

        return array_map(
            function ($collectionItem) use ($extractor) {
                return $extractor->extract($collectionItem);
            },
            $value
        );
    }
}
