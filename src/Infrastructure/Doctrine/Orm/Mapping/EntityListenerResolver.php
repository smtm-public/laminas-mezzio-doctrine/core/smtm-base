<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm\Mapping;

use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManagerDecorator;
use Smtm\Base\Infrastructure\Exception\InvalidArgumentException;
use Smtm\Base\Infrastructure\Service\InfrastructureServiceInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareTrait;
use Doctrine\ORM\Mapping\DefaultEntityListenerResolver;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EntityListenerResolver extends DefaultEntityListenerResolver implements
    \Doctrine\ORM\Mapping\EntityListenerResolver,
    InfrastructureServiceInterface,
    InfrastructureServicePluginManagerAwareInterface
{

    use InfrastructureServicePluginManagerAwareTrait;

    protected EntityManagerDecorator $entityManager;
    protected ?int $authenticatedUserId = null;
    /** @psalm-var array<class-string, object> Map to store entity listener instances. */
    protected array $instances = [];

    /**
     * {@inheritDoc}
     */
    public function clear(?string $className = null): void
    {
        if ($className === null) {
            $this->instances = [];

            return;
        }

        $className = trim($className, '\\');
        if (isset($this->instances[$className])) {
            unset($this->instances[$className]);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function register(object $object): void
    {
        if (! is_object($object)) {
            throw new InvalidArgumentException(sprintf('An object was expected, but got "%s".', gettype($object)));
        }

        $this->instances[get_class($object)] = $object;
    }

    public function resolve(string $className): object
    {
        $className = trim($className, '\\');

        $instance = $this->instances[$className] ?? $this->infrastructureServicePluginManager->get($className);

        if ($instance instanceof AbstractEntityListener) {
            $instance->setInfrastructureServicePluginManager($this->infrastructureServicePluginManager);
            $instance->setEntityManager($this->entityManager);
            $instance->setAuthenticatedUserId($this->authenticatedUserId);
        }

        return $instance;
    }

    public function getEntityManager(): EntityManagerDecorator
    {
        return $this->entityManager;
    }

    public function setEntityManager(EntityManagerDecorator $entityManager): static
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    public function getAuthenticatedUserId(): ?int
    {
        return $this->authenticatedUserId;
    }

    public function setAuthenticatedUserId(?int $authenticatedUserId): static
    {
        $this->authenticatedUserId = $authenticatedUserId;

        return $this;
    }
}
