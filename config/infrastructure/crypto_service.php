<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../../.env.smtm.smtm-base.infrastructure.crypto-service')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../../',
        '.env.smtm.smtm-base.infrastructure.crypto-service'
    );
    $dotenv->load();
}

return [
    'keyPath' =>
        __DIR__ . '/../../../../../data/keys/'
            . EnvHelper::getEnvFromProcessOrSuperGlobal(['SMTM_BASE_INFRASTRUCTURE_CRYPTO_SERVICE_KEY_FILE_NAME', 'BASE_INFRASTRUCTURE_CRYPTO_SERVICE_KEY_FILE_NAME']),
];
