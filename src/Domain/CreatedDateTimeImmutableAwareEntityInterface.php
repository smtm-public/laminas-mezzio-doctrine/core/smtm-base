<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface CreatedDateTimeImmutableAwareEntityInterface
{
    public function getCreated(): \DateTimeImmutable;
    public function setCreated(\DateTimeImmutable $created): static;
}
