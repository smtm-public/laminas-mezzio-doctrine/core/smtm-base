<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait Sha256AwareEntityTrait
{
    protected string $sha256;

    public function getSha256(): string
    {
        return $this->sha256;
    }
}
