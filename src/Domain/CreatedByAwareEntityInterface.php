<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface CreatedByAwareEntityInterface
{
    public function getCreatedBy();
    public function setCreated($createdBy): static;
}
