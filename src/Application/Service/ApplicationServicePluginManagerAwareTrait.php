<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ApplicationServicePluginManagerAwareTrait
{
    protected ApplicationServicePluginManager $applicationServicePluginManager;

    public function getApplicationServicePluginManager(): ApplicationServicePluginManager
    {
        return $this->applicationServicePluginManager;
    }

    public function setApplicationServicePluginManager(ApplicationServicePluginManager $applicationServicePluginManager): static
    {
        $this->applicationServicePluginManager = $applicationServicePluginManager;

        return $this;
    }
}
