<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Codeception\Support\Helper;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;
use Smtm\Base\Infrastructure\Codeception\Module\DataFactory;
use Smtm\Base\Infrastructure\Codeception\Module\Doctrine3;
use Smtm\Base\Infrastructure\Codeception\Module\Mezzio;
use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManagerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DataFactoryHelper extends Module implements DependsOnModule
{
    protected Mezzio $dependentModule;
    protected DataFactory $dataFactory;
    protected EntityManagerInterface $entityManager;
    protected Doctrine3 $doctrineModule;

    public function _initialize()
    {
        parent::_initialize();

        $this->doctrineModule = $this->getModule(Doctrine3::class);
        $this->dataFactory = $this->getModule(DataFactory::class);
        $this->entityManager = $this->doctrineModule->_getEntityManager();
    }

    public function _depends(): array
    {
        return [$this->config['depends']];
    }

    public function _inject(Mezzio $dependentModule = null): void
    {
        $this->dependentModule = $dependentModule;
    }

    protected function defineBaseEntities(EntityManagerInterface $entityManager, callable $factoryDefinitions)
    {
        $doctrineModule = $this->doctrineModule;
        $dataFactoryModule = $this->dataFactory;

        foreach ($factoryDefinitions($doctrineModule, $dataFactoryModule) as $modelName => $factoryDefinition) {
            $callback = null;

            if (array_key_exists('__callback', $factoryDefinition)) {
                $callback = $factoryDefinition['__callback'];
                unset($factoryDefinition['__callback']);
            }

            $factory = $this->dataFactory->_define($modelName, $factoryDefinition);

            if ($callback !== null) {
                $factory->setCallback($callback);
            }
        }
    }
}
