<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator\Factory;

use Smtm\Base\Infrastructure\Laminas\InputFilter\Factory\InputFilterFactory;
use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterFactoryAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InputFilterFactoryAwareValidatorDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var InputFilterFactoryAwareInterface $validator */
        $validator = $callback();
        $validator->setInputFilterFactory($container->get(InputFilterFactory::class));

        return $validator;
    }
}
