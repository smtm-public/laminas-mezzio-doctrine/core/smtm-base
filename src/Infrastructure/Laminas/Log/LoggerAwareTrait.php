<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Log;

use Laminas\Log\LoggerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait LoggerAwareTrait
{

    /* use LoggerAwareTrait; */

    protected ?LoggerInterface $logger = null;

    public function getLogger(): ?LoggerInterface
    {
        return $this->logger;
    }

    public function setLogger(LoggerInterface $logger): static
    {
        $this->logger = $logger;

        return $this;
    }
}
