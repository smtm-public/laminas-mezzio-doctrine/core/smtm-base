<?php

namespace Smtm\Base\Infrastructure\Symfony\Process;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

class ProcessFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        return new $requestedName(
            $options['commandline'],
            $options['cwd'],
            $options['env'],
            $options['input'],
            $options['timeout']
        );
    }
}
