<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait RemoteServiceResponseExceptionTrait
{
    protected int $responseStatus;
    protected string $responseBody;
    protected array|string|null $responseBodyParsed;

    public function getResponseStatus(): int
    {
        return $this->responseStatus;
    }

    public function setResponseStatus(int $responseStatus): static
    {
        $this->responseStatus = $responseStatus;

        return $this;
    }

    public function getResponseBody(): string
    {
        return $this->responseBody;
    }

    public function setResponseBody(string $responseBody): static
    {
        $this->responseBody = $responseBody;

        return $this;
    }

    public function getResponseBodyParsed(): array|string|null
    {
        return $this->responseBodyParsed;
    }

    public function setResponseBodyParsed(array|string|null $responseBodyParsed): static
    {
        $this->responseBodyParsed = $responseBodyParsed;

        return $this;
    }

    public function toArray(): array
    {
        $problem = [
            'responseStatus' => $this->responseStatus,
            'responseBody' => $this->responseBody,
            'resposneBodyParsed'  => $this->responseBodyParsed,
        ];

        return $problem;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
