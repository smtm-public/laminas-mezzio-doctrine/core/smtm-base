<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Codeception\Support\Helper;

use Codeception\Module;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class FactoryMuffinHelper extends Module
{
    protected \League\FactoryMuffin\FactoryMuffin $factory;

    public function _initialize()
    {
        parent::_initialize();

        $this->factory = new \League\FactoryMuffin\FactoryMuffin();
    }

    public function getFactory(): \League\FactoryMuffin\FactoryMuffin
    {
        return $this->factory;
    }

    protected function defineBaseEntities(callable $factoryDefinitions)
    {
        foreach ($factoryDefinitions() as $modelName => $factoryDefinition) {
            $this->factory->define($modelName)->setDefinitions($factoryDefinition);
//        ->setCallback(function ($obj, $saved) {
//            $obj->saved = $saved;
//        });
        }
    }
}
