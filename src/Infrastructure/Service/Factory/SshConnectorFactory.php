<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Base\Infrastructure\Service\SshConnector;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class SshConnectorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): SshConnector
    {
        return new SshConnector(
            $container->get(InfrastructureServicePluginManager::class),
            $options
        );
    }
}
