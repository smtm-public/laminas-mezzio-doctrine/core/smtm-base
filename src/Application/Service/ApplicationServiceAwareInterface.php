<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ApplicationServiceAwareInterface
{
    public function getApplicationService(): ?AbstractApplicationService;
    public function setApplicationService(?AbstractApplicationService $applicationService): static;
}
