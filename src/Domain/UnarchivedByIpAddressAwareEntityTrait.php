<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait UnarchivedByIpAddressAwareEntityTrait
{
    protected ?string $unarchivedByIpAddress = null;

    public function getUnarchivedByIpAddress(): ?string
    {
        return $this->unarchivedByIpAddress;
    }

    public function setUnarchivedByIpAddress(?string $unarchivedByIpAddress): static
    {
        $this->unarchivedByIpAddress = $unarchivedByIpAddress;

        return $this;
    }
}
