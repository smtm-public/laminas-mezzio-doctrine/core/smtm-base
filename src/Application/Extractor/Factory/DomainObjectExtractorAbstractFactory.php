<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Factory;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;
use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Extractor\NamingStrategy\ExtractionNamingStrategyPluginManager;
use Smtm\Base\Application\Extractor\Strategy\ExtractionStrategyPluginManager;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DomainObjectExtractorAbstractFactory implements AbstractFactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get(ExtractorPluginManager::class),
            $container->get(ExtractionNamingStrategyPluginManager::class),
            $container->get(ExtractionStrategyPluginManager::class),
            $options
        );
    }

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return is_subclass_of($requestedName, AbstractDomainObjectExtractor::class);
    }
}
