<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Log\Writer;

use Smtm\Base\Infrastructure\Doctrine\DBAL\Helper\StatementHelper;
use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManagerInterface;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryAwareInterface;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryAwareTrait;
use Doctrine\DBAL\Connection;
use Laminas\Log\Exception;
use Laminas\Log\Formatter\Db as DbFormatter;
use Laminas\Log\Writer\AbstractWriter;
use Ramsey\Uuid\Rfc4122\UuidV4;
use Traversable;
use function array_keys;
use function array_map;
use function implode;
use function is_array;
use function is_scalar;
use function iterator_to_array;
use function var_export;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Doctrine extends AbstractWriter implements ManagerRegistryAwareInterface
{

    use ManagerRegistryAwareTrait;

    protected string $entityManagerName;
    protected string $tableName;
    protected ?array $columnMap;
    protected ?string $separator = '_';

    public function __construct(
        Traversable | array | string $configOrEntityManagerName = null,
        string $tableName = null,
        array $columnMap = null,
        string $separator = null
    ) {
        if ($configOrEntityManagerName instanceof Traversable) {
            $configOrEntityManagerName = iterator_to_array($configOrEntityManagerName);
        }

        $entityManagerName = null;

        if (is_array($configOrEntityManagerName)) {
            parent::__construct($configOrEntityManagerName);
            $entityManagerName = $configOrEntityManagerName['entityManagerName'] ?? null;
            $tableName = $configOrEntityManagerName['tableName'] ?? null;
            $columnMap = $configOrEntityManagerName['columnMap'] ?? null;
            $separator = $configOrEntityManagerName['separator'] ?? null;
        } elseif (is_string($configOrEntityManagerName)) {
            $entityManagerName = $configOrEntityManagerName;
        }

        $entityManagerName = (string) $entityManagerName;

        if ($entityManagerName === '') {
            throw new Exception\InvalidArgumentException(
                'You must specify an EntityManager name either directly in the constructor, or via options'
            );
        }

        $tableName = (string) $tableName;

        if ($tableName === '') {
            throw new Exception\InvalidArgumentException(
                'You must specify a table name either directly in the constructor, or via options'
            );
        }

        $this->entityManagerName = $entityManagerName;
        $this->tableName = $tableName;
        $this->columnMap = $columnMap;

        if (! empty($separator)) {
            $this->separator = $separator;
        }

        if (! $this->hasFormatter()) {
            $this->setFormatter(new DbFormatter());
        }
    }

    protected function doWrite(array $event)
    {
        try {
            $event = $this->formatter->format($event);

            // Transform the event array into fields
            if ($this->columnMap === null) {
                $dataToInsert = $this->eventIntoColumn($event);
            } else {
                $dataToInsert = $this->mapEventIntoColumn($event, $this->columnMap);
            }

            $dataToInsert['uuid'] = UuidV4::uuid4()->toString();
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->getManagerRegistry()->getManager($this->entityManagerName);
            $connection = $entityManager->getConnection();
            $query = $this->prepareInsert($connection, $dataToInsert);
            $stmt = $connection->prepare($query);
            StatementHelper::bindValues($stmt, $dataToInsert);
            $stmt->executeStatement();
        } catch (\Throwable) {
            // Skip throwing of exceptions to avoid userd login problems.
        }
    }

    /**
     * Prepare the INSERT SQL statement
     *
     * @param  array $fields
     * @return string
     */
    protected function prepareInsert(Connection $connection, array $fields)
    {
        $table = $connection->quoteIdentifier($this->tableName);
        $keys = array_keys($fields);
        $columns = implode(",", array_map([$connection, 'quoteIdentifier'], $keys));
        $identifiers = implode(",", array_map(fn (string $key) => ':' . $key, $keys));
        return <<< EOT
        INSERT INTO $table
        ($columns)
        VALUES ($identifiers)
        EOT;
    }

    /**
     * Map event into column using the $columnMap array
     *
     * @param  array $event
     * @param  array $columnMap
     * @return array
     */
    protected function mapEventIntoColumn(array $event, ?array $columnMap = null)
    {
        if (empty($event)) {
            return [];
        }

        $data = [];
        foreach ($event as $name => $value) {
            if (is_array($value)) {
                foreach ($value as $key => $subvalue) {
                    if (isset($columnMap[$name][$key])) {
                        if (is_scalar($subvalue)) {
                            $data[$columnMap[$name][$key]] = $subvalue;
                            continue;
                        }

                        $data[$columnMap[$name][$key]] = var_export($subvalue, true);
                    }
                }
            } elseif (isset($columnMap[$name])) {
                $data[$columnMap[$name]] = $value;
            }
        }
        return $data;
    }

    /**
     * Transform event into column for the db table
     *
     * @param  array $event
     * @return array
     */
    protected function eventIntoColumn(array $event)
    {
        if (empty($event)) {
            return [];
        }

        $data = [];
        foreach ($event as $name => $value) {
            if (is_array($value)) {
                foreach ($value as $key => $subvalue) {
                    if (is_scalar($subvalue)) {
                        $data[$name . $this->separator . $key] = $subvalue;
                        continue;
                    }

                    $data[$name . $this->separator . $key] = var_export($subvalue, true);
                }
            } else {
                $data[$name] = $value;
            }
        }
        return $data;
    }
}
