<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class OopHelper
{
    public static function classImplements(
        string $className,
        string $interfaceName
    ): bool {
        return in_array(
            $interfaceName,
            class_implements($className),
            true
        );
    }

    public static function classUses(
        string $className,
        string $traitName
    ): bool {
        return in_array(
            $traitName,
            class_uses($className),
            true
        );
    }
}
