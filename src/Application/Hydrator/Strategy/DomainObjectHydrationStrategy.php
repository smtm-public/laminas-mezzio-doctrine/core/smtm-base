<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DomainObjectHydrationStrategy extends AbstractServicePluginManagerAwareHydrationStrategy
{
    public const OPTION_KEY_APPLICATION_SERVICE_NAME = 'applicationServiceName';
    public const OPTION_KEY_APPLICATION_HYDRATOR_OPTIONS = 'hydratorOptions';

    public function hydrate($value, ?array $data)
    {
        if ($value === null) {
            return null;
        }

        /** @var AbstractDbService $service */
        $service = $this->applicationServicePluginManager->get(
            $this->options[static::OPTION_KEY_APPLICATION_SERVICE_NAME]
        );
        $hydrator = $service->getHydrator();

        if (isset($this->options[static::OPTION_KEY_APPLICATION_HYDRATOR_OPTIONS]['mustHydrate'])) {
            $hydrator->setMustHydrate([]);

            foreach ($this->options[static::OPTION_KEY_APPLICATION_HYDRATOR_OPTIONS]['mustHydrate']
                     as $propertyName => $errorMessage
            ) {
                $hydrator->addMustHydrate($propertyName, $errorMessage);
            }
        }

        $result = $service->hydrateDomainObject($value, hydrator: $hydrator);

        return $result;
    }
}
