<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\DBAL\Logger\Factory;

use Smtm\Base\Infrastructure\LaminasPsrLogger;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Doctrine\DBAL\Logging\Middleware;
use Laminas\Log\Writer\Stream;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MiddlewareFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): Middleware {
        /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);

        $logger = $options !== null
            ? (
                isset($options['logger']['options'])
                ? $infrastructureServicePluginManager->build(
                    $options['logger']['name'],
                    $options['logger']['options']
                )
                : $infrastructureServicePluginManager->get(
                    $options['logger']['name'],
                )
            )
            : $infrastructureServicePluginManager->build(
                LaminasPsrLogger::class,
                [
                    'writers' => [
                        Stream::class,
                    ],
                ]
            );

        return new Middleware($logger);
    }
}
