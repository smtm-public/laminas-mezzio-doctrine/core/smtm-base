<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Exception;

use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CannotHydrateException extends RuntimeException
{

}
