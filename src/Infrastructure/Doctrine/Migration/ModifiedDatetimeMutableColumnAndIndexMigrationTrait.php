<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ModifiedDatetimeMutableColumnAndIndexMigrationTrait
{
    public function addModifiedDatetimeMutableColumn(
        Table $table,
        array $options = ['notNull' => false]
    ): void {
        $name = $options['name'] ?? 'modified';
        unset($options['name']);

        $table->addColumn(
            $name,
            Types::DATETIME_MUTABLE,
            $options
        );
    }

    public function addModifiedDatetimeMutableColumnAndIndex(
        Table $table,
        ?string $indexName = null,
        array $options = ['notNull' => false]
    ): void {
        $name = $options['name'] ?? 'modified';

        $this->addModifiedDatetimeMutableColumn($table, $options);

        $table->addIndex(
            [$name],
            substr(
                $indexName ?? 'idx_' . $table->getName() . '_' . $name,
                0,
                SqlHelper::IDENTIFIER_LENGTH_INDEX
            )
        );
    }
}
