<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CollectionHydrationStrategy extends AbstractServicePluginManagerAwareHydrationStrategy
{
    public const OPTION_KEY_APPLICATION_SERVICE_NAME = 'applicationServiceName';
    public const OPTION_KEY_APPLICATION_HYDRATOR_OPTIONS = 'hydratorOptions';

    public function hydrate($value, ?array $data)
    {
        /** @var AbstractDbService $service */
        $service = $this->applicationServicePluginManager->get(
            $this->options[static::OPTION_KEY_APPLICATION_SERVICE_NAME]
        );
        $hydrator = $service->getHydrator();

        if (isset($this->options[static::OPTION_KEY_APPLICATION_HYDRATOR_OPTIONS]['mustHydrate'])) {
            $hydrator->setMustHydrate([]);

            foreach ($this->options[static::OPTION_KEY_APPLICATION_HYDRATOR_OPTIONS]['mustHydrate']
                     as $propertyName => $errorMessage
            ) {
                $hydrator->addMustHydrate($propertyName, $errorMessage);
            }
        }

        return array_map(
            function ($item) use ($service, $hydrator) {
                return $service->hydrateDomainObject($item, hydrator: $hydrator);
            },
            $value ?? []
        );
    }
}
