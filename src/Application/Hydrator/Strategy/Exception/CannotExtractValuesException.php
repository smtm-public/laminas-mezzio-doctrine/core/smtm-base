<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy\Exception;

use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CannotExtractValuesException extends RuntimeException
{

}
