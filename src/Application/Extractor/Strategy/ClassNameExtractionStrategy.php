<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

/**
 * @author Nikolay Mikov <n.mikov@smtm.com>
 */
class ClassNameExtractionStrategy extends AbstractExtractionStrategy
{
    public const OPTION_KEY_FQN = 'fqn';

    public function extract($value, ?object $object = null): ?string
    {
        return ($this->options[self::OPTION_KEY_FQN] ?? true) ? get_class($object) : (new \ReflectionClass($object))->getShortName();
    }
}
