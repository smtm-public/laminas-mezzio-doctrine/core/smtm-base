<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ArchivedByIpAddressAwareEntityTrait
{
    protected ?string $archivedByIpAddress = null;

    public function getArchivedByIpAddress(): ?string
    {
        return $this->archivedByIpAddress;
    }

    public function setArchivedByIpAddress(?string $archivedByIpAddress): static
    {
        $this->archivedByIpAddress = $archivedByIpAddress;

        return $this;
    }
}
