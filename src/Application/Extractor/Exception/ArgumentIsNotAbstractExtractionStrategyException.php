<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Exception;

use InvalidArgumentException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ArgumentIsNotAbstractExtractionStrategyException extends InvalidArgumentException
{

}
