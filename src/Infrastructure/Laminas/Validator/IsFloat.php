<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator;

use Laminas\Validator\AbstractValidator;
use Laminas\Validator\Exception\ExtensionNotLoadedException;
use Laminas\Validator\Exception\InvalidArgumentException;
use Traversable;

/**
 * Bidirectional (is|is not) validation for expected value type: float
 *
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IsFloat extends AbstractValidator
{
    const INVALID_TRUE = 'invalidValueIs';
    const INVALID_FALSE = 'invalidValueIsNot';

    protected array $messageTemplates = [
        self::INVALID_TRUE => 'Invalid value given. Float expected.',
        self::INVALID_FALSE => 'Invalid value given. Cannot be a float.',
    ];

    protected array $options = [];

    /**
     * $options can have the following keys:
     * - is - true|false - whether it should be of type float (true) or not (false)
     *
     * @param array|Traversable $options
     *
     * @throws ExtensionNotLoadedException if ext/intl is not present
     */
    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    /**
     * @param $value
     *
     * @return bool
     *
     * @throws InvalidArgumentException
     */
    public function isValid($value)
    {
        $is = $this->options['is'] ?? true;

        if ($is && !is_float($value) || !$is && is_float($value)) {
            $this->error(constant('self::INVALID_' . strtoupper(var_export($is, true))));

            return false;
        }

        return true;
    }
}
