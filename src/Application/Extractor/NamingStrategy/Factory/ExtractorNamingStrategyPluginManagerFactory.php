<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\NamingStrategy\Factory;

use Smtm\Base\Application\Extractor\NamingStrategy\ExtractionNamingStrategyPluginManager;
use Smtm\Base\Application\Extractor\NamingStrategy\LetterCaseExtractionNamingStrategy;
use Smtm\Base\Application\Extractor\NamingStrategy\MappingExtractionNamingStrategy;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExtractorNamingStrategyPluginManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ExtractionNamingStrategyPluginManager(
            $container,
            [
                'shared' => [
                    LetterCaseExtractionNamingStrategy::class => false,
                    MappingExtractionNamingStrategy::class => false,
                ],
            ]
        );
    }
}
