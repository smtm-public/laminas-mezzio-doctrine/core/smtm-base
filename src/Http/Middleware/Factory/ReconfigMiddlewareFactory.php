<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\Factory;

use Smtm\Base\Http\Middleware\Laminas\ConfigAggregator\ReconfigMiddleware;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ReconfigMiddlewareFactory
{
    public function __invoke(ContainerInterface $container, string $requestedName, ?array $options = []): ReconfigMiddleware
    {
        return new $requestedName();
    }
}
