<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Exception;

use Smtm\Base\Infrastructure\Helper\HttpHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ForbiddenException extends AbstractHttpException
{
    public const MESSAGE = 'Access denied';
    public const CODE = 0;
    public const STATUS = HttpHelper::STATUS_CODE_FORBIDDEN;
    public const TYPE = 'https://example.com/problems/forbidden';

    protected int $status = self::STATUS;
    protected string $title = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_FORBIDDEN];
    protected string $detail = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_FORBIDDEN];
    protected string $type = self::TYPE;
}
