<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler\DbServiceEntity;

use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Http\Middleware\ServerParamsAndRequestIdMiddleware;
use Laminas\Diactoros\Response\EmptyResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractDeleteHandler extends AbstractHandler
{
    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $parsedQueryParams = static::parseQueryParams($this->getMergedFilteredQueryParams($request));
        $entity = $this->applicationService->getById(
            $this->getMergedFilteredAttributes($request)['id'] ?? ''
        );
        $this->applicationService->delete(
            $entity,
            array_replace(
                [ApplicationServiceInterface::OPTION_KEY_PARAMS => $parsedQueryParams],
                [ApplicationServiceInterface::OPTION_KEY_METHOD => $request->getMethod()],
                ServerParamsAndRequestIdMiddleware::getRequestParams($request)
            )
        );

        return new EmptyResponse();
    }
}
