<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Log\Factory;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConsoleLoggerAwareDelegator extends LoggerAwareDelegator
{
    protected ?string $loggerName = 'logger-console';
}
