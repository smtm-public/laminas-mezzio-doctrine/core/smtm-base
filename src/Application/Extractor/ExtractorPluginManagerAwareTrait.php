<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ExtractorPluginManagerAwareTrait
{
    protected ExtractorPluginManager $extractorPluginManager;

    public function getExtractorPluginManager(): ExtractorPluginManager
    {
        return $this->extractorPluginManager;
    }

    public function setExtractorPluginManager(ExtractorPluginManager $extractorPluginManager): static
    {
        $this->extractorPluginManager = $extractorPluginManager;

        return $this;
    }
}
