<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Laminas\Log\LoggerAwareInterface;
use Smtm\Base\Infrastructure\Laminas\Log\LoggerAwareTrait;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteMethodNotAllowedResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceBadRequestResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceConflictResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceConnectionException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceForbiddenResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceGatewayTimeoutResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceInternalServerErrorResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceNotFoundResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceServiceUnavailableResponseException;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceUnauthorizedResponseException;
use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Utils;
use GuzzleHttp\RequestOptions;
use JetBrains\PhpStorm\Pure;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractWebConnector extends AbstractInfrastructureService implements
    ConfigAwareInterface, LoggerAwareInterface
{

    use ConfigAwareTrait, LoggerAwareTrait;

    public const OPTION_NAME_LOG_EXTRA_DATA =
        'optionNameLogExtraData';
    public const LOG_EXTRA_DATA = [];

    #[Pure] public function __construct(
        InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected ClientInterface $client
    ) {
        parent::__construct($infrastructureServicePluginManager);
    }

    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    public function setClient(ClientInterface $client): static
    {
        $this->client = $client;

        return $this;
    }

    protected function createRequest(string $method, string $url): RequestInterface
    {
        return new Request($method, $url);
    }

    protected function setAuthenticationBasicOnRequest(
        RequestInterface $request,
        string $username,
        ?string $password = null
    ): RequestInterface {
        $request = $request->withHeader(
            'Authorization',
            HttpHelper::buildHeaderAuthorizationBasicValue($username, $password)
        );

        return $request;
    }

    protected function setAuthenticationOAuthOnRequest(
        RequestInterface $request,
        string $consumerKey,
        string $consumerSecret,
        string $signatureMethod,
        string $httpMethod,
        string $url,
        ?string $callback = null,
        ?string $verifier = null,
        ?string $realm = null
    ): RequestInterface {
        $request = $request->withHeader(
            'Authorization',
            HttpHelper::buildHeaderAuthorizationOAuthValue(
                $consumerKey,
                $consumerSecret,
                $signatureMethod,
                $httpMethod,
                $url,
                $callback,
                $verifier,
                $realm
            )
        );

        return $request;
    }

    protected function setAuthenticationBearerOnRequest(RequestInterface $request, string $token): RequestInterface
    {
        $request = $request->withHeader(
            'Authorization',
            HttpHelper::buildHeaderAuthorizationBearerValue($token)
        );

        return $request;
    }

    protected function setJsonBodyOnRequest(
        RequestInterface $request,
        array|object|string|int|float|null $body
    ): RequestInterface {
        $request = $request->withHeader(
            'Content-Type',
            'application/json'
        );
        $stream = Utils::streamFor(json_encode($body));

        return $request->withBody($stream);
    }

    protected function setBinaryBodyOnRequest(RequestInterface $request, string $body): RequestInterface
    {
        $request = $request->withHeader(
            'Content-Type',
            'application/octet-stream'
        );
        $stream = Utils::streamFor($body);

        return $request->withBody($stream);
    }

    protected function setFormUrlEncodedBodyOnRequest(RequestInterface $request, array $formData): RequestInterface
    {
        $request = $request->withHeader(
            'Content-Type',
            'application/x-www-form-urlencoded'
        );
        $stream = Utils::streamFor(http_build_query($formData));

        return $request->withBody($stream);
    }

    protected function sendRequest(
        RequestInterface $request,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA =>
                self::LOG_EXTRA_DATA,
        ]
    ): ResponseInterface {
        try {
            $response = $this->getClient()->request(
                $request->getMethod(),
                $request->getUri(),
                [
                    RequestOptions::HEADERS => $request->getHeaders(),
                    RequestOptions::BODY => $request->getBody(),
                ]
            );
        } catch (ClientException | ServerException $e) {
            $response = $e->getResponse();
        } catch (Exception $e) {
            throw new RemoteServiceConnectionException(
                'HTTP connection error',
                HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR,
                $e
            );
        }

        $request->getBody()->rewind();

        $this->getLogger() !== null
        && $this->getLogger()->info(
            '',
            array_merge(
                $options[self::OPTION_NAME_LOG_EXTRA_DATA],
                [
                    'request_method' => $request->getMethod(),
                    'request_headers' => HttpHelper::formatHeadersArrayAsString($request->getHeaders()),
                    'request_uri' => (string) $request->getUri(),
                    'request_body' => $request->getBody()->getContents(),
                    'response_status_code' => $response->getStatusCode(),
                    'response_headers' => HttpHelper::formatHeadersArrayAsString($response->getHeaders()),
                    'response_body' => $response->getBody()->getContents(),
                ]
            )
        );

        $request->getBody()->rewind();
        $response->getBody()->rewind();

        return $response;
    }

    protected function validateResponse(
        ResponseInterface $response,
        int $expectedStatusCode = HttpHelper::STATUS_CODE_OK
    ): string {
        $responseStatusCode = $response->getStatusCode();
        $responseBody = $response->getBody()->getContents();

        if ($responseStatusCode !== $expectedStatusCode) {
            $errorMessage = $this->buildErrorMessage(
                $expectedStatusCode,
                $responseStatusCode,
                $responseBody
            );

            $e = match ($responseStatusCode) {
                HttpHelper::STATUS_CODE_BAD_REQUEST =>
                new RemoteServiceBadRequestResponseException($errorMessage, 0),
                HttpHelper::STATUS_CODE_UNAUTHORIZED =>
                new RemoteServiceUnauthorizedResponseException($errorMessage, 0),
                HttpHelper::STATUS_CODE_FORBIDDEN =>
                new RemoteServiceForbiddenResponseException($errorMessage, 0),
                HttpHelper::STATUS_CODE_NOT_FOUND =>
                new RemoteServiceNotFoundResponseException($errorMessage, 0),
                HttpHelper::STATUS_CODE_METHOD_NOT_ALLOWED =>
                new RemoteMethodNotAllowedResponseException($errorMessage, 0),
                HttpHelper::STATUS_CODE_CONFLICT =>
                new RemoteServiceConflictResponseException($errorMessage, 0),
                HttpHelper::STATUS_CODE_GATEWAY_TIMEOUT =>
                new RemoteServiceGatewayTimeoutResponseException($errorMessage, 0),
                HttpHelper::STATUS_CODE_SERVICE_UNAVAILABLE =>
                new RemoteServiceServiceUnavailableResponseException($errorMessage, 0),
                HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR =>
                new RemoteServiceInternalServerErrorResponseException($errorMessage, 0),
                default => new RemoteServiceResponseException($errorMessage, 0),
            };
            $e
                ->setResponseStatus($responseStatusCode)
                ->setResponseBody($responseBody)
                ->setResponseBodyParsed(null);

            throw $e;
        }

        return $responseBody;
    }

    protected function buildErrorMessage(
        int $expectedStatusCode,
        int $responseStatusCode,
        string $responseBody
    ): string {
        $message = 'Remote service response exception. ';

        $message .= 'Expected status: ' . $expectedStatusCode . "\n"
            . 'Got status: ' . $responseStatusCode . "\n"
            . 'Response body: ' . $responseBody;

        return $message;
    }
}
