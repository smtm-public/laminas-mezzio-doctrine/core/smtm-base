<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\Exception;

use Smtm\Base\Application\Exception\InvalidConfigException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UndefinedEntityException extends InvalidConfigException
{

}
