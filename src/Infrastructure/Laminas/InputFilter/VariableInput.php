<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\InputFilter;

use Laminas\Filter\FilterChain;
use Laminas\InputFilter\Input;
use Laminas\InputFilter\InputInterface;
use Laminas\ServiceManager\AbstractPluginManager;
use Laminas\Validator\NotEmpty;
use Laminas\Validator\Translator\TranslatorInterface;
use Laminas\Validator\ValidatorChain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class VariableInput implements InputInterface
{
    protected bool $allowEmpty = false;
    protected bool $continueIfEmpty = false;
    protected bool $breakOnFailure = false;
    protected ?array $errorMessage = null;
    protected ?FilterChain $filterChain = null;
    protected ?string $name;
    protected bool $notEmptyValidator = false;
    protected bool $required = true;
    protected ?ValidatorChain $validatorChain = null;
    protected mixed $value;
    protected bool $hasValue = false;
    protected mixed $fallbackValue;
    protected bool $hasFallback = false;
    /** @var InputInterface[] $inputs */
    protected array $inputs = [];

    public function __construct(?string $name = null)
    {
        $this->name = $name;
    }

    /**
     * @param bool $allowEmpty
     */
    public function setAllowEmpty($allowEmpty): static
    {
        $this->allowEmpty = (bool) $allowEmpty;

        return $this;
    }

    /**
     * @param bool $breakOnFailure
     */
    public function setBreakOnFailure($breakOnFailure): static
    {
        $this->breakOnFailure = (bool) $breakOnFailure;

        return $this;
    }

    /**
     * @param bool $continueIfEmpty
     */
    public function setContinueIfEmpty(bool $continueIfEmpty): static
    {
        $this->continueIfEmpty = $continueIfEmpty;

        return $this;
    }

    /**
     * @param string|null $errorMessage
     */
    public function setErrorMessage($errorMessage): static
    {
        $this->errorMessage = null === $errorMessage ? null : (string) $errorMessage;

        return $this;
    }

    public function setFilterChain(FilterChain $filterChain): static
    {
        $this->filterChain = $filterChain;

        return $this;
    }

    /**
     * @param string $name
     */
    public function setName($name): static
    {
        /** @psalm-suppress RedundantCastGivenDocblockType */
        $this->name = (string) $name;

        foreach ($this->inputs as $input) {
            $input->setName($name);
        }

        return $this;
    }

    /**
     * @param bool $required
     */
    public function setRequired($required): static
    {
        /** @psalm-suppress RedundantCastGivenDocblockType */
        $this->required = (bool) $required;

        return $this;
    }

    public function setValidatorChain(ValidatorChain $validatorChain): static
    {
        $this->validatorChain = $validatorChain;

        return $this;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): static
    {
        /**
         * TODO:
         * The if below handles cases where data is missing for the input when it contains an ArrayInput component.
         * In such cases we hit the logic at \Laminas\InputFilter\BaseInputFilter.php:542
         * Trying to ArrayInput::setData(null) results in a `Value must be an array, NULL given.` exception.
         * This workaround might be problematic when parsed JSON body does actually contain a null value. Not tested.
         */
        if ($value === null) { // TODO: MD - This workaround for missing ArrayInput data throwing error might be problematic
            return $this->resetValue();
        }

        $this->value    = $value;
        $this->hasValue = true;

        foreach ($this->inputs as $input) {
            $input->setValue($value);
        }

        return $this;
    }

    public function resetValue(): static
    {
        $this->value    = null;
        $this->hasValue = false;

        foreach ($this->inputs as $input) {
            $input->resetValue();
        }

        return $this;
    }

    public function setFallbackValue(mixed $value): static
    {
        $this->fallbackValue = $value;
        $this->hasFallback   = true;

        foreach ($this->inputs as $input) {
            if (method_exists($input, 'setFallbackValue')) {
                $input->setFallbackValue($value);
            }
        }

        return $this;
    }

    public function allowEmpty(): bool
    {
        return $this->allowEmpty;
    }

    public function breakOnFailure(): bool
    {
        return $this->breakOnFailure;
    }

    public function continueIfEmpty(): bool
    {
        return $this->continueIfEmpty;
    }

    public function getErrorMessage(): ?array
    {
        return $this->errorMessage;
    }

    public function getFilterChain(): FilterChain
    {
        if (! $this->filterChain) {
            $this->filterChain = new FilterChain();
        }

        return $this->filterChain;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getRawValue(): mixed
    {
        return $this->value;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function getValidatorChain(): ValidatorChain
    {
        if (! $this->validatorChain) {
            $this->validatorChain = new ValidatorChain();
        }

        return $this->validatorChain;
    }

    public function getValue(): mixed
    {
        $filter = $this->getFilterChain();

        return $filter->filter($this->value);
    }

    public function hasValue(): bool
    {
        return $this->hasValue;
    }

    public function getFallbackValue(): mixed
    {
        return $this->fallbackValue;
    }

    public function hasFallback(): bool
    {
        return $this->hasFallback;
    }

    public function clearFallbackValue(): void
    {
        $this->hasFallback   = false;
        $this->fallbackValue = null;

        foreach ($this->inputs as $input) {
            if (method_exists($input, 'clearFallbackValue')) {
                $input->clearFallbackValue();
            }
        }
    }

    public function merge(InputInterface $input): static
    {
        $this->setBreakOnFailure($input->breakOnFailure());

        if ($input instanceof Input) {
            $this->setContinueIfEmpty($input->continueIfEmpty());
        }

        $this->setErrorMessage($input->getErrorMessage());
        $this->setName($input->getName());
        $this->setRequired($input->isRequired());
        $this->setAllowEmpty($input->allowEmpty());

        if (! $input instanceof Input || $input->hasValue()) {
            $this->setValue($input->getRawValue());
        }

        $filterChain = $input->getFilterChain();
        $this->getFilterChain()->merge($filterChain);

        $validatorChain = $input->getValidatorChain();
        $this->getValidatorChain()->merge($validatorChain);

        return $this;
    }

    /**
     * @param  mixed $context Extra "context" to provide the validator
     */
    public function isValid($context = null): bool
    {
        $result = false;

        /** @var  $input */
        foreach ($this->inputs as $input) {
            $result = $result || $input->isValid($context);
        }

        return $result;
    }

    /**
     * @return array<array-key, string>
     */
    public function getMessages(): array
    {
        $messages = [];

        foreach ($this->inputs as $input) {
            $messages[] = $input->getMessages();
        }

        return $messages;
    }

    protected function injectNotEmptyValidator(): void
    {
        if ((! $this->isRequired() && $this->allowEmpty()) || $this->notEmptyValidator) {
            return;
        }

        $chain = $this->getValidatorChain();

        // Check if NotEmpty validator is already in chain
        $validators = $chain->getValidators();

        foreach ($validators as $validator) {
            if ($validator['instance'] instanceof NotEmpty) {
                $this->notEmptyValidator = true;

                return;
            }
        }

        $this->notEmptyValidator = true;

        if (class_exists(AbstractPluginManager::class)) {
            $chain->prependByName(NotEmpty::class, [], true);

            return;
        }

        $chain->prependValidator(new NotEmpty(), true);
    }

    protected function prepareRequiredValidationFailureMessage(): array
    {
        $chain    = $this->getValidatorChain();
        $notEmpty = $chain->plugin(NotEmpty::class);

        foreach ($chain->getValidators() as $validator) {
            if ($validator['instance'] instanceof NotEmpty) {
                $notEmpty = $validator['instance'];

                break;
            }
        }

        /** @psalm-var array<string, string> $templates */
        $templates  = $notEmpty->getOption('messageTemplates');
        $message    = $templates[NotEmpty::IS_EMPTY];
        $translator = $notEmpty->getTranslator();

        if ($translator instanceof TranslatorInterface) {
            $message = $translator->translate($message, $notEmpty->getTranslatorTextDomain());
        }

        return [
            NotEmpty::IS_EMPTY => $message,
        ];
    }

    public function addInput(string|int $index, InputInterface $input): static
    {
        $this->inputs[$index] = $input;

        return $this;
    }

    public function getInput(string|int $index): InputInterface
    {
        return $this->inputs[$index];
    }
}
