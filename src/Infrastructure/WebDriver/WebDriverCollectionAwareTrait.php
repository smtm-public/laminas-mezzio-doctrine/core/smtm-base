<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\WebDriver;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait WebDriverCollectionAwareTrait
{
    /** @var WebDriver[] $webDriverCollection */
    protected array $webDriverCollection;

    public function getWebDriverCollection(): array
    {
        return $this->webDriverCollection;
    }

    public function setWebDriverCollection(array $webDriverCollection): static
    {
        $this->webDriverCollection = $webDriverCollection;

        return $this;
    }

    public function addWebDriver(string $name, WebDriver $webDriver): static
    {
        $this->webDriverCollection[$name] = $webDriver;

        return $this;
    }

    public function getWebDriverByName(string $name): ?WebDriver
    {
        return $this->webDriverCollection[$name] ?? null;
    }

    public function removeWebDriver(string $name): static
    {
        unset($this->webDriverCollection[$name]);

        return $this;
    }
}
