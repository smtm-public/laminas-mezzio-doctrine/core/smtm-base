<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

use DateTime;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ArchivedAtAwareEntityInterface
{
    public function getArchivedAt(): ?\DateTime;
    public function setArchivedAt(?DateTime $archivedAt): static;
}
