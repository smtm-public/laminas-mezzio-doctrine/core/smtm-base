<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait UuidStringColumnAndUniqueIndexMigrationTrait
{
    protected function addUuidStringColumnAndUniqueIndex(Table $table, ?string $uniqueIndexName = null): void
    {
        $table->addColumn('uuid', Types::STRING, ['notNull' => true, 'length' => 36]);
        $table->addUniqueIndex(
            ['uuid'],
            substr(
                $uniqueIndexName ?? 'idx_un_' . $table->getName() . '_uuid',
                0,
                SqlHelper::IDENTIFIER_LENGTH_INDEX
            )
        );
    }
}
