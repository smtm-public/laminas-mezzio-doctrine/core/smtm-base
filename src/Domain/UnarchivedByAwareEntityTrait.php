<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait UnarchivedByAwareEntityTrait
{
    protected $unarchivedBy;

    public function getUnarchivedBy()
    {
        return $this->unarchivedBy;
    }

    public function setUnarchivedBy($unarchivedBy): static
    {
        $this->unarchivedBy = $unarchivedBy;

        return $this;
    }
}
