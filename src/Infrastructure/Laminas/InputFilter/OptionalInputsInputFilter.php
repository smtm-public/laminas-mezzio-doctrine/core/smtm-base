<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\InputFilter;

use Laminas\InputFilter\InputFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class OptionalInputsInputFilter extends InputFilter
{
    public function setData($data)
    {
        parent::setData($data ?: []);

        return $this;
    }

    /**
     * Run validation, or return true if the data was empty
     *
     * {@inheritDoc}
     */
    public function isValid($context = null)
    {
        if ($this->data) {
            return parent::isValid($context);
        }

        return true;
    }

    public function getValues()
    {
        return array_intersect_key(parent::getValues(), $this->data);
    }
}
