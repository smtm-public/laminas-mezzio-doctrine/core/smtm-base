<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractInfrastructureService implements InfrastructureServiceInterface
{
    public function __construct(
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager
    ) {

    }
}
