<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../../../.env.laminas.laminas-diactoros')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../../../',
        '.env.laminas.laminas-diactoros'
    );
    $dotenv->load();
}

return [
    'uri' => [
        'services' => json_decode(
            EnvHelper::getEnvFromProcessOrSuperGlobal('LAMINAS_LAMINAS_DIACTOROS_URI_SERVICES'),
            true
        ),
    ],
];
