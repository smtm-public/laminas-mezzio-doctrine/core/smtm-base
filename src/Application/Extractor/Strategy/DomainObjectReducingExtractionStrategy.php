<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DomainObjectReducingExtractionStrategy extends AbstractExtractorPluginManagerAwareExtractionStrategy
{
    public const OPTION_KEY_PROPERTY_NAME = 'name';

    public function extract($value, ?object $object = null): object|array|string|float|int|bool|null
    {
        if ($value === null) {
            return null;
        }

        /** @var AbstractDomainObjectExtractor $extractor */
        $extractor = $this->extractorPluginManager->get(
            $this->options[static::OPTION_KEY_EXTRACTOR_NAME],
            $this->options[static::OPTION_KEY_EXTRACTOR_OPTIONS] ?? []
        );

        $result = $extractor->extractValue(
            $this->options[static::OPTION_KEY_PROPERTY_NAME],
            $value,
            $object
        );

        return $result;
    }
}
