<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class JsonEncodeExtractionStrategy extends AbstractExtractionStrategy
{
    public const OPTION_KEY_FLAGS = 'flags';

    public function extract($value, ?object $object = null): ?string
    {
        return json_encode($value, $this->options[static::OPTION_KEY_FLAGS] ?? JSON_UNESCAPED_UNICODE);
    }
}
