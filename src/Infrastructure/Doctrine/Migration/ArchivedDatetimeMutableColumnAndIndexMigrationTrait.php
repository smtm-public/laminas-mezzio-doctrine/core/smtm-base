<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ArchivedDatetimeMutableColumnAndIndexMigrationTrait
{
    protected function addArchivedDatetimeMutableColumnAndIndex(
        Table $table,
        ?string $indexName = null
    ): void {
        $table->addColumn('archived', Types::DATETIME_MUTABLE, ['notNull' => false]);
        $table->addIndex(
            ['archived'],
            substr(
                $indexName ?? 'idx_' . $table->getName() . '_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_INDEX
            )
        );
    }
}
