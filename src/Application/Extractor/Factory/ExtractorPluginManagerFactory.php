<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Factory;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExtractorPluginManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ExtractorPluginManager(
            $container,
            [
                'abstract_factories' => [
                    DomainObjectExtractorAbstractFactory::class,
                ],
            ]
        );
    }
}
