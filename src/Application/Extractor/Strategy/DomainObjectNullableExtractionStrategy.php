<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;

/**
 * @author Dimitar Genov <mitko@smtm.bg>
 */
class DomainObjectNullableExtractionStrategy extends AbstractExtractorPluginManagerAwareExtractionStrategy
{
    public function extract($value, ?object $object = null): ?array
    {
        if ($value === null) {
            return null;
        }

        /** @var AbstractDomainObjectExtractor $extractor */
        $extractor = $this->extractorPluginManager->get(
            $this->options[static::OPTION_KEY_EXTRACTOR_NAME],
            $this->options[static::OPTION_KEY_EXTRACTOR_OPTIONS] ?? []
        );

        try {
            $result = $extractor->extract($value);
        } catch (\Doctrine\ORM\EntityNotFoundException $e) {
            return null;
        }

        return $result;
    }
}
