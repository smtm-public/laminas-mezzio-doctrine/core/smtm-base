<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\DbPersistStrategy\DbPersistStrategyPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface DbPersistStrategyPluginManagerAwareInterface
{
    public function getDbPersistStrategyPluginManager(): DbPersistStrategyPluginManager;
    public function setDbPersistStrategyPluginManager(DbPersistStrategyPluginManager $dbPersistStrategyPluginManager): static;
}
