<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Smtm\Base\Application\Extractor\ExtractorPluginManagerAwareInterface;
use Smtm\Base\Application\Extractor\ExtractorPluginManagerAwareTrait;
use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareTrait;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractServiceAndExtractorPluginManagersAwareExtractionStrategy extends AbstractExtractionStrategy
    implements ApplicationServicePluginManagerAwareInterface, InfrastructureServicePluginManagerAwareInterface,
               ExtractorPluginManagerAwareInterface
{
    use ApplicationServicePluginManagerAwareTrait,
        InfrastructureServicePluginManagerAwareTrait,
        ExtractorPluginManagerAwareTrait;

    public const OPTION_KEY_EXTRACTOR_NAME = 'extractorName';
    public const OPTION_KEY_EXTRACTOR_OPTIONS = 'extractorOptions';
}
