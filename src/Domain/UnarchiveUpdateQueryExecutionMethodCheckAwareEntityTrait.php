<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait UnarchiveUpdateQueryExecutionMethodCheckAwareEntityTrait
{
    protected ?int $unarchiveUpdateQueryExecutionMethodCheck = null;

    public function getUnarchiveUpdateQueryExecutionMethodCheck(): ?int
    {
        return $this->unarchiveUpdateQueryExecutionMethodCheck;
    }

    public function setUnarchiveUpdateQueryExecutionMethodCheck(?int $unarchiveUpdateQueryExecutionMethodCheck): static
    {
        $this->unarchiveUpdateQueryExecutionMethodCheck = $unarchiveUpdateQueryExecutionMethodCheck;

        return $this;
    }
}
