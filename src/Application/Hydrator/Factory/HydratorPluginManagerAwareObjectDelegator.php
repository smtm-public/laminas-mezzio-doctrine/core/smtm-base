<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Factory;

use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Application\Hydrator\HydratorPluginManagerAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class HydratorPluginManagerAwareObjectDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var HydratorPluginManagerAwareInterface $object */
        $object = $callback();

        $object->setHydratorPluginManager($container->get(HydratorPluginManager::class));

        return $object;
    }
}
