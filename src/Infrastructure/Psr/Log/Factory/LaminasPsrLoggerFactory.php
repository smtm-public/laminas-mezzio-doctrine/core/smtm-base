<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Psr\Log\Factory;

use Smtm\Base\Infrastructure\LaminasPsrLogger;
use Laminas\Log\ProcessorPluginManager;
use Laminas\Log\WriterPluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LaminasPsrLoggerFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): LaminasPsrLogger {
        $logConfig = array_merge(
            [
                'writer_plugin_manager' => $container->get(WriterPluginManager::class),
                'processor_plugin_manager' => $container->get(ProcessorPluginManager::class),
            ],
            $options ?? $container->get('config')['log'] ?? []
        );

        return new $requestedName($logConfig);
    }
}
