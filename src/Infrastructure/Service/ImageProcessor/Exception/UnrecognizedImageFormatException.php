<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\ImageProcessor\Exception;

use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UnrecognizedImageFormatException extends RuntimeException
{

}
