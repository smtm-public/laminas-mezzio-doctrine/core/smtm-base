<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy\Factory;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ServiceAndExtractorPluginManagersAwareExtractionStrategyFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get(ApplicationServicePluginManager::class),
            $container->get(InfrastructureServicePluginManager::class),
            $container->get(ExtractorPluginManager::class)
        );
    }
}
