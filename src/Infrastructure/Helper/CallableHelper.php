<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CallableHelper
{
    public static function stringifyCallable(callable $callable): string
    {
        return match (true) {
            is_string($callable) && strpos($callable, '::') => '[static] ' . $callable,
            is_string($callable) => '[function] ' . $callable,
            is_array($callable) && is_object($callable[0]) => '[method] ' . get_class($callable[0]) . '->' . $callable[1],
            is_array($callable) => '[static] ' . $callable[0] . '::' . $callable[1],
            $callable instanceof \Closure => '[closure]' . new \ReflectionFunction($callable),
            is_object($callable) => '[invokable] ' . get_class($callable),
            default => '[unknown]',
        };
    }
}
