<?php

declare(strict_types=1);

namespace Smtm\Base\Command;

use Smtm\Base\Infrastructure\Helper\ThrowableHelper;
use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterSpecificationInterface;
use Smtm\Base\Infrastructure\Laminas\Validator\IsString;
use Smtm\Base\Infrastructure\Service\SpreadsheetService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * NOTE: To clear invisible cells/empty rows in MS Excel:
 *  1. Type the range of cells in the name box/field in the top left corner above the sheet contents (e.g. AB8902:ZZ2000000) and press Enter
 *  2. Go to the Home tab
 *  3. Click on the Format button and choose Format cells... from the dropdown
 *  4. On the Protection tab uncheck the Locked checkbox and press OK
 *  5. Click on the Delete button and choose Delete cells... from the dropdown
 *  6. Select the Entire row option and press OK
 *  7. Click on the Delete button and choose Delete Sheet Rows from the dropdown
 *  8. Save the file
 *
 * NOTE: To clear invisible cells/empty columns in MS Excel:
 *  1. Type the range of cells in the name box/field in the top left corner above the sheet contents (e.g. AN1:ZZ10000) and press Enter
 *  2. Go to the Home tab
 *  3. Click on the Format button and choose Format cells... from the dropdown
 *  4. On the Protection tab uncheck the Locked checkbox and press OK
 *  5. Click on the Delete button and choose Delete cells... from the dropdown
 *  6. Select the Entire column option and press OK
 *  7. Click on the Delete button and choose Delete Sheet Columns from the dropdown
 *  8. Save the file
 *
 * Example:
 * vendor/bin/symfony-console base:sheet-file-stats -vvv --in-file=example.xls
 */
class SheetFileStats extends Command
{
    protected array $inputFilterSpecification = [
        InputFilterSpecificationInterface::INPUT_COLLECTION => [
            'inFile' => [
                'name' => 'inFile',
                'required' => true,
                'allow_empty' => false,
                'validators' => [
                    [
                        'name' => IsString::class,
                    ],
                ],
            ],
        ],
    ];

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('base:sheet-file-stats')
            // the short description shown while running "php bin/console list"
            ->setDescription(
                'Smtm - Base - Sheet File Stats'
            )
            // the full command description shown when running the command with
            // the "--help" option
            //->setHelp('Could write some useful text here')

            //->addArgument('example-argument', InputArgument::OPTIONAL,
            //    'Some example argument')
            ->addOption(
                'upload-log-files',
                'w',
                InputOption::VALUE_NONE,
                'Whether to upload the log files to cloud' . "\n"
            )
            ->addOption(
                'unlink-log-files-after-upload',
                'x',
                InputOption::VALUE_NONE,
                'Whether to delete the log files after uploading to cloud. Works only with --upload-log-files provided.' . "\n"
            )
            ->addOption(
                'debug-queries',
                'y',
                InputOption::VALUE_NONE,
                'Whether to show query debug information' . "\n"
            )
            ->addOption(
                'dry-run',
                'z',
                InputOption::VALUE_OPTIONAL,
                'Only show the differences; do not carry out any updates. This is the default behavior. Can be disabled explicitly by passing as value either the integer 0 or the string "false"' . "\n",
                1
            )

            ->addOption(
                'in-file',
                'a',
                InputOption::VALUE_REQUIRED,
                'Input .xls or .xlsx file path' . "\n"
            )
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$this->logger->info('This is some info.');
        //$this->logger->error('This is an error!');

        $options = [];

        $options['uploadLogFiles'] = $input->getOption('upload-log-files') ? 1 : 0;
        $options['unlinkLogFilesAfterUpload'] = $input->getOption('unlink-log-files-after-upload') ? 1 : 0;
        $options['debugQueries'] = $input->getOption('debug-queries') ? 1 : 0;
        $options['dryRun'] = static::parseOptionValueAsInt($input->getOption('dry-run'));

        $options['inFile'] =
            $input->getOption('in-file')
                ? $input->getOption('in-file')
                : null;

        if (!$this->validate($options, $output)) {
            return static::INVALID;
        }

        /** @var SpreadsheetService $spreadsheetService */
        $spreadsheetService = $this->infrastructureServicePluginManager->get(SpreadsheetService::class);
        $spreadsheetInFile = [];

        try {
            $spreadsheetInFile['reader'] = $spreadsheetService->createReaderFromFile($options['inFile']);
            $spreadsheetInFile['sheets'] = $spreadsheetInFile['reader']->getAllSheets();

            foreach ($spreadsheetInFile['sheets'] as $sheetIndex => $sheet) {
                $spreadsheetInFile['sheetHeadingsRaw'][$sheetIndex] = $spreadsheetService->getWorksheetHeadingsRowCellValuesAsArray($sheet);
                $spreadsheetInFile['sheetHeadingsEmptyKeysUnfiltered'][$sheetIndex] = array_map(
                    fn (int $key, ?string $value) => $value === null ? $key : null,
                    array_keys($spreadsheetInFile['sheetHeadingsRaw'][$sheetIndex]),
                    $spreadsheetInFile['sheetHeadingsRaw'][$sheetIndex]
                );
                $spreadsheetInFile['sheetHeadingsEmptyKeysFiltered'][$sheetIndex] = array_filter($spreadsheetInFile['sheetHeadingsEmptyKeysUnfiltered'][$sheetIndex]);
                $spreadsheetInFile['sheetHeadings'][$sheetIndex] = array_filter(
                    $spreadsheetInFile['sheetHeadingsRaw'][$sheetIndex],
                    fn (?string $content) => $content !== null
                );

                $sheetCoordinates = $sheet->getCoordinates();
                $sheetCoordinateFirst = '';
                $sheetCoordinateLast = '';

                if (!empty($sheetCoordinates)) {
                    $sheetCoordinateFirst = $sheetCoordinates[array_key_first($sheetCoordinates)];
                    $sheetCoordinateLast = $sheetCoordinates[array_key_last($sheetCoordinates)];
                }

                unset($sheetCoordinates);
                $sheetRowHighest = $sheet->getHighestRow();
                $sheetDataRowHighest = $sheet->getHighestDataRow();
                $sheetColumnHighest = $sheet->getHighestColumn();
                $sheetDataColumnHighest = $sheet->getHighestDataColumn();
                $sheetColumnIndexHighest = SpreadsheetService::getColumnIndexFromString($sheetColumnHighest);
                //$sheetCellValue = $sheet->getCellByColumnAndRow($sheetColumnIndexHighest , $sheetRowHighest);

                $this->getLogger()->info('=========================================================');
                $this->getLogger()->info('Worksheet title: ' . $sheet->getTitle());
                $this->getLogger()->info('Worksheet code name: ' . $sheet->getCodeName());
                $this->getLogger()->info('Worksheet dimension: ' . $sheet->calculateWorksheetDimension());
                $this->getLogger()->info('Worksheet data dimension: ' . $sheet->calculateWorksheetDataDimension());
                $this->getLogger()->info('Worksheet coordinates range: ' . $sheetCoordinateFirst . ' - ' . $sheetCoordinateLast);
                $this->getLogger()->info('Column dimensions: ' . count($sheet->getColumnDimensions()));
                $this->getLogger()->info('Highest row: ' . $sheetRowHighest);
                $this->getLogger()->info('Highest data row: ' . $sheetDataRowHighest);
                $this->getLogger()->info('Highest column: ' . $sheetColumnHighest);
                $this->getLogger()->info('Highest column index: ' . $sheetColumnIndexHighest);
                $this->getLogger()->info('Highest data column: ' . $sheetDataColumnHighest);
                $this->getLogger()->info('Highest row and column: ' . "\n" . var_export($sheet->getHighestRowAndColumn(), true));
                $this->getLogger()->info('=========================================================');
            }
        } catch (\Throwable $t) {
            $this->getLogger()->err(ThrowableHelper::formatAsString($t));
        }

        return static::SUCCESS;
    }
}
