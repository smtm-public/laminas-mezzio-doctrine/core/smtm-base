<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception\RemoteServiceResponseExceptionInterface;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ThrowableHelper
{
    public const DEFAULT_OPTIONS = [
        'debug' => false,
        'traceLevel' => 30,
        'traceArgumentValueLength' => 65535,
        'traceArgumentArrayValueElementCount' => 10,
    ];

    /**
     * Creates and returns a callable error handler that raises exceptions.
     *
     * Only raises exceptions for errors that are within the error_reporting mask.
     */
    public static function createErrorHandler(): callable
    {
        /**
         * @throws \ErrorException if error is not within the error_reporting mask.
         */
        return function (int $errno, string $errstr, string $errfile, int $errline): void {
            if (!(error_reporting() & $errno)) {
                // error_reporting does not include this error
                return;
            }

            throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
        };
    }

    public static function formatAsString(
        \Throwable $t
    ): string {
        $output = '---------------------' . "\n";
        $output .= static::flatten($t);
        $output .= '---------------------' . "\n";

        return $output;
    }

    public static function flatten(\Throwable $t, int $level = 0): string
    {
        $output = str_repeat('    ', $level) . 'Class: ' . get_class($t) . "\n";
        $output .= str_repeat('    ', $level) . 'Message: ' . $t->getMessage() . "\n";
        $output .= str_repeat('    ', $level) . 'File (Line): ' . $t->getFile() . '(' . $t->getLine() . ')' . "\n";

        if ($t instanceof RemoteServiceResponseExceptionInterface) {
            $output .= str_repeat('    ', $level) . 'Response status: ' . $t->getResponseStatus();
            $output .= str_repeat('    ', $level) . 'Response body: ' . $t->getResponseBody();
            $output .= str_repeat('    ', $level) . 'Response body (parsed): ' . "\n";
            $output .=
                preg_replace(
                    '#^(.*)#',
                    str_repeat('    ', $level) . '$1',
                    (
                        is_array($t->getResponseBodyParsed())
                            ? json_encode($t->getResponseBodyParsed(), JSON_PRETTY_PRINT)
                            : ($t->getResponseBodyParsed() ?? '')
                    )
                );
        }

        $output .= str_repeat('    ', $level) . 'Trace:' . "\n";
        $output .= preg_replace('#^(.*)#', str_repeat('    ', $level) . '$1', $t->getTraceAsString()) . "\n";

        if ($t->getPrevious()) {
            $output .= str_repeat('    ', $level) . 'Previous:' . "\n";
            $output .= static::flatten($t->getPrevious(), $level + 1);
        }

        return $output . "\n";
    }

    #[ArrayShape([
        'name' => 'string',
        'status' => 'int',
        'message' => 'null|string',
        'code' => 'int|null',
        'details' => 'array|null',
        'type' => 'string|null',
        'file:line' => 'string|null',
        'previous' => 'array|null',
        'trace' => 'array|null',
    ])] public static function formatAsArray(
        \Throwable $t,
        int $httpStatusCode,
        array $options = self::DEFAULT_OPTIONS
    ): array {
        $type = null;
        $message = null;
        $details = null;
        $file = null;
        $line = null;
        $trace = null;
        $previous = null;

        if ($options['debug']) {
            $type = get_class($t);
            $message = $t->getMessage();
            $file = $t->getFile();
            $line = $t->getLine();
            $trace = static::serializeTrace($t->getTrace(), $options);
            $previous = static::serializePrevious($t->getPrevious(), $options);
        }

        return static::serialize(
            HttpHelper::STATUS_CODES_DESCRIPTIONS[$httpStatusCode],
            $httpStatusCode,
            $message,
            $t->getCode(),
            $details,
            $type,
            $file,
            $line,
            $previous,
            $trace
        );
    }

    /**
     * @SWG\Definition(
     *     definition="Error",
     *     type="object",
     *     @SWG\Property(
     *          property="name",
     *          type="string"
     *     ),
     *     @SWG\Property(
     *          property="status",
     *          type="integer",
     *          format="int64",
     *          description="HTTP status code of the error message"
     *     ),
     *     @SWG\Property(
     *          property="message",
     *          type="string"
     *     ),
     *     @SWG\Property(
     *          property="code",
     *          type="string",
     *          description=SWAGGER_ERROR_CODES
     *     ),
     *     @SWG\Property(
     *          property="trace",
     *          type="string"
     *     ),
     *   ),
     * )
     */
    #[ArrayShape([
        'name' => 'string',
        'status' => 'int',
        'message' => 'null|string',
        'code' => 'int|null',
        'details' => 'array|null',
        'type' => 'string|null',
        'file:line' => 'string|null',
        'previous' => 'array|null',
        'trace' => 'array|null',
    ])] public static function serialize(
        string $name,
        int $status,
        ?string $message = null,
        ?int $code = null,
        ?array $details = null,
        ?string $type = null,
        ?string $file = null,
        ?int $line = null,
        ?array $previous = null,
        ?array $trace = null
    ): array {
        $serialized = [
            'name' => $name,
            'status' => $status,
        ];

        if ($message !== null) {
            $serialized += [
                'message' => $message,
            ];
        }

        $serialized += [
            'code' => $code,
        ];

        if ($details !== null) {
            $serialized += [
                'details' => $details,
            ];
        }

        if ($type !== null) {
            $serialized += [
                'type' => $type,
            ];
        }

        if ($file !== null) {
            $serialized += [
                'file:line' => $file . ':' . $line,
            ];
        } else {
            $serialized += [
                'file:line' => '{closure}',
            ];
        }

        if ($previous !== null) {
            $serialized += [
                'previous' => $previous,
            ];
        }

        if ($trace !== null) {
            $serialized += [
                'trace' => $trace,
            ];
        }

        return $serialized;
    }

    public static function serializeTrace(
        array $trace,
        array $options = self::DEFAULT_OPTIONS
    ): array {
        $stackTrace = [];

        foreach (array_slice($trace, 0, $options['traceLevel']) as $traceElement) {
            $stackTrace[] = [
                'file:line' => isset($traceElement['file'])
                    ? $traceElement['file'] . ':' . $traceElement['line']
                    : '{closure}',
                'callee' => ($traceElement['type'] ?? '') !== ''
                    ? $traceElement['class'] . $traceElement['type'] . $traceElement['function']
                    : null,
                'args' => isset($traceElement['args'])
                    ? array_combine(
                        array_keys($traceElement['args']),
                        array_map(
                            fn($value) => is_resource($value) ? '{resource}' : static::serializeArg($value, $options),
                            $traceElement['args']
                        )
                    )
                    : null,
            ];
        }

        return $stackTrace;
    }

    public static function serializeArg(
        object|callable|array|string|float|int|bool|null $value,
        array $options = self::DEFAULT_OPTIONS
    ): array|string|float|int|bool|null {
        if (is_object($value)) {
            return '{instanceof ' . get_class($value) . '}';
        }

        if (is_resource($value)) {
            return '{resource}';
        }

        if (is_callable($value)) {
            return '{callable}';
        }

        if (is_array($value)) {
            return str_replace(
                "\n",
                '',
                substr(
                    var_export(
                        array_combine(
                            array_keys(array_slice($value, 0, $options['traceArgumentArrayValueElementCount'])),
                            array_map(
                                fn($value) => substr(
                                    is_resource($value) ? '{resource}' : static::serializeArg($value, $options),
                                    0,
                                    $options['traceArgumentValueLength']
                                ),
                                array_slice($value, 0, $options['traceArgumentArrayValueElementCount'])
                            )
                        ),
                        true
                    ),
                    0,
                    $options['traceArgumentValueLength']
                )
            );
        }

        return substr(var_export($value, true), 0, $options['traceArgumentValueLength']);
    }

    public static function serializePrevious(
        ?\Throwable $previous,
        array $options = self::DEFAULT_OPTIONS
    ): ?array {
        if ($previous === null) {
            return null;
        }

        $data = [
            'message' => $previous->getMessage(),
            'code' => $previous->getCode(),
            'type' => get_class($previous),
            'file:line' => $previous->getFile() !== null
                ? $previous->getFile() . ':' . $previous->getLine()
                : '{closure}',
        ];

        if ($previous instanceof RemoteServiceResponseExceptionInterface) {
            $data['responseStatus'] = $previous->getResponseStatus();
            $data['responseBody'] = $previous->getResponseBody();
            $data['responseBodyParsed'] = $previous->getResponseBodyParsed();
        }

        $data += [
            'previous' => static::serializePrevious($previous->getPrevious(), $options),
            'trace' => static::serializeTrace($previous->getTrace(), $options),
        ];

        return $data;
    }

    public static function isLevelFatal($level): bool
    {
        $errors = E_ERROR;
        $errors |= E_PARSE;
        $errors |= E_CORE_ERROR;
        $errors |= E_CORE_WARNING;
        $errors |= E_COMPILE_ERROR;
        $errors |= E_COMPILE_WARNING;

        return ($level & $errors) > 0;
    }
}
