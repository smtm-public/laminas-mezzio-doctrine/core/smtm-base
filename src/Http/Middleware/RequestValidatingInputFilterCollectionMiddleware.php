<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware;

use Smtm\Base\Http\Exception\BadRequestException;
use Smtm\Base\Http\InputFilter\AbstractRequestValidatingInputFilterCollection;
use Smtm\Base\Http\InputFilter\RequestValidatingInputFilterCollectionPluginManager;
use Mezzio\Router\RouteResult;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RequestValidatingInputFilterCollectionMiddleware implements MiddlewareInterface
{
    public const REQUEST_ATTRIBUTE_NAME_REQUEST_VALIDATING_INPUT_FILTER_COLLECTION =
        'requestValidatingInputFilterCollection';

    protected RequestValidatingInputFilterCollectionPluginManager $requestValidatingInputFilterCollectionManager;

    public function __construct(
        RequestValidatingInputFilterCollectionPluginManager $requestValidatingInputFilterCollectionManager
    ) {
        $this->requestValidatingInputFilterCollectionManager = $requestValidatingInputFilterCollectionManager;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $request = $request->withAttribute(
            self::REQUEST_ATTRIBUTE_NAME_REQUEST_VALIDATING_INPUT_FILTER_COLLECTION,
            null
        );
        $routeResult = $request->getAttribute(RouteResult::class);

        if ($routeResult && $routeResult->isSuccess()) {
            $matchedRoute = $routeResult->getMatchedRoute();

            if (array_key_exists('requestValidatingInputFilterCollectionName', $matchedRoute->getOptions())
                && $matchedRoute->getOptions()['requestValidatingInputFilterCollectionName'] !== null
            ) {
                /** @var AbstractRequestValidatingInputFilterCollection $requestValidatingInputFilterCollection */
                $requestValidatingInputFilterCollection = $this->requestValidatingInputFilterCollectionManager->get(
                    $matchedRoute->getOptions()['requestValidatingInputFilterCollectionName']
                );
                $request = $request->withAttribute(
                    self::REQUEST_ATTRIBUTE_NAME_REQUEST_VALIDATING_INPUT_FILTER_COLLECTION,
                    $requestValidatingInputFilterCollection
                );
                $validationResultSet =
                    $requestValidatingInputFilterCollection
                        ->setDataFromRequest($request)
                        ->validate();

                foreach ($validationResultSet as $validationResult) {
                    if (!$validationResult[AbstractRequestValidatingInputFilterCollection::KEY_RESULTS_IS_VALID]) {
                        throw new BadRequestException(
                            '',
                            0,
                            null,
                            $validationResult[AbstractRequestValidatingInputFilterCollection::KEY_RESULTS_MESSAGES]
                        );
                    }
                }
            }
        }

        return $handler->handle($request);
    }
}
