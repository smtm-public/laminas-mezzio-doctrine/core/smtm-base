<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Log;

use Smtm\Base\Infrastructure\LaminasPsrLogger;
use Laminas\Log\Filter\Priority;
use Laminas\Log\Formatter\Simple;
use Laminas\Log\Logger;
use Laminas\Log\Writer\AbstractWriter;
use Laminas\Log\Writer\Stream;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConsoleAndFileLogger extends LaminasPsrLogger
{
    public const OPTION_KEY_LOG_TO_CONSOLE = 'logToConsole';

    protected bool $logToConsole = true;

    public function __construct($options = null)
    {
        parent::__construct($options);

        $this->logToConsole = (bool) ($options[self::OPTION_KEY_LOG_TO_CONSOLE] ?? true);

        if ($this->logToConsole) {
            /** @var AbstractWriter $writerStdout */
            $writerStdout = $this->writerPlugin(
                Stream::class,
                [
                    'stream' => 'php://stdout',
                ]
            );
            $writerStdout->addFilter(
                $writerStdout->filterPlugin(
                    Priority::class,
                    [
                        'operator' => '>=',
                        'priority' => Logger::INFO,
                    ]
                )
            );
            $writerStdout->setFormatter(
                $writerStdout->formatterPlugin(
                    Simple::class,
                    $this->formatterConfig
                )
            );
            $this->addWriter($writerStdout);

            /** @var AbstractWriter $writerStderr */
            $writerStderr = $this->writerPlugin(
                Stream::class,
                [
                    'stream' => 'php://stderr',
                ]
            );
            $writerStderr->addFilter(
                $writerStderr->filterPlugin(
                    Priority::class,
                    [
                        'operator' => '<=',
                        'priority' => Logger::NOTICE,
                    ]
                )
            );
            $writerStderr->setFormatter(
                $writerStderr->formatterPlugin(
                    Simple::class,
                    $this->formatterConfig
                )
            );
            $this->addWriter($writerStderr);
        }
    }
}
