<?php

declare(strict_types=1);

namespace IntegrationTest\Infrastructure\Repositories;

use Codeception\Test\Unit;
use DateInterval;
use DateTime;
use Smtm\Base\Infrastructure\Repository\Exception\DeleteRecordException;
use Smtm\Base\Infrastructure\Repository\Exception\NotFoundRecordException;
use Smtm\Base\Infrastructure\Repository\Exception\SaveRecordException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Doctrine\ORM\Tools\SchemaTool;
use Exception;
use IntegrationTester;
use Stub\EntityRepositoryStub;
use Stub\EntityStub;
use Stub\Relation\CommonRelationStub;
use Stub\Relation\ManyToOneRelationStub;
use Stub\Relation\OneToManyRelationStub;

/**
 * @author Grigor Milchev <grigor@smtm.bg>
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * @property IntegrationTester $tester
 */
class AbstractDoctrineBaseRepositoryTest extends Unit
{
    private EntityRepositoryStub $entityRepository;
    private SchemaTool $schemaTool;
    private ClassMetadata $entityStubMetadata;
    private ClassMetadata $oneToManyRelationStubMetadata;
    private ClassMetadata $manyToOneRelationStubMetadata;
    private ClassMetadata $commonRelationStubMetadata;

    public function testSave(): void
    {
        $entity = new EntityStub();
        $entity->setProperty('test');

        $this->entityRepository->save($entity);

        $this->assertIsInt($entity->getId());

        $foundEntity = $this->entityRepository->findOneBy(['property' => $entity->getProperty()]);

        $this->assertInstanceOf(EntityStub::class, $foundEntity);
    }

    /**
     * Provides test cases `EntityManager` mocked methods.
     *
     * @return array
     */
    public function getEntityManagerMethodsWithExceptionsWhenSaving(): array
    {
        return [
            [
                [
                    'persist' => function () {
                        throw new ORMException();
                    },
                ],
            ],
            [
                [
                    'persist' => function () {
                        throw new ORMInvalidArgumentException();
                    },
                ],
            ],
            [
                [
                    'flush' => function () {
                        throw new OptimisticLockException('', new \stdClass());
                    },
                ],
            ],
            [
                [
                    'flush' => function () {
                        throw new ORMException();
                    },
                ],
            ],
        ];
    }

    /**
     * Tests if AbstractDoctrineRepository catches Doctrine's exceptions when
     * removing an entity.
     *
     * @noinspection PhpDocMissingThrowsInspection
     *
     * @dataProvider getEntityManagerMethodsWithExceptionsWhenSaving
     *
     * @param array $mockedMethods
     */
    public function testSaveCatchesDoctrineExceptions(array $mockedMethods): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $entity = $this->makeEmpty(EntityStub::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $entityRepository = $this->getEntityRepositoryStub($mockedMethods);

        $this->expectException(SaveRecordException::class);
        $entityRepository->save($entity);
    }

    public function testRemove(): void
    {
        $entity = $this->tester->have(EntityStub::class, ['property' => 'test']);

        $this->entityRepository->remove($entity);

        $this->expectException(NotFoundRecordException::class);
        $this->entityRepository->findOneBy(['property' => $entity->getProperty()]);
    }

    /**
     * Provides test cases `EntityManager` mocked methods.
     *
     * @return array
     */
    public function getEntityManagerMethodsWithExceptionsWhenDeleting(): array
    {
        return [
            [
                [
                    'remove' => function () {
                        throw new ORMException();
                    },
                ],
            ],
            [
                [
                    'remove' => function () {
                        throw new ORMInvalidArgumentException();
                    },
                ],
            ],
            [
                [
                    'flush' => function () {
                        throw new OptimisticLockException('', new \stdClass());
                    },
                ],
            ],
            [
                [
                    'flush' => function () {
                        throw new ORMException();
                    },
                ],
            ],
        ];
    }

    /**
     * Tests if AbstractDoctrineRepository catches Doctrine's exceptions when
     * removing an entity.
     *
     * @noinspection PhpDocMissingThrowsInspection
     *
     * @dataProvider getEntityManagerMethodsWithExceptionsWhenDeleting
     *
     * @param array $mockedMethods
     */
    public function testRemoveCatchesDoctrineExceptions(array $mockedMethods): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $entity = $this->makeEmpty(EntityStub::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $entityRepository = $this->getEntityRepositoryStub($mockedMethods);
        $this->expectException(DeleteRecordException::class);
        $entityRepository->remove($entity);
    }

    public function testFind(): void
    {
        $entity = $this->tester->have(EntityStub::class);

        $entityResult = $this->entityRepository->find($entity->getId());

        $this->assertInstanceOf(EntityStub::class, $entityResult);

        $this->expectException(NotFoundRecordException::class);

        $this->entityRepository->findOneBy(['property' => 'invalid']);
    }

    public function testFindOneBy(): void
    {
        $entity = $this->tester->have(EntityStub::class, ['property' => 'test']);

        $foundEntity = $this->entityRepository->findOneBy(['property' => $entity->getProperty()]);

        $this->assertInstanceOf(EntityStub::class, $foundEntity);

        $this->expectException(NotFoundRecordException::class);

        $this->entityRepository->findOneBy(['property' => 'invalid']);
    }

    /**
     * Tests findOneBy() method with nested criteria and referring to related entities
     */
    public function testFindOneByWithNestedCriteriaAndRelations(): void
    {
        $stringValue0 = 'true';
        $textValue0 = 'random text';
        $intValue0 = 1;
        $intValue1 = -1;
        $decimalValue0 = 1.5;
        $booleanTrue = true;
        $entity0 = $this->tester->have(
            EntityStub::class,
            [
                'property' => $stringValue0,
                'boolProperty' => $booleanTrue,
                'intProperty' => $intValue0,
                'decimalProperty' => $decimalValue0,
                'textProperty' => $textValue0,
            ]
        );
        $relation0 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'archived' => false,
            ]
        );
        $relation1 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'archived' => false,
            ]
        );
        $relation2 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'archived' => true,
            ]
        );
        $entity1 = $this->tester->have(
            EntityStub::class,
            [
                'property' => $stringValue0,
                'boolProperty' => $booleanTrue,
                'intProperty' => 0,
                'decimalProperty' => 0.1,
                'textProperty' => 'random text 2'
            ]
        );
        $relation3 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity1,
                'archived' => true,
            ]
        );
        $entity2 = $this->tester->have(
            EntityStub::class,
            [
                'property' => 'false',
                'boolProperty' => $booleanTrue,
                'intProperty' => 0,
                'decimalProperty' => 0.2,
                'textProperty' => null
            ]
        );
        $entity3 = $this->tester->have(
            EntityStub::class,
            [
                'property' => 'false',
                'boolProperty' => $booleanTrue,
                'intProperty' => 0,
                'decimalProperty' => 0.3,
                'textProperty' => 'will fetch'
            ]
        );
        $entity4 = $this->tester->have(
            EntityStub::class,
            [
                'property' => 'false',
                'boolProperty' => $booleanTrue,
                'intProperty' => 0,
                'decimalProperty' => 0.4,
                'textProperty' => 'will not fetch'
            ]
        );

        $foundEntity = $this->entityRepository->findOneBy(
            [
                'and',
                ['boolProperty' => $booleanTrue],
                [
                    'and',
                    ['=', 'property', $stringValue0],
                    ['=', 'oneToManyRelations.archived', false],
                    [
                        'or',
                        ['=', 'intProperty', $intValue1],
                        ['in', 'decimalProperty', [null, $decimalValue0]],
                    ],
                ],
            ]
        );

        $this->assertInstanceOf(EntityStub::class, $foundEntity);
        $this->assertEquals($stringValue0, $foundEntity->getProperty());
        $this->assertEquals($booleanTrue, $foundEntity->getBoolProperty());
        $this->assertEquals($intValue0, $foundEntity->getIntProperty());
        $this->assertEquals($decimalValue0, $foundEntity->getDecimalProperty());
        $this->assertEquals($textValue0, $foundEntity->getTextProperty());

        $this->expectException(NotFoundRecordException::class);

        $foundEntity = $this->entityRepository->findOneBy(
            [
                'and',
                ['=', 'boolProperty', $booleanTrue],
                [
                    'and',
                    ['=', 'property', $stringValue0],
                    [
                        'or',
                        ['=', 'intProperty', $intValue1],
                        ['in', 'decimalProperty', [null, $intValue1]],
                    ],
                ],
            ]
        );
    }

    public function testFindAll(): void
    {
        $entitiesCount = 3;
        $this->tester->haveMultiple(EntityStub::class, $entitiesCount);

        $result = $this->entityRepository->findAll();

        $this->assertEquals(count($result), $entitiesCount);
    }

    public function testFindBy(): void
    {
        $this->tester->haveMultiple(EntityStub::class, 5);
        $specificEntitiesCount = 2;
        $this->tester->haveMultiple(EntityStub::class, $specificEntitiesCount, ['property' => 'test']);

        $result = $this->entityRepository->findBy(['property' => 'test']);

        $this->assertEquals(count($result), $specificEntitiesCount);
    }

    /**
     * Tests findAllBy() method with nested criteria and referring to related entities
     *
     * @throws Exception
     */
    public function testFindByWithNestedCriteriaAndRelations(): void
    {
        $stringValue0 = 'true';
        $intValue0 = 1;
        $intValue1 = 2;
        $booleanTrue = true;
        $entity0 = $this->tester->have(
            EntityStub::class,
            [
                'property' => $stringValue0,
                'boolProperty' => $booleanTrue,
                'intProperty' => $intValue0,
                'decimalProperty' => null,
                'textProperty' => 'random text'
            ]
        );
        $relation0 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'archived' => false,
            ]
        );
        $relation1 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'archived' => false,
            ]
        );
        $relation2 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'archived' => true,
            ]
        );
        $entity1 = $this->tester->have(
            EntityStub::class,
            [
                'property' => $stringValue0,
                'boolProperty' => $booleanTrue,
                'intProperty' => $intValue1,
                'decimalProperty' => 0.1,
                'textProperty' => 'random text 2'
            ]
        );
        $relation3 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity1,
                'archived' => true,
            ]
        );
        $relation4 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity1,
                'archived' => false,
            ]
        );
        $relation5 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity1,
                'archived' => true,
            ]
        );
        $entity2 = $this->tester->have(
            EntityStub::class,
            [
                'property' => 'false',
                'boolProperty' => $booleanTrue,
                'intProperty' => 0,
                'decimalProperty' => 0.2,
                'textProperty' => null
            ]
        );
        $entity3 = $this->tester->have(
            EntityStub::class,
            [
                'property' => null,
                'boolProperty' => $booleanTrue,
                'intProperty' => 0,
                'decimalProperty' => 0.3,
                'textProperty' => 'will fetch'
            ]
        );
        $entity4 = $this->tester->have(
            EntityStub::class,
            [
                'property' => 'false',
                'boolProperty' => false,
                'intProperty' => 0,
                'decimalProperty' => 0.4,
                'textProperty' => 'will not fetch'
            ]
        );

        $results = $this->entityRepository->findBy(
            [
                'and',
                ['=', 'boolProperty', $booleanTrue],
                [
                    'and',
                    ['in', 'property', [null, $stringValue0]],
                    ['=', 'oneToManyRelations.archived', false],
                    [
                        'or',
                        ['=', 'intProperty', $intValue0],
                        ['=', 'intProperty', $intValue1],
                    ],
                ],
            ]
        );

        $this->assertEquals(2, count($results));

        foreach ($results as $foundEntity) {
            $this->assertTrue(
                $foundEntity->getBoolProperty() == $booleanTrue
                && in_array($foundEntity->getProperty(), [null, $stringValue0], true)
                && (
                    $foundEntity->getIntProperty() === $intValue0
                    || $foundEntity->getIntProperty() === $intValue1
                )
            );
        }
    }

    /**
     * Tests findAllBy() method with order
     */
    public function testFindByWithOrder()
    {
        $this->tester->haveMultiple(EntityStub::class, 1, ['property' => 'z']);
        $this->tester->haveMultiple(EntityStub::class, 1, ['property' => 'a']);
        $this->tester->haveMultiple(EntityStub::class, 1, ['property' => 'c']);
        $this->tester->haveMultiple(EntityStub::class, 1, ['property' => 'n']);

        $result = $this->entityRepository->findBy([], ['property' => 'ASC']);
        $data = $result;

        $this->assertLessThan(0, strcmp($data[0]->getProperty(), $data[1]->getProperty()));

        $result = $this->entityRepository->findBy([], ['property' => 'DESC']);
        $data = $result;

        $this->assertGreaterThan(0, strcmp($data[0]->getProperty(), $data[1]->getProperty()));
    }

    /**
     * Tests findAllBy() method with order also referring to relation property
     *
     * @throws Exception
     */
    public function testFindByWithOrderByRelationProperty(): void
    {
        $letterA = 'a';
        $letterC = 'c';
        $letterN = 'n';
        $letterP = 'p';
        $letterQ = 'q';
        $letterR = 'r';
        $letterS = 's';
        $letterT = 't';
        $letterU = 'u';
        $letterZ = 'z';
        $entity0 = $this->tester->have(EntityStub::class, ['property' => $letterZ]);
        $manyToOneRelation0 = $this->tester->have(
            ManyToOneRelationStub::class,
            [
                'entity' => $entity0,
                'property' => $letterQ,
            ]
        );
        $oneToManyRelation0 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'property' => $letterQ,
            ]
        );
        $oneToManyRelation1 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'property' => $letterP,
            ]
        );
        $entity1 = $this->tester->have(EntityStub::class, ['property' => $letterA]);
        $manyToOneRelation1 = $this->tester->have(
            ManyToOneRelationStub::class,
            [
                'entity' => $entity1,
                'property' => $letterQ,
            ]
        );
        $oneToManyRelation2 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity1,
                'property' => $letterR,
            ]
        );
        $oneToManyRelation3 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity1,
                'property' => $letterS,
            ]
        );
        $entity2 = $this->tester->have(EntityStub::class, ['property' => $letterC]);
        $manyToOneRelation0 = $this->tester->have(
            ManyToOneRelationStub::class,
            [
                'entity' => $entity2,
                'property' => $letterT,
            ]
        );
        $oneToManyRelation4 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity2,
                'property' => $letterT,
            ]
        );
        $oneToManyRelation5 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity2,
                'property' => $letterU,
            ]
        );
        $entity3 = $this->tester->have(EntityStub::class, ['property' => $letterC]);
        $manyToOneRelation3 = $this->tester->have(
            ManyToOneRelationStub::class,
            [
                'entity' => $entity3,
                'property' => $letterC,
            ]
        );
        $oneToManyRelation6 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity3,
                'property' => $letterU,
            ]
        );
        $oneToManyRelation7 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity3,
                'property' => $letterT,
            ]
        );

        $result = $this->entityRepository->findBy([], ['property' => 'ASC']);
        $this->assertEquals($letterA, $result[0]->getProperty());
        $this->assertEquals($letterC, $result[1]->getProperty());
        $this->assertEquals($letterC, $result[2]->getProperty());
        $this->assertEquals($letterZ, $result[3]->getProperty());

        $result = $this->entityRepository->findBy([], ['property' => 'DESC']);
        $this->assertEquals($letterZ, $result[0]->getProperty());
        $this->assertEquals($letterC, $result[1]->getProperty());
        $this->assertEquals($letterC, $result[2]->getProperty());
        $this->assertEquals($letterA, $result[3]->getProperty());

        $result = $this->entityRepository->findBy([], ['manyToOneRelation.property' => 'DESC', 'property' => 'ASC']);
        $this->assertEquals($letterC, $result[0]->getProperty());
        $this->assertEquals($letterT, $result[0]->getManyToOneRelation()->getProperty());
        $this->assertEquals($letterA, $result[1]->getProperty());
        $this->assertEquals($letterQ, $result[1]->getManyToOneRelation()->getProperty());
        $this->assertEquals($letterZ, $result[2]->getProperty());
        $this->assertEquals($letterQ, $result[2]->getManyToOneRelation()->getProperty());
        $this->assertEquals($letterC, $result[3]->getProperty());
        $this->assertEquals($letterC, $result[3]->getManyToOneRelation()->getProperty());
    }

    /**
     * Tests findBy() method #2
     */
    public function testFindBy2(): void
    {
        $this->tester->haveMultiple(EntityStub::class, 5);

        $limit = 1;
        $offset = 0;

        /** @noinspection PhpUnhandledExceptionInspection */
        $result = $this->entityRepository->findBy([], null, $limit, $offset);

        $this->assertEquals($limit, count($result));
    }

    /**
     * Tests findBy() method with criteria
     */
    public function testFindByWithCriteria(): void
    {
        $this->tester->haveMultiple(EntityStub::class, 5);
        $this->tester->haveMultiple(EntityStub::class, 2, ['property' => 'test']);

        $limit = 3;
        $offset = 0;

        /** @noinspection PhpUnhandledExceptionInspection */
        $result = $this->entityRepository->findBy(['property' => 'test'], null, $limit, $offset);

        $this->assertEquals(2, count($result));
    }

    /**
     * Tests findBy() method with nested criteria and referring to related entities #2
     */
    public function testFindByWithNestedCriteriaAndRelations2(): void
    {
        $stringValue0 = 'true';
        $intValue0 = 1;
        $intValue1 = 2;
        $booleanTrue = true;
        $entity0 = $this->tester->have(
            EntityStub::class,
            [
                'property' => $stringValue0,
                'boolProperty' => $booleanTrue,
                'intProperty' => $intValue0,
                'decimalProperty' => null,
                'textProperty' => 'random text'
            ]
        );
        $relation0 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'archived' => false,
            ]
        );
        $relation1 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'archived' => false,
            ]
        );
        $relation2 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'archived' => true,
            ]
        );
        $entity1 = $this->tester->have(
            EntityStub::class,
            [
                'property' => $stringValue0,
                'boolProperty' => $booleanTrue,
                'intProperty' => $intValue1,
                'decimalProperty' => 0.1,
                'textProperty' => 'random text 2'
            ]
        );
        $relation3 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity1,
                'archived' => true,
            ]
        );
        $relation4 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity1,
                'archived' => false,
            ]
        );
        $relation5 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity1,
                'archived' => true,
            ]
        );
        $entity2 = $this->tester->have(
            EntityStub::class,
            [
                'property' => 'false',
                'boolProperty' => $booleanTrue,
                'intProperty' => 0,
                'decimalProperty' => 0.2,
                'textProperty' => null
            ]
        );
        $entity3 = $this->tester->have(
            EntityStub::class,
            [
                'property' => null,
                'boolProperty' => $booleanTrue,
                'intProperty' => 0,
                'decimalProperty' => 0.3,
                'textProperty' => 'will fetch'
            ]
        );
        $entity4 = $this->tester->have(
            EntityStub::class,
            [
                'property' => 'false',
                'boolProperty' => false,
                'intProperty' => 0,
                'decimalProperty' => 0.4,
                'textProperty' => 'will not fetch'
            ]
        );

        $limit = 3;
        $offset = 0;

        /** @noinspection PhpUnhandledExceptionInspection */
        $results = $this->entityRepository->findBy(
            [
                'and',
                ['=', 'boolProperty', $booleanTrue],
                [
                    'and',
                    ['in', 'property', [null, $stringValue0]],
                    ['=', 'oneToManyRelations.archived', false],
                    [
                        'or',
                        ['=', 'intProperty', $intValue0],
                        ['=', 'intProperty', $intValue1],
                    ],
                ],
            ],
            null,
            $limit,
            $offset
        );

        $this->assertEquals(2, count($results));

        foreach ($results as $foundEntity) {
            $this->assertTrue(
                $foundEntity->getBoolProperty() == $booleanTrue
                && in_array($foundEntity->getProperty(), [null, $stringValue0], true)
                && (
                    $foundEntity->getIntProperty() === $intValue0
                    || $foundEntity->getIntProperty() === $intValue1
                )
            );
        }
    }

    /**
     * Tests findBy() method with order #2
     */
    public function testFindByWithOrder2(): void
    {
        $this->tester->haveMultiple(EntityStub::class, 1, ['property' => 'z']);
        $this->tester->haveMultiple(EntityStub::class, 1, ['property' => 'a']);
        $this->tester->haveMultiple(EntityStub::class, 1, ['property' => 'c']);
        $this->tester->haveMultiple(EntityStub::class, 1, ['property' => 'n']);

        $limit = 3;
        $offset = 0;

        /** @noinspection PhpUnhandledExceptionInspection */
        $result = $this->entityRepository->findBy([], ['property' => 'ASC'], $limit, $offset);

        $this->assertEquals(count($result), $limit);
        $this->assertLessThan(0, strcmp($result[0]->getProperty(), $result[1]->getProperty()));

        /** @noinspection PhpUnhandledExceptionInspection */
        $result = $this->entityRepository->findBy([], ['property' => 'DESC'], $limit, $offset);

        $this->assertEquals($limit, count($result));
        $this->assertGreaterThan(0, strcmp($result[0]->getProperty(), $result[1]->getProperty()));
    }

    public function testFindCountBy(): void
    {
        $entitiesCount = 5;
        $this->tester->haveMultiple(EntityStub::class, $entitiesCount);

        /** @noinspection PhpUnhandledExceptionInspection */
        $result = $this->entityRepository->findCountBy([]);

        $this->assertEquals($result, $entitiesCount);
    }

    /**
     * Tests findCountBy() method covering edge case related to
     * The same table joined to another two different tables and try to count by filter
     *
     * Covering edge case related to counting results from a query that joins one table to
     * two different tables and filters the results by a column from the first table
     *
     * Table 1 joined to table 2 and table 3 . If there are 2 records in table 2 and 2 records in table 3
     * related to the table 1 findCountBy() method would return incorrect results. This test case simulates this behaviour
     *
     * - user_role
     *      - user_id : 21, 21, 21, 21, 21, 21 |
     *      - role_id :  8,  9, 10, 11, 12, 13 |
     * - user
     *      - id : 21
     * - employee :
     *      - user_id : 21
     *      - role_id : 12
     *
     * ......T1
     * (m2m)/..\(o2m)
     * ...T3....T2
     * ..........\(o2o)
     * ...........T3
     */
    public function testFindCountByForMultipleRelations(): void
    {
        $testProperty = 'test_property_1';

        $entityStub = $this->tester->have(EntityStub::class, [
            'property' => 'test_property_0',
        ]);

        $commonRelationStub = $this->tester->have(CommonRelationStub::class, [
            'stringProperty' => 'test_property_0',
            'entity' => $entityStub
        ]);

        $entityStub->addCommonRelation($commonRelationStub);

        $commonRelationStub1 = $this->tester->have(CommonRelationStub::class, [
            'stringProperty' => $testProperty,
            'entity' => $entityStub
        ]);

        $entityStub->addCommonRelation($commonRelationStub1);

        $oneToManyRelationStub = $this->tester->have(OneToManyRelationStub::class, [
            'archived' => false,
            'commonRelation' => $commonRelationStub1,
            'entity' => $entityStub
        ]);

        /** @noinspection PhpUnhandledExceptionInspection */
        $result = $this->entityRepository->findCountBy([
            'or',
            ['=', 'oneToManyRelations.commonRelation.stringProperty', $testProperty],
            ['=', 'commonRelations.stringProperty', $testProperty]
        ]);

        $this->assertEquals(1, $result);
    }

    public function testFindCountByWithCriteria(): void
    {
        $specificEntitiesCount = 2;
        $this->tester->haveMultiple(
            EntityStub::class,
            $specificEntitiesCount,
            ['property' => 'test']
        );
        $this->tester->haveMultiple(EntityStub::class, 5);

        /** @noinspection PhpUnhandledExceptionInspection */
        $result = $this->entityRepository->findCountBy(['property' => 'test']);

        $this->assertEquals($result, $specificEntitiesCount);
    }

    public function testFindCountByWithNestedCriteriaAndRelations(): void
    {
        $stringValue0 = 'true';
        $intValue0 = 1;
        $intValue1 = 2;
        $booleanTrue = true;
        // This entity will not be fetched because all of its oneToManyRelations are archived
        $entity0 = $this->tester->have(
            EntityStub::class,
            [
                'property' => $stringValue0,
                'boolProperty' => $booleanTrue,
                'intProperty' => $intValue0,
                'decimalProperty' => null,
                'textProperty' => 'random text'
            ]
        );
        $relation0 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'archived' => true,
            ]
        );
        $relation1 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'archived' => true,
            ]
        );
        $relation2 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity0,
                'archived' => true,
            ]
        );
        $entity1 = $this->tester->have(
            EntityStub::class,
            [
                'property' => $stringValue0,
                'boolProperty' => $booleanTrue,
                'intProperty' => $intValue1,
                'decimalProperty' => 0.1,
                'textProperty' => 'random text 2'
            ]
        );
        $relation3 = $this->tester->have(
            OneToManyRelationStub::class,
            [
                'entity' => $entity1,
                'archived' => false,
            ]
        );
        $entity2 = $this->tester->have(
            EntityStub::class,
            [
                'property' => 'false',
                'boolProperty' => $booleanTrue,
                'intProperty' => 0,
                'decimalProperty' => 0.2,
                'textProperty' => null
            ]
        );
        $entity3 = $this->tester->have(
            EntityStub::class,
            [
                'property' => null,
                'boolProperty' => $booleanTrue,
                'intProperty' => 0,
                'decimalProperty' => 0.3,
                'textProperty' => 'will fetch'
            ]
        );
        $entity4 = $this->tester->have(
            EntityStub::class,
            [
                'property' => 'false',
                'boolProperty' => false,
                'intProperty' => 0,
                'decimalProperty' => 0.4,
                'textProperty' => 'will not fetch'
            ]
        );

        $specificEntitiesCount = 1;
        /** @noinspection PhpUnhandledExceptionInspection */
        $result = $this->entityRepository->findCountBy(
            [
                'and',
                ['=', 'boolProperty', $booleanTrue],
                [
                    'and',
                    ['in', 'property', [null, $stringValue0]],
                    ['=', 'oneToManyRelations.archived', false],
                    [
                        'or',
                        ['=', 'intProperty', $intValue0],
                        ['=', 'intProperty', $intValue1],
                    ],
                ],
            ]
        );

        $this->assertEquals($specificEntitiesCount, $result);
    }

    public function testFindByWithCriteria2(): void
    {
        $pastDate = (new DateTime())->sub(new DateInterval('P1D'));
        $futureDate = (new DateTime())->add(new DateInterval('P10D'));
        $this->tester->haveMultiple(EntityStub::class, 5, ['property' => 'invalid']);
        $this->tester->have(EntityStub::class, ['property' => null]);
        $this->tester->have(
            EntityStub::class,
            [
                'property' => 'valid',
                'intProperty' => 0,
                'dateProperty' => $pastDate,
            ]
        );
        $this->tester->have(EntityStub::class, ['dateProperty' => $futureDate]);

        $criteria = ['property' => 'valid'];
        $result = $this->entityRepository->findBy($criteria);
        $this->assertEquals('valid', $result[0]->getProperty());

        $criteria = ['property' => 'valid', 'dateProperty' => $pastDate];
        $result = $this->entityRepository->findBy($criteria);
        $this->assertEquals('valid', $result[0]->getProperty());
        $this->assertEquals($pastDate, $result[0]->getDateProperty());

        // test with three key=>value criteria to cover one of the corner cases in the logic
        $criteria = [
            'property' => 'valid',
            'intProperty' => 0,
            'dateProperty' => $pastDate,
        ];
        $result = $this->entityRepository->findBy($criteria);
        $this->assertEquals('valid', $result[0]->getProperty());
        $this->assertEquals(0, $result[0]->getIntProperty());
        $this->assertEquals($pastDate, $result[0]->getDateProperty());

        $criteria = [
            'and',
            ['property' => 'valid'],
            ['dateProperty' => $pastDate],
        ];
        $result = $this->entityRepository->findBy($criteria);
        $this->assertEquals('valid', $result[0]->getProperty());
        $this->assertEquals($pastDate, $result[0]->getDateProperty());

        $criteria = ['=', 'property', 'valid'];
        $result = $this->entityRepository->findBy($criteria);
        $this->assertEquals(1, count($result));
        $this->assertEquals('valid', $result[0]->getProperty());

        $criteria = ['=', 'property', null];
        $result = $this->entityRepository->findBy($criteria);
        $this->assertNotEmpty($result);
        foreach ($result as $value) {
            $this->assertNull($value->getProperty());
        }

        $criteria = ['like', 'property', 'valid'];
        $result = $this->entityRepository->findBy($criteria);
        $this->assertEquals(6, count($result)); //should contains valid and invalid

        $criteria = ['in', 'property', ['valid', 'invalid']];
        $result = $this->entityRepository->findBy($criteria);
        $this->assertEquals(6, count($result)); //should contains valid and invalid

        $criteria = ['is null', 'property', $pastDate];
        $result = $this->entityRepository->findBy($criteria);
        $this->assertNull($result[0]->getProperty());

        $notEquals = 'valid';
        $criteria = ['!=', 'property', $notEquals];
        $result = $this->entityRepository->findBy($criteria);
        $this->assertNotEmpty($result);
        foreach ($result as $value) {
            $this->assertNotEquals($notEquals, $value->getProperty());
        }

        $criteria = ['>', 'dateProperty', $pastDate];
        $result = $this->entityRepository->findBy($criteria);
        $this->assertGreaterThan($pastDate, $result[0]->getDateProperty());

        $criteria = ['<', 'dateProperty', $futureDate];
        $result = $this->entityRepository->findBy($criteria);
        $this->assertLessThan($futureDate, $result[0]->getDateProperty());

        $criteria = ['!=', 'property', null];
        $result = $this->entityRepository->findBy($criteria);
        $this->assertNotEmpty($result);
        foreach ($result as $value) {
            $this->assertNotNull($value->getProperty());
        }
    }

    /**
     * @inheritdoc
     */
    protected function setUp(): void
    {
        parent::setUp();

        $entityManager = $this->getModule('Doctrine2')->em;
        $this->schemaTool = new SchemaTool($entityManager);
        $this->entityStubMetadata = $entityManager->getClassMetadata(EntityStub::class);
        $this->oneToManyRelationStubMetadata = $entityManager->getClassMetadata(OneToManyRelationStub::class);
        $this->manyToOneRelationStubMetadata = $entityManager->getClassMetadata(ManyToOneRelationStub::class);
        $this->commonRelationStubMetadata = $entityManager->getClassMetadata(CommonRelationStub::class);
        $this->schemaTool->createSchema([
            $this->entityStubMetadata,
            $this->oneToManyRelationStubMetadata,
            $this->manyToOneRelationStubMetadata,
            $this->commonRelationStubMetadata
        ]);

        $this->entityRepository = $entityManager->getRepository(EntityStub::class);
    }

    /**
     * @inheritdoc
     */
    protected function tearDown(): void
    {
        $this->schemaTool->dropSchema([$this->entityStubMetadata]);

        parent::tearDown();
    }

    /**
     * @param array $entityManagerMockedMethods
     *
     * @return EntityRepositoryStub
     * @throws Exception if `makeEmpty` method fails to create a mock
     */
    private function getEntityRepositoryStub(array $entityManagerMockedMethods): EntityRepositoryStub
    {
        $entityManager = $this->makeEmpty(EntityManager::class, $entityManagerMockedMethods);
        $classMetaData = $this->makeEmpty(ClassMetadata::class,
            [
                'name' => EntityStub::class,
                'rootEntityName' => EntityStub::class,
            ]
        );

        return new EntityRepositoryStub($entityManager, $classMetaData);
    }
}
