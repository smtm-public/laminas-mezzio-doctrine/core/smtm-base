<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\View\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\I18n\Translator\TranslatorInterface;
use Laminas\I18n\View\Helper\Translate;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Laminas\View\HelperPluginManager;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class HelperPluginManagerDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container,
        $name,
        callable $callback,
        array $options = null
    ) {
        /** @var HelperPluginManager $helperPluginManager */
        $helperPluginManager = $callback();

        $helperPluginManager->setFactory(
            Translate::class,
            function (
                ContainerInterface $container,
                $requestedName,
                ?array $options = null
            ) {
                return (new Translate())->setTranslator(
                    $container->get(InfrastructureServicePluginManager::class)->get(TranslatorInterface::class)
                );
            }
        );

        return $helperPluginManager;
    }
}
