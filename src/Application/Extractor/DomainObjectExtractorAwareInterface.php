<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface DomainObjectExtractorAwareInterface
{
    public function getDomainObjectExtractor(): ?AbstractDomainObjectExtractor;
    public function setDomainObjectExtractor(?AbstractDomainObjectExtractor $domainObjectExtractor): static;
}
