<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Alek Nikolov <a.nikolov@smtm.com>
 * @author Milen Dzhambazov <m.dzhambazov@smtm.com>
 */
trait CommonMigrationTrait
{

    use ArchivedByIntColumnAndIndexAndForeignKeyMigrationTrait,
        ArchivedByIpAddressVarcharColumnMigrationTrait,
        ArchivedDatetimeMutableColumnAndIndexMigrationTrait,
        ArchiveUpdateQueryExecutionMethodCheckIntColumnMigrationTrait,
        CreatedByIntColumnAndIndexAndForeignKeyMigrationTrait,
        CreatedDatetimeImmutableColumnAndIndexMigrationTrait,
        ModifiedByIntColumnAndIndexAndForeignKeyMigrationTrait,
        ModifiedDatetimeMutableColumnAndIndexMigrationTrait,
        NotArchivedSmallintColumnAndIndexMigrationTrait,
        RelationColumnAndIndexAndForeignKeyMigrationTrait,
        TablePopulatingMigrationTrait,
        UnarchivedByIntColumnAndIndexAndForeignKeyMigrationTrait,
        UnarchivedByIpAddressVarcharColumnMigrationTrait,
        UnarchivedDatetimeMutableColumnMigrationTrait,
        UnarchiveUpdateQueryExecutionMethodCheckIntColumnMigrationTrait,
        UuidStringColumnAndUniqueIndexMigrationTrait;

    protected function createTableAndPrimaryKey(
        Schema $schema,
        string $tableName,
        string $primaryKeyColumnName = 'id',
        string $primaryKeyColumnType = Types::INTEGER,
        array $primaryKeyColumnOptions = [],
        bool $autoIncrement = true
    ): Table {
        $table = $schema->createTable($tableName);
        $table->addColumn($primaryKeyColumnName, $primaryKeyColumnType, $primaryKeyColumnOptions)
            ->setAutoincrement($autoIncrement);
        $table->setPrimaryKey([$primaryKeyColumnName]);

        return $table;
    }

    protected function dropTableIfExistsMysql(
        Schema $schema,
        string $tableName,
        ?string $schemaName = null
    ): void {
        if ($schemaName === null) {
            $query = <<< EOT
            DROP PROCEDURE IF EXISTS `DropTableIfExists`;
            CREATE PROCEDURE `DropTableIfExists`(
                IN i_table_name VARCHAR(64)
            )
            BEGIN
                IF EXISTS(
                    SELECT *
                    FROM information_schema.tables 
                    WHERE table_schema = DATABASE()
                        AND table_type = 'BASE TABLE'
                        AND table_name = i_table_name
                )
                THEN
                    SET @query = CONCAT('DROP TABLE `', i_table_name, '`;');
                    PREPARE stmt FROM @query;
                    EXECUTE stmt;
                    DEALLOCATE PREPARE stmt;
                END IF;
            END;
            SET @i_table_name = :i_table_name;
            PREPARE dtie FROM 'CALL `DropTableIfExists`(?)';
            EXECUTE dtie USING @i_table_name;
            DEALLOCATE PREPARE dtie;
            DROP PROCEDURE IF EXISTS `DropTableIfExists`;
            EOT;
            $this->addSql(
                $query,
                [
                    'i_table_name' => $tableName,
                ]
            );
        } else {
            $query = <<< EOT
            DROP PROCEDURE IF EXISTS `DropTableIfExists`;
            CREATE PROCEDURE `DropTableIfExists`(
                IN i_schema_name VARCHAR(64),
                IN i_table_name VARCHAR(64)
            )
            BEGIN
                IF EXISTS(
                    SELECT *
                    FROM information_schema.tables 
                    WHERE table_schema = DATABASE()
                        AND table_type = 'BASE TABLE'
                        AND table_name = i_table_name
                )
                THEN
                    SET @query = CONCAT('DROP TABLE `', i_schema_name, '`.`', i_table_name, '`;');
                    PREPARE stmt FROM @query;
                    EXECUTE stmt;
                    DEALLOCATE PREPARE stmt;
                END IF;
            END;
            SET @i_schema_name = :i_schema_name;
            SET @i_table_name = :i_table_name;
            PREPARE dtie FROM 'CALL `DropTableIfExists`(?, ?)';
            EXECUTE dtie USING @i_schema_name, @i_table_name;
            DEALLOCATE PREPARE dtie;
            DROP PROCEDURE IF EXISTS `DropTableIfExists`;
            EOT;
            $this->addSql(
                $query,
                [
                    'i_schema_name' => $schemaName,
                    'i_table_name' => $tableName,
                ]
            );
        }
    }

    protected function renameTableIfExistsMysql(
        Schema $schema,
        string $oldTableName,
        string $newTableName,
        ?string $newSchemaName = null
    ): void {
        if ($newSchemaName === null) {
            $query = <<< EOT
            DROP PROCEDURE IF EXISTS `RenameTableIfExists`;
            CREATE PROCEDURE `RenameTableIfExists`(
                IN i_old_table_name VARCHAR(64),
                IN i_new_table_name VARCHAR(64)
            )
            BEGIN
                IF EXISTS(
                    SELECT *
                    FROM information_schema.tables 
                    WHERE table_schema = DATABASE()
                        AND table_type = 'BASE TABLE'
                        AND table_name = i_old_table_name
                )
                THEN
                    SET @query = CONCAT('RENAME TABLE `', i_old_table_name, '` TO `', i_new_table_name, '`;');
                    PREPARE stmt FROM @query;
                    EXECUTE stmt;
                    DEALLOCATE PREPARE stmt;
                END IF;
            END;
            SET @i_old_table_name = :i_old_table_name;
            SET @i_new_table_name = :i_new_table_name;
            PREPARE rtie FROM 'CALL `RenameTableIfExists`(?, ?)';
            EXECUTE rtie USING @i_old_table_name, @i_new_table_name;
            DEALLOCATE PREPARE rtie;
            DROP PROCEDURE IF EXISTS `RenameTableIfExists`;
            EOT;
            $this->addSql(
                $query,
                [
                    'i_old_table_name' => $oldTableName,
                    'i_new_table_name' => $newTableName,
                ]
            );
        } else {
            $query = <<< EOT
            DROP PROCEDURE IF EXISTS `RenameTableIfExists`;
            CREATE PROCEDURE `RenameTableIfExists`(
                IN i_old_table_name VARCHAR(64),
                IN i_new_schema_name VARCHAR(64)
                IN i_new_table_name VARCHAR(64)
            )
            BEGIN
                IF EXISTS(
                    SELECT *
                    FROM information_schema.tables 
                    WHERE table_schema = DATABASE()
                        AND table_type = 'BASE TABLE'
                        AND table_name = i_old_table_name
                )
                THEN
                    SET @query = CONCAT('RENAME TABLE `', i_old_table_name, '` TO `', i_new_schema_name, '`.`', i_new_table_name, '`;');
                    PREPARE stmt FROM @query;
                    EXECUTE stmt;
                    DEALLOCATE PREPARE stmt;
                END IF;
            END;
            SET @i_old_table_name = :i_old_table_name;
            SET @i_new_schema_name = :i_new_schema_name;
            SET @i_new_table_name = :i_new_table_name;
            PREPARE rtie FROM 'CALL `RenameTableIfExists`(?, ?, ?)';
            EXECUTE rtie USING @i_old_table_name, @i_new_schema_name, @i_new_table_name;
            DEALLOCATE PREPARE rtie;
            DROP PROCEDURE IF EXISTS `RenameTableIfExists`;
            EOT;
            $this->addSql(
                $query,
                [
                    'i_old_table_name' => $oldTableName,
                    'i_new_schema_name' => $newSchemaName,
                    'i_new_table_name' => $newTableName,
                ]
            );
        }
    }

    protected function dropColumnIfExistsMysql(
        Schema $schema,
        string $tableName,
        string $columnName
    ): void {
        $query = <<< EOT
        DROP PROCEDURE IF EXISTS `DropColumnIfExists`;
        CREATE PROCEDURE `DropColumnIfExists`(
            IN i_table_name VARCHAR(128),
            IN i_column_name VARCHAR(128)
        )
        BEGIN
            IF EXISTS(
                SELECT *
                FROM information_schema.columns
                WHERE
                        table_schema = DATABASE()
                    AND table_name = i_table_name
                    AND column_name = i_column_name
            )
            THEN
                SET @query = CONCAT('ALTER TABLE `', i_table_name, '` DROP COLUMN `', i_column_name, '`;');
                PREPARE stmt FROM @query;
                EXECUTE stmt;
                DEALLOCATE PREPARE stmt;
            END IF;
        END;
        SET @i_table_name = :i_table_name;
        SET @i_column_name = :i_column_name;
        PREPARE dcie FROM 'CALL `DropColumnIfExists`(?, ?)';
        EXECUTE dcie USING @i_table_name, @i_column_name;
        DEALLOCATE PREPARE dcie;
        DROP PROCEDURE IF EXISTS `DropColumnIfExists`;
        EOT;
        $this->addSql(
            $query,
            [
                'i_table_name' => $tableName,
                'i_column_name' => $columnName,
            ]
        );
    }

    protected function dropUniqueConstraintIfExistsMysql(
        Schema $schema,
        string $tableName,
        string $constraintName
    ): void {
        $query = <<< EOT
        DROP PROCEDURE IF EXISTS `DropUniqueConstraintIfExists`;
        CREATE PROCEDURE `DropUniqueConstraintIfExists`(
            IN i_table_name VARCHAR(64),
            IN i_constraint_name VARCHAR(64)
        )
        BEGIN
            IF EXISTS(
                SELECT *
                FROM information_schema.table_constraints
                WHERE
                        table_schema = DATABASE()
                    AND table_name  = i_table_name
                    AND constraint_name = i_constraint_name
                    AND constraint_type = 'UNIQUE'
            )
            THEN
                SET @query = CONCAT('ALTER TABLE ', i_table_name, ' DROP KEY ', i_constraint_name, ';');
                PREPARE stmt FROM @query;
                EXECUTE stmt;
                DEALLOCATE PREPARE stmt;
            END IF;
        END;
        SET @i_table_name = :i_table_name;
        SET @i_constraint_name = :i_constraint_name;
        PREPARE ducie FROM 'CALL `DropUniqueConstraintIfExists`(?, ?)';
        EXECUTE ducie USING @i_table_name, @i_constraint_name;
        DEALLOCATE PREPARE ducie;
        DROP PROCEDURE IF EXISTS `DropUniqueConstraintIfExists`;
        EOT;
        $this->addSql(
            $query,
            [
                'i_table_name' => $tableName,
                'i_constraint_name' => $constraintName,
            ]
        );
    }

    protected function addUniqueConstraintIfNotExistsMysql(
        Schema $schema,
        string $tableName,
        string $constraintName,
        array $columns
    ): void {
        $query = <<< EOT
        DROP PROCEDURE IF EXISTS `AddUniqueConstraintIfNotExists`;
        CREATE PROCEDURE `AddUniqueConstraintIfNotExists`(
            IN i_table_name VARCHAR(64),
            IN i_constraint_name VARCHAR(64),
            IN i_columns TEXT
        )
        BEGIN
            IF NOT EXISTS(
                SELECT *
                FROM information_schema.table_constraints
                WHERE
                        table_schema = DATABASE()
                    AND table_name  = i_table_name
                    AND constraint_name = i_constraint_name
                    AND constraint_type = 'UNIQUE'
            )
            THEN
                SET @query = CONCAT('ALTER TABLE ', i_table_name, ' ADD CONSTRAINT ', i_constraint_name, ' UNIQUE(', i_columns, ');');
                PREPARE stmt FROM @query;
                EXECUTE stmt;
                DEALLOCATE PREPARE stmt;
            END IF;
        END;
        SET @i_table_name = :i_table_name;
        SET @i_constraint_name = :i_constraint_name;
        SET @i_columns = :i_columns;
        PREPARE aucine FROM 'CALL `AddUniqueConstraintIfNotExists`(?, ?, ?)';
        EXECUTE aucine USING @i_table_name, @i_constraint_name, @i_columns;
        DEALLOCATE PREPARE aucine;
        DROP PROCEDURE IF EXISTS `AddUniqueConstraintIfNotExists`;
        EOT;
        $this->addSql(
            $query,
            [
                'i_table_name' => $tableName,
                'i_constraint_name' => $constraintName,
                'i_columns' => implode(', ', $columns),
            ]
        );
    }

    protected function dropForeignKeyIfExistsMysql(
        Schema $schema,
        string $tableName,
        string $constraintName
    ): void {
        $query = <<< EOT
        DROP PROCEDURE IF EXISTS `DropForeignKeyIfExists`;
        CREATE PROCEDURE `DropForeignKeyIfExists`(
            IN i_table_name VARCHAR(64),
            IN i_constraint_name VARCHAR(64)
        )
        BEGIN
            IF EXISTS(
                SELECT *
                FROM information_schema.table_constraints
                WHERE
                        table_schema = DATABASE()
                    AND table_name  = i_table_name
                    AND constraint_name = i_constraint_name
                    AND constraint_type = 'FOREIGN KEY'
            )
            THEN
                SET @query = CONCAT('ALTER TABLE ', i_table_name, ' DROP FOREIGN KEY ', i_constraint_name, ';');
                PREPARE stmt FROM @query;
                EXECUTE stmt;
                DEALLOCATE PREPARE stmt;
            END IF;
        END;
        SET @i_table_name = :i_table_name;
        SET @i_constraint_name = :i_constraint_name;
        PREPARE dfkie FROM 'CALL `DropForeignKeyIfExists`(?, ?)';
        EXECUTE dfkie USING @i_table_name, @i_constraint_name;
        DEALLOCATE PREPARE dfkie;
        DROP PROCEDURE IF EXISTS `DropForeignKeyIfExists`;
        EOT;
        $this->addSql(
            $query,
            [
                'i_table_name' => $tableName,
                'i_constraint_name' => $constraintName,
            ]
        );
    }

    protected function dropIndexIfExistsMysql(
        Schema $schema,
        string $tableName,
        string $indexName
    ): void {
        $query = <<< EOT
        DROP PROCEDURE IF EXISTS `DropIndexIfExists`;
        CREATE PROCEDURE `DropIndexIfExists`(
            IN i_table_name VARCHAR(128),
            IN i_index_name VARCHAR(128)
        )
        BEGIN
            IF EXISTS(
                SELECT *
                FROM information_schema.statistics
                WHERE
                        table_schema = DATABASE()
                    AND table_name = i_table_name
                    AND index_name = i_index_name
            )
            THEN
                SET @query = CONCAT('DROP INDEX `', i_index_name, '` ON `', i_table_name, '`', ';');
                PREPARE stmt FROM @query;
                EXECUTE stmt;
                DEALLOCATE PREPARE stmt;
            END IF;
        END;
        SET @i_table_name = :i_table_name;
        SET @i_index_name = :i_index_name;
        PREPARE diie FROM 'CALL `DropIndexIfExists`(?, ?)';
        EXECUTE diie USING @i_table_name, @i_index_name;
        DEALLOCATE PREPARE diie;
        DROP PROCEDURE IF EXISTS `DropIndexIfExists`;
        EOT;
        $this->addSql(
            $query,
            [
                'i_table_name' => $tableName,
                'i_index_name' => $indexName,
            ]
        );
    }

    protected function dropPrimaryKeyIfExistsMysql(
        Schema $schema,
        string $tableName,
        bool $removeAutoIncrement = false
    ): void {
        $query = <<< EOT
        DROP PROCEDURE IF EXISTS `DropPrimaryKeyIfExists`;
        CREATE PROCEDURE `DropPrimaryKeyIfExists`(
            IN i_table_name VARCHAR(128)
        )
        BEGIN
            SELECT @primary_key_columns_count := COUNT(*)
            FROM information_schema.statistics
            WHERE
                    table_schema = DATABASE()
                AND table_name = i_table_name
                AND index_name = 'PRIMARY';
        
            IF @primary_key_columns_count > 0
            THEN
        
        EOT;

        if ($removeAutoIncrement) {
            $query .= <<< EOT
                IF @primary_key_columns_count = 1
                THEN
                    SELECT @primary_key_column_name := column_name
                    FROM information_schema.statistics
                    WHERE
                            table_schema = DATABASE()
                        AND table_name = i_table_name
                        AND index_name = 'PRIMARY';
            
                    SELECT @primary_key_column_type := column_type
                    FROM information_schema.columns
                    WHERE
                            table_schema = DATABASE()
                        AND table_name = i_table_name
                        AND column_name = primary_key_column_name;
            
                    SELECT @primary_key_column_nullable := is_nullable
                    FROM information_schema.columns
                    WHERE
                            table_schema = DATABASE()
                        AND table_name = i_table_name
                        AND column_name = primary_key_column_name;
                    
                    SET @query = CONCAT('ALTER TABLE `', i_table_name, '` MODIFY `', primary_key_column_name, '` ', primary_key_column_type, ' NOT NULL;');
                    
                    IF @primary_key_column_nullable = 'YES' THEN
                        SET @query = CONCAT('ALTER TABLE `', i_table_name, '` MODIFY `', primary_key_column_name, '` ', primary_key_column_type, ' NULL;');
                    END IF;
                    
                    PREPARE stmt FROM @query;
                    EXECUTE stmt;
                    DEALLOCATE PREPARE stmt;
                END IF;
            EOT;
        }

        $query .= <<< EOT
                SET @query = CONCAT('ALTER TABLE `', i_table_name, '` DROP PRIMARY KEY');
                PREPARE stmt FROM @query;
                EXECUTE stmt;
                DEALLOCATE PREPARE stmt;
            END IF;
        END;
        SET @i_table_name = :i_table_name;
        PREPARE dpkie FROM 'CALL `DropPrimaryKeyIfExists`(?)';
        EXECUTE dpkie USING @i_table_name;
        DEALLOCATE PREPARE dpkie;
        DROP PROCEDURE IF EXISTS `DropPrimaryKeyIfExists`;
        EOT;
        $this->addSql(
            $query,
            [
                'i_table_name' => $tableName,
            ]
        );
    }
}
