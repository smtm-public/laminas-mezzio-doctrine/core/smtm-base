<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface HydratorPluginManagerAwareInterface
{
    public function getHydratorPluginManager(): HydratorPluginManager;
    public function setHydratorPluginManager(HydratorPluginManager $hydratorPluginManager): static;
}
