<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Base64DecodeHydrationStrategy extends AbstractHydrationStrategy
{
    public const OPTION_KEY_STRICT = 'strict';

    public function hydrate($value, ?array $data)
    {
        return base64_decode($value, $this->options[static::OPTION_KEY_STRICT] ?? false);
    }
}
