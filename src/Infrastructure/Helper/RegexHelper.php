<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

use Smtm\Base\Infrastructure\Exception\RuntimeException;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RegexHelper
{
    public const PARSE_EXPRESSION_FLAG_THROW_ON_INVALID_MATCH = 0b0001;

    public const DELIMITERS = [
        '`',
        '~',
        '@',
        '#',
        '$',
        '%',
        '^',
        '&',
        '*',
        '(',
        ')',
        '-',
        '_',
        '=',
        '+',
        '[',
        '{',
        ']',
        '}',
        '|',
        ';',
        ':',
        '\'',
        '"',
        ',',
        '<',
        '.',
        '>',
        '/',
        '?',
    ];
    public const SPECIAL_CHARS = [
        '.',
        '\\',
        '+',
        '*',
        '?',
        '[',
        '^',
        ']',
        '$',
        '(',
        ')',
        '{',
        '}',
        '=',
        '!',
        '<',
        '>',
        '|',
        ':',
        '-',
        '#',
    ];
    public const LITERAL_REGEX = '(?<string%d>(?:\'(?:(?:\\\')|[^\'])*?\')|(?:"(?:(?:\\")|[^"])*?"))|(?<float%d>[0-9]+\.[0-9]+)|(?<int%d>[0-9]+)|(?<null%d>null)';
    public const IDENTIFIER_REGEX = '[a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]*';
    public const OPERATOR_REGEX = '(?:\s*(?<operator%d>^|(?:\?:)|(?:&&)|(?:\|\|)|\.|\*|/|\+|-|;)?\s*)';
    public const CAST_REGEX = '(?:\s*(?<cast%d>(?:\(string\)|\(float\)|\(int\)|\(bool\)))?\s*)';
    public const FC_EXPR_INNER_REGEX = '(?:\\((?<fc_expr_inner%d>(?>[^\\(\\)]+)|(?-2))*\\))';
    public const AA_EXPR_INNER_REGEX = '(?:\\[(?<aa_expr_inner%d>(?>[^\\[\\]]+)|(?-2))*\\])';

    public const PARSE_EXPRESSION_OPTION_REMAP_VALUES = 'remapValues';

    public static function escapeSpecialCharsAndConvertLineBreaks(string $source, ?string $delimiter = null): string
    {
        return str_replace("\n", '\R', str_replace("\r", '\R', str_replace("\r\n", '\R', preg_quote($source, $delimiter))));
    }

    public static function findValidDelimiter(string $undelimitedInput): string|false
    {
        foreach (static::DELIMITERS as $delimiter) {
            if (!str_contains($undelimitedInput, $delimiter)) {
                return $delimiter;
            }
        }

        return false;
    }

    public static function getLiteralRegex(string $index = '0'): string
    {
        return sprintf(static::LITERAL_REGEX, $index, $index, $index, $index);
    }

    public static function getOperatorRegex(string $index = '0'): string
    {
        return sprintf(static::OPERATOR_REGEX, $index);
    }

    public static function getCastRegex(string $index = '0'): string
    {
        return sprintf(static::CAST_REGEX, $index);
    }

    public static function getFcExprInnerRegex(string $index = '0'): string
    {
        return sprintf(static::FC_EXPR_INNER_REGEX, $index);
    }

    public static function getAaExprInnerRegex(string $index = '0'): string
    {
        return sprintf(static::AA_EXPR_INNER_REGEX, $index);
    }

    public static function getFunctionCallRegex(string $index = '0'): string
    {
        return sprintf('(?:\s*(?<fc_expr%d>' . static::getFcExprInnerRegex($index) . '+)\s*)', $index);
    }

    public static function getArrayAccessRegex(string $index = '0'): string
    {
        return sprintf('(?:\s*(?<aa_expr%d>' . static::getAaExprInnerRegex($index) . '+)\s*)', $index);
    }

    public static function getFunctionCallOrArrayAccessRegex(string $index = '0'): string
    {
        return '(?:' . static::getFunctionCallRegex($index) . '|' . static::getArrayAccessRegex($index) . ')?';
    }

    public static function getVarOrClassRegex(string $index = '0'): string
    {
        return sprintf('(?:(?<var%d>\\$' . static::IDENTIFIER_REGEX . ')|(?<class%d>(?:\\\\?' . static::IDENTIFIER_REGEX . ')(?:\\\\' . static::IDENTIFIER_REGEX . ')*))', $index, $index);
    }

    public static function getObjectOrClassMemberAccessRegex(string $index0 = '0', string $index1 = '1'): string
    {
        return static::getVarOrClassRegex($index0)
            . sprintf('(?:(?:(?<static%d>::)|(?<object_member%d>->))(?:', $index0, $index0)
            . static::getVarOrClassRegex($index1) . '))?';
    }

    public static function getArrayOrFunctionCallRegex(string $index0 = '0', string $index1 = '1'): string
    {
        return '(?:' . static::getObjectOrClassMemberAccessRegex($index0, $index1) . static::getFunctionCallOrArrayAccessRegex($index0) . ')';
    }

    public static function getExpressionRegex(string $index0 = '0', string $index1 = '1'): string
    {
        return static::getLiteralRegex($index0). '|' . static::getArrayOrFunctionCallRegex($index0, $index1);
    }

    public static function getExpressionWithOperatorAndCastRegex(string $index0 = '0', string $index1 = '1'): string
    {
        return '(?:' . static::getOperatorRegex($index0) . static::getCastRegex($index0) . '(?:' . static::getExpressionRegex($index0, $index1) . '))';
    }

    public static function getExpressionWithOperatorAndCastAndDefaultRegex(string $index0 = '0', string $index1 = '1', string $index2 = '2', string $index3 = '3'): string
    {
        return '(?:' . static::getExpressionWithOperatorAndCastRegex($index0, $index1) . ')(?<default>\s*\?\?\s*(?<default_expr>' . static::getExpressionWithOperatorAndCastRegex($index2, $index3) . '))?';
    }

    public static function getArgumentListRegex(): string
    {
        return '(?<arg>' . static::getExpressionWithOperatorAndCastAndDefaultRegex() . ')\s*,?\s*';
    }

    public static function parseExpression(
        string $expression,
        ?callable $variableResolverCallback = null,
        array $context = [],
        #[ArrayShape([
            self::PARSE_EXPRESSION_OPTION_REMAP_VALUES => 'array',
        ])] array $options = [],
        int $flags = 0b0000
    ): ?\Closure {
        $remapValues = $options[self::PARSE_EXPRESSION_OPTION_REMAP_VALUES] ?? null;
        $result = null;

        if (preg_match_all('#' . static::getExpressionWithOperatorAndCastAndDefaultRegex() . '#', $expression, $expressionMatches, PREG_SET_ORDER)) {
            foreach ($expressionMatches as $expressionMatch) {
                $expressionMatchTrimmed = trim($expressionMatch[0]);
                $resolved = null;
                $default = ($expressionMatch['default'] ?? null) || null;
                $defaultValue = null;

                if ($default) {
                    $defaultValue = static::parseExpression(
                        $expressionMatch['default_expr'],
                        $variableResolverCallback,
                        $context,
                        $options,
                        $flags
                    );
                }

                if (array_key_exists('var0', $expressionMatch) && $expressionMatch['var0'] !== '') {
                    if (array_key_exists('object_member0', $expressionMatch) && $expressionMatch['object_member0'] !== '') {
                        if (array_key_exists('fc_expr0', $expressionMatch) && $expressionMatch['fc_expr0'] !== '') {
                            $resolved = fn () => [$variableResolverCallback($expressionMatch['var0']), $expressionMatch['class1']];
                        } else {
                            $resolved = fn () => $variableResolverCallback($expressionMatch['var0'])->{$expressionMatch['class1']};
                        }
                    } else {
                        $resolved = fn () => $variableResolverCallback($expressionMatch['var0']);
                    }
                } elseif (array_key_exists('class0', $expressionMatch) && $expressionMatch['class0'] !== '') {
                    if (array_key_exists('static0', $expressionMatch) && $expressionMatch['static0'] !== '') {
                        if (array_key_exists('fc_expr0', $expressionMatch) && $expressionMatch['fc_expr0'] !== '') {
                            $resolved = fn () => [$expressionMatch['class0'], $expressionMatch['class1']];
                        } else {
                            $resolved = fn () => constant($variableResolverCallback($expressionMatch['class0']) . '::' . $expressionMatch['class1']);
                        }
                    } else {
                        $resolved = fn () => $expressionMatch['class0'];
                    }
                }

                if (array_key_exists('fc_expr0', $expressionMatch) && $expressionMatch['fc_expr0'] !== '') {
                    $fcParsed = static::parseFunctionCallExpression(
                        $expressionMatch['fc_expr0'],
                        $variableResolverCallback,
                        $context
                    );

                    foreach ($fcParsed as $fcArgs) {
                        $resolved = function () use ($expression, $expressionMatch, $expressionMatchTrimmed, $remapValues, $default, $defaultValue, $resolved, $fcArgs) {
                            $result = $resolved()(...array_map(fn (callable $arg) => $arg(), $fcArgs));

                            if ($default) {
                                $result = $result ?? $defaultValue();
                            }

                            if ($remapValues
                                && is_string($result)
                                && array_key_exists($result, $remapValues[$expressionMatchTrimmed] ?? [])
                            ) {
                                $result = $remapValues[$expressionMatchTrimmed][$result];
                            }

                            return $result;
                        };
                    }
                } elseif (array_key_exists('aa_expr0', $expressionMatch) && $expressionMatch['aa_expr0'] !== '') {
                    $aaParsed = static::parseArrayAccessExpression(
                        $expressionMatch['aa_expr0'],
                        $variableResolverCallback,
                        $context
                    );

                    foreach ($aaParsed as $aaIndex) {
                        $var = $resolved();
                        $index = $aaIndex();
                        $resolved = fn () => $default ? $var[$index] ?? $defaultValue() : $var[$index];
                    }

                    if ($remapValues) {
                        $resolved = function () use ($expression, $expressionMatch, $expressionMatchTrimmed, $remapValues, $resolved) {
                            $result = $resolved();

                            if (is_string($result)
                                && array_key_exists($result, $remapValues[$expressionMatchTrimmed] ?? [])
                            ) {
                                $result = $remapValues[$expressionMatchTrimmed][$result];
                            }

                            return $result;
                        };
                    }
                } elseif (array_key_exists('string0', $expressionMatch) && $expressionMatch['string0'] !== '') {
                    $resolved = fn () => substr($expressionMatch['string0'], 1, strlen($expressionMatch['string0']) - 2);
                } elseif (array_key_exists('float0', $expressionMatch) && $expressionMatch['float0'] !== '') {
                    $resolved = fn () => (float) $expressionMatch['float0'];
                } elseif (array_key_exists('int0', $expressionMatch) && $expressionMatch['int0'] !== '') {
                    $resolved = fn () => (int) $expressionMatch['int0'];
                } elseif (array_key_exists('null0', $expressionMatch) && $expressionMatch['null0'] !== '') {
                    $resolved = fn () => null;
                }

                if ($resolved !== null) {
                    $resolved = fn () => match (($expressionMatch['cast0'] ?? null) ?: null) {
                        'string' => (string) $resolved(),
                        'float' => (float) $resolved(),
                        'int' => (int) $resolved(),
                        'bool' => (bool) $resolved(),
                        default => $resolved(),
                    };

                    $operator = ($expressionMatch['operator0'] ?? null) ?: null;

                    if (empty($operator) || $operator === ';') {
                        $result = $resolved;
                    } else {
                        $result = match ($operator) {
                            '?:' => fn() => $result() ?: $resolved(),
                            '??' => fn() => $result() ?? $resolved(),
                            '&&' => fn() => $result() && $resolved(),
                            '||' => fn() => $result() || $resolved(),
                            '.' => fn() => $result() . $resolved(),
                            '*' => fn() => $result() * $resolved(),
                            '/' => fn() => $result() / $resolved(),
                            '+' => fn() => $result() + $resolved(),
                            '-' => fn() => $result() - $resolved(),
                        };
                    }
                } elseif ($flags & self::PARSE_EXPRESSION_FLAG_THROW_ON_INVALID_MATCH) {
                    throw new RuntimeException('Expression - ' . $expression . ' does not resolve to a valid result. Regex matches ' . var_export($expressionMatches, true));
                }
            }
        }

        return $result;
    }

    public static function parseFunctionCallExpression(
        string $sourceMatch,
        ?callable $variableResolverCallback = null,
        array $context = []
    ): array {
        preg_match_all('#' . '(?<fc_expr>' . static::getFcExprInnerRegex() . ')' . '#', $sourceMatch, $matches, PREG_SET_ORDER);

        $functionCallExpressions = [];

        foreach ($matches as $match) {
            if (array_key_exists('fc_expr', $match)
                && $match['fc_expr'] !== ''
            ) {
                $argList = substr($match['fc_expr'], 1, strlen($match['fc_expr']) - 2);
                preg_match_all('#' . static::getArgumentListRegex() . '#', $argList, $argMatches, PREG_SET_ORDER);
                $argListResolved = [];

                foreach ($argMatches as $argMatch) {
                    $argListResolved[] =
                        static::parseExpression(
                            $argMatch['arg'],
                            $variableResolverCallback,
                            $context
                        );
                }

                $functionCallExpressions[] = $argListResolved;
            }
        }

        return $functionCallExpressions;
    }

    public static function parseArrayAccessExpression(
        string $sourceMatch,
        ?callable $variableResolverCallback = null,
        array $context = []
    ): array {
        preg_match_all('#' . '(?<aa_expr>' . static::getAaExprInnerRegex() . ')' . '#', $sourceMatch, $matches, PREG_SET_ORDER);

        $arrayAccessExpressions = [];

        foreach ($matches as $match) {
            if (array_key_exists('aa_expr', $match)
                && $match['aa_expr'] !== ''
            ) {
                $arrayAccessExpressions[] =
                    static::parseExpression(
                        substr($match['aa_expr'], 1, strlen($match['aa_expr']) - 2),
                        $variableResolverCallback,
                        $context
                    );
            }
        }

        return $arrayAccessExpressions;
    }

    public static function parseExpression20231122(string $expression, ?callable $variableResolverCallback = null, array $context = []): mixed
    {
        $functionCallRegexNoDelimitersAndNoFlags = '(?<cast>(?:\(string\))|(?:\(float\))|(?:\(int\))|(?:\(bool\)))? *(?P<function_call>(?:(?:(?P<class_name>(?:\\\\?[a-z_]+)(?:\\\\?[a-z0-9_]+)*) *:: *)|(?:\$(?P<object_name>.*?) *-> *))?(?P<function_name>\w+)\((?P<function_arguments_expression>.*)\)) *';
        $functionCallRegex = '#' . $functionCallRegexNoDelimitersAndNoFlags . '#i';
        $stringArgumentRegex = '(?P<string>"(.*?)(?<!\\\\)")';
        $intArgumentRegex = '(?P<float>[0-9]+\.[0-9]+)';
        $floatArgumentRegex = '(?P<int>[0-9]+)';
        $variableArgumentRegex = '(?P<variable>\$[a-z_]+[0-9a-z_\.\$\[\]]*)';
        $otherArgumentRegex = '(?P<other>[0-9a-z_\.]+)';
        $argumentListRegexNoDelimitersAndNoFlags = '(?:' . $functionCallRegexNoDelimitersAndNoFlags . '|' . $stringArgumentRegex . '|' . $intArgumentRegex . '|' . $floatArgumentRegex . '|' . $variableArgumentRegex . '|' . $otherArgumentRegex . ')(?=(?: *, *)|$)';
        $argumentListRegex = '#' . $argumentListRegexNoDelimitersAndNoFlags . '#i';

        if (preg_match($functionCallRegex, $expression, $simpleFunctionMatches)) {
            $cast = $simpleFunctionMatches['cast'] ?? null;
            $className = $simpleFunctionMatches['class_name'];
            $objectName = $simpleFunctionMatches['object_name'];
            $functionName = $simpleFunctionMatches['function_name'];
            $functionArgumentsExpression = $simpleFunctionMatches['function_arguments_expression'];
            preg_match_all(
                $argumentListRegex,
                $functionArgumentsExpression,
                $functionArgumentsMatches,
                PREG_SET_ORDER
            );

            $functionArguments = [];

            foreach ($functionArgumentsMatches as $functionArgumentMatch) {
                if (array_key_exists('function_call', $functionArgumentMatch) && $functionArgumentMatch['function_call'] !== '') {
                    $functionArguments[] = static::parseExpression20231122($functionArgumentMatch['function_call'], $variableResolverCallback, $context);
                } elseif (array_key_exists('string', $functionArgumentMatch) && $functionArgumentMatch['string'] !== '') {
                    $functionArguments[] = trim($functionArgumentMatch['string'], '"');
                } elseif (array_key_exists('float', $functionArgumentMatch) && $functionArgumentMatch['float'] !== '') {
                    $functionArguments[] = (float) $functionArgumentMatch['float'];
                } elseif (array_key_exists('int', $functionArgumentMatch) && $functionArgumentMatch['int'] !== '') {
                    $functionArguments[] = (int) $functionArgumentMatch['int'];
                } elseif (array_key_exists('variable', $functionArgumentMatch) && $functionArgumentMatch['variable'] !== '') {
                    if ($variableResolverCallback !== null) {
                        $functionArguments[] = $variableResolverCallback($functionArgumentMatch['variable'], $context);
                    } else {
                        $functionArguments[] = $context[$functionArgumentMatch['variable']] ?? null;
                    }
                } elseif (array_key_exists('other', $functionArgumentMatch)) {
                    $functionArguments[] = null;
                } else {
                    $functionArguments[] = null;
                }
            }

            $callback =
                $className === ''
                    ? (
                $objectName === ''
                    ? $functionName
                    : [$context[$objectName], $functionName]
                )
                    : [$className, $functionName];

            return function () use ($callback, $cast, $functionArguments, $variableResolverCallback, $context) {
                $functionArgumentsResolved = [];

                foreach ($functionArguments as $functionArgument) {
                    if ($functionArgument instanceof \Closure) {
                        if (!isset($context['this'])) {
                            $functionArgumentsResolved[] = $functionArgument();
                        } else {
                            $functionArgumentsResolved[] = $functionArgument->call($context['this']);
                        }
                    } else {
                        $functionArgumentsResolved[] = $functionArgument;
                    }
                }

                return match ($cast) {
                    'string' => (string) call_user_func_array($callback, $functionArgumentsResolved),
                    'float' => (float) call_user_func_array($callback, $functionArgumentsResolved),
                    'int' => (int) call_user_func_array($callback, $functionArgumentsResolved),
                    'bool' => (bool) call_user_func_array($callback, $functionArgumentsResolved),
                    default => call_user_func_array($callback, $functionArgumentsResolved)
                };
            };
        }

        return false;
    }

    public static function parseExpression_old(string $expression, ?callable $variableResolverCallback = null, array $context = []): mixed
    {
        $functionCallRegexNoDelimitersAndNoFlags = ' *(?P<function_call>(?:(?:(?P<class_name>(?:\\\\?[a-z_]+)(?:\\\\?[a-z0-9_]+)*) *:: *)|(?:\$(?P<object_name>.*?) *-> *))?(?P<function_name>\w+)\((?P<function_arguments_expression>.*)\)) *';
        $functionCallRegex = '#' . $functionCallRegexNoDelimitersAndNoFlags . '#i';
        $stringArgumentRegex = '(?P<string>"(.*?)(?<!\\\\)")';
        $intArgumentRegex = '(?P<float>[0-9]+\.[0-9]+)';
        $floatArgumentRegex = '(?P<int>[0-9]+)';
        $variableArgumentRegex = '(?P<variable>\$[a-z_]+[0-9a-z_\.\$\[\]]*)';
        $otherArgumentRegex = '(?P<other>[0-9a-z_\.]+)';
        $argumentListRegexNoDelimitersAndNoFlags = '(?:' . $functionCallRegexNoDelimitersAndNoFlags . '|' . $stringArgumentRegex . '|' . $intArgumentRegex . '|' . $floatArgumentRegex . '|' . $variableArgumentRegex . '|' . $otherArgumentRegex . ')(?=(?: *, *)|$)';
        $argumentListRegex = '#' . $argumentListRegexNoDelimitersAndNoFlags . '#i';

        if (preg_match($functionCallRegex, $expression, $simpleFunctionMatches)) {
            $className = $simpleFunctionMatches['class_name'];
            $objectName = $simpleFunctionMatches['object_name'];
            $functionName = $simpleFunctionMatches['function_name'];
            $functionArgumentsExpression = $simpleFunctionMatches['function_arguments_expression'];
            preg_match_all(
                $argumentListRegex,
                $functionArgumentsExpression,
                $functionArgumentsMatches,
                PREG_SET_ORDER
            );

            $functionArguments = [];

            foreach ($functionArgumentsMatches as $functionArgumentMatch) {
                if (array_key_exists('function_call', $functionArgumentMatch) && $functionArgumentMatch['function_call'] !== '') {
                    $functionArguments[] = static::parseExpression20231122($functionArgumentMatch['function_call'], $variableResolverCallback, $context);
                } elseif (array_key_exists('string', $functionArgumentMatch) && $functionArgumentMatch['string'] !== '') {
                    $functionArguments[] = trim($functionArgumentMatch['string'], '"');
                } elseif (array_key_exists('float', $functionArgumentMatch) && $functionArgumentMatch['float'] !== '') {
                    $functionArguments[] = (float) $functionArgumentMatch['float'];
                } elseif (array_key_exists('int', $functionArgumentMatch) && $functionArgumentMatch['int'] !== '') {
                    $functionArguments[] = (int) $functionArgumentMatch['int'];
                } elseif (array_key_exists('variable', $functionArgumentMatch) && $functionArgumentMatch['variable'] !== '') {
                    if ($variableResolverCallback !== null) {
                        $functionArguments[] = $variableResolverCallback($functionArgumentMatch['variable'], $context);
                    } else {
                        $functionArguments[] = $context[$functionArgumentMatch['variable']] ?? null;
                    }
                } elseif (array_key_exists('other', $functionArgumentMatch)) {
                    $functionArguments[] = null;
                } else {
                    $functionArguments[] = null;
                }
            }

            $callback =
                $className === ''
                    ? (
                $objectName === ''
                    ? $functionName
                    : [$context[$objectName], $functionName]
                )
                    : [$className, $functionName];

            return function () use ($callback, $functionArguments, $variableResolverCallback, $context) {
                $functionArgumentsResolved = [];

                foreach ($functionArguments as $functionArgument) {
                    if ($functionArgument instanceof \Closure) {
                        if (!isset($context['this'])) {
                            $functionArgumentsResolved[] = $functionArgument();
                        } else {
                            $functionArgumentsResolved[] = $functionArgument->call($context['this']);
                        }
                    } else {
                        $functionArgumentsResolved[] = $functionArgument;
                    }
                }

                return call_user_func_array($callback, $functionArgumentsResolved);
            };
        }

        return false;
    }
}
