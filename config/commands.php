<?php

declare(strict_types=1);

namespace Smtm\Base;

return [
    Command\Doctrine\GenerateMigration::class,
    Command\PhpInfo::class,
    Command\ThrowException::class,
    Command\HelloWorld::class,
    Command\SheetFileStats::class,
    Command\ConfigMap::class,
    Command\SocketServer::class,
];
