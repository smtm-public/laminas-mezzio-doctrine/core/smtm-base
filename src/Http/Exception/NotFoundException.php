<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Exception;

use Smtm\Base\Infrastructure\Helper\HttpHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class NotFoundException extends AbstractHttpException
{
    public const MESSAGE = 'Not found';
    public const CODE = 0;
    public const STATUS = HttpHelper::STATUS_CODE_NOT_FOUND;
    public const TYPE = 'https://example.com/problems/not_found';

    protected int $status = self::STATUS;
    protected string $title = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_NOT_FOUND];
    protected string $detail = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_NOT_FOUND];
    protected string $type = self::TYPE;
}
