<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbArchivedAccessServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbReaderServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\EntityManagerConfigAwareDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use JetBrains\PhpStorm\Pure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AbstractEntityManagerConfigAwareAndUuidAwareEntityDbService extends AbstractDbService
    implements EntityManagerConfigAwareDbServiceInterface, UuidAwareEntityDbServiceInterface
{

    use UuidAwareEntityDbServiceTrait;

    #[Pure] public function __construct(
        ApplicationServicePluginManager $applicationServicePluginManager,
        InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected HydratorPluginManager $hydratorPluginManager,
        protected ExtractorPluginManager $extractorPluginManager,
        protected \Laminas\Hydrator\HydratorPluginManager $baseHydratorPluginManager,
        protected ManagerRegistryInterface $entityManagerRegistry,
        protected array $config
    ) {
        parent::__construct(
            $applicationServicePluginManager,
            $infrastructureServicePluginManager,
            $hydratorPluginManager,
            $extractorPluginManager,
            $baseHydratorPluginManager,
            $entityManagerRegistry
        );

        if ($this instanceof DbReaderServiceInterface && $this instanceof DbArchivedAccessServiceInterface) {
            $this->entityManagerName = $config['archivedAccessReaderEntityManagerName'];
        } elseif ($this instanceof DbReaderServiceInterface) {
            $this->entityManagerName = $config['readerEntityManagerName'];
        } elseif ($this instanceof DbArchivedAccessServiceInterface) {
            $this->entityManagerName = $config['archivedAccessEntityManagerName'];
        } else {
            $this->entityManagerName = $config['entityManagerName'];
        }
    }
}
