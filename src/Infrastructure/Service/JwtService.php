<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

use DateTimeImmutable;
use JetBrains\PhpStorm\Pure;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\Token\RegisteredClaims;
use Lcobucci\JWT\UnencryptedToken;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class JwtService extends AbstractInfrastructureService
{
    #[Pure] public function __construct(
        InfrastructureServicePluginManager $infrastructureServiceManager,
        protected Configuration $configuration
    ) {
        parent::__construct($infrastructureServiceManager);
    }

    public function parse(string $jwt): UnencryptedToken
    {
        return $this->configuration->parser()->parse($jwt);
    }

    public function validate(Token $jwt): bool
    {
        return $this->configuration->validator()->validate($jwt, ...$this->configuration->validationConstraints());
    }

    public function getClaimJti(Token $jwt): string
    {
        return $jwt->claims()->get(RegisteredClaims::ID);
    }

    public function build(
        string $id,
        DateTimeImmutable $now,
        ?DateTimeImmutable $expires,
        array $claims
    ): Token {
        /**
         * # The private key must be in PKCS#1 PEM format:
         * ssh-keygen -b 4096 -t rsa -m pem -C "sso@connector.mock" -f ./tests/_data/sso_jwt_signature_private_key
         * # ...or PKCS#8 format:
         * ssh-keygen -b 4096 -t rsa -m pkcs8 -C "sso@connector.mock" -f ./tests/_data/sso_jwt_signature_private_key
         * # The default RFC4716 format will not work and \Lcobucci\JWT\Signer\OpenSSL::getPrivateKey will throw an
         * # exception due to openssl_pkey_get_private($pem, $passphrase) generating an openssl_error_string():
         * # error:0909006C:PEM routines:get_name:no start line
         *
         * # Then rename the public key:
         * mv ./tests/_data/sso_jwt_signature_private_key.pub ./tests/_data/sso_jwt_signature_public_key.pub.old
         *
         * # Finally convert the public key into PKCS#8 PEM format:
         * ssh-keygen -e -f ./tests/_data/sso_jwt_signature_public_key.pub.old -m pkcs8 > ./tests/_data/sso_jwt_signature_public_key.pub
         * # The default OpenSSH specific format will not work and neither will PKCS#1 or RFC4716.
         * # \Lcobucci\JWT\Signer\OpenSSL::getPublicKey will throw an exception due to openssl_pkey_get_public($pem)
         * # generating an openssl_error_string():
         * # error:0909006C:PEM routines:get_name:no start line
         */
        $tokenBuilder = $this->configuration->builder()
            // Configures the id (jti claim) (Token\RegisteredClaims::ID claim), NOT replicating as a header item.
            ->identifiedBy($id)
            // Configures the time that the token was issued at (Token\RegisteredClaims::ISSUED_AT claim)
            ->issuedAt($now)
        ;

        if ($expires !== null) {
            $tokenBuilder
                // Configures the expiration time of the token (Token\RegisteredClaims::EXPIRATION_TIME claim)
                ->expiresAt($expires);
        }

        array_walk(
            $claims,
            function ($claimValue, $claimKey) use (&$tokenBuilder) {
                switch ($claimKey) {
                    case Token\RegisteredClaims::AUDIENCE:
                        $tokenBuilder->permittedFor($claimValue);
                        break;
                    case Token\RegisteredClaims::ISSUER:
                        $tokenBuilder->issuedBy($claimValue);
                        break;
                    case Token\RegisteredClaims::NOT_BEFORE:
                        $tokenBuilder->canOnlyBeUsedAfter($claimValue);
                        break;
                    case Token\RegisteredClaims::SUBJECT:
                        $tokenBuilder->relatedTo($claimValue);
                        break;
                    default:
                        $tokenBuilder->withClaim($claimKey, $claimValue);
                        break;
                }
            }
        );

        return $tokenBuilder->getToken($this->configuration->signer(), $this->configuration->signingKey());
    }
}
