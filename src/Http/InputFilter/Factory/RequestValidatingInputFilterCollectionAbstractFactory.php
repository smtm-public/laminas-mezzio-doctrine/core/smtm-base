<?php

declare(strict_types=1);

namespace Smtm\Base\Http\InputFilter\Factory;

use Smtm\Base\Http\InputFilter\AbstractRequestValidatingInputFilterCollection;
use Smtm\Base\Infrastructure\Laminas\InputFilter\Factory\InputFilterFactory;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RequestValidatingInputFilterCollectionAbstractFactory implements AbstractFactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get(InputFilterFactory::class),
        );
    }

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return is_subclass_of($requestedName, AbstractRequestValidatingInputFilterCollection::class);
    }
}
