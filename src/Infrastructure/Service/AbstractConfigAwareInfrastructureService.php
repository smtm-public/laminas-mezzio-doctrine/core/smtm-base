<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

use JetBrains\PhpStorm\Pure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractConfigAwareInfrastructureService extends AbstractInfrastructureService
{
    #[Pure] public function __construct(
        InfrastructureServicePluginManager $infrastructureServiceManager,
        protected array $config
    ) {
        parent::__construct($infrastructureServiceManager);
    }
}
