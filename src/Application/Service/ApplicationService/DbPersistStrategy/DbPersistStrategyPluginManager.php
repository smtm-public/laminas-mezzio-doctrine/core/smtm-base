<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\DbPersistStrategy;

use Laminas\ServiceManager\AbstractPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DbPersistStrategyPluginManager extends AbstractPluginManager
{
    protected $instanceOf = AbstractDbPersistStrategy::class;
}
