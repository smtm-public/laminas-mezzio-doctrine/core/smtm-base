<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler\DbServiceEntity;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Http\Handler\AbstractHandler as BaseAbstractHandler;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractHandler extends BaseAbstractHandler
{
    /** @var AbstractDbService|null $applicationService */
    protected ?AbstractApplicationService $applicationService = null;

    public function __construct(
        ApplicationServicePluginManager $applicationServicePluginManager,
        InfrastructureServicePluginManager $infrastructureServicePluginManager,
        ExtractorPluginManager $extractorPluginManager,
        array $config
    ) {
        parent::__construct(
            $applicationServicePluginManager,
            $infrastructureServicePluginManager,
            $extractorPluginManager,
            $config
        );
    }

    /**
     * Formats the given $queryParams as array of db criteria. If includeOnly
     * is set, the method will parse only the query params listed in the variable
     */
    protected static function prepareCriteria(array $queryParams, array $includeOnly = null): array
    {
        if ($includeOnly !== null) {
            $queryParams = array_filter(
                $queryParams,
                function ($key) use ($includeOnly): bool {
                    return in_array($key, $includeOnly);
                },
                ARRAY_FILTER_USE_KEY
            );
        }

        $queryParamsCount = count($queryParams);
        $conditions = [];

        foreach ($queryParams as $key => $value) {
            $key = str_replace('_', '.', $key);

            if (is_numeric($value)) {
                $conditions[] = ['=', $key, $value];
            } elseif ($value === chr(0)) {
                $conditions[] = ['is null', $key, $value];
            } else {
                $conditions[] = ['like', $key, $value];
            }
        }

        if ($queryParamsCount > 1) {
            $criteria = array_merge(['and'], $conditions);
        } else {
            $criteria = $conditions;
        }

        return $criteria;
    }
}
