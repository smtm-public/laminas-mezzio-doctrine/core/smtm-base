<?php

declare(strict_types=1);

namespace Smtm\Base\Factory;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ServiceNameAwareInterface
{
    public function getServiceName(): ?string;
    public function setServiceName(?string $serviceName): static;
}
