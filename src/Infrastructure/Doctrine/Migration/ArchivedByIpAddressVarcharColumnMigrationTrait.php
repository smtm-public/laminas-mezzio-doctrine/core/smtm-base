<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ArchivedByIpAddressVarcharColumnMigrationTrait
{
    protected function addArchivedByIpAddressVarcharColumn(Table $table): void
    {
        $table->addColumn(
            'archived_by_ip_address',
            Types::STRING,
            ['notNull' => false, 'length' => 15]
        );
    }

    protected function addArchivedByIpAddressIndex(Table $table): void
    {
        $table->addIndex(
            ['archived_by_ip_address'],
            substr(
                'fk_' . $table->getName() . '_archived_by_ip_address',
                0,
                SqlHelper::IDENTIFIER_LENGTH_INDEX
            )
        );
    }
}
