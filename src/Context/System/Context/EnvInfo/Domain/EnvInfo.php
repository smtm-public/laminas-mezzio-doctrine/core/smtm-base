<?php

declare(strict_types=1);

namespace Smtm\Base\Context\System\Context\EnvInfo\Domain;

use Smtm\Base\Domain\ValueObjectInterface;
use Smtm\Base\Domain\ValueObjectTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EnvInfo implements ValueObjectInterface
{

    use ValueObjectTrait;

    protected string $gmDate;
    protected \DateTime $date;
    protected \DateTimeZone $timeZone;

    public function getGmDate(): string
    {
        return $this->gmDate;
    }

    public function setGmDate(string $gmDate): static
    {
        $this->gmDate = $gmDate;

        return $this;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getTimeZone(): \DateTimeZone
    {
        return $this->timeZone;
    }

    public function setTimeZone(\DateTimeZone $timeZone): static
    {
        $this->timeZone = $timeZone;

        return $this;
    }
}
