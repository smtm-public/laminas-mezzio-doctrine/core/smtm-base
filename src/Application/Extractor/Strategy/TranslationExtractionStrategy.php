<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Laminas\I18n\Translator\TranslatorAwareInterface;
use Laminas\I18n\Translator\TranslatorAwareTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TranslationExtractionStrategy extends AbstractExtractionStrategy implements TranslatorAwareInterface
{

    use TranslatorAwareTrait;

    public const OPTION_KEY_LOCALE = 'locale';
    public const OPTION_KEY_TEXT_DOMAIN = 'textDomain';

    public function extract($value, ?object $object = null): ?string
    {
        return $this->getTranslator()->translate(
            $value,
            $this->options[static::OPTION_KEY_TEXT_DOMAIN] ?? 'default',
            $this->options[static::OPTION_KEY_TEXT_DOMAIN] ?? null
        );
    }
}
