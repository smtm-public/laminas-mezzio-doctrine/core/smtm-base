<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait NotArchivedSmallintColumnAndIndexMigrationTrait
{
    protected function addNotArchivedSmallintColumnAndIndex(
        Table $table,
        ?string $indexName = null
    ): void {
        $table->addColumn('not_archived', Types::SMALLINT, ['notNull' => false, 'default' => 1]);
        $table->addIndex(
            ['not_archived'],
            substr(
                $indexName ?? 'idx_' . $table->getName() . '_not_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_INDEX
            )
        );
    }
}
