<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\WebDriver;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait WebDriverAwareTrait
{
    protected WebDriver $webDriver;

    public function getWebDriver(): WebDriver
    {
        return $this->webDriver;
    }

    public function setWebDriver(WebDriver $webDriver): static
    {
        $this->webDriver = $webDriver;

        return $this;
    }
}
