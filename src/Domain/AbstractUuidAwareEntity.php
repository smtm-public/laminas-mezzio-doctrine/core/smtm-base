<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

use Ramsey\Uuid\Rfc4122\UuidV4;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractUuidAwareEntity extends AbstractIdAwareEntity implements UuidAwareEntityInterface
{
    protected string $uuid;

    public function __construct()
    {
        $this->uuid = UuidV4::uuid4()->toString();
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): static
    {
        $this->uuid = $uuid;

        return $this;
    }
}
