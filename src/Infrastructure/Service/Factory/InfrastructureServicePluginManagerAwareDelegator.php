<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InfrastructureServicePluginManagerAwareDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var InfrastructureServicePluginManagerAwareInterface $object */
        $object = $callback();

        $object->setInfrastructureServicePluginManager($container->get(InfrastructureServicePluginManager::class));

        return $object;
    }
}
