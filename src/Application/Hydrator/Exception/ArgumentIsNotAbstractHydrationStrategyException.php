<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Exception;

use InvalidArgumentException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ArgumentIsNotAbstractHydrationStrategyException extends InvalidArgumentException
{

}
