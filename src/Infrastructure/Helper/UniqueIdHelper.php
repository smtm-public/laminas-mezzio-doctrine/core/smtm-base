<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UniqueIdHelper
{
    public static function generateHashedUniqueId(
        array $components,
        string $separator = '',
        string $hashAlgo = 'sha256',
    ): string {
        return hash($hashAlgo, implode($separator, $components));
    }

    public static function generateEncryptedUniqueId(
        array $components,
        callable $cryptoFunction,
        string $separator = ''
    ): string {
        return $cryptoFunction(implode($separator, $components));
    }
}
