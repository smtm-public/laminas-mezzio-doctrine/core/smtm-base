<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DomainObjectExtractionStrategy extends AbstractExtractorPluginManagerAwareExtractionStrategy
{
    public function extract($value, ?object $object = null): ?array
    {
        if ($value === null) {
            return null;
        }

        /** @var AbstractDomainObjectExtractor $extractor */
        $extractor = $this->extractorPluginManager->get(
            $this->options[self::OPTION_KEY_EXTRACTOR_NAME],
            $this->options[self::OPTION_KEY_EXTRACTOR_OPTIONS] ?? []
        );

        $result = $extractor->extract($value);

        return $result;
    }
}
