<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\NamingStrategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MappingExtractionNamingStrategy extends AbstractExtractionNamingStrategy
{
    public const OPTION_KEY_MAP = 'map';

    public function extract(string $name, ?object $object = null): string
    {
        if (isset($this->options[static::OPTION_KEY_MAP][$name])) {
            return $this->options[static::OPTION_KEY_MAP][$name];
        }

        return $name;
    }
}
