<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler;

use Laminas\Diactoros\Response\HtmlResponse;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait HtmlHandlerTrait
{
    public function prepareHtmlResponse(
        mixed $body = 'php://temp',
        int $status = 200,
        array $headers = [],
        ?string $contentType = null,
    ): HtmlResponse {
        return new HtmlResponse(
            $body,
            $status,
            array_replace_recursive(
                $headers,
                $contentType !== null ? ['Content-Type' => $contentType] : []
            )
        );
    }
}
