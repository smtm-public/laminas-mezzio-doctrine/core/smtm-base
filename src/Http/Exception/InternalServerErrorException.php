<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Exception;

use Smtm\Base\Infrastructure\Helper\HttpHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InternalServerErrorException extends AbstractHttpException
{
    public const MESSAGE = 'Internal Server Error';
    public const CODE = 0;
    public const STATUS = HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR;
    public const TYPE = 'https://example.com/problems/internal-server-error';

    protected int $status = self::STATUS;
    protected string $title = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR];
    protected string $detail = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR];
    protected string $type = self::TYPE;
}
