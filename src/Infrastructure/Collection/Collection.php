<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Collection;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Collection implements CollectionInterface
{
    use CollectionTrait;
}
