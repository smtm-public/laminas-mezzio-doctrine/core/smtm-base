<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ModifiedByAwareEntityTrait
{
    protected $modifiedBy = null;

    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    public function setModified($modifiedBy): static
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }
}
