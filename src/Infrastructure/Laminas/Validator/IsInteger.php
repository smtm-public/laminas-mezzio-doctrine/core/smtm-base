<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator;

use Laminas\Validator\AbstractValidator;
use Laminas\Validator\Exception\ExtensionNotLoadedException;
use Laminas\Validator\Exception\InvalidArgumentException;
use Traversable;

/**
 * Bidirectional (is|is not) validation for expected value type: integer
 *
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IsInteger extends AbstractValidator
{
    public const OPTION_KEY_LT = 'lt';
    public const OPTION_KEY_GT = 'gt';
    public const OPTION_KEY_INCLUSIVE = 'inclusive';

    protected const INVALID_TRUE = 'invalidValueIs';
    protected const INVALID_FALSE = 'invalidValueIsNot';
    protected const INVALID_NOT_LESS = 'invalidValueNotLess';
    protected const INVALID_NOT_LESS_INCLUSIVE = 'invalidValueNotLessInclusive';
    protected const INVALID_NOT_GREATER = 'invalidValueNotGreater';
    protected const INVALID_NOT_GREATER_INCLUSIVE = 'invalidValueNotGreaterInclusive';

    protected array $options = [
        'is' => true,
        'strict' => true,
        self::OPTION_KEY_LT => null,
        self::OPTION_KEY_GT => null,
        self::OPTION_KEY_INCLUSIVE => true,
    ];
    protected array $messageTemplates = [
        self::INVALID_TRUE => 'Invalid value given. Integer expected.',
        self::INVALID_FALSE => 'Invalid value given. Cannot be an integer.',
        self::INVALID_NOT_LESS => 'Invalid value given. Must be less than %max%; received "%value%".',
        self::INVALID_NOT_LESS_INCLUSIVE => 'Invalid value given. Must be less than or equal to %max%; received "%value%"',
        self::INVALID_NOT_GREATER => 'Invalid value given. Must be greater than %min%; received "%value%".',
        self::INVALID_NOT_GREATER_INCLUSIVE => 'Invalid value given. Must be greater than or equal to %min%; received "%value%"',

    ];
    protected array $messageVariables = [
        'max' => ['options' => 'lt'],
        'min' => ['options' => 'gt'],
    ];

    /**
     * $options can have the following keys:
     * - is - true|false - whether it should be of type integer (true) or not (false)
     *
     * @param array|Traversable $options
     *
     * @throws ExtensionNotLoadedException if ext/intl is not present
     */
    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    /**
     * @param $value
     *
     * @return bool
     *
     * @throws InvalidArgumentException
     */
    public function isValid($value)
    {
        $is = $this->options['is'];
        $value = $this->options['strict'] ? (int) $value : $value;

        if (($is && !is_int($value)) || (!$is && is_int($value))) {
            $this->error(constant('self::INVALID_' . strtoupper(var_export($is, true))));

            return false;
        }

        $this->setValue($value);
        $inclusive = $this->options[self::OPTION_KEY_INCLUSIVE];
        $lt = $this->options[self::OPTION_KEY_LT];

        if (!is_bool($inclusive) && in_array($value, ['true', 'false', '1', '0', 1, 0], true)) {
            $inclusive = filter_var($inclusive, FILTER_VALIDATE_BOOLEAN);
        }

        if ($lt !== null && $inclusive && $value > $lt) {
            $this->error(self::INVALID_NOT_LESS_INCLUSIVE);

            return false;
        }

        if ($lt !== null && !$inclusive && $value >= $lt) {
            $this->error(self::INVALID_NOT_LESS);

            return false;
        }

        $gt = $this->options[self::OPTION_KEY_GT];

        if ($gt !== null && $inclusive && $value < $gt) {
            $this->error(self::INVALID_NOT_GREATER_INCLUSIVE);

            return false;
        }

        if ($gt !== null && !$inclusive && $value <= $gt) {
            $this->error(self::INVALID_NOT_GREATER);

            return false;
        }

        return true;
    }
}
