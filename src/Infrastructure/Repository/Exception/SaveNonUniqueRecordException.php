<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Repository\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class SaveNonUniqueRecordException extends SaveRecordException
{

}
