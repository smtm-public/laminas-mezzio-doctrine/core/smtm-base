<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\Mezzio\Cors\Configuration;

use Mezzio\Cors\Configuration\AbstractConfiguration;
use Webmozart\Assert\Assert;

class ProjectConfiguration extends AbstractConfiguration
{
    /**
     * @psalm-var list<string>
     */
    protected array $allowedHeadersPreflight = [];

    /**
     * @psalm-param list<string> $headers
     */
    public function setAllowedHeadersPreflight(array $headers): void
    {
        Assert::allString($headers);
        $this->allowedHeadersPreflight = array_values(array_unique($headers));
    }

    public function allowedHeadersPreflight(): array
    {
        return $this->allowedHeadersPreflight;
    }
}
