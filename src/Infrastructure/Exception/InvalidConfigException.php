<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Exception;

class InvalidConfigException extends InvalidArgumentException
{

}
