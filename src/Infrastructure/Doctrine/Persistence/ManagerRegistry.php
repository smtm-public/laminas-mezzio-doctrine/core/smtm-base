<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Persistence;

use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManager;
use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManagerInterface;
use Smtm\Base\Infrastructure\Exception\RuntimeException;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Logging\DebugStack;
use Doctrine\ORM\Cache\DefaultCacheFactory;
use Doctrine\ORM\Cache\RegionsConfiguration;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Doctrine\ORM\Proxy\ProxyFactory;
use Doctrine\Persistence\AbstractManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Laminas\Cache\Psr\CacheItemPool\CacheItemPoolDecorator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ManagerRegistry extends AbstractManagerRegistry implements ConfigAwareInterface, InfrastructureServicePluginManagerAwareInterface,
    ManagerRegistryInterface
{

    use ConfigAwareTrait,
        InfrastructureServicePluginManagerAwareTrait;

    /** @var ObjectManager[] $services */
    protected array $services = [];

    public static function createEntityManager(
        InfrastructureServicePluginManager $infrastructureServicePluginManager,
        array $config,
        array $connections,
        string $name
    ): EntityManagerInterface {
        $entityManagerConfig = $config['orm']['entityManagers'][$name];

        $supportedConnections = $entityManagerConfig['connections'];
        $entityManagerConnections = [];

        foreach ($supportedConnections as $supportedConnectionName => $supportedConnectionConfigName) {
            $entityManagerConnections[$supportedConnectionName] = $connections[$supportedConnectionConfigName];
        }

        return
            $infrastructureServicePluginManager
                ->build(
                    EntityManagerInterface::class,
                    [
                        'entityManager' => $entityManagerConfig,
                        'connections' => $entityManagerConnections,
                        'dbal' => $config['dbal'],
                        'orm' => $config['orm'],
                        'secondLevelCache' => $config['orm']['secondLevelCache'],
                        'logging' => $config['logging'],
                    ]
                );
    }

    public function getService($name)
    {
        if ($name instanceof EntityManagerInterface) {
            return $name;
        }

        if ($name instanceof Connection) {
            return $name;
        }

        if (isset($this->services[$name])) {
            return $this->services[$name];
        }

        $nameParts = explode(':', $name);
        $entityManagerName = $nameParts[0];
        $entityManagerConfig = $this->config['orm']['entityManagers'][$entityManagerName];
        $connectionName = $nameParts[1];
        $connectionConfig = $this->config['connections'][$connectionName];

        if (!isset($options['connection'])) {
            throw new RuntimeException('Missing doctrine connection config');
        }

        $config = new Configuration();

        $config->setAutoGenerateProxyClasses(
            isset($entityManagerConfig['autoGenerateProxyClasses'])
                ? (int) $entityManagerConfig['autoGenerateProxyClasses']
                : ProxyFactory::AUTOGENERATE_EVAL
        );
        $config->setProxyDir(__DIR__ . '/../../../../../../../../data/cache');
        $config->setProxyNamespace('Doctrine\Entities');

        if (isset($entityManagerConfig['result']['cache'])) {
            $resultSetCache = new CacheItemPoolDecorator(
                $this->infrastructureServicePluginManager->get($entityManagerConfig['result']['cache'])
            );
            $config->setResultCache($resultSetCache);
        }

        if (isset($entityManagerConfig['metadata']['cache'])) {
            $config->setMetadataCache(
                new CacheItemPoolDecorator(
                    $this->infrastructureServicePluginManager->get($entityManagerConfig['metadata']['cache'])
                )
            );
        }

        if (isset($entityManagerConfig['query']['cache'])) {
            $config->setQueryCache(
                new CacheItemPoolDecorator(
                    $this->infrastructureServicePluginManager->get($entityManagerConfig['query']['cache'])
                )
            );
        }

        if (isset($this->config['orm']['secondLevelCache'])) {
            $factory = new DefaultCacheFactory(
                new RegionsConfiguration(),
                new CacheItemPoolDecorator(
                    $this->infrastructureServicePluginManager->get($options['secondLevelCache'])
                )
            );
            $config->setSecondLevelCacheEnabled(true);
            $config->getSecondLevelCacheConfiguration()->setCacheFactory($factory);
        }

        $config->setMetadataDriverImpl(
            new XmlDriver($options['orm']['mapping']['paths'] ?? [])
        );
        $config->setNamingStrategy(new UnderscoreNamingStrategy(CASE_LOWER, true));

        if ($options['logging']['enabled'] ?? false) {
            $logger = new DebugStack();
            $logger->enabled = true;
            $config->setSQLLogger($logger);
        }

        $filters = $entityManagerConfig['query']['filters'] ?? [];

        foreach ($filters as $filterName => $filterClass) {
            $config->addFilter($filterName, $filterClass);
        }

        $entityManager = EntityManager::create(
            $connectionConfig,
            $config
        );

        $filterNames = array_keys($filters);
        array_walk(
            $filterNames,
            fn (string $filterName) => $entityManager->getFilters()->enable($filterName)
        );

        return $this->services[$name] = $entityManager;
    }

    public function resetService($name)
    {
        if ($name instanceof EntityManagerInterface) {
            $name->reopen();
        }
    }

    public function getAliasNamespace($alias)
    {
        // TODO: Implement getAliasNamespace() method.
    }
}
