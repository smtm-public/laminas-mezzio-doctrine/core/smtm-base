<?php

namespace Smtm\Base\Infrastructure\Service\Factory;

use Smtm\Base\Infrastructure\Service\ApplicationCacheService;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

class ApplicationCacheServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $adapterClass = $container->get('config')['base']['infrastructure']['application_cache']['storage']['adapter'];
        $adapter = $container->get(InfrastructureServicePluginManager::class)->get($adapterClass);

        return new ApplicationCacheService(
            $container->get(InfrastructureServicePluginManager::class),
            $adapter
        );
    }
}
