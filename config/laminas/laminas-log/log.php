<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Doctrine\Persistence\Factory\ManagerRegistryAwareDelegator;
use Smtm\Base\Infrastructure\Helper\EnvHelper;
use Smtm\Base\Infrastructure\Laminas\Log\Writer\Doctrine;
use Laminas\Log\Writer\Factory\WriterFactory;

$logFilters = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal(
        'SMTM_BASE_INFRASTRUCTURE_LAMINAS_LOG_LOGGER_FILTER_PLUGIN_MANAGER',
        '{}'
    ),
    true,
    JSON_THROW_ON_ERROR
);
$logFormatters = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal(
        'SMTM_BASE_INFRASTRUCTURE_LAMINAS_LOG_LOGGER_FORMATTER_PLUGIN_MANAGER',
        '{}'
    ),
    true,
    JSON_THROW_ON_ERROR
);
$logProcessors = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal(
        'SMTM_BASE_INFRASTRUCTURE_LAMINAS_LOG_LOGGER_PROCESSOR_PLUGIN_MANAGER',
        '{}'
    ),
    true,
    JSON_THROW_ON_ERROR
);
$logWriters = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal(
        'SMTM_BASE_INFRASTRUCTURE_LAMINAS_LOG_LOGGER_WRITER_PLUGIN_MANAGER',
        '{}'
    ),
    true,
    JSON_THROW_ON_ERROR
);

return [
    'log_filters' => $logFilters,
    'log_formatters' => $logFormatters,
    'log_processors' => $logProcessors,
    'log_writers' => array_replace_recursive(
        [
            'abstract_factories' => ($logWriters['abstract_factories'] ?? []),
            'factories' => array_merge(
                [
                    Doctrine::class => WriterFactory::class,
                ],
                ($logWriters['factories'] ?? [])
            ),
            'delegators' => array_merge(
                [
                    Doctrine::class => [
                        ManagerRegistryAwareDelegator::class,
                    ],
                ],
                ($logWriters['delegators'] ?? [])
            ),
            'shared' => array_merge(
                [
                    Doctrine::class => false,
                ],
                ($logWriters['shared'] ?? [])
            ),
        ],
        $logWriters
    ),
    'log' => [],
];
