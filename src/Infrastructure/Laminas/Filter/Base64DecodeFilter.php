<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Laminas\Filter\AbstractFilter;
use Traversable;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Base64DecodeFilter extends AbstractFilter
{
    /**
     * @param string|array|Traversable $options (Optional) Options to set
     */
    /*
    public function __construct($options = null)
    {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }
    }
    */

    public function filter($value)
    {
        if ($value === null) {
            return null;
        }

        return base64_decode($value);
    }
}
