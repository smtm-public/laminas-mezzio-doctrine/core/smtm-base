<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ApplicationServicePluginManagerAwareInterface
{
    public function getApplicationServicePluginManager(): ApplicationServicePluginManager;
    public function setApplicationServicePluginManager(ApplicationServicePluginManager $applicationServicePluginManager): static;
}
