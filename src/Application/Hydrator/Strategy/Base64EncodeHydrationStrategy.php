<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Base64EncodeHydrationStrategy extends AbstractHydrationStrategy
{
    public function hydrate($value, ?array $data)
    {
        return base64_encode($value);
    }
}
