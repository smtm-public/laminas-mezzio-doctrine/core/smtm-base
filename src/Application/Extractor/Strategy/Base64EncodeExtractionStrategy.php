<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Base64EncodeExtractionStrategy extends AbstractExtractionStrategy
{
    public function extract($value, ?object $object = null): ?string
    {
        return base64_encode($value);
    }
}
