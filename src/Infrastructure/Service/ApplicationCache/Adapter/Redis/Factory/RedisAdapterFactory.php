<?php

namespace Smtm\Base\Infrastructure\Service\ApplicationCache\Adapter\Redis\Factory;

use Smtm\Base\Infrastructure\Service\ApplicationCache\Adapter\Redis\RedisAdapter;
use Laminas\Serializer\Adapter\Json;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Predis\Client;
use Psr\Container\ContainerInterface;

class RedisAdapterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $config = $container->get('config')['laminas']['cache']['storage']['adapter']['redis'];

        return new RedisAdapter(
            new Client($config['server']),
            new Json()
        );
    }
}
