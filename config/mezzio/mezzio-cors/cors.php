<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../../../.env.mezzio.mezzio-cors')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../../../',
        '.env.mezzio.mezzio-cors'
    );
    $dotenv->load();
}

return [
    'allowed_origins' => json_decode(
        EnvHelper::getEnvFromProcessOrSuperGlobal('MEZZIO_MEZZIO_CORS_ALLOWED_ORIGINS'),
        true,
        JSON_THROW_ON_ERROR
    ) ?: [\Mezzio\Cors\Configuration\ConfigurationInterface::ANY_ORIGIN],
    'allowed_headers_preflight' => json_decode(
        EnvHelper::getEnvFromProcessOrSuperGlobal('MEZZIO_MEZZIO_CORS_ALLOWED_HEADERS_PREFLIGHT'),
        true,
        JSON_THROW_ON_ERROR
    ) ?: [],
    'allowed_headers' => json_decode(
        EnvHelper::getEnvFromProcessOrSuperGlobal('MEZZIO_MEZZIO_CORS_ALLOWED_HEADERS'),
        true,
        JSON_THROW_ON_ERROR
    ) ?: [],
    'allowed_max_age' =>
        EnvHelper::getEnvFromProcessOrSuperGlobal('MEZZIO_MEZZIO_CORS_MAX_AGE'),
    'credentials_allowed' => filter_var(
        EnvHelper::getEnvFromProcessOrSuperGlobal('MEZZIO_MEZZIO_CORS_CREDENTIALS_ALLOWED'),
        FILTER_VALIDATE_BOOLEAN
    ),
    'exposed_headers' => json_decode(
        EnvHelper::getEnvFromProcessOrSuperGlobal('MEZZIO_MEZZIO_CORS_EXPOSED_HEADERS'),
        true,
        JSON_THROW_ON_ERROR
    ) ?: [],
];
