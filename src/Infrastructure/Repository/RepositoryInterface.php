<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Repository;

use Doctrine\Persistence\ObjectRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface RepositoryInterface extends ObjectRepository
{
    public const ORDER_ASC = 'ASC';
    public const ORDER_DESC = 'DESC';

    public const OPTION_KEY_COUNT_FIELD_NAME = 'id';
    public const OPTION_KEY_CACHE_RESULT_SET = 'cacheResultSet';
    public const OPTION_KEY_CACHE_RESULT_SET_KEY = 'cacheResultSetKey';
    public const OPTION_KEY_CACHE_RESULT_SET_LIFETIME = 'cacheResultSetLifetime';
    public const OPTION_KEY_CACHE_STORAGE = 'cacheStorage';
    public const OPTION_KEY_HYDRATION_MODE = 'hydrationMode';
    public const OPTION_KEY_RESULT_COLLECTION_INDEX_BY = 'resultCollectionIndexBy';
    public const OPTION_KEY_RESULT_COLLECTION_INDEX_BY_LETTER_CASE = 'resultCollectionIndexByLetterCase';
    public const OPTION_KEY_FILTERS_ENABLE = 'filtersEnable';
    public const OPTION_KEY_FILTERS_DISABLE = 'filtersDisable';

    public function save(object $entity): void;
    public function remove(object $entity): void;
    public function findCountBy(array $criteria): int;
}
