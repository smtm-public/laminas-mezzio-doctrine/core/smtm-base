<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Handler\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InvalidArgumentException extends \Smtm\Base\Http\Exception\InvalidArgumentException
{

}
