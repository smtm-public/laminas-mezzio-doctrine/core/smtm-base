<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class SetValueExtractionStrategy extends AbstractExtractionStrategy
{
    public const OPTION_KEY_VALUE = 'value';

    public function extract($value, ?object $object = null): object|array|string|float|int|bool|null
    {
        return $this->options[static::OPTION_KEY_VALUE];
    }
}
