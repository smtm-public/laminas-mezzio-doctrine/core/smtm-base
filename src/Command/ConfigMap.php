<?php

declare(strict_types=1);

namespace Smtm\Base\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * Example:
 * vendor/bin/symfony-console base:config-map -vvv --file='devops/env/dev/dev-configmap.yaml' --view
 * vendor/bin/symfony-console base:config-map -vvv --file='devops/env/dev/dev-configmap.yaml' --html
 * vendor/bin/symfony-console base:config-map -vvv --file='devops/env/dev/dev-secret.yaml' --view --base64DecodeValues
 * vendor/bin/symfony-console base:config-map -vvv --file='devops/env/dev/dev-secret.yaml' --html --base64DecodeValues
 * vendor/bin/symfony-console base:config-map -vvv --file='devops/env/dev/dev-configmap.yaml' --key='DOCTRINE_ORM_AUTO_GENERATE_PROXY_CLASSES' --value='1'
 * vendor/bin/symfony-console base:config-map -vvv --file='devops/env/dev/dev-secret.yaml' --key='DOCTRINE_DB_CONNECTIONS' --base64JsonKey='default.driverClass' --base64JsonValue='Doctrine\\DBAL\\Driver\\PDO\\SQLite\\Driver'
 */
class ConfigMap extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('base:config-map')
            // the short description shown while running "php bin/console list"
            ->setDescription(
                'Smtm - Base - Config Map Viewer/Editor'
            )
            // the full command description shown when running the command with
            // the "--help" option
            //->setHelp('Could write some useful text here')

            //->addArgument('doctrineMigrationCommand', InputArgument::OPTIONAL,
            //    'The actual Doctrine migration command to run')
            ->addOption(
                'file',
                'f',
                InputOption::VALUE_REQUIRED,
                'File' . "\n"
            )
            ->addOption(
                'view',
                'i',
                InputOption::VALUE_NONE,
                'Only view contents. Does not make any changes to the file. Disables modification related options.' . "\n"
            )
            ->addOption(
                'html',
                't',
                InputOption::VALUE_NONE,
                'Generate an HTML page for easier editing.' . "\n"
            )
            ->addOption(
                'base64DecodeValues',
                'd',
                InputOption::VALUE_NONE,
                'Base64 decode values. Only useful with the --view option' . "\n"
            )
            ->addOption(
                'key',
                'k',
                InputOption::VALUE_REQUIRED,
                'Key' . "\n"
            )
            ->addOption(
                'value',
                'l',
                InputOption::VALUE_OPTIONAL,
                'The new value' . "\n"
            )
            ->addOption(
                'jsonKey',
                'j',
                InputOption::VALUE_OPTIONAL,
                'The path to the JSON element' . "\n"
            )
            ->addOption(
                'jsonValue',
                's',
                InputOption::VALUE_OPTIONAL,
                'The new value of the JSON element' . "\n"
            )
            ->addOption(
                'base64JsonKey',
                'b',
                InputOption::VALUE_OPTIONAL,
                'The path to the JSON element in a Base64 encoded value' . "\n"
            )
            ->addOption(
                'base64JsonValue',
                'a',
                InputOption::VALUE_OPTIONAL,
                'The new value of the JSON element in a Base64 encoded value' . "\n"
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$this->logger->info('This is some info.');
        //$this->logger->error('This is an error!');

        $optionFile = $input->getOption('file');
        $optionFileFullPath = $optionFile;

        if ($optionFile[0] !== '/') {
            $optionFileFullPath = getcwd() . '/' . $optionFile;
        }

        $optionView = $input->getOption('view');
        $optionHtml = $input->getOption('html');
        $optionBase64DecodeValues = $input->getOption('base64DecodeValues');
        $optionKey = $input->getOption('key');
        $optionValue = $input->getOption('value') ?: null;
        $optionJsonKey = $input->getOption('jsonKey') ?: null;
        $optionJsonValue = $input->getOption('jsonValue') ?: null;
        $optionBase64JsonKey = $input->getOption('base64JsonKey') ?: null;
        $optionBase64JsonValue = $input->getOption('base64JsonValue') ?: null;

        if (!is_file($optionFile)) {
            echo 'Error: Invalid file path provided ('
                . $optionFile . ') resolving to ('
                . realpath($optionFileFullPath) . ') is either non-existent or is not a file.' . "\n";

            return Command::INVALID;
        }

        $fileContent = file_get_contents($optionFile);

        if (empty($fileContent)) {
            echo 'Error: The specified file is empty.';

            return Command::INVALID;
        }

        $parsed = yaml_parse_file($optionFile);

        if (empty($parsed)) {
            echo 'Error: The specified file cannot be parsed.';

            return Command::INVALID;
        }

        if ($optionHtml) {
            preg_match('#^.*?^data:#sm', $fileContent, $contentHeading);
            $contentHeading = $contentHeading[0];
            $parsedForViewing = $parsed;

            if ($optionBase64DecodeValues) {
                $parsedForViewing['data'] = $this->base64DecodeValues($parsedForViewing['data']);
            }

            $data = [];

            foreach ($parsedForViewing['data'] as $key => $value) {
                $row = <<< EOT
                <div class="clear-fix">
                EOT;
                $row .= <<< EOT
                <span class="span-textarea data-key" style="float: left; width: 40%;" role="textbox" contenteditable="true">$key</span>
                EOT;
                $row .= <<< EOT
                <span class="span-textarea data-value" style="float: right; width: 60%;" role="textbox" contenteditable="true">$value</span>
                EOT;
                $row .= <<< EOT
                </div>
                EOT;
                $data[] = $row;
            }

            $rows = implode($data);
            $checkboxBase64EncodeValuesChecked = $optionBase64DecodeValues ? 'checked="checked"' : '';

            $html = <<< EOT
            <!DOCTYPE html>
            <html>
            <head>
                <title>$optionFile</title>
                <style type="text/css">
                .clear-fix:after {
                    content: ".";
                    display: block;
                    height: 0px;
                    visibility: hidden;
                    clear: both;
                }
                
                .span-textarea {
                    display: block;
                    overflow: auto;
                    resize: vertical;
                    
                    accent-color: auto;
                    align-content: normal;
                    align-items: normal;
                    align-self: auto;
                    animation-delay: 0s;
                    animation-direction: normal;
                    animation-duration: 0s;
                    animation-fill-mode: none;
                    animation-iteration-count: 1;
                    animation-name: none;
                    animation-play-state: running;
                    animation-timing-function: ease;
                    appearance: auto;
                    aspect-ratio: auto;
                    backface-visibility: visible;
                    background-attachment: scroll;
                    background-blend-mode: normal;
                    background-clip: border-box;
                    background-color: rgb(255, 255, 255);
                    background-image: none;
                    background-origin: padding-box;
                    background-position-x: 0%;
                    background-position-y: 0%;
                    background-repeat: repeat;
                    background-size: auto;
                    /* block-size: 55px; */
                    border-block-end-color: rgb(227, 227, 227);
                    border-block-end-style: inset;
                    border-block-end-width: 2px;
                    border-block-start-color: rgb(227, 227, 227);
                    border-block-start-style: inset;
                    border-block-start-width: 2px;
                    border-bottom-color: rgb(227, 227, 227);
                    border-bottom-left-radius: 0px;
                    border-bottom-right-radius: 0px;
                    border-bottom-style: inset;
                    border-bottom-width: 2px;
                    border-collapse: separate;
                    border-end-end-radius: 0px;
                    border-end-start-radius: 0px;
                    border-image-outset: 0;
                    border-image-repeat: stretch;
                    border-image-slice: 100%;
                    border-image-source: none;
                    border-image-width: 1;
                    border-inline-end-color: rgb(227, 227, 227);
                    border-inline-end-style: inset;
                    border-inline-end-width: 2px;
                    border-inline-start-color: rgb(227, 227, 227);
                    border-inline-start-style: inset;
                    border-inline-start-width: 2px;
                    border-left-color: rgb(227, 227, 227);
                    border-left-style: inset;
                    border-left-width: 2px;
                    border-right-color: rgb(227, 227, 227);
                    border-right-style: inset;
                    border-right-width: 2px;
                    border-spacing: 0px 0px;
                    border-start-end-radius: 0px;
                    border-start-start-radius: 0px;
                    border-top-color: rgb(227, 227, 227);
                    border-top-left-radius: 0px;
                    border-top-right-radius: 0px;
                    border-top-style: inset;
                    border-top-width: 2px;
                    bottom: auto;
                    box-decoration-break: slice;
                    box-shadow: none;
                    box-sizing: border-box;
                    break-after: auto;
                    break-before: auto;
                    break-inside: auto;
                    caption-side: top;
                    caret-color: rgb(0, 0, 0);
                    clear: none;
                    clip: auto;
                    clip-path: none;
                    clip-rule: nonzero;
                    color: rgb(0, 0, 0);
                    color-interpolation: srgb;
                    color-interpolation-filters: linearrgb;
                    color-scheme: normal;
                    column-count: auto;
                    column-fill: balance;
                    column-gap: normal;
                    column-rule-color: rgb(0, 0, 0);
                    column-rule-style: none;
                    column-rule-width: 0px;
                    column-span: none;
                    column-width: auto;
                    contain: none;
                    content: normal;
                    counter-increment: none;
                    counter-reset: none;
                    counter-set: none;
                    cursor: text;
                    cx: 0px;
                    cy: 0px;
                    d: none;
                    direction: ltr;
                    /* display: inline-block; */
                    dominant-baseline: auto;
                    empty-cells: show;
                    fill: rgb(0, 0, 0);
                    fill-opacity: 1;
                    fill-rule: nonzero;
                    filter: none;
                    flex-basis: auto;
                    flex-direction: row;
                    flex-grow: 0;
                    flex-shrink: 1;
                    flex-wrap: nowrap;
                    float: none;
                    flood-color: rgb(0, 0, 0);
                    flood-opacity: 1;
                    font-family: monospace;
                    font-feature-settings: normal;
                    font-kerning: auto;
                    font-language-override: normal;
                    font-optical-sizing: auto;
                    font-size: 13px;
                    font-size-adjust: none;
                    font-stretch: 100%;
                    font-style: normal;
                    font-synthesis: weight style small-caps;
                    font-variant-alternates: normal;
                    font-variant-caps: normal;
                    font-variant-east-asian: normal;
                    font-variant-ligatures: normal;
                    font-variant-numeric: normal;
                    font-variant-position: normal;
                    font-variation-settings: normal;
                    font-weight: 400;
                    grid-auto-columns: auto;
                    grid-auto-flow: row;
                    grid-auto-rows: auto;
                    grid-column-end: auto;
                    grid-column-start: auto;
                    grid-row-end: auto;
                    grid-row-start: auto;
                    grid-template-areas: none;
                    grid-template-columns: none;
                    grid-template-rows: none;
                    /* height: 55px; */
                    hyphenate-character: auto;
                    hyphens: manual;
                    image-orientation: from-image;
                    image-rendering: auto;
                    ime-mode: auto;
                    /* inline-size: 661.2px; */
                    inset-block-end: auto;
                    inset-block-start: auto;
                    inset-inline-end: auto;
                    inset-inline-start: auto;
                    isolation: auto;
                    justify-content: normal;
                    justify-items: normal;
                    justify-self: auto;
                    left: auto;
                    letter-spacing: normal;
                    lighting-color: rgb(255, 255, 255);
                    line-break: auto;
                    line-height: normal;
                    list-style-image: none;
                    list-style-position: outside;
                    list-style-type: disc;
                    margin-block-end: 1px;
                    margin-block-start: 1px;
                    margin-bottom: 1px;
                    margin-inline-end: 0px;
                    margin-inline-start: 0px;
                    margin-left: 0px;
                    margin-right: 0px;
                    margin-top: 1px;
                    marker-end: none;
                    marker-mid: none;
                    marker-start: none;
                    mask-clip: border-box;
                    mask-composite: add;
                    mask-image: none;
                    mask-mode: match-source;
                    mask-origin: border-box;
                    mask-position-x: 0%;
                    mask-position-y: 0%;
                    mask-repeat: repeat;
                    mask-size: auto;
                    mask-type: luminance;
                    max-block-size: none;
                    max-height: none;
                    max-inline-size: none;
                    max-width: none;
                    min-block-size: 0px;
                    min-height: 0px;
                    min-inline-size: 0px;
                    min-width: 0px;
                    mix-blend-mode: normal;
                    object-fit: fill;
                    object-position: 50% 50%;
                    offset-anchor: auto;
                    offset-distance: 0px;
                    offset-path: none;
                    offset-rotate: auto;
                    opacity: 1;
                    order: 0;
                    outline-color: rgb(0, 0, 0);
                    outline-offset: 0px;
                    outline-style: none;
                    outline-width: 0px;
                     overflow-anchor: auto;
                    /* overflow-block: visible; */
                    /* overflow-inline: visible; */
                    overflow-wrap: break-word;
                    /* overflow-x: visible; */
                    /* overflow-y: visible; */
                    overscroll-behavior-block: auto;
                    overscroll-behavior-inline: auto;
                    overscroll-behavior-x: auto;
                    overscroll-behavior-y: auto;
                    padding-block-end: 2px;
                    padding-block-start: 2px;
                    padding-bottom: 2px;
                    padding-inline-end: 2px;
                    padding-inline-start: 2px;
                    padding-left: 2px;
                    padding-right: 2px;
                    padding-top: 2px;
                    paint-order: normal;
                    perspective: none;
                    /* perspective-origin: 330.6px 27.5px; */
                    pointer-events: auto;
                    position: static;
                    print-color-adjust: economy;
                    quotes: auto;
                    r: 0px;
                    /* resize: both; */
                    right: auto;
                    rotate: none;
                    row-gap: normal;
                    ruby-align: space-around;
                    ruby-position: alternate;
                    rx: auto;
                    ry: auto;
                    scale: none;
                    scroll-behavior: auto;
                    scroll-margin-block-end: 0px;
                    scroll-margin-block-start: 0px;
                    scroll-margin-bottom: 0px;
                    scroll-margin-inline-end: 0px;
                    scroll-margin-inline-start: 0px;
                    scroll-margin-left: 0px;
                    scroll-margin-right: 0px;
                    scroll-margin-top: 0px;
                    scroll-padding-block-end: auto;
                    scroll-padding-block-start: auto;
                    scroll-padding-bottom: auto;
                    scroll-padding-inline-end: auto;
                    scroll-padding-inline-start: auto;
                    scroll-padding-left: auto;
                    scroll-padding-right: auto;
                    scroll-padding-top: auto;
                    scroll-snap-align: none;
                    scroll-snap-type: none;
                    scrollbar-color: auto;
                    scrollbar-gutter: auto;
                    scrollbar-width: auto;
                    shape-image-threshold: 0;
                    shape-margin: 0px;
                    shape-outside: none;
                    shape-rendering: auto;
                    stop-color: rgb(0, 0, 0);
                    stop-opacity: 1;
                    stroke: none;
                    stroke-dasharray: none;
                    stroke-dashoffset: 0px;
                    stroke-linecap: butt;
                    stroke-linejoin: miter;
                    stroke-miterlimit: 4;
                    stroke-opacity: 1;
                    stroke-width: 1px;
                    tab-size: 8;
                    table-layout: auto;
                    text-align: start;
                    text-align-last: auto;
                    text-anchor: start;
                    text-combine-upright: none;
                    text-decoration-color: rgb(0, 0, 0);
                    text-decoration-line: none;
                    text-decoration-skip-ink: auto;
                    text-decoration-style: solid;
                    text-decoration-thickness: auto;
                    text-emphasis-color: rgb(0, 0, 0);
                    text-emphasis-position: over right;
                    text-emphasis-style: none;
                    text-indent: 0px;
                    text-justify: auto;
                    text-orientation: mixed;
                    text-overflow: clip;
                    text-rendering: optimizelegibility;
                    text-shadow: none;
                    text-transform: none;
                    text-underline-offset: auto;
                    text-underline-position: auto;
                    top: auto;
                    touch-action: auto;
                    transform: none;
                    transform-box: border-box;
                    /* transform-origin: 330.6px 27.5px; */
                    transform-style: flat;
                    transition-delay: 0s;
                    transition-duration: 0s;
                    transition-property: all;
                    transition-timing-function: ease;
                    translate: none;
                    unicode-bidi: normal;
                    user-select: auto;
                    vector-effect: none;
                    vertical-align: text-bottom;
                    visibility: visible;
                    white-space: pre-wrap;
                    /* width: 661.2px; */
                    will-change: auto;
                    word-break: normal;
                    word-spacing: 0px;
                    writing-mode: horizontal-tb;
                    x: 0px;
                    y: 0px;
                    z-index: auto
                }
                </style>
                <script type="text/javascript">
                function yamlize() {
                    let rowsContainer = document.getElementById('rows');
                    let rows = rowsContainer.getElementsByTagName('div');
                    let yaml = document.getElementById('yaml');
                    yaml.innerText = document.getElementById('yaml-heading').innerText + "\\n";
                    for (let i in rows) {
                        if (rows.hasOwnProperty(i)) {
                            let keyValue = rows[i].getElementsByTagName('span');
                            yaml.innerText += '  ' + keyValue[0].innerText + ': '
                                + '\''
                                + (
                                    document.getElementById('base64-encode-values').checked
                                        ? btoa(keyValue[1].innerText)
                                        : keyValue[1].innerText
                                )
                                + '\'' + "\\n";
                        }
                    }
                    yaml.innerText += "\\n";
                }
                </script>
            </head>
            
            <body>
                <span id="yaml-heading" class="span-textarea" style="" role="textbox" contenteditable="true">$contentHeading</span>
                <div id="rows">
                $rows
                </div>
                <button onclick="yamlize();">YAMLize</button>&nbsp;Base64 Encode Values:&nbsp;<input type="checkbox" id="base64-encode-values" $checkboxBase64EncodeValuesChecked />
                <span id="yaml" class="span-textarea" style="" role="textbox" contenteditable="true"></span>
            </body>
            
            </html>
            EOT;

            $htmlFilePath = $optionFileFullPath . '.html';
            file_put_contents($htmlFilePath, $html);
            echo 'Wrote HTML to (' . $htmlFilePath . ').' . "\n";

            return Command::SUCCESS;
        }

        if ($optionView) {
            $parsedForViewing = $parsed;

            if ($optionBase64DecodeValues) {
                $parsedForViewing['data'] = $this->base64DecodeValues($parsedForViewing['data']);
            }

            echo $this->yamlize($parsedForViewing);

            return Command::SUCCESS;
        }

        if ($optionBase64JsonKey !== null) {
            $base64JsonKeyElements = explode('.', $optionBase64JsonKey);
            $base64DecodedValue = base64_decode($parsed['data'][$optionKey]);

            if ($base64DecodedValue === false) {
                echo 'Error: Could not base64 decode the config entry (' . $optionKey . ').';

                return Command::FAILURE;
            }

            $jsonDecodedBase64DecodedValue = json_decode($base64DecodedValue, true);

            $currentElement = &$jsonDecodedBase64DecodedValue;
            $currentPath = [];

            while(!empty($base64JsonKeyElements)) {
                $currentKey = array_shift($base64JsonKeyElements);

                if (!array_key_exists($currentKey, $currentElement)) {
                    echo 'Error: Invalid value for option --base64JsonKey provided. No key ('
                        . $currentKey . ') found in path (' .implode('.', $currentPath) . ') of config key ('
                        . $optionKey . '). The parsed value of the config entry is:' . "\n";
                    echo base64_decode($parsed['data'][$optionKey]);

                    return Command::INVALID;
                }

                $currentPath[] = $currentKey;
                $currentElement = &$currentElement[$currentKey];
            }

            $currentElement = $optionBase64JsonValue;
            $jsonEncodedValue = json_encode($jsonDecodedBase64DecodedValue);
            $base64JsonEncodedValue = base64_encode($jsonEncodedValue);
            $parsed['data'][$optionKey] = $base64JsonEncodedValue;
            $index = 0;

            while (file_exists($optionFile . '.old' . $index)) {
                $index++;
            }

            $yaml = $this->yamlize($parsed);
            rename($optionFile, $optionFile . '.old' . $index);
            file_put_contents($optionFile, $yaml);

            return Command::SUCCESS;
        }

        return Command::SUCCESS;
    }

    protected function base64DecodeValues(array $data): array
    {
        return array_combine(
            array_keys($data),
            array_map(
                fn (string $value) => base64_decode($value),
                $data
            )
        );
    }

    protected function yamlize(array $content): string
    {
        $yaml = yaml_emit($content, YAML_ANY_ENCODING, YAML_LN_BREAK);
        #yaml_emit_file($optionFile, $parsed, YAML_ANY_ENCODING, YAML_LN_BREAK);
        $yaml = preg_replace('#^([^:]*?)(: *)(.+)$#m', '$1$2\'$3\'', $yaml);
        $yaml = preg_replace('#^([^:]*?)(: *)\'""\'$#m', '$1$2\'\'', $yaml);
        $yaml = preg_replace('#^---$\n#m', '', $yaml);
        $yaml = preg_replace('#^...$\n#m', '', $yaml);

        return $yaml;
    }
}
