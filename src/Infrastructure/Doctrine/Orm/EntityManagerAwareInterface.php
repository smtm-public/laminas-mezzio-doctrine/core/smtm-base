<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface EntityManagerAwareInterface
{
    public function getEntityManager(): ?EntityManagerInterface;
    public function setEntityManager(EntityManagerInterface $entityManager): static;
}
