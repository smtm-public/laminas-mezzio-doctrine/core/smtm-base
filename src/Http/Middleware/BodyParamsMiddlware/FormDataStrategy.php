<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\BodyParamsMiddlware;

use Laminas\Diactoros\UploadedFile;
use Mezzio\Helper\BodyParams\StrategyInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Grigor Milchev <grigor@smtm.bg>
 */
class FormDataStrategy implements StrategyInterface
{
    protected string $contentType;
    protected string $rawBody;

    /**
     * @inheritDoc
     */
    public function match(string $contentType): bool
    {
        $this->contentType = $contentType;

        return preg_match('#^multipart/form-data($|[ ;])#', $this->contentType) === 1;
    }

    /**
     * @inheritDoc
     */
    public function parse(ServerRequestInterface $request): ServerRequestInterface
    {
        $parsedBody = $request->getParsedBody();

        if (! empty($parsedBody)) {
            return $request;
        }

        $this->rawBody = (string) $request->getBody();

        if (empty($this->rawBody)) {
            return $request;
        }

        return $this->parseRawBody($request);
    }

    /**
     * Parses the raw body
     *
     * @param ServerRequestInterface $request
     *
     * @return ServerRequestInterface
     */
    protected function parseRawBody(ServerRequestInterface $request): ServerRequestInterface
    {
        $parsedBody = [];
        preg_match('/boundary=(.*)$/', $this->contentType, $matches);
        $bodyBlocks = preg_split("/-+$matches[1]/", $this->rawBody);
        array_pop($bodyBlocks);

        foreach ($bodyBlocks as $block) {
            if (empty($block)) {
                continue;
            }

            if (strpos($block, 'filename=') !== false) {
                preg_match(
                    '/name=\"([^\"]*)\"; filename=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s',
                    $this->rawBody,
                    $match
                );

                if (!empty($match)) {
                    preg_match('/Content-Type: (.*)?/', $match[3], $mime);

                    $fileContent = preg_replace('/Content-Type: (.*)[^\n\r]/', '', $match[3]);
                    $path = sys_get_temp_dir().'/php'.substr(sha1((string) rand()), 0, 6);
                    $err = file_put_contents($path, trim($fileContent));

                    $file = new UploadedFile(
                        $path,
                        filesize($path),
                        $err === false ? $err : UPLOAD_ERR_OK,
                        $match[2],
                        $mime[1]
                    );

                    $request = $request->withUploadedFiles([$match[1] => $file]);
                }
            } else {
                preg_match('/name=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $matches);
                $parsedBody[$matches[1]] = $matches[2];
            }
        }

        return $request->withParsedBody($parsedBody);
    }
}
