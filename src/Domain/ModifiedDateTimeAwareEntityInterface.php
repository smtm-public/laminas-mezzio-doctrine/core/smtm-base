<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ModifiedDateTimeAwareEntityInterface
{
    public function getModified(): ?\DateTime;
    public function setModified(?\DateTime $modified): static;
}
