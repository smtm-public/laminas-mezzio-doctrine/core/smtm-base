<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure;

use Smtm\Base\Infrastructure\Helper\ProcessHelper;
use Laminas\Log\Formatter\Simple;
use Laminas\Log\Logger;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * TODO: MD - Due to \Laminas\Log\Logger::__destruct() being called before \Doctrine\DBAL\Logging\Connection::__destruct()
 * TODO:      and \Laminas\Log\Logger::__destruct() shutting down/removing all of its writers
 * TODO:      and \Doctrine\DBAL\Logging\Connection::__destruct() trying to log "Disconnecting" the \Laminas\Log\Logger
 * TODO:      will throw a "PHP Fatal error:  Uncaught Laminas\Log\Exception\RuntimeException: No log writer specified"
 */
class LaminasPsrLogger extends Logger implements LaminasPsrLoggerInterface
{
    public const OPTION_KEY_LOG_MEMORY_GET_USAGE = 'logMemoryGetUsage';
    public const OPTION_KEY_LOG_MEMORY_GET_USAGE_FORMATTED = 'logMemoryGetUsageFormatted';
    public const OPTION_KEY_LOG_MEMORY_GET_USAGE_REAL = 'logMemoryGetUsageReal';
    public const OPTION_KEY_LOG_MEMORY_GET_USAGE_REAL_FORMATTED = 'logMemoryGetUsageRealFormatted';

    protected bool $logMemoryGetUsage = false;
    protected bool $logMemoryGetUsageFormatted = false;
    protected bool $logMemoryGetUsageReal = false;
    protected bool $logMemoryGetUsageRealFormatted = false;
    protected array $formatterConfig = [
        'format' => Simple::DEFAULT_FORMAT,
    ];

    public function __construct($options = null)
    {
        parent::__construct($options);

        $this->logMemoryGetUsage = (bool) ($options[self::OPTION_KEY_LOG_MEMORY_GET_USAGE] ?? false);
        $this->logMemoryGetUsageFormatted = (bool) ($options[self::OPTION_KEY_LOG_MEMORY_GET_USAGE_FORMATTED] ?? false);
        $this->logMemoryGetUsageReal = (bool) ($options[self::OPTION_KEY_LOG_MEMORY_GET_USAGE_REAL] ?? false);
        $this->logMemoryGetUsageRealFormatted = (bool) ($options[self::OPTION_KEY_LOG_MEMORY_GET_USAGE_REAL_FORMATTED] ?? false);

        $formatterMessageTemplate = '%timestamp% %priorityName% (%priority%): %message%';

        if ($this->logMemoryGetUsage || $this->logMemoryGetUsageFormatted || $this->logMemoryGetUsageReal || $this->logMemoryGetUsageRealFormatted) {
            $formatterMessageTemplate .= "\n" . '%extra%';
        }

        $this->formatterConfig = [
            'format' => $formatterMessageTemplate,
        ];
    }

    public function log($level, $message, $context = [])
    {
        if ($this->logMemoryGetUsage) {
            $context['MEM'] = ProcessHelper::getStatsMemoryGetUsage()['memory_get_usage'];
        }

        if ($this->logMemoryGetUsageFormatted) {
            $context['MEMf'] = ProcessHelper::getStatsMemoryGetUsage()['memory_get_usage_formatted'];
        }

        if ($this->logMemoryGetUsageReal) {
            $context['rMEM'] = ProcessHelper::getStatsMemoryGetUsageReal()['memory_get_usage_real'];
        }

        if ($this->logMemoryGetUsageRealFormatted) {
            $context['rMEMf'] = ProcessHelper::getStatsMemoryGetUsageReal()['memory_get_usage_real_formatted'];
        }

        if (is_string($level)) {
            $priority = match ($level) {
                'emerg', 'emergency' => Logger::EMERG,
                'alert' => Logger::ALERT,
                'crit', 'critical' => Logger::CRIT,
                'err', 'error' => Logger::ERR,
                'warn', 'warning' => Logger::WARN,
                'notice' => Logger::NOTICE,
                'info' => Logger::INFO,
                'debug' => Logger::DEBUG,
            };
        }

        parent::log($level, $message, $context);
    }

    public function critical($message, array $context = [])
    {
        return $this->crit($message, $context);
    }

    public function emergency($message, array $context = [])
    {
        return $this->emerg($message, $context);
    }

    public function error($message, array $context = [])
    {
        return $this->err($message, $context);
    }

    public function warning($message, array $context = [])
    {
        return $this->warn($message, $context);
    }

    public function getFormatterConfig(): array
    {
        return $this->formatterConfig;
    }

    public function setFormatterConfig(array $formatterConfig): static
    {
        $this->formatterConfig = $formatterConfig;

        return $this;
    }
}
