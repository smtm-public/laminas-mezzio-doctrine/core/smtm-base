<?php

declare(strict_types=1);

namespace Smtm\Base\Factory;

use Smtm\Base\ConfigAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigAwareDelegator implements DelegatorFactoryInterface
{
    protected array|string|null $configKey = null;

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var ConfigAwareInterface $object */
        $object = $callback();

        if ($this->configKey === null) {
            $object->setConfig($container->get('config'));
        } elseif (is_string($this->configKey)) {
            $object->setConfig($container->get('config')[$this->configKey]);
        } else {
            $configKey = $this->configKey;
            $config = $container->get('config');

            do {
                $config = $config[array_shift($configKey)];
            } while (!empty($configKey));

            $object->setConfig($config);
        }

        return $object;
    }

    public function getConfigKey(): array|string|null
    {
        return $this->configKey;
    }

    public function setConfigKey(array|string|null $configKey): static
    {
        $this->configKey = $configKey;

        return $this;
    }
}
