<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Service\ImageProcessor\Exception\ImageBlobValidationException;
use Smtm\Base\Infrastructure\Service\ImageProcessor\Exception\ImageProcessorException;
use Smtm\Base\Infrastructure\Service\ImageProcessor\Exception\InvalidImageMimeTypeException;
use Smtm\Base\Infrastructure\Service\ImageProcessor\Exception\UnrecognizedImageFormatException;
use Imagick;
use ImagickException;
use JetBrains\PhpStorm\Pure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 * @author Atanas Pyuskyulev <a.puskulev@smtm.com>
 */
class ImageProcessor extends AbstractInfrastructureService
{
    /**
     * Those two defined constants are used by the setQuality() method.
     * For more information why we need this:
     * @see https://www.php.net/manual/en/imagick.setcompressionquality.php
     */
    public const CREATE_FROM_NEW = 1;
    public const CREATE_FROM_EXISTING = 2;

    public const IMAGE_QUALITY_HIGH = 100;
    public const IMAGE_QUALITY_HALF = 50;
    public const IMAGE_QUALITY_LOW = 1;

    public const FORMAT_GIF  = 'gif';
    public const FORMAT_JPG  = 'jpg';
    public const FORMAT_PNG  = 'png';
    public const FORMAT_WEBP = 'webp';
    public const FORMAT_BMP  = 'bmp';

    protected array $allowedMimeTypesAndMapping = [
        'image/jpeg' => self::FORMAT_JPG,
        'image/jpg'  => self::FORMAT_JPG,
        'image/png'  => self::FORMAT_PNG,
        'image/gif'  => self::FORMAT_GIF,
        'image/webp' => self::FORMAT_WEBP,
        'image/bmp'  => self::FORMAT_BMP
    ];

    #[Pure] public function __construct(
        InfrastructureServicePluginManager $infrastructureServiceManager,
        protected Imagick $imagick
    ) {
        parent::__construct($infrastructureServiceManager);
    }

    public function addMimetypeAllowance(string $mimeType, string $allowedExtension): self
    {
        $this->allowedMimeTypesAndMapping[$mimeType] = $allowedExtension;

        return $this;
    }

    public function prepareRawBase64Image(string $base64Image): array
    {
        if(!str_contains($base64Image, ';base64') || !str_contains($base64Image, 'data:')) {
            throw new ImageProcessorException(
                'Invalid base64 string received. Cannot process this image.',
                HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR
            );
        }

        $dataParts = explode(';base64,', $base64Image);
        $mimePart = strtolower(ltrim($dataParts[0], 'data:'));
        $imagePart = trim($dataParts[1]);

        if (!array_key_exists($mimePart, $this->allowedMimeTypesAndMapping)) {
            throw new InvalidImageMimeTypeException(
                sprintf("%s is not valid mime type for this file", $mimePart),
                HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR
            );
        }

        return [
            'extension' => $this->getStandardFormatName(
                $this->allowedMimeTypesAndMapping[$mimePart]
            ),
            'mimetype' => $mimePart,
            'data' => $imagePart
        ];
    }

    public function writeImageOnDisk(
        string $destinationFile,
        string $fileData,
        array $extension,
        bool $shouldDecodeData = true
    ): string
    {
        $imageData = $shouldDecodeData ? base64_decode($fileData) : $fileData;

        $this->validateImageBlobFormat($imageData, $extension);

        $fileHandle = fopen($destinationFile, 'wb');
        fwrite($fileHandle, $imageData);
        fclose($fileHandle);

        return $destinationFile;
    }

    public function crop(string $data, string $format, array $coordinates, int $width, int $height): array
    {
        try {
            $data = base64_decode($data);
            $this->imagick->readImageBlob($data);
            $this->setQuality(static::IMAGE_QUALITY_HIGH);

            $this->imagick->cropImage(
                $width,
                $height,
                (int) $coordinates['x1'],
                (int) $coordinates['y1']
            );

            return [
                'data' => $this->imagick->getImagesBlob(),
                'extension' => $this->getStandardFormatName($format),
                'size' => $this->imagick->getImageLength(), // in bytes
                'MimeType' => $this->imagick->getImageMimeType()
            ];
        } catch(ImagickException $e) {
            throw new ImageProcessorException(
                $e->getMessage(),
                HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR,
                $e
            );
        }
    }

    public function resizeAndConvert(
        string $data,
        int $width,
        int $height,
        string $format = self::FORMAT_PNG,
        bool $base64decode = false,
        bool $returnMetadata = false,
    ): string|array
    {
        $format = $this->getStandardFormatName($format);

        $this->imagick->clear();

        if($base64decode) {
            $data = base64_decode($data);
        }

        try {
            $this->imagick->readImageBlob($data);

            if($width > $this->getWidth() || $height > $this->getHeight()) {
                $this->imagick->resizeImage(
                    $width,
                    $height,
                    Imagick::FILTER_UNDEFINED,
                    0,
                    true
                );
            }

            $this->imagick->setFormat($format);

            return !$returnMetadata
                ? $this->imagick->getImagesBlob()
                : [
                    'data' => $this->imagick->getImagesBlob(),
                    'extension' => $format,
                    'size' => $this->imagick->getImageLength(), // in bytes
                    'MimeType' => $this->imagick->getImageMimeType()
                ];
        } catch (ImagickException $e) {
            throw new ImageProcessorException(
                $e->getMessage(),
                HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR,
                $e
            );
        }
    }

    public function setQuality(int $quality, int $qualityFlag = self::CREATE_FROM_EXISTING): string
    {
        $callableMethod = $qualityFlag === static::CREATE_FROM_NEW
            ? 'setCompressionQuality'
            : 'setImageCompressionQuality';

        if(true === $this->imagick->$callableMethod($quality)) {
            return $this->imagick->getImagesBlob();
        }

        throw new ImageProcessorException(
            'Imagick cannot set image quality.',
            HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR
        );
    }

    public function validateImageBlobFormat(string $data, ?array $validFormats = null): bool
    {
        $this->imagick->clear();

        try {
            $this->imagick->readImageBlob($data);
        } catch (ImagickException $e) {
            throw new ImageProcessorException(
                'Error reading the image blob.',
                HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR,
                $e
            );
        }

        $format = $this->getStandardFormatName(
            $this->imagick->getImageFormat()
        );

        if (null !== $validFormats && !in_array($format, $validFormats, true)) {
            throw new ImageBlobValidationException(
                sprintf(
                    'Blob image is in "%s" format. Valid formats are: %s',
                    $format,
                    implode(',', $validFormats)
                )
            );
        }

        return true;
    }

    /**
     * @param string $format
     *
     * @return string
     */
    private function getStandardFormatName(string $format): string
    {
        switch (strtolower($format)) {
            case 'gif':
                return static::FORMAT_GIF;

            case 'jpeg':
            case 'jpg':
                return static::FORMAT_JPG;

            case 'png':
                return static::FORMAT_PNG;

            case 'webp':
                return static::FORMAT_WEBP;

            case 'bmp':
                return static::FORMAT_BMP;
        }

        throw new UnrecognizedImageFormatException(
            sprintf('Invalid/Unrecognized format: "%s"', $format)
        );
    }

    public function getWidth(): int
    {
        return $this->imagick->getImageWidth();
    }

    public function getHeight(): int
    {
        return $this->imagick->getImageHeight();
    }
}
