<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

use Smtm\Base\Infrastructure\Exception\InvalidArgumentException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class OAuth2Helper
{
    public const AUTHORIZATION_REQUEST_RESPONSE_TYPE_CODE = 'code';
    public const AUTHORIZATION_REQUEST_RESPONSE_TYPE_TOKEN = 'token';
    public const AUTHORIZATION_REQUEST_RESPONSE_TYPES = [
        self::AUTHORIZATION_REQUEST_RESPONSE_TYPE_CODE,
        self::AUTHORIZATION_REQUEST_RESPONSE_TYPE_TOKEN,
    ];

    public const TOKEN_REQUEST_GRANT_TYPE_AUTHORIZATION_CODE = 'authorization_code';
    public const TOKEN_REQUEST_GRANT_TYPE_REFRESH_TOKEN = 'refresh_token';
    public const TOKEN_REQUEST_GRANT_TYPES = [
        self::TOKEN_REQUEST_GRANT_TYPE_AUTHORIZATION_CODE,
        self::TOKEN_REQUEST_GRANT_TYPE_REFRESH_TOKEN,
    ];

    public const TOKEN_TYPE_BEARER = 'Bearer';
    public const TOKEN_TYPES = [
        self::TOKEN_TYPE_BEARER,
    ];

    public const PKCE_CODE_CHALLENGE_METHOD_S256 = 'S256';
    public const PKCE_CODE_CHALLENGE_METHOD_PLAIN = 'plain';
    public const PKCE_CODE_CHALLENGE_METHODS = [
        self::PKCE_CODE_CHALLENGE_METHOD_S256,
        self::PKCE_CODE_CHALLENGE_METHOD_PLAIN,
    ];

    public static function hasValidCodeChallengeAndCodeChallengeMethod(
        ?string $codeChallenge = null,
        ?string $codeChallengeMethod = null
    ): bool {
        if ($codeChallenge !== null) {
            $codeChallengeLength = strlen($codeChallenge);
            if ($codeChallengeLength < 43 || $codeChallengeLength > 128) {
                throw new InvalidArgumentException(
                    'The PKCE codeChallenge must be between 43 and 128 characters long'
                );
            }

            if ($codeChallengeMethod === null) {
                throw new InvalidArgumentException('No PKCE codeChallengeMethod provided with the codeChallenge');
            } elseif (!in_array($codeChallengeMethod, self::PKCE_CODE_CHALLENGE_METHODS, true)) {
                throw new InvalidArgumentException(
                    'Invalid PKCE codeChallengeMethod value \'' . $codeChallengeMethod . '\' provided. '
                    . 'The value must be one of [' . implode(
                        ', ',
                        self::PKCE_CODE_CHALLENGE_METHODS
                    ) . '].'
                );
            }

            return true;
        }

        return false;
    }

    public static function verifyCodeChallenge(
        ?string $codeChallenge,
        ?string $codeChallengeMethod,
        ?string $codeVerifier
    ): void {
        if ($codeChallenge !== null) {
            if ($codeVerifier === null
                || ($codeChallengeMethod === self::PKCE_CODE_CHALLENGE_METHOD_PLAIN
                    && $codeChallenge !== $codeVerifier)
                || ($codeChallengeMethod === self::PKCE_CODE_CHALLENGE_METHOD_S256
                    && ($codeChallenge !== static::createS256CodeChallenge($codeVerifier)
                        && $codeChallenge !== static::createS256CodeChallenge($codeVerifier, true)
                    )
                )
            ) {
                throw new InvalidArgumentException('Invalid PKCE code_verifier');
            }
        }
    }

    public static function generateCodeVerifier(int $length = 128): string
    {
        if ($length < 43) {
            throw new InvalidArgumentException(
                'Code verifier length should be in the range: (43 <= $codeVerifier <= 128). '
                . $length . ' provided.'
            );
        }

        $characters = AsciiHelper::CHARACTERS_PRINTABLE;
        $charactersCount = strlen($characters);
        $str = '';

        for ($i = 0; $i < $length; $i++) {
            $str .= $characters[random_int(0, ($charactersCount - 1))];
        }

        return $str;
    }

    public static function createS256CodeChallenge(string $codeVerifier, ?bool $urlSafe = false): string
    {
        $hash = hash('sha256', $codeVerifier);
        $packedHash = pack('H*', $hash);
        $codeChallenge = rtrim(base64_encode($packedHash), '=');

        if ($urlSafe) {
            $codeChallenge = str_replace(['/', '+'], ['_', '-'], $codeChallenge);
        }

        return $codeChallenge;
    }
}
