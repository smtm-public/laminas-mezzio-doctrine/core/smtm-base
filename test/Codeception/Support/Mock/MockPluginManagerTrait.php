<?php

declare(strict_types=1);

namespace SmtmTest\Base\Codeception\Support\Mock;

use Laminas\ServiceManager\PluginManagerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait MockPluginManagerTrait
{
    protected array $mockFactories = [];
    protected array $mockServices = [];

    public function __construct(
        $configInstanceOrParentLocator = null,
        array $config = [],
        protected ?PluginManagerInterface $wrapped = null
    ) {
        //mt_rand();
    }

    public function configure(array $config)
    {
        $this->wrapped->configure($config);

        return $this;
    }

    public function setFactory($name, $factory)
    {
        $this->wrapped->setFactory($name, $factory);
    }

    public function addDelegator($name, $factory)
    {
        $this->wrapped->addDelegator($name, $factory);
    }

    public function setAlias($alias, $target)
    {
        $this->wrapped->setAlias($alias, $target);
    }

    public function setShared($name, $flag)
    {
        parent::setShared($name, $flag);
    }

    public function get($name, ?array $options = null)
    {
        if (array_key_exists($name, $this->mockServices)) {
            return $this->mockServices[$name];
        } elseif (array_key_exists($name, $this->mockFactories)) {
            return $this->mockFactories[$name]($name, $options);
        }

        return $this->wrapped->get($name);
    }

    public function has($name): bool
    {
        return $this->wrapped->has($name);
    }

    public function validate(mixed $instance): void
    {
        $this->wrapped->validate($instance);
    }

    public function build($name, ?array $options = null)
    {
        if (array_key_exists($name, $this->mockFactories)) {
            return $this->mockFactories[$name]($name, $options);
        }

        return $this->wrapped->build($name, $options);
    }

    public function setWrapped(PluginManagerInterface $wrapped): static
    {
        $this->wrapped = $wrapped;

        return $this;
    }

    public function setMockFactory(string $name, callable $factory): static
    {
        $this->mockFactories[$name] = $factory;

        return $this;
    }

    public function setMockService(string $name, object|array $service): static
    {
        $this->mockServices[$name] = $service;

        return $this;
    }
}
