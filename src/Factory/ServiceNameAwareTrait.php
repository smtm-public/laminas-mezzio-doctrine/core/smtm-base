<?php

declare(strict_types=1);

namespace Smtm\Base\Factory;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ServiceNameAwareTrait
{
    protected ?string $serviceName = null;

    public function getServiceName(): ?string
    {
        return $this->serviceName;
    }

    public function setServiceName(?string $serviceName): static
    {
        $this->serviceName = $serviceName;

        return $this;
    }
}
