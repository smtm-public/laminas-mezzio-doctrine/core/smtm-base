<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\PhpOffice\PhpSpreadsheet;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IOFactory extends \PhpOffice\PhpSpreadsheet\IOFactory
{
    protected $fileHandle = null;

    public function __destruct()
    {
        $this->reset();
    }

    protected function reset(): void
    {
        if (isset($this->fileHandle) && is_resource($this->fileHandle) && get_resource_type($this->fileHandle) === 'stream') {
            fclose($this->fileHandle);
        }

        $this->fileHandle = null;
    }

    public function loadFromString(string $data, int $flags = 0, ?array $readers = null): Spreadsheet
    {
        $this->fileHandle = fopen('php://temp', 'wb+');
        fwrite($this->fileHandle, $data);

        $reader = self::createReaderForFile('php://temp', $readers);
        $reader = $reader->load('php://temp', $flags);

        return $reader;
    }
}
