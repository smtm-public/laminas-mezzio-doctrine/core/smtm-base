<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy;

use Smtm\Base\Application\Hydrator\Strategy\Exception\CannotExtractValuesException;
use Laminas\Hydrator\Strategy\StrategyInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractHydrationStrategy implements StrategyInterface
{
    protected array $options = [];

    /**
     * @throws CannotExtractValuesException
     */
    public function extract($value, ?object $object = null): void
    {
        throw new CannotExtractValuesException('The ' . __CLASS__ . ' cannot extract values.');
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): static
    {
        $this->options = $options;

        return $this;
    }
}
