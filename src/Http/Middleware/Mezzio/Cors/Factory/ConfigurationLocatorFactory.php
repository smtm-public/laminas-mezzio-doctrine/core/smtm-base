<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\Mezzio\Cors\Factory;

use Smtm\Base\Http\Middleware\Mezzio\Cors\Configuration\ProjectConfiguration;
use Smtm\Base\Http\Middleware\Mezzio\Cors\ConfigurationLocator;
use Mezzio\Cors\Configuration\RouteConfigurationFactoryInterface;
use Mezzio\Router\RouterInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Webmozart\Assert\Assert;

final class ConfigurationLocatorFactory
{
    public function __invoke(ContainerInterface $container): ConfigurationLocator
    {
        $configuration = $container->get(ProjectConfiguration::class);
        Assert::isInstanceOf($configuration, ProjectConfiguration::class);
        $requestFactory = $container->get(ServerRequestFactoryInterface::class);
        Assert::isInstanceOf($requestFactory, ServerRequestFactoryInterface::class);
        $router = $container->get(RouterInterface::class);
        Assert::isInstanceOf($router, RouterInterface::class);
        $routeConfigurationFactory = $container->get(RouteConfigurationFactoryInterface::class);
        Assert::isInstanceOf($routeConfigurationFactory, RouteConfigurationFactoryInterface::class);

        return new ConfigurationLocator(
            $configuration,
            $requestFactory,
            $router,
            $routeConfigurationFactory
        );
    }
}
