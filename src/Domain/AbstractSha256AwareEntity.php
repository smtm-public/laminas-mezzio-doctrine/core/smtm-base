<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractSha256AwareEntity implements Sha256AwareEntityInterface
{
    use Sha256AwareEntityTrait;
}
