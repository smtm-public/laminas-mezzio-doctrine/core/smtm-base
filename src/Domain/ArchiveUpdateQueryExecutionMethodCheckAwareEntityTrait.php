<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ArchiveUpdateQueryExecutionMethodCheckAwareEntityTrait
{
    protected ?int $archiveUpdateQueryExecutionMethodCheck = null;

    public function getArchiveUpdateQueryExecutionMethodCheck(): ?int
    {
        return $this->archiveUpdateQueryExecutionMethodCheck;
    }

    public function setArchiveUpdateQueryExecutionMethodCheck(?int $archiveUpdateQueryExecutionMethodCheck): static
    {
        $this->archiveUpdateQueryExecutionMethodCheck = $archiveUpdateQueryExecutionMethodCheck;

        return $this;
    }
}
