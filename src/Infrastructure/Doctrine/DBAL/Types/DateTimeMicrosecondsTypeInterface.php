<?php

namespace Smtm\Base\Infrastructure\Doctrine\DBAL\Types;

use Smtm\Base\Infrastructure\Helper\DateTimeHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface DateTimeMicrosecondsTypeInterface
{
    public const FORMAT = DateTimeHelper::FORMAT_DATETIME_MICROSECONDS;
}
