<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware;

use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;
use Smtm\Base\Infrastructure\Helper\IetfHelper;
use Smtm\Base\Infrastructure\Helper\UniqueIdHelper;
use Smtm\Base\Infrastructure\Service\CryptoService;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareTrait;
use Mezzio\Helper\ServerUrlMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Ramsey\Uuid\Uuid;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ServerParamsAndRequestIdMiddleware extends ServerUrlMiddleware implements
    InfrastructureServicePluginManagerAwareInterface
{

    use InfrastructureServicePluginManagerAwareTrait;

    public const REQUEST_ATTRIBUTE_NAME_HEADER_FORWARDED_PARSED = 'headerForwardedParsed';
    public const REQUEST_ATTRIBUTE_NAME_REMOTE_ADDRESS = 'remoteAddress';
    public const REQUEST_ATTRIBUTE_NAME_REMOTE_ADDRESS_HEADER_X_FORWARDED_FOR = 'remoteAddressHeaderXForwardedFor';
    public const REQUEST_ATTRIBUTE_NAME_REMOTE_ADDRESS_HEADER_FORWARDED = 'remoteAddressHeaderForwarded';
    public const REQUEST_ATTRIBUTE_NAME_REQUEST_TIME_FROM_SERVER = 'requestTimeFromServer';
    public const REQUEST_ATTRIBUTE_NAME_MICROTIME = 'microtime';
    public const REQUEST_ATTRIBUTE_NAME_TIME = 'time';
    public const REQUEST_ATTRIBUTE_NAME_DATETIME = 'datetime';
    public const REQUEST_ATTRIBUTE_NAME_ID = 'id';
    public const ID_GENERATOR_SEPARATOR = '_';

    public static function getResolvedIpAddress(ServerRequestInterface $request): string
    {
        return $request->getAttribute(
            static::REQUEST_ATTRIBUTE_NAME_REMOTE_ADDRESS_HEADER_FORWARDED
        ) ?? $request->getAttribute(
            static::REQUEST_ATTRIBUTE_NAME_REMOTE_ADDRESS
        ) ?? 'N/A';
    }

    public static function getRequestParams(ServerRequestInterface $request): array
    {
        return [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS =>
                static::getResolvedIpAddress($request),
        ];
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $serverParams = $request->getServerParams();

        $headerForwardedParsed =
            array_key_exists('HTTP_FORWARDED', $serverParams)
                ? IetfHelper::parseRfc7239ForwardedHeader(
                    $serverParams['HTTP_FORWARDED']
                )
                : null;
        $request = $request->withAttribute(
            self::REQUEST_ATTRIBUTE_NAME_HEADER_FORWARDED_PARSED,
            $headerForwardedParsed
        );
        $remoteAddr = $serverParams['REMOTE_ADDR'] ?? null;
        $request = $request->withAttribute(
            self::REQUEST_ATTRIBUTE_NAME_REMOTE_ADDRESS,
            $remoteAddr
        );
        $remoteAddressHeaderForwarded = ($headerForwardedParsed ?? [])['for'] ?? null;
        $request = $request->withAttribute(
            self::REQUEST_ATTRIBUTE_NAME_REMOTE_ADDRESS_HEADER_FORWARDED,
            $remoteAddressHeaderForwarded
        );
        $remoteAddressHeaderXForwardedFor = $serverParams['HTTP_X_FORWARDED_FOR'] ?? null;
        $request = $request->withAttribute(
            self::REQUEST_ATTRIBUTE_NAME_REMOTE_ADDRESS_HEADER_X_FORWARDED_FOR,
            $remoteAddressHeaderXForwardedFor
        );

        $remoteAddress = $remoteAddr ?? $remoteAddressHeaderForwarded ?? $remoteAddressHeaderXForwardedFor;

        $microtime = microtime(true);
        $requestTimeFromServer = false;

        if (array_key_exists('REQUEST_TIME_FLOAT', $serverParams)) {
            $microtime = $serverParams['REQUEST_TIME_FLOAT'];
            $requestTimeFromServer = true;
        }

        $request = $request->withAttribute(
            self::REQUEST_ATTRIBUTE_NAME_REQUEST_TIME_FROM_SERVER,
            $requestTimeFromServer
        );
        $request = $request->withAttribute(
            self::REQUEST_ATTRIBUTE_NAME_MICROTIME,
            $microtime
        );
        $request = $request->withAttribute(
            self::REQUEST_ATTRIBUTE_NAME_TIME,
            (int) floor($microtime)
        );
        $request = $request->withAttribute(
            self::REQUEST_ATTRIBUTE_NAME_DATETIME,
            \DateTimeImmutable::createFromFormat(DateTimeHelper::FORMAT_MICROTIME_FLOAT, (string) $microtime)
        );

        $id = UniqueIdHelper::generateEncryptedUniqueId(
            [
                'remoteAddress' => $remoteAddress,
                'time' => $microtime * 1000,
                'uuid' => Uuid::uuid4()
            ],
            [
                $this->infrastructureServicePluginManager->get(CryptoService::class),
                'encrypt'
            ],
            static::ID_GENERATOR_SEPARATOR
        );
        $request = $request->withAttribute(
            self::REQUEST_ATTRIBUTE_NAME_ID,
            $id
        );

        return parent::process($request, $handler);
    }
}
