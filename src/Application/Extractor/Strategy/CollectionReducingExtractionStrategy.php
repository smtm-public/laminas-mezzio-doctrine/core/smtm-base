<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;
use Smtm\Base\Infrastructure\Collection\CollectionInterface;
use Laminas\Hydrator\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CollectionReducingExtractionStrategy extends AbstractExtractorPluginManagerAwareExtractionStrategy
{

    public const OPTION_KEY_PROPERTY_NAME = 'name';

    public function extract($value, ?object $object = null)
    {
        if (!is_array($value) && !$value instanceof CollectionInterface) {
            throw new Exception\InvalidArgumentException(
                sprintf(
                    'Value needs to be an array or an instance of CollectionInterface, got "%s" instead.',
                    is_object($value) ? get_class($value) : gettype($value)
                )
            );
        }

        if ($value instanceof CollectionInterface) {
            $value = $value->getValues();
        }

        /** @var AbstractDomainObjectExtractor $extractor */
        $extractor = $this->extractorPluginManager->get(
            $this->options[static::OPTION_KEY_EXTRACTOR_NAME],
            $this->options[static::OPTION_KEY_EXTRACTOR_OPTIONS] ?? []
        );

        return array_map(
            function (
                object|array|string|float|int|bool|null $collectionElement
            ) use ($extractor, $object): object|array|string|float|int|bool|null {
                return $extractor->extractValue(
                    $this->options[static::OPTION_KEY_PROPERTY_NAME],
                    $collectionElement,
                    $object
                );
            },
            $value
        );
    }
}
