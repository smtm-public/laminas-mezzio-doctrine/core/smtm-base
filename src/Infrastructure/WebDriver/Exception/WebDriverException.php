<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\WebDriver\Exception;

class WebDriverException extends \Exception
{

}
