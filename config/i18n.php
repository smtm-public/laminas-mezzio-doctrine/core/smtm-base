<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-base.i18n')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.smtm.smtm-base.i18n');
    $dotenv->load();
}

$translators = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_I18N_TRANSLATORS'),
    true,
    flags: JSON_THROW_ON_ERROR
);

return [
    'translation' => [
        'baseDir' => [
            'po' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_I18N_TRANSLATION_BASE_DIR_PO'),
            'mo' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_I18N_TRANSLATION_BASE_DIR_MO'),
        ],
    ],
    'gettext' => [
        'generateMoOnlyIfNotExists' => filter_var(
            EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_I18N_TRANSLATION_GETTEXT_GENERATE_MO_ONLY_IF_NOT_EXIST'),
            FILTER_VALIDATE_BOOL
        )
    ],
    'translators' => $translators,
];
