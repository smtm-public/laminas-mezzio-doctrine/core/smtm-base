<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Exception;

use Smtm\Base\Infrastructure\Helper\HttpHelper;
use JetBrains\PhpStorm\Pure;
use Throwable;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class BadRequestException extends AbstractHttpException
{
    public const MESSAGE = 'Bad request';
    public const CODE = 0;
    public const STATUS = HttpHelper::STATUS_CODE_BAD_REQUEST;
    public const TYPE = 'https://example.com/problems/bad-request';

    protected int $status = self::STATUS;
    protected string $title = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_BAD_REQUEST];
    protected string $detail = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_BAD_REQUEST];
    protected string $type = self::TYPE;

    #[Pure] public function __construct(
        $message = null,
        $code = null,
        Throwable $previous = null,
        protected array $errors = []
    ) {
        parent::__construct($message ?? static::MESSAGE, $code ?? static::CODE, $previous);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
