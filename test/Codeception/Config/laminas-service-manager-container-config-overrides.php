<?php

declare(strict_types=1);

namespace SmtmTest\Base\Codeception\Config;

use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use SmtmTest\Base\Codeception\Support\Mock\Application\Service\Factory\ApplicationServicePluginManagerMockFactory;
use SmtmTest\Base\Codeception\Support\Mock\Infrastructure\Service\Factory\InfrastructureServicePluginManagerMockFactory;

return [
    'dependencies' => [
        'factories' => [
            InfrastructureServicePluginManager::class => InfrastructureServicePluginManagerMockFactory::class,
            ApplicationServicePluginManager::class => ApplicationServicePluginManagerMockFactory::class,
        ],
    ],
];
