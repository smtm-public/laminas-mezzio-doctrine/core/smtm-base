<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ApplicationServiceInterface
{
    public const OPTION_KEY_REMOTE_ADDRESS = 'remoteAddress';
    public const OPTION_KEY_PARAMS = 'params';
    public const OPTION_KEY_METHOD = 'method';
}
