<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator;

use DateTime;
use DateTimeImmutable;
use Smtm\Base\Application\Exception\ValidationException;
use Smtm\Base\Application\Hydrator\Exception\ArgumentIsNotAbstractHydrationStrategyException;
use Smtm\Base\Application\Hydrator\Exception\CannotExtractException;
use Smtm\Base\Application\Hydrator\Exception\DidNotHydratePropertyException;
use Smtm\Base\Application\Hydrator\Exception\ObjectIsNotDomainObjectException;
use Smtm\Base\Application\Hydrator\Strategy\AbstractHydrationStrategy;
use Smtm\Base\Application\Hydrator\Strategy\HydrationStrategyPluginManager;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Domain\CreatedDateTimeImmutableAwareEntityInterface;
use Smtm\Base\Domain\DomainObjectInterface;
use Smtm\Base\Domain\IdAwareEntityInterface;
use Smtm\Base\Domain\MarkedForUpdateInterface;
use Smtm\Base\Domain\ModifiedDateTimeAwareEntityInterface;
use Smtm\Base\Infrastructure\Helper\ReflectionHelper;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\Hydrator\ClassMethodsHydrator;
use Laminas\Hydrator\Exception\InvalidArgumentException;
use Laminas\Hydrator\Strategy\StrategyInterface;
use Laminas\Validator\ValidatorInterface;
use Laminas\Validator\ValidatorPluginManager;
use ReflectionProperty;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DomainObjectHydrator extends ClassMethodsHydrator
{
    /**
     * An array of key=>value elements
     * - key is a property name so that the property with that name must be in an initialized state after hydration or
     *   an exception is thrown
     * - value is the exception message
     *
     * @var array
     */
    protected array $mustHydrate = [];
    /**
     * An array of key=>value elements
     * - key is a property name so that when the property with that name is in an uninitialized state after hydration a
     *   default value will be provided
     * - value is the default value to be provided
     *
     * @var array
     */
    protected array $defaultValueOnHydrate = [];
    /**
     * An array of key=>value elements
     * - key is a property name that will use a strategy obtained from the StrategyManager service locator
     * - value is the strategy service id as configured in the StrategyManager service locator
     *
     * @var array
     */
    protected array $properties = [];

    /** @var array */
    protected array $validators = [];

    /**
     * @inheritDoc
     *
     * @var AbstractHydrationStrategy[]
     */
    protected $strategies = [];

    public function __construct(
        protected ApplicationServicePluginManager $applicationServicePluginManager,
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected HydrationStrategyPluginManager $hydrationStrategyPluginManager,
        protected ValidatorPluginManager $validatorPluginManager
    ) {
        parent::__construct(true, false);

        foreach ($this->properties as $propertyName => $propertyHydrationDefinition) {
            if (is_array($propertyHydrationDefinition)) {
                $this->initializePropertyHydration($propertyName, $propertyHydrationDefinition);
            }
        }
    }

    /**
     * @inheritDoc
     *
     * @throws ArgumentIsNotAbstractHydrationStrategyException
     */
    public function addStrategy(string $name, StrategyInterface $strategy): void
    {
        if (!$strategy instanceof AbstractHydrationStrategy) {
            throw new ArgumentIsNotAbstractHydrationStrategyException(
                'The provided strategy of type ' . get_class($strategy)
                . ' is not a ' . AbstractHydrationStrategy::class
            );
        }

        parent::addStrategy($name, $strategy);
    }

    /**
     * @inheritDoc
     *
     * @return AbstractHydrationStrategy
     */
    public function getStrategy(string $name): AbstractHydrationStrategy
    {
        return parent::getStrategy($name);
    }

    /**
     * This class cannot extract.
     */
    public function extract(object $object): array
    {
        throw new CannotExtractException('The ' . __CLASS__ . ' cannot extract.');
    }

    /**
     * @throws DidNotHydratePropertyException
     */
    protected function mustHydrateProperty(
        DomainObjectInterface $entity,
        string $propertyName,
        array $data,
        string $exceptionMessage
    ): void {
        try {
            $reflectionProperty = new ReflectionProperty(get_class($entity), $propertyName);
        } catch (\ReflectionException $e) {
            throw new InvalidArgumentException(
                'Invalid mustHydrate property \'' . get_class($entity) . '::$' . $propertyName . '\'.'
            );
        }

        if ($reflectionProperty->isPrivate() || $reflectionProperty->isProtected()) {
            $reflectionProperty->setAccessible(true);
        }

        try {
            if (!array_key_exists($propertyName, $data) && !$reflectionProperty->isInitialized($entity)) {
                throw new DidNotHydratePropertyException($exceptionMessage);
            }
        } finally {
            if ($reflectionProperty->isPrivate() || $reflectionProperty->isProtected()) {
                $reflectionProperty->setAccessible(false);
            }
        }
    }

    protected function defaultValueOnHydrateProperty(
        DomainObjectInterface $entity,
        string $propertyName,
        array $data,
        $defaultValue
    ): void {
        /** @noinspection PhpUnhandledExceptionInspection */
        $reflectionProperty = new ReflectionProperty(get_class($entity), $propertyName);

        if ($reflectionProperty->isPrivate() || $reflectionProperty->isProtected()) {
            $reflectionProperty->setAccessible(true);
        }

        if (!array_key_exists($propertyName, $data) && !$reflectionProperty->isInitialized($entity)) {
            if (is_callable($defaultValue)) {
                $reflectionProperty->setValue($entity, $defaultValue());
            } else {
                $reflectionProperty->setValue($entity, $defaultValue);
            }
        }

        if ($reflectionProperty->isPrivate() || $reflectionProperty->isProtected()) {
            $reflectionProperty->setAccessible(false);
        }
    }

    public function hydrateCreatedModifiedTimestamps(
        DomainObjectInterface $entity,
        array $data
    ): static {
        /** @noinspection PhpUnhandledExceptionInspection */
        $reflectionPropertyCreated = new ReflectionProperty(get_class($entity), 'created');

        if ($reflectionPropertyCreated->isPrivate() || $reflectionPropertyCreated->isProtected()) {
            $reflectionPropertyCreated->setAccessible(true);
        }

        if (!array_key_exists('created', $data) && !$reflectionPropertyCreated->isInitialized($entity)) {
            $reflectionPropertyCreated->setValue($entity, new DateTimeImmutable());
        } else {
            if ($entity instanceof IdAwareEntityInterface) {
                if (!ReflectionHelper::isPropertyInitialized('id', $entity)) {
                    return $this;
                }
            }

            if ($entity instanceof MarkedForUpdateInterface) {
                if (!$entity->__getMarkedForUpdate()) {
                    return $this;
                }
            }

            /** @noinspection PhpUnhandledExceptionInspection */
            $reflectionPropertyModified = new ReflectionProperty(get_class($entity), 'modified');

            if ($reflectionPropertyModified->isPrivate() || $reflectionPropertyModified->isProtected()) {
                $reflectionPropertyModified->setAccessible(true);
            }

            if (!array_key_exists('modified', $data)) {
                if (!($entity instanceof MarkedForUpdateInterface) || $entity->__getMarkedForUpdate()) {
                    $reflectionPropertyModified->setValue($entity, new DateTime());
                }
            }

            if ($reflectionPropertyModified->isPrivate() || $reflectionPropertyModified->isProtected()) {
                $reflectionPropertyModified->setAccessible(false);
            }
        }

        if ($reflectionPropertyCreated->isPrivate() || $reflectionPropertyCreated->isProtected()) {
            $reflectionPropertyCreated->setAccessible(false);
        }

        return $this;
    }

    /**
     * @throws ObjectIsNotDomainObjectException
     */
    public function hydrate(array $data, object $object): DomainObjectInterface
    {
        if (!$object instanceof DomainObjectInterface) {
            throw new ObjectIsNotDomainObjectException(
                'The ' . __CLASS__ . ' can only hydrate EntityInterface instances.'
            );
        }

        foreach ($this->mustHydrate as $propertyName => $exceptionMessage) {
            $this->mustHydrateProperty($object, $propertyName, $data, $exceptionMessage);
        }

        foreach ($this->defaultValueOnHydrate as $propertyName => $defaultValue) {
            $this->defaultValueOnHydrateProperty($object, $propertyName, $data, $defaultValue);
        }

        foreach ($this->validators as $propertyName => $validatorCollection) {
            $this->validateProperty($data, $propertyName, $validatorCollection);
        }

        $object = parent::hydrate($data, $object);

        if (($object instanceof CreatedDateTimeImmutableAwareEntityInterface
            || $object instanceof ModifiedDateTimeAwareEntityInterface)
        ) {
            $this->hydrateCreatedModifiedTimestamps($object, $data);
        }

        return $object;
    }

    public function getMustHydrate(): array
    {
        return $this->mustHydrate;
    }

    public function setMustHydrate(array $mustHydrate): static
    {
        $this->mustHydrate = $mustHydrate;

        return $this;
    }

    public function addMustHydrate(string $propertyName, string $errorMessage): static
    {
        if (!array_key_exists($propertyName, $this->mustHydrate)) {
            $this->mustHydrate[$propertyName] = $errorMessage;
        }

        return $this;
    }

    public function removeMustHydrate(string $propertyName): static
    {
        if (array_key_exists($propertyName, $this->mustHydrate)) {
            unset($this->mustHydrate[$propertyName]);
        }

        return $this;
    }

    public function hasMustHydrate(string $propertyName): bool
    {
        return array_key_exists($propertyName, $this->mustHydrate);
    }

    protected function isPropertyInitialized($propertyName, DomainObjectInterface $domainObject): bool
    {
        return ReflectionHelper::isPropertyInitialized($propertyName, $domainObject);
    }

    protected function initializePropertyHydration(string $propertyName, array $propertyHydrationDefinition)
    {
        if (!empty($propertyHydrationDefinition['strategy'])) {
            /** @var AbstractHydrationStrategy $strategy */
            $strategy = $this->hydrationStrategyPluginManager->get(
                $propertyHydrationDefinition['strategy']['name']
            );

            if (!empty($propertyHydrationDefinition['strategy']['options'])) {
                $strategy->setOptions($propertyHydrationDefinition['strategy']['options']);
            }

            $this->addStrategy(
                $propertyName,
                $strategy
            );
        }
    }

    protected function validateProperty($data, string $propertyName, array $validatorCollection)
    {
        $isPropertyValid = true;
        $messages = [];

        $propertyValue = array_key_exists($propertyName, $data) ? $data[$propertyName] : null;

        /** @var ValidatorInterface $validator */
        foreach ($validatorCollection as $validator) {
            if (!$validator->isValid($propertyValue)) {
                $isPropertyValid = false;
                $messages = array_merge($messages, $validator->getMessages());
            }
        }

        if (!$isPropertyValid) {
            throw new ValidationException(
                sprintf('Property %s has invalid value', $propertyName),
                0,
                null,
                $messages
            );
        }
    }
}
