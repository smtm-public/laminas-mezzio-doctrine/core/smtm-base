<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\DbPersistStrategy;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * TODO: Implement
 */
class BroadcastingDbPersistStrategy implements InfrastructureServicePluginManagerAwareInterface
{

    use InfrastructureServicePluginManagerAwareTrait;
}
