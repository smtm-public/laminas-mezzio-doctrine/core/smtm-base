<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class OAuth1Helper
{
    public const VERSION_1_0 = '1.0';
    public const VERSION_2_0 = '2.0';

    public const SIGNATURE_METHOD_RSA_SHA1 = OAUTH_SIG_METHOD_RSASHA1;
    public const SIGNATURE_METHOD_RSA_SHA256 = 'RSA-SHA256';
    public const SIGNATURE_METHOD_RSA_SHA512 = 'RSA-SHA512';
    public const SIGNATURE_METHOD_HMAC_SHA1 = OAUTH_SIG_METHOD_HMACSHA1;
    public const SIGNATURE_METHOD_HMAC_SHA256 = OAUTH_SIG_METHOD_HMACSHA256;
    public const SIGNATURE_METHOD_HMAC_SHA512 = 'HMAC-SHA512';
    public const SIGNATURE_METHOD_PLAINTEXT = 'PLAINTEXT';
    public const SIGNATURE_METHODS = [
        self::SIGNATURE_METHOD_RSA_SHA1,
        self::SIGNATURE_METHOD_RSA_SHA256,
        self::SIGNATURE_METHOD_RSA_SHA512,
        self::SIGNATURE_METHOD_HMAC_SHA1,
        self::SIGNATURE_METHOD_HMAC_SHA256,
        self::SIGNATURE_METHOD_HMAC_SHA512,
        self::SIGNATURE_METHOD_PLAINTEXT,
    ];

    public static function buildHeaderAuthorizationOAuthValue(
        string $consumerKey,
        string $consumerSecret,
        string $signatureMethod,
        string $httpMethod,
        string $url,
        ?string $callback = null,
        ?string $verifier = null,
        ?string $realm = null
    ): string {
        $oAuth = new \OAuth($consumerKey, $consumerSecret, $signatureMethod);
        $oAuthAuthorizationHeader = explode(
            'OAuth ',
            $oAuth->getRequestHeader($httpMethod, $url)
        );
        $oAuthAuthorizationHeader = array_pop($oAuthAuthorizationHeader);
        $oAuthAuthorizationHeader .=
            $callback !== null
                ? ',oauth_callback="' . oauth_urlencode($callback) . '"'
                : '';
        $oAuthAuthorizationHeader .=
            $verifier !== null
                ? ',oauth_verifier="' . oauth_urlencode($verifier) . '"'
                : '';

        return $oAuthAuthorizationHeader;
//        OAuth
//            oauth_consumer_key="dgfgdffhdhfjgej3555j45thrghf",
//            oauth_signature_method="HMAC-SHA256",
//            oauth_timestamp="1623870776",
//            oauth_nonce="5B766F745A72754C",
//            oauth_version="1.0",
//            oauth_callback="about:blank",
//            oauth_verifier="fddgdfkjdoijhoijitjhitr9hj9ri",
//            oauth_signature="ADSASDSAFSDFSDFSDFASDFfSDFsdfsdgFGSDGFDSG"
//        return
//            'oauth_consumer_key="' . urlencode($consumerKey) . '"'
//                . ' oauth_signature="' . $consumerKey . '"'
//                . ' oauth_signature_method="' . $signatureMethod . '"'
//                . ' oauth_verifier="' . $consumerKey . '"'
//                . ' oauth_callback="' . $callback . '"'
//                . ' oauth_nonce="' . $consumerKey . '"'
//                . ' oauth_timestamp="' . $consumerKey . '"'
//                . ' oauth_version="' . $consumerKey . '"';
    }
}
