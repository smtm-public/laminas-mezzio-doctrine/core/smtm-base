<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ArchivedDateTimeAwareEntityInterface
{
    public function getArchived(): ?\DateTime;
    public function setArchived(?\DateTime $archived): static;
}
