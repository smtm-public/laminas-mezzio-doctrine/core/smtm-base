<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Codeception\Support\Helper;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;
use Smtm\Base\Infrastructure\Codeception\Module\Doctrine3;
use Smtm\Base\Infrastructure\Codeception\Module\Mezzio;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DoctrineHelper extends Module implements DependsOnModule
{
    private Mezzio $dependentModule;
    private Doctrine3 $doctrineModule;

    public function _initialize()
    {
        $this->doctrineModule = $this->getModule(Doctrine3::class);
    }

    public function _depends(): array
    {
        return [$this->config['depends']];
    }

    public function _inject(Mezzio $dependentModule = null): void
    {
        $this->dependentModule = $dependentModule;
    }

    public function removeEntityFromRepository($object)
    {
        $this->dependentModule->_getEntityManager()->remove($object);
    }
}
