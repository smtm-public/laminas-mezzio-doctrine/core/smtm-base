<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Mezzio\Router\Middleware;

use Mezzio\Router\Middleware\MethodNotAllowedMiddleware as MezzioMethodNotAllowedMiddleware;
use Mezzio\Router\Middleware\RouteMiddleware as MezzioRouteMiddleware;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RouteMiddlewareFactory
{
    public function __invoke(ContainerInterface $container): RouteMiddleware
    {
        return new RouteMiddleware(
            $container->get(MezzioRouteMiddleware::class),
            $container->get(MezzioMethodNotAllowedMiddleware::class),
        );
    }


}
