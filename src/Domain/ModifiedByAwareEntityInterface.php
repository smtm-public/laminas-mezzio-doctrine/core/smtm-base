<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ModifiedByAwareEntityInterface
{
    public function getModifiedBy();
    public function setModifiedBy($modified): static;
}
