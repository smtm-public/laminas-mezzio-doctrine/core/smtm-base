<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\Mezzio\ProblemDetails\Exception;

trait CommonProblemDetailsExceptionTrait
{
    protected string $class;
    protected int $status;
    protected string $detail;
    protected string $title;
    protected string $type;
    protected array $additional = [];

    public function getClass(): string
    {
        return $this->class;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDetail(): string
    {
        return $this->detail;
    }

    public function getAdditionalData(): array
    {
        return $this->additional;
    }

    public function setAdditional(array $additional): static
    {
        $this->additional = $additional;

        return $this;
    }

    public function toArray(): array
    {
        $problem = [
            'class' => $this->class,
            'status' => $this->status,
            'detail' => $this->detail,
            'title'  => $this->title,
            'type'   => $this->type,
        ];

        if ($this->additional) {
            $problem = array_merge($this->additional, $problem);
        }

        return $problem;
    }

    public function jsonSerialize(): mixed
    {
        return $this->toArray();
    }
}
