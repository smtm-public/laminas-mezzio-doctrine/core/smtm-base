<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Log\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Base\Infrastructure\Service\Log\LoggerCollectionAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LoggerCollectionAwareDelegator implements DelegatorFactoryInterface
{

    protected array $loggerCollection = [];

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var LoggerCollectionAwareInterface $object */
        $object = $callback();

        foreach ($this->loggerCollection as $loggerName => $loggerServiceName) {
            $loggerServiceName !== null &&
            $loggerServiceName !== '' &&
            $object->addLogger($loggerName, $container->get(InfrastructureServicePluginManager::class)->get($loggerServiceName));
        }

        return $object;
    }
}
