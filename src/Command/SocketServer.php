<?php

declare(strict_types=1);

namespace Smtm\Base\Command;

use Smtm\Base\Infrastructure\Helper\StringHelper;
use Smtm\Base\Infrastructure\Laminas\Validator\IsInteger;
use Laminas\Validator\Ip;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * Example:
 * vendor/bin/symfony-console base:socket-server --bind-address='0.0.0.0' --bind-port=1234 -vvv
 */
class SocketServer extends Command
{
    protected array $inputFilterSpecification = [
        'bindAddress' => [
            'name' => 'bindAddress',
            'required' => true,
            'allow_empty' => false,
            'validators' => [
                [
                    'name' => Ip::class,
                ],
            ],
        ],
        'bindPort' => [
            'name' => 'bindPort',
            'required' => true,
            'allow_empty' => false,
            'validators' => [
                [
                    'name' => IsInteger::class,
                ],
            ],
        ],
    ];

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('base:socket-server')
            // the short description shown while running "php bin/console list"
            ->setDescription(
                'Smtm - Base - Hello World'
            )
            ->addOption(
                'bind-address',
                'a',
                InputOption::VALUE_REQUIRED,
                'Bind address' . "\n"
            )
            ->addOption(
                'bind-port',
                'b',
                InputOption::VALUE_REQUIRED,
                'Bind port' . "\n"
            )
            ->addOption(
                'destination',
                'c',
                InputOption::VALUE_REQUIRED,
                'Destination' . "\n"
            )
            // the full command description shown when running the command with
            // the "--help" option
            //->setHelp('Could write some useful text here')

            //->addArgument('doctrineMigrationCommand', InputArgument::OPTIONAL,
            //    'The actual Doctrine migration command to run')
        ;
    }

    public function __construct(string $name = null)
    {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $options['bindAddress'] =
            $input->getOption('bind-address')
                ? $input->getOption('bind-address')
                : null;
        $options['bindPort'] =
            $input->getOption('bind-port')
                ? (int) $input->getOption('bind-port')
                : null;
        $options['destination'] =
            $input->getOption('destination')
                ? $input->getOption('destination')
                : null;

        error_reporting(E_ALL);

        /* Allow the script to hang around waiting for connections. */
        set_time_limit(0);

        /* Turn on implicit output flushing so we see what we're getting
         * as it comes in. */
        ob_implicit_flush();

        $address = $options['bindAddress'];
        $port = $options['bindPort'];

        if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
            echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
        }

        if (socket_bind($sock, $address, $port) === false) {
            echo "socket_bind() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
        }

        if (socket_listen($sock, 5) === false) {
            echo "socket_listen() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
        }

        do {
            if (($msgsock = socket_accept($sock)) === false) {
                echo "socket_accept() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";

                break;
            }

            $requestLine = '';

            while (true) {
                $read = [
                    'msgsock' => $msgsock,
                ];
                $write = [];
                $except = [];
                $sockSelect = socket_select($read, $write, $except, 0);

                if (empty($read['msgsock'])) {
                    break;
                }

                $buf = socket_read($msgsock, 1);

                if ($buf === false) {
                    $this->logger->error("socket_read() failed: reason: " . socket_strerror(socket_last_error($msgsock)));

                    break;
                }

                $requestLine .= $buf;

                if (str_ends_with($requestLine, "\r\n")) {
                    break;
                }
            }

            preg_match('#^(?P<request_method>.+?) #', $requestLine, $requestMethodMatches);
            $requestMethod = $requestMethodMatches['request_method'];
            $requestMethodLowercase = strtolower($requestMethod);
            $requestHeaders = '';

            while (true) {
                $read = [
                    'msgsock' => $msgsock,
                ];
                $write = [];
                $except = [];
                $sockSelect = socket_select($read, $write, $except, 0);

                if (empty($read['msgsock'])) {
                    break;
                }

                $buf = socket_read($msgsock, 1);

                if ($buf === false) {
                    $this->logger->error("socket_read() failed: reason: " . socket_strerror(socket_last_error($msgsock)));

                    break;
                }

                $requestHeaders .= $buf;

                if (str_ends_with($requestHeaders, "\r\n\r\n")) {
                    break;
                }
            }

            $requestBody = '';

            if ($requestMethodLowercase === 'post' || $requestMethodLowercase === 'put' || $requestMethodLowercase === 'patch') {
                preg_match(
                    '#content-length: (?P<content_length>[0-9]+)#i',
                    $requestHeaders,
                    $requestContentLengthMatches
                );

                if (!empty($requestContentLengthMatches['content_length'])) {
                    $requestContentLength = (int) $requestContentLengthMatches['content_length'];

                    while (true) {
                        $read = [
                            'msgsock' => $msgsock,
                        ];
                        $write = [];
                        $except = [];
                        $sockSelect = socket_select($read, $write, $except, 0);

                        if (empty($read['msgsock'])) {
                            break;
                        }

                        $buf = socket_read($msgsock, 1);

                        if ($buf === false) {
                            $this->logger->error("socket_read() failed: reason: " . socket_strerror(socket_last_error($msgsock)));

                            break;
                        }

                        $requestBody .= $buf;

                        if (strlen($requestBody) === $requestContentLength) {
                            break;
                        }
                    }
                }
            }

            $responseBody = '{"accessToken":"asd","expires":"2023-08-07T10:43:05Z"}';
            $responseContentLength = strlen($responseBody);
            $response = <<< EOT
            HTTP/1.1 201 Created
            Server: nginx/1.15.2
            Date: Mon, 07 Aug 2023 09:43:05 GMT
            Content-Type: application/json
            Connection: close
            X-Powered-By: PHP/8.1.21
            Vary: Origin
            Strict-Transport-Security: max-age=31449600; includeSubDomains
            Content-Security-Policy: object-src 'none'; script-src 'self'; script-src-elem 'self'; base-uri 'self'; require-trusted-types-for 'script'
            X-Frame-Options: DENY
            X-Content-Type-Options: nosniff
            Referrer-Policy: strict-origin
            Feature-Policy: microphone 'none'; geolocation 'none'; camera 'none'
            Content-Length: $responseContentLength

            $responseBody
            EOT;
            $response = StringHelper::lineSeparatorsLfToCrLf($response);
            socket_write($msgsock, $response, strlen($response));

            socket_close($msgsock);
            $this->logger->info('Received message:' . "\n" . $requestLine . $requestHeaders . $requestBody);
            $this->logger->info('Responded with:' . "\n" . $response);

            if (str_contains($requestLine, '/shutdown HTTP')) {
                break;
            }
        } while (true);

        socket_close($sock);

        $this->logger->info('Server quitting!');

        return Command::SUCCESS;
    }
}
