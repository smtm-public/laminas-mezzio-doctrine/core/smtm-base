<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface UnarchivedDateTimeAwareEntityInterface
{
    public function getUnarchived(): ?\DateTime;
    public function setUnarchived(?\DateTime $unarchived): static;
}
