<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

use DateInterval;
use DatePeriod;
use DateTime;
use DateTimeInterface;
use DateTimeZone;
use Smtm\Base\Application\Exception\InvalidArgumentException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DateTimeHelper
{
    public const FORMAT_ISO8601_1_2019_WITH_DECIMAL_FRACTION = 'Y-m-d\TH:i:s.v\Z';
    public const FORMAT_ISO8601 = 'Y-m-d\TH:i:s\Z';
    public const FORMAT_DATE = 'Y-m-d';
    public const FORMAT_MICROTIME_FLOAT = 'U.u';
    public const FORMAT_DATETIME_MICROSECONDS = 'Y-m-d H:i:s.u';
    public const FORMAT_SQL_DATE = 'Y-m-d';
    public const FORMAT_SQL_DATETIME = 'Y-m-d H:i:s';
    public const FORMAT_SQL_DATETIME_LINUX_STRFTIME = '%Y-%m-%d %H:%M:%S';
    public const FORMAT_SQL_DATETIME_NANOSECONDS_LINUX_STRFTIME = '%Y-%m-%d %H:%M:%S.%N';
    public const FORMAT_FILENAME = 'Y-m-d\TH_i_s\Z';
    public const DEFAULT_FORMAT = self::FORMAT_ISO8601;

    public const FORMAT_DATE_INTERVAL_FLAG_FILTER_EMPTY_VALUES = 0b0001;
    public const FORMAT_DATE_INTERVAL_FLAG_PAD_LEFT_ZERO = 0b0010;

    public const DATE_PERIOD_RANGE_COLLECTION_FORMAT_NUMERIC = 'numeric';
    public const DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS = 'microseconds';
    public const DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS = 'seconds';
    public const DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES = 'minutes';
    public const DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS = 'hours';
    public const DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS = 'days';
    public const DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS = 'months';
    public const DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS = 'years';
    public const DATE_INTERVAL_COMPARISON_EQ = '=';
    public const DATE_INTERVAL_COMPARISON_NEQ = '!=';
    public const DATE_INTERVAL_COMPARISON_LT = '<';
    public const DATE_INTERVAL_COMPARISON_LTE = '<=';
    public const DATE_INTERVAL_COMPARISON_GT = '>';
    public const DATE_INTERVAL_COMPARISON_GTE = '>=';

    public const DATE_INTERVAL_AMOUNTS_TO_FORMAT_SEQUENCES_MAPPING = [
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS => '%F',
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS => '%S',
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES => '%I',
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS => '%H',
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS => '%D',
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS => '%M',
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS => '%Y',
    ];

    public const DATE_INTERVAL_FORMAT_FULL_ISO_8601 = 'P%Y%M%DT%H%I%S%F';
    public const DATE_INTERVAL_FORMAT_FULL_JOINED = '%Y%M%D%H%I%S%F';
    public const DATE_INTERVAL_FORMAT_FULL_COMMA_SEPARATED = '%Y,%M,%D,%H,%I,%S,%F';
    public const DEFAULT_DATE_INTERVAL_FORMAT = self::DATE_INTERVAL_FORMAT_FULL_ISO_8601;
    public const DATE_INTERVAL_FORMAT_COMPONENTS_MAPPING_FULL = [
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS => [
            'singular' => 'microsecond',
            'plural' => 'microseconds',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS => [
            'singular' => 'second',
            'plural' => 'seconds',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES => [
            'singular' => 'minute',
            'plural' => 'minutes',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS => [
            'singular' => 'hour',
            'plural' => 'hours',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS => [
            'singular' => 'day',
            'plural' => 'days',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS => [
            'singular' => 'month',
            'plural' => 'months',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS => [
            'singular' => 'year',
            'plural' => 'years',
        ],
    ];
    public const DATE_INTERVAL_FORMAT_COMPONENTS_MAPPING_SHORT = [
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS => [
            'singular' => 'us',
            'plural' => 'us',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS => [
            'singular' => 'sec',
            'plural' => 'sec',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES => [
            'singular' => 'min',
            'plural' => 'min',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS => [
            'singular' => 'hr',
            'plural' => 'hr',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS => [
            'singular' => 'days',
            'plural' => 'days',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS => [
            'singular' => 'mo',
            'plural' => 'mo',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS => [
            'singular' => 'yr',
            'plural' => 'yr',
        ],
    ];
    public const DATE_INTERVAL_FORMAT_COMPONENTS_MAPPING_SHORTER = [
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS => [
            'singular' => 'us',
            'plural' => 'us',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS => [
            'singular' => 's',
            'plural' => 's',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES => [
            'singular' => 'm',
            'plural' => 'm',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS => [
            'singular' => 'h',
            'plural' => 'h',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS => [
            'singular' => 'd',
            'plural' => 'd',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS => [
            'singular' => 'mo',
            'plural' => 'mo',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS => [
            'singular' => 'y',
            'plural' => 'y',
        ],
    ];
    public const DATE_INTERVAL_FORMAT_COMPONENTS_MAPPING_NONE = [
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS => [
            'singular' => '',
            'plural' => '',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS => [
            'singular' => '',
            'plural' => '',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES => [
            'singular' => '',
            'plural' => '',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS => [
            'singular' => '',
            'plural' => '',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS => [
            'singular' => '',
            'plural' => '',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS => [
            'singular' => '',
            'plural' => '',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS => [
            'singular' => '',
            'plural' => '',
        ],
    ];
    public const DATE_INTERVAL_FORMAT_COMPONENTS_MAPPING_ISO_8601 = [
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS => [
            'singular' => 'uS',
            'plural' => 'uS',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS => [
            'singular' => 'S',
            'plural' => 'S',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES => [
            'singular' => 'M',
            'plural' => 'M',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS => [
            'singular' => 'H',
            'plural' => 'H',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS => [
            'singular' => 'D',
            'plural' => 'D',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS => [
            'singular' => 'M',
            'plural' => 'M',
        ],
        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS => [
            'singular' => 'Y',
            'plural' => 'Y',
        ],
    ];

    public static function format(
        DateTimeInterface $dateTime,
        string $format = self::DEFAULT_FORMAT
    ): string {
        return $dateTime->format($format);
    }

    public static function offsetDateTimeToTimeZone(
        DateTimeInterface $dateTime,
        ?DateTimeZone $dateTimeZone = null
    ): DateTimeInterface {
        if ($dateTimeZone === null) {
            return $dateTime;
        }

        if ($dateTime instanceof \DateTimeImmutable) {
            return (new \DateTimeImmutable())
                ->setTimezone($dateTimeZone)
                ->setDate((int) $dateTime->format('Y'), (int) $dateTime->format('n'), (int) $dateTime->format('j'))
                ->setTime((int) $dateTime->format('G'), (int) $dateTime->format('i'), (int) $dateTime->format('s'), (int) $dateTime->format('u'));
        }

        return (new \DateTime())
            ->setTimezone($dateTimeZone)
            ->setDate((int) $dateTime->format('Y'), (int) $dateTime->format('n'), (int) $dateTime->format('j'))
            ->setTime((int) $dateTime->format('G'), (int) $dateTime->format('i'), (int) $dateTime->format('s'), (int) $dateTime->format('u'));
    }

    public static function convertDateTime(
        DateTimeInterface|string|int $dateTime,
        string $timeZoneFrom = 'UTC',
        string $timeZoneTo = 'UTC',
        string $format = \DateTimeInterface::ATOM
    ): \DateTimeInterface {
        if (is_string($dateTime)) {
            $dateTime = DateTime::createFromFormat($format, $dateTime, new DateTimeZone($timeZoneFrom));
        } elseif (is_int($dateTime)) {
            $dateTime = (new DateTime())->setTimestamp($dateTime);
        }

        $dateTime->setTimezone(new DateTimeZone($timeZoneTo));

        return $dateTime;
    }

    public static function createDatePeriod(
        DateTimeInterface|string|int $from,
        string $interval,
        DateTimeInterface|string|int $to,
        ?DateTimeZone $timeZone = null
    ): DatePeriod {
        $fromDateTime = is_string($from)
            ? new DateTime($from, $timeZone)
            : (
                $from instanceof DateTimeInterface
                    ? DateTime::createFromInterface($from)->setTimezone($timeZone ?? new DateTimeZone(date_default_timezone_get()))
                    : (new DateTime())->setTimestamp($from)->setTimezone($timeZone ?? new DateTimeZone(date_default_timezone_get()))
            );
        $toDateTime = is_string($from)
            ? new DateTime($to, $timeZone)
            : (
                $to instanceof DateTimeInterface
                    ? DateTime::createFromInterface($to)->setTimezone($timeZone ?? new DateTimeZone(date_default_timezone_get()))
                    : (new DateTime())->setTimestamp($to)->setTimezone($timeZone ?? new DateTimeZone(date_default_timezone_get()))
            );

        return new DatePeriod(
            $fromDateTime,
            DateInterval::createFromDateString($interval),
            $toDateTime
        );
    }

    public static function fullFormatDateInterval(
        DateInterval $dateInterval,
        string $format = self::DATE_INTERVAL_FORMAT_FULL_ISO_8601,
        int $flags = 0b0000,
        array $componentMapping = self::DATE_INTERVAL_FORMAT_COMPONENTS_MAPPING_SHORT
    ): string {
        $dateIntervalAmountsBreakdown = static::getDateIntervalAmountsBreakdown($dateInterval);

        if ($flags & self::FORMAT_DATE_INTERVAL_FLAG_FILTER_EMPTY_VALUES) {
            $dateIntervalAmountsBreakdown = array_filter($dateIntervalAmountsBreakdown);
        }

        if ($flags & self::FORMAT_DATE_INTERVAL_FLAG_PAD_LEFT_ZERO) {
            $dateIntervalAmountsBreakdown = array_combine(
                array_keys($dateIntervalAmountsBreakdown),
                array_map(
                    fn (string|int|null $value) => str_pad((string) $value, 2, '0', STR_PAD_LEFT),
                    $dateIntervalAmountsBreakdown
                )
            );
        }

        $dateIntervalAmountsBreakdown = array_combine(
            array_keys($dateIntervalAmountsBreakdown),
            array_map(
                fn (string $key, string|int|null $value) =>
                    $value === 1 ? $value . $componentMapping[$key]['singular'] : $value . $componentMapping[$key]['plural'],
                array_keys($dateIntervalAmountsBreakdown),
                $dateIntervalAmountsBreakdown
            )
        );

        switch ($format) {
            case self::DATE_INTERVAL_FORMAT_FULL_JOINED:
                return implode($dateIntervalAmountsBreakdown);
            case self::DATE_INTERVAL_FORMAT_FULL_COMMA_SEPARATED:
                return implode(',', $dateIntervalAmountsBreakdown);
            case self::DATE_INTERVAL_FORMAT_FULL_ISO_8601:
            default:
                $timeComponents = array_intersect_key(
                    $dateIntervalAmountsBreakdown,
                    [
                        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS => self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS,
                        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES => self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES,
                        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS => self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS,
                        self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS => self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS,
                    ]
                );

                return 'P'
                    . implode(
                        array_intersect_key(
                            $dateIntervalAmountsBreakdown,
                            [
                                self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS => self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS,
                                self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS => self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS,
                                self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS => self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS,
                            ]
                        )
                    )
                    . (empty($timeComponents) ? '' : 'T' . implode($timeComponents));
        }
    }

    public static function getDateIntervalAmountsBreakdown(DateInterval $dateInterval): array
    {
        $dateIntervalMicroseconds = (int) $dateInterval->format('%F');
        $dateIntervalSeconds = (int) $dateInterval->format('%S');
        $dateIntervalMinutes = (int) $dateInterval->format('%I');
        $dateIntervalHours = (int) $dateInterval->format('%H');
        $dateIntervalDays = (int) $dateInterval->format('%D');
        $dateIntervalMonths = (int) $dateInterval->format('%M');
        $dateIntervalYears = (int) $dateInterval->format('%Y');

        return [
            self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS => $dateIntervalYears,
            self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS => $dateIntervalMonths,
            self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS => $dateIntervalDays,
            self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS => $dateIntervalHours,
            self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES => $dateIntervalMinutes,
            self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS => $dateIntervalSeconds,
            self::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS => $dateIntervalMicroseconds,
        ];
    }

    public static function compareDateIntervalToSeconds(
        DateInterval $dateInterval,
        string $comparisonOperator,
        int $seconds
    ): bool {
        $intervalInSeconds = (new DateTime())->setTimestamp(0)->add($dateInterval)->getTimestamp();

        return match ($comparisonOperator) {
            self::DATE_INTERVAL_COMPARISON_EQ => $intervalInSeconds === $seconds,
            self::DATE_INTERVAL_COMPARISON_NEQ => $intervalInSeconds != $seconds,
            self::DATE_INTERVAL_COMPARISON_LT => $intervalInSeconds < $seconds,
            self::DATE_INTERVAL_COMPARISON_LTE => $intervalInSeconds <= $seconds,
            self::DATE_INTERVAL_COMPARISON_GT => $intervalInSeconds > $seconds,
            self::DATE_INTERVAL_COMPARISON_GTE => $intervalInSeconds >= $seconds,
            default => throw new InvalidArgumentException(
                sprintf('Invalid "%s" comparison operator', $comparisonOperator)
            )
        };
    }

    public static function getDatePeriodRangeCollection(
        DatePeriod $datePeriod,
        bool $immutable = false,
        string $arrayIndexFormat = self::DATE_PERIOD_RANGE_COLLECTION_FORMAT_NUMERIC
    ): array {
        // The UTC conversion fixes a long-standing issue with PHP DST handling
        // https://github.com/php/php-src/issues/8860
        $timeZoneUtc = new DateTimeZone('UTC');
        $startDateTimeZone = $datePeriod->getStartDate()->getTimezone();
        $datePeriodUtc = new DatePeriod(
            DateTime::createFromInterface($datePeriod->getStartDate())->setTimezone($timeZoneUtc),
            $datePeriod->getDateInterval(),
            DateTime::createFromInterface($datePeriod->getEndDate())->setTimezone($timeZoneUtc)
        );

        $dateTimeCollection = [];

        if ($arrayIndexFormat === self::DATE_PERIOD_RANGE_COLLECTION_FORMAT_NUMERIC) {
            if ($immutable) {
                /** @var DateTimeInterface $dateTime */
                foreach ($datePeriodUtc as $dateTime) {
                    $dateTime->setTimezone($startDateTimeZone);

                    $dateTimeCollection[] = \DateTimeImmutable::createFromInterface($dateTime)->setTimezone($startDateTimeZone);
                }
            } else {
                /** @var DateTimeInterface $dateTime */
                foreach ($datePeriodUtc as $dateTime) {
                    $dateTime->setTimezone($startDateTimeZone);

                    $dateTimeCollection[] = \DateTime::createFromInterface($dateTime)->setTimezone($startDateTimeZone);
                }
            }
        } else {
            if ($immutable) {
                /** @var DateTimeInterface $dateTime */
                foreach ($datePeriodUtc as $dateTime) {
                    $dateTime->setTimezone($startDateTimeZone);

                    $index = static::format($dateTime, $arrayIndexFormat);

                    if (array_key_exists($index, $dateTimeCollection)) {
                        $i = 0;

                        while (array_key_exists($index . '_' . $i, $dateTimeCollection)) {
                            $i++;
                        }

                        $index .= '_' . $i;
                    }

                    $dateTimeCollection[$index] = \DateTimeImmutable::createFromInterface($dateTime);
                }
            } else {
                /** @var DateTimeInterface $dateTime */
                foreach ($datePeriodUtc as $dateTime) {
                    $dateTime->setTimezone($startDateTimeZone);

                    $index = static::format($dateTime, $arrayIndexFormat);

                    if (array_key_exists($index, $dateTimeCollection)) {
                        $i = 0;

                        while (array_key_exists($index . '_' . $i, $dateTimeCollection)) {
                            $i++;
                        }

                        $index .= '_' . $i;
                    }

                    $dateTimeCollection[$index] = \DateTime::createFromInterface($dateTime)->setTimezone($startDateTimeZone);
                }
            }
        }

        return $dateTimeCollection;
    }

    public static function getDatePeriodMonths(DatePeriod $datePeriod): DatePeriod
    {
        $from = \DateTime::createFromInterface($datePeriod->getStartDate())->modify('this month');
        $to = \DateTime::createFromInterface($datePeriod->getEndDate()->setTime(0, 0, 0, 0))->modify('first day of next month');
        $datePeriodMonthly = new DatePeriod(
            $from,
            new DateInterval('P1M'),
            $to
        );

        return $datePeriodMonthly;
    }

    public static function getCurrentMonthDatePeriod(
        DateTimeInterface $reference,
        DateInterval $dateInterval,
        ?DateTimeZone $timeZone = null
    ): DatePeriod {
        $from = \DateTime::createFromInterface($reference)
            ->setTime(0, 0, 0, 0)
            ->modify('first day of this month');

        if ($timeZone !== null) {
            $from->setTimezone($timeZone);
        }

        $to = (\DateTime::createFromInterface($from))->modify('first day of next month')->modify('-1 second');

        if ($timeZone !== null) {
            $to->setTimezone($timeZone);
        }

        $datePeriod = new DatePeriod(
            $from,
            $dateInterval,
            $to
        );

        return $datePeriod;
    }

    public static function getCurrentMonthDatePeriodOffsetByTimeZone(
        DateTimeInterface $reference,
        DateInterval $dateInterval,
        ?DateTimeZone $timeZone = null
    ): DatePeriod {
        $from = self::offsetDateTimeToTimeZone(
            \DateTime::createFromInterface($reference)
                ->setTime(0, 0, 0, 0),
            $timeZone
        );
        $from->modify('first day of this month');
        $to = (\DateTime::createFromInterface($from))->modify('+1 month')->modify('-1 second');
        $datePeriod = new DatePeriod(
            $from,
            $dateInterval,
            $to
        );

        return $datePeriod;
    }

    public static function getPreviousPeriodFromDates(
        DateTime $from,
        DateTime $to
    ): array
    {
        $diffInDays = $from->diff($to)->days;

        $startDateTime = clone $from;
        $previousPeriodStartTime = $startDateTime->sub(new DateInterval("P{$diffInDays}D"));

        return
            [
                'from' => $previousPeriodStartTime,
                'to' => $from,
            ];
    }

    public static function calculatePeriodEndForWorkingDays(DateTime $referenceDate, int $daysToAdd, array $holidayDates = []): DateTime
    {
        $returnDate = DateTime::createFromInterface($referenceDate);

        if ($daysToAdd === 0) {
            while (self::isDayNonWorking($returnDate, $holidayDates)) {
                $returnDate->add(new DateInterval('P1D'));
            }
        } else {
            while ($daysToAdd > 0) {
                $returnDate->add(new DateInterval("P1D"));

                if (self::isDayNonWorking($returnDate, $holidayDates)) {
                    $daysToAdd++;
                }
                $daysToAdd--;
            }
        }

        return $returnDate;
    }

    private static function isDayNonWorking(DateTime $date, array $holidayDates): bool
    {
        if ($date->format('N') == 6 || $date->format('N') == 7) {
            return true;
        }

        if (in_array($date->format('Y-m-d'), $holidayDates)) {
            return true;
        }

        return false;
    }
}
