<?php

namespace Smtm\Base\Infrastructure\Doctrine\DBAL\Types;

use DateTimeImmutable;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\DateTimeImmutableType;
use function date_create_immutable;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DateTimeMicrosecondsImmutableType extends DateTimeImmutableType implements
    DateTimeMicrosecondsTypeInterface
{
    public const DATETIME_MICROSECONDS_IMMUTABLE   = 'datetime_microseconds_immutable';

    public function getName()
    {
        return static::DATETIME_MICROSECONDS_IMMUTABLE;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return $value;
        }

        if ($value instanceof DateTimeImmutable) {
            return $value->format(static::FORMAT);
        }

        throw ConversionException::conversionFailedInvalidType(
            $value,
            $this->getName(),
            ['null', DateTimeImmutable::class]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value instanceof DateTimeImmutable) {
            return $value;
        }

        $dateTime = DateTimeImmutable::createFromFormat(static::FORMAT, $value);

        if ($dateTime === false) {
            $dateTime = date_create_immutable($value);
        }

        if ($dateTime === false) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                $platform->getDateTimeFormatString()
            );
        }

        return $dateTime;
    }
}
