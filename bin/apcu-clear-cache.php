<?php

declare(strict_types=1);

if (function_exists('apcu_clear_cache')) {
    apcu_clear_cache();
}
