<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Persistence;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ManagerRegistryInterface extends \Doctrine\Persistence\ManagerRegistry
{
    public const DEFAULT_CONNECTION_NAME = 'default';
    public const DEFAULT_READER_CONNECTION_NAME = 'default-reader';
    public const DEFAULT_MANAGER_NAME = 'default';
    public const DEFAULT_READER_MANAGER_NAME = 'default-reader';
    public const DEFAULT_ARCHIVED_ACCESS_MANAGER_NAME = 'default-archived-access';
    public const DEFAULT_ARCHIVED_ACCESS_READER_MANAGER_NAME = 'default-archived-access-reader';
}
