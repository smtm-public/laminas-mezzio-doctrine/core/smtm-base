<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Collection;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait CollectionTrait
{
    protected array $container;

    public function __construct(array $values = [])
    {
        $this->container = $values;
    }

    public function getValues(): array
    {
        return $this->container;
    }

    public function keyFirst(): string|int
    {
        return array_key_first($this->container);
    }

    public function first(): mixed
    {
        return $this->container[$this->keyFirst()];
    }

    public function keyLast(): string|int
    {
        return array_key_last($this->container);
    }

    public function last(): mixed
    {
        return $this->container[$this->keyLast()];
    }

    public function rewind(): void
    {
        reset($this->container);
    }

    public function current(): mixed
    {
        return current($this->container);
    }

    public function key(): mixed
    {
        return key($this->container);
    }

    public function next(): void
    {
        next($this->container);
    }

    public function valid(): bool
    {
        return key($this->container) !== null;
    }

    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    public function &offsetGet($offset): mixed
    {
        $elm = null;

        if (array_key_exists($offset, $this->container)) {
            $elm = &$this->container[$offset];
        }

        return $elm;
    }

    public function count(): int
    {
        return count($this->container);
    }

    public function mergeValue(mixed $value, string $key = null): static
    {
        if ($key !== null) {
            if (!array_key_exists($key, $this->container)) {
                $this->container[$key] = $value;
            }
        } else {
            $this->container[] = $value;
        }

        return $this;
    }

    public function replaceValue(mixed $value, string $key = null): static
    {
        if ($key !== null) {
            $this->container[$key] = $value;
        } else {
            $this->container[] = $value;
        }

        return $this;
    }

    public function shift(): mixed
    {
        return array_shift($this->container);
    }

    public function unshift(mixed $value): static
    {
        array_unshift($this->container, $value);

        return $this;
    }

    public function push(mixed $value): static
    {
        $this->container[] = $value;

        return $this;
    }

    public function pop(): mixed
    {
        return array_pop($this->container);
    }

    public function merge(array $values): static
    {
        $this->container = array_merge($this->container, $values);

        return $this;
    }

    public function mergeRecursive(array $values): static
    {
        $this->container = array_merge_recursive($this->container, $values);

        return $this;
    }

    public function replace(array $values): static
    {
        $this->container = array_replace($this->container, $values);

        return $this;
    }

    public function replaceRecursive(array $values): static
    {
        $this->container = array_replace_recursive($this->container, $values);

        return $this;
    }

    public function union(array $values): static
    {
        $this->container += $values;

        return $this;
    }
}
