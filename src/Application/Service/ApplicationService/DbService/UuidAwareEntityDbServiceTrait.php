<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\DbService;

use Smtm\Base\Application\Service\ApplicationService\Exception\EntityNotFoundException;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Domain\UuidAwareEntityInterface;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait UuidAwareEntityDbServiceTrait
{
    public function getOneByUuid(string $uuid, array $options = []): UuidAwareEntityInterface
    {
        return $this->getOneBy(['uuid' => $uuid], null, $options);
    }

    public function getOneOrNullByUuid(string $uuid, array $options = []): ?UuidAwareEntityInterface
    {
        try {
            return $this->getOneBy(['uuid' => $uuid], null, $options);
        } catch (EntityNotFoundException $e) {
            return null;
        }
    }

    public function updateByUuid(
        string $uuid,
        array $data,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): UuidAwareEntityInterface {
        $updateFunc = function () use ($uuid, $data, $options): UuidAwareEntityInterface {
            $entity = $this->getOneByUuid($uuid);

            return $this->update($entity, $data, $options);
        };

        /**
         * @deprecated due to functional tests already working inside a transaction because of the DataFactory module
         * rollback mechanism
         */
//        if ($this->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
//            return $updateFunc();
//        }

        return $this->transactional($updateFunc);
    }

    public function deleteByUuid(
        string $uuid,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): void {
        $deleteFunc = function () use ($uuid, $options): void {
            $entity = $this->getOneByUuid($uuid);

            $this->delete($entity, $options);
        };

        if ($this->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
            $deleteFunc();

            return;
        }

        $this->transactional($deleteFunc);
    }
}
