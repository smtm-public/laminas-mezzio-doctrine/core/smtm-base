<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\WebDriver;

use Smtm\Base\Infrastructure\WebDriver\Exception\WebDriverException;
use Facebook\WebDriver\Exception\TimeoutException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverElement;
use Facebook\WebDriver\WebDriverNavigationInterface;
use Facebook\WebDriver\WebDriverOptions;
use Facebook\WebDriver\WebDriverTargetLocator;
use Facebook\WebDriver\WebDriverWait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * @method execute(string $name, array $params)
 * @method quit()
 * @method WebDriver close()
 * @method string getCurrentURL()
 * @method string getPageSource()
 * @method string getTitle()
 * @method string getWindowHandle()
 * @method array getWindowHandles()
 * @method WebDriverOptions manage()
 * @method WebDriverNavigationInterface navigate()
 * @method WebDriverTargetLocator switchTo()
 * @method string takeScreenshot(?string $save_as = null)
 * @method WebDriverElement|RemoteWebElement findElement(\Facebook\Webdriver\WebDriverBy $locator)
 * @method WebDriverElement[]|RemoteWebElement[] findElements(\Facebook\Webdriver\WebDriverBy $locator)
 */
class WebDriver
{
    protected \Facebook\WebDriver\WebDriver|RemoteWebDriver|null $webDriver = null;

    public function __construct(
        protected string $serverUrl,
        protected DesiredCapabilities $capabilities
    ) {

    }

    public function __destruct()
    {
        $this->webDriver !== null && $this->webDriver->close();
    }

    public function get(string $url): static
    {
        if ($this->webDriver === null) {
            $this->webDriver = RemoteWebDriver::create($this->serverUrl, $this->capabilities);
        }

        $this->webDriver->get($url);

        return $this;
    }

    /**
     * Inject a snippet of JavaScript into the page for execution in the context of the currently selected frame.
     * The executed script is assumed to be synchronous and the result of evaluating the script will be returned.
     */
    public function executeScript(string $script, array $arguments = [])
    {
        return $this->webDriver->executeScript($script, $arguments);
    }

    /**
     * Construct a new WebDriverWait by the current WebDriver instance.
     * Sample usage:
     *
     * ```
     *   $driver->wait(20, 1000)->until(
     *     WebDriverExpectedCondition::titleIs('WebDriver Page')
     *   );
     * ```
     */
    public function wait(int $timeoutInSecond = 30, int $intervalInMillisecond = 250): WebDriverWait
    {
        return $this->webDriver->wait($timeoutInSecond, $intervalInMillisecond);
    }

    public function __call($name, $arguments)
    {
        if ($this->webDriver === null) {
            $this->webDriver = RemoteWebDriver::create($this->serverUrl, $this->capabilities);
        }

        return $this->webDriver->{$name}(...$arguments);
    }

    /**
     * If the element is found it gets passed as first argument to $callback along with the rest of the arguments to the
     * call to this method:
     * [
     *     'element' => $element,
     *     'webDriverBy' => $webDriverBy,
     *     'callback' => $callback,
     *     'extraArgs' => $extraArgs,
     *     'timeoutSeconds' => $timeoutSeconds,
     *     'intervalMilliseconds' => $intervalMilliseconds,
     *     'timeoutErrorMessage' => $timeoutErrorMessage,
     * ]
     */
    public function waitForElementToAppearAndCallbackOrNull(
        WebDriverBy $webDriverBy,
        callable $callback,
        array $extraArgs = [],
        int $timeoutSeconds = 1,
        int $intervalMilliseconds = 250,
        string $timeoutErrorMessage = 'The timeout was reached'
    ): object|array|string|float|int|bool|null {
        $element = $this->waitForElementToAppearOrTrue(
            $webDriverBy,
            $timeoutSeconds,
            $intervalMilliseconds,
            $timeoutErrorMessage
        );

        if ($element !== true) {
            $args = [
                $element,
                $webDriverBy,
                $callback,
                $extraArgs,
                $timeoutSeconds,
                $intervalMilliseconds,
                $timeoutErrorMessage,
            ];

            return $callback(...$args);
        }

        return null;
    }

    public function waitForElementToAppear(
        WebDriverBy $webDriverBy,
        string $timeoutErrorMessage,
        int $timeoutSeconds = 30,
        int $intervalMilliseconds = 250
    ): WebDriverElement|RemoteWebElement {
        $element = $this->wait($timeoutSeconds, $intervalMilliseconds)->until(
            function () use ($webDriverBy) {
                $element = null;

                try {
                    $element = $this->findElement($webDriverBy);
                } catch (\Throwable $t) {

                }

                return $element;
            },
            $timeoutErrorMessage
        );

        return $element;
    }

//    public function waitForElementToAppear(
//        WebDriverBy $webDriverBy,
//        int $timeoutSeconds = 1,
//        int $intervalMilliseconds = 250,
//        string $timeoutErrorMessage = 'The timeout was reached'
//    ): WebDriverElement|RemoteWebElement {
//        $totalTime = 0;
//
//        return $this->wait($timeoutSeconds, $intervalMilliseconds)->until(
//            function () use ($webDriverBy, &$totalTime, $timeoutSeconds, $intervalMilliseconds, $timeoutErrorMessage) {
//                $totalTime += $intervalMilliseconds;
//                $element = null;
//
//                try {
//                    $element = $this->findElement($webDriverBy);
//                } catch (\Throwable $t) {
//
//                }
//
//                if ($totalTime >= $timeoutSeconds * 1000) {
//                    //WebDriverException::throwException('timeout', $timeoutErrorMessage, null);
//                    throw new WebDriverException(
//                        'WebDriver wait timed out',
//                        0,
//                        new TimeoutException($timeoutErrorMessage)
//                    );
//                }
//
//                return $element;
//            },
//            $timeoutErrorMessage
//        );
//    }

    public function waitForElementToAppearOrTrue(
        WebDriverBy $webDriverBy,
        int $timeoutSeconds = 1,
        int $intervalMilliseconds = 250,
        string $timeoutErrorMessage = 'The timeout was reached'
    ): WebDriverElement|RemoteWebElement|bool {
        $totalTime = 0;

        return $this->wait($timeoutSeconds, $intervalMilliseconds)->until(
            function () use ($webDriverBy, &$totalTime, $timeoutSeconds, $intervalMilliseconds) {
                $totalTime += $intervalMilliseconds;
                $element = null;

                try {
                    $element = $this->findElement($webDriverBy);
                } catch (\Throwable $t) {

                }

                if ($totalTime >= $timeoutSeconds * 1000) {
                    return true;
                }

                return $element;
            },
            $timeoutErrorMessage
        );
    }

    public function waitForElementToDisappear(
        WebDriverBy $webDriverBy,
        string $timeoutErrorMessage,
        int $timeoutSeconds = 30,
        int $intervalMilliseconds = 250
    ): bool {
        return $this->wait($timeoutSeconds, $intervalMilliseconds)->until(
            function () use ($webDriverBy) {
                $element = null;

                try {
                    $element = $this->findElement($webDriverBy);
                } catch (\Throwable $t) {

                }

                return !$element;
            },
            $timeoutErrorMessage
        );
    }

    public function waitForElementHtmlAttributeToHaveValue(
        WebDriverBy $webDriverBy,
        string $htmlAttribute,
        string|int|bool|null $value,
        int $timeoutSeconds = 1,
        int $intervalMilliseconds = 250,
        string $timeoutErrorMessage = 'The timeout was reached'
    ): bool {
        $totalTime = 0;

        return $this->wait($timeoutSeconds, $intervalMilliseconds)->until(
            function () use ($webDriverBy, $htmlAttribute, $value, &$totalTime, $timeoutSeconds, $intervalMilliseconds, $timeoutErrorMessage) {
                $totalTime += $intervalMilliseconds;
                $element = $this->waitForElementToAppear(
                    $webDriverBy,
                    $timeoutErrorMessage,
                    $timeoutSeconds,
                    $intervalMilliseconds
                );

                if ($totalTime >= $timeoutSeconds * 1000) {
                    //WebDriverException::throwException('timeout', $timeoutErrorMessage, null);
                    throw new WebDriverException(
                        'WebDriver wait timed out',
                        0,
                        new TimeoutException($timeoutErrorMessage)
                    );
                }

                $htmlAttributeValue = $element->getAttribute($htmlAttribute);

                return $htmlAttributeValue === $value;
            },
            $timeoutErrorMessage
        );
    }

    public function waitForElementHtmlDomPropertyToHaveValue(
        WebDriverBy $webDriverBy,
        string $domProperty,
        string|int|bool|null $value,
        int $timeoutSeconds = 1,
        int $intervalMilliseconds = 250,
        string $timeoutErrorMessage = 'The timeout was reached'
    ): bool {
        $totalTime = 0;

        return $this->wait($timeoutSeconds, $intervalMilliseconds)->until(
            function () use ($webDriverBy, $domProperty, $value, &$totalTime, $timeoutSeconds, $intervalMilliseconds, $timeoutErrorMessage) {
                $totalTime += $intervalMilliseconds;
                $element = $this->waitForElementToAppear(
                    $webDriverBy,
                    $timeoutErrorMessage,
                    $timeoutSeconds,
                    $intervalMilliseconds
                );

                if ($totalTime >= $timeoutSeconds * 1000) {
                    //WebDriverException::throwException('timeout', $timeoutErrorMessage, null);
                    throw new WebDriverException(
                        'WebDriver wait timed out',
                        0,
                        new TimeoutException($timeoutErrorMessage)
                    );
                }

                $domPropertyValue = $element->getDomProperty($domProperty);

                return $domPropertyValue === $value;
            },
            $timeoutErrorMessage
        );
    }

    public function scrollIntoViewAndClickElement($element)
    {
        $this->executeScript('arguments[0].scrollIntoView(true);', [$element]);

        return $element->click();
    }

    public function waitForElementToAppearAndGetText(
        WebDriverBy $webDriverBy,
        string $timeoutErrorMessage,
        int $timeoutSeconds = 30,
        int $intervalMilliseconds = 250
    ): string {
        $element = $this->wait($timeoutSeconds, $intervalMilliseconds)->until(
            function () use ($webDriverBy) {
                $element = null;

                try {
                    $element = $this->findElement($webDriverBy);
                } catch (\Throwable $t) {

                }

                return $element;
            },
            $timeoutErrorMessage
        );

        return $element->getText();
    }

    public function waitForElementToAppearAndGetTextOrNull(
        WebDriverBy $webDriverBy,
        string $timeoutErrorMessage,
        int $timeoutSeconds = 30,
        int $intervalMilliseconds = 250
    ): string|null {
        try {
            return $this->waitForElementToAppearAndGetText(
                $webDriverBy,
                $timeoutErrorMessage,
                $timeoutSeconds,
                $intervalMilliseconds
            );
        } catch (\Throwable $t) {
            return null;
        }
    }

    public function waitForElementToAppearAndGetOuterHtml(
        WebDriverBy $webDriverBy,
        string $timeoutErrorMessage,
        int $timeoutSeconds = 30,
        int $intervalMilliseconds = 250
    ): string {
        $element = $this->wait($timeoutSeconds, $intervalMilliseconds)->until(
            function () use ($webDriverBy) {
                $element = null;

                try {
                    $element = $this->findElement($webDriverBy);
                } catch (\Throwable $t) {

                }

                return $element;
            },
            $timeoutErrorMessage
        );

        return $element->getDomProperty('outerHTML');
    }

    public function waitForElementToAppearAndGetOuterHtmlOrNull(
        WebDriverBy $webDriverBy,
        string $timeoutErrorMessage,
        int $timeoutSeconds = 30,
        int $intervalMilliseconds = 250
    ): string|null {
        try {
            return $this->waitForElementToAppearAndGetOuterHtml(
                $webDriverBy,
                $timeoutErrorMessage,
                $timeoutSeconds,
                $intervalMilliseconds
            );
        } catch (\Throwable $t) {
            return null;
        }
    }

    public function waitForElementToAppearAndGetInnerHtml(
        WebDriverBy $webDriverBy,
        string $timeoutErrorMessage,
        int $timeoutSeconds = 30,
        int $intervalMilliseconds = 250
    ): string {
        $element = $this->wait($timeoutSeconds, $intervalMilliseconds)->until(
            function () use ($webDriverBy) {
                $element = null;

                try {
                    $element = $this->findElement($webDriverBy);
                } catch (\Throwable $t) {

                }

                return $element;
            },
            $timeoutErrorMessage
        );

        return $element->getDomProperty('innerHTML');
    }

    public function waitForElementToAppearAndGetInnerHtmlOrNull(
        WebDriverBy $webDriverBy,
        string $timeoutErrorMessage,
        int $timeoutSeconds = 30,
        int $intervalMilliseconds = 250
    ): string|null {
        try {
            return $this->waitForElementToAppearAndGetInnerHtml(
                $webDriverBy,
                $timeoutErrorMessage,
                $timeoutSeconds,
                $intervalMilliseconds
            );
        } catch (\Throwable $t) {
            return null;
        }
    }

    public function waitForElementToAppearAndTakeScreenshot(
        WebDriverBy $webDriverBy,
        string $timeoutErrorMessage,
        int $timeoutSeconds = 30,
        int $intervalMilliseconds = 250
    ): string {
        $element = $this->wait($timeoutSeconds, $intervalMilliseconds)->until(
            function () use ($webDriverBy) {
                $element = null;

                try {
                    $element = $this->findElement($webDriverBy);
                } catch (\Throwable $t) {

                }

                return $element;
            },
            $timeoutErrorMessage
        );

        return $element->takeElementScreenshot();
    }

    public function waitForElementToAppearAndTakeScreenshotOrNull(
        WebDriverBy $webDriverBy,
        string $timeoutErrorMessage,
        int $timeoutSeconds = 30,
        int $intervalMilliseconds = 250
    ): string|null {
        try {
            return $this->waitForElementToAppearAndTakeScreenshot(
                $webDriverBy,
                $timeoutErrorMessage,
                $timeoutSeconds,
                $intervalMilliseconds
            );
        } catch (\Throwable $t) {
            return null;
        }
    }
}
