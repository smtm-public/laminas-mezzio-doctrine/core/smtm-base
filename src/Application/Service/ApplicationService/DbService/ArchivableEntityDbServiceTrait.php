<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\DbService;

use DateTime;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityArchiveException;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Domain\ArchivedByIpAddressAwareEntityInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\ArchiveUpdateQueryExecutionMethodCheckAwareEntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Domain\UnarchivedByIpAddressAwareEntityInterface;
use Smtm\Base\Domain\UnarchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\UnarchiveUpdateQueryExecutionMethodCheckAwareEntityInterface;
use Smtm\Base\Infrastructure\Helper\OopHelper;
use Smtm\Base\Infrastructure\Repository\Exception\SaveRecordException;
use Doctrine\DBAL\Driver\Result;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Exception;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ArchivableEntityDbServiceTrait
{
    protected function archiveFunc(
        NotArchivedAwareEntityInterface $entity,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): void {
        $entity->setNotArchived(null);

        if ($entity instanceof ArchivedByIpAddressAwareEntityInterface) {
            $entity->setArchivedByIpAddress(
                $options[ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS]
            );
        }

        if ($entity instanceof ArchivedDateTimeAwareEntityInterface) {
            $entity->setArchived(new DateTime());
        }

        if ($entity instanceof ArchiveUpdateQueryExecutionMethodCheckAwareEntityInterface) {
            $entity->setArchiveUpdateQueryExecutionMethodCheck(1);
        }

        /** @var EntityRepository $repository */
        $repository = $this->getRepository();

        try {
            $repository->save($entity);
        } catch (SaveRecordException $e) {
            throw new EntityArchiveException(
                EntityArchiveException::formatMessageForEntity($this->domainObjectName),
                0,
                $e
            );
        }
    }

    public function archive(
        NotArchivedAwareEntityInterface $entity,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): void {
        if ($this->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
            $this->archiveFunc($entity, $options);

            return;
        }

        $this->transactional(
            function () use ($entity, $options) {
                $this->archiveFunc($entity, $options);
            }
        );
    }

    public function archiveById(
        array|string|float|int|bool $id,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): void {
        if ($this->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
            $entity = $this->getById($id);
            $this->archiveFunc($entity, $options);

            return;
        }

        $this->transactional(
            function () use ($id, $options): void {
                $entity = $this->getById($id);
                $this->archiveFunc($entity, $options);
            }
        );
    }

    public function bulkArchiveById(
        array $id,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): int {
        return $this->doBulkArchive($this->prepareBulkArchiveById($id, $options));
    }

    public function unarchiveFunc(
        NotArchivedAwareEntityInterface $entity,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): NotArchivedAwareEntityInterface {
        $entity->setNotArchived(true);

        if ($entity instanceof UnarchivedByIpAddressAwareEntityInterface) {
            $entity->setUnarchivedByIpAddress(
                $options[ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS]
            );
        }

        if ($entity instanceof UnarchivedDateTimeAwareEntityInterface) {
            $entity->setUnarchived(new DateTime());
        }

        if ($entity instanceof UnarchiveUpdateQueryExecutionMethodCheckAwareEntityInterface) {
            $entity->setUnarchiveUpdateQueryExecutionMethodCheck(1);
        }

        /** @var EntityRepository $repository */
        $repository = $this->getRepository();

        try {
            $repository->save($entity);
        } catch (SaveRecordException $e) {
            throw new EntityArchiveException(
                EntityArchiveException::formatMessageForEntity($this->domainObjectName),
                0,
                $e
            );
        }

        return $entity;
    }

    public function unarchive(
        NotArchivedAwareEntityInterface $entity,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): NotArchivedAwareEntityInterface {
        if ($this->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
            return $this->unarchiveFunc($entity, $options);
        }

        return $this->transactional(
            function () use ($entity, $options): NotArchivedAwareEntityInterface {
                return $this->unarchiveFunc($entity, $options);
            }
        );
    }

    public function unarchiveById(
        array|string|float|int|bool $id,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): NotArchivedAwareEntityInterface {
        if ($this->getRepository()->getEntityManager()->getConnection()->isTransactionActive()) {
            $entity = $this->getById($id);

            return $this->unarchiveFunc($entity, $options);
        }

        return $this->transactional(
            function () use ($id, $options): NotArchivedAwareEntityInterface {
                $entity = $this->getById($id);

                return $this->unarchiveFunc($entity, $options);
            }
        );
    }

    protected function prepareBulkArchive(
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): QueryBuilder {
        /** @var QueryBuilder $qb */
        $qb = $this->entityManagerRegistry
            ->getManager($this->entityManagerName)
            ->createQueryBuilder();
        $dateTime = new DateTime();
        $qb
            ->update($this->domainObjectName, 'o')
            ->set('o.notArchived', ':notArchived')
            ->set('o.archivedAt', ':archivedAt');
        $qb->setParameter('notArchived', null);
        $qb->setParameter('archivedAt', $dateTime, Types::DATETIME_MUTABLE);

        if (OopHelper::classImplements($this->domainObjectName, ArchivedByIpAddressAwareEntityInterface::class)) {
            if (($options[ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS] ?? null) !== null) {
                $qb->set('o.archivedByIpAddress', ':archivedByIpAddress');
                $qb->setParameter(
                    'archivedByIpAddress',
                    $options[ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS]
                );
            }
        }

        if (OopHelper::classImplements($this->domainObjectName, ArchivedDateTimeAwareEntityInterface::class)) {
            $qb->set('o.archived', ':archived');
            $qb->setParameter('archived', $dateTime, Types::DATETIME_MUTABLE);
        }

        if (OopHelper::classImplements($this->domainObjectName, ArchiveUpdateQueryExecutionMethodCheckAwareEntityInterface::class)) {
            $qb->set('o.archiveUpdateQueryExecutionMethodCheck', ':archiveUpdateQueryExecutionMethodCheck');
            $qb->setParameter('archiveUpdateQueryExecutionMethodCheck', 1, Types::INTEGER);
        }

        return $qb;
    }

    protected function prepareBulkArchiveById(
        array $id,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): QueryBuilder {
        /** @var QueryBuilder $qb */
        $qb = $this->prepareBulkArchive($options);
        $qb->where($qb->expr()->in('o.id', $id));

        return $qb;
    }

    protected function doBulkArchive(
        QueryBuilder $qb
    ): int {
        try {
            /** @var Result $result */
            return $qb->getQuery()->execute();
        } catch (Exception $e) {
            throw new EntityArchiveException(
                EntityArchiveException::formatMessageForEntity($this->domainObjectName),
                0,
                $e
            );
        }
    }
}
