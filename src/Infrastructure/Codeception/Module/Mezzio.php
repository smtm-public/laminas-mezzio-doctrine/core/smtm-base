<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Codeception\Module;

use Codeception\Lib\Framework;
use Codeception\Lib\Interfaces\DataMapper;
use Codeception\Lib\Interfaces\DoctrineProvider;
use Codeception\TestInterface;
use Smtm\Base\Infrastructure\Codeception\Lib\Connector\Mezzio as MezzioConnector;
use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManagerInterface;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Mezzio\Application;
use PHPUnit\Framework\AssertionFailedError;
use Psr\Container\ContainerInterface;
use Symfony\Component\BrowserKit\AbstractBrowser;

/**
 * This module allows you to run tests inside Mezzio.
 *
 * Uses `config/container.php` file by default.
 *
 * ## Status
 *
 * * Maintainer: **Naktibalda**
 * * Stability: **alpha**
 *
 * ## Config
 *
 * * `container` - (default: `config/container.php`) relative path to file which returns Container
 * * `recreateApplicationBetweenTests` - (default: false) whether to recreate the whole application before each test
 * * `recreateApplicationBetweenRequests` - (default: false) whether to recreate the whole application before each request
 *
 * ## Public properties
 *
 * * application -  instance of `\Mezzio\Application`
 * * container - instance of `\Interop\Container\ContainerInterface`
 * * client - BrowserKit client
 *
 */
class Mezzio extends Framework implements DoctrineProvider, DataMapper
{
    // Doesn't work
//    protected ?array $internalDomains = [
//        'https?://127.0.0.1'
//    ];

    /**
     * @var array<string, mixed>
     */
    protected array $config = [
        'container'                          => 'config/container.php',
        'recreateApplicationBetweenTests'    => false,
        'recreateApplicationBetweenRequests' => false,
    ];

    /**
     * @var \Smtm\Base\Infrastructure\Codeception\Lib\Connector\Mezzio
     */
    public ?AbstractBrowser $client;

    /**
     * @deprecated Doesn't work as expected if Application is recreated between requests
     */
    public ContainerInterface $container;

    /**
     * @deprecated Doesn't work as expected if Application is recreated between requests
     */
    public Application $application;

    public function getInternalDomains(): array
    {
        return [
            '#127\.0\.0\.1#',
        ];
    }

    public function _initialize(): void
    {
        $this->client = new MezzioConnector();
        $this->client->setConfig($this->config);

        $this->application = $this->client->initApplication();
        $this->container   = $this->client->getContainer();
    }

    public function _before(TestInterface $test): void
    {
        $this->client = new MezzioConnector();
        $this->client->setConfig($this->config);

        if (($this->config['recreateApplicationBetweenTests'] ?? false) && !($this->config['recreateApplicationBetweenRequests'] ?? false)) {
            $this->application = $this->client->initApplication();
            /**
             * Does not work when mixing
             * \Codeception\Module\Doctrine2::grab*FromRepository()
             * and
             * \Codeception\Module\DataFactory::have*()
             * Recreating the container will also recreate the EntityManager which will raise
             * ```
             * [Doctrine\ORM\ORMInvalidArgumentException] A new entity was found through the relationship
             * 'ENTITYCLASS#RELATIONSHIP_PROPERTY' that was not configured to cascade persist operations for entity:
             * ENTITY@ENTITY_OBJECT_INSTANCE_SPL_OBJECT_ID. To solve this issue: Either explicitly call
             * EntityManager#persist() on this unknown entity or configure cascade persist this association in the
             * mapping for example @ManyToOne(..,cascade={"persist"}). If you cannot find out which entity causes the
             * problem implement 'RELATIONSHIP_CLASS#__toString()' to get a clue.
             * ```
             * exception when trying
             * to persist with the DataFactory entities, the relations of which have been retrieved through the
             * Doctrine2 module.
             */
            $this->container   = $this->client->getContainer();
        } elseif ($this->application !== null) {
            $this->client->setApplication($this->application);
        }
    }

    public function _after(TestInterface $test): void
    {
        //Close the session, if any are open
        if (session_status() == PHP_SESSION_ACTIVE) {
            session_write_close();
        }
        if (isset($_SESSION)) {
            $_SESSION = [];
        }

        parent::_after($test);
    }

    public function _getEntityManager(): EntityManagerInterface
    {
        if (!$this->container->get(InfrastructureServicePluginManager::class)->has(ManagerRegistryInterface::class)) {
            throw new AssertionFailedError('Service ' . ManagerRegistryInterface::class . ' is not available in container');
        }

        $em = $this
            ->container
            ->get(InfrastructureServicePluginManager::class)
            ->get(ManagerRegistryInterface::class)
            ->getManager(\Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface::DEFAULT_MANAGER_NAME);

        return $em;
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function haveInRepository(string $entity, array $data)
    {}

    public function seeInRepository(string $entity, array $params = []): void
    {}

    public function dontSeeInRepository(string $entity, array $params = []): void
    {}

    public function grabFromRepository(string $entity, string $field, array $params = [])
    {}
}
