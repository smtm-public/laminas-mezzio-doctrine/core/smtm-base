<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator;

use Laminas\Validator\AbstractValidator;
use Laminas\Validator\Exception\InvalidArgumentException;
use ReflectionEnum;

/**
 * @author Nikolay Nikolov <n.nikolov@smtm.bg>
 */
class IsBackedEnum extends AbstractValidator
{
    const INVALID_BACKED_ENUM = 'invalidBackedEnum';

    protected array $messageTemplates = [
        self::INVALID_BACKED_ENUM => '"%value%" is not a valid backing value'
    ];

    protected array $options = [];

    protected ReflectionEnum $enum;
    protected bool $nullable = false;

    public function getEnum(): ReflectionEnum
    {
        return $this->enum;
    }

    public function setEnum(string $enum): static
    {
        try {
            $reflectionEnum = new ReflectionEnum($enum);
        } catch (\ReflectionException) {
            throw new InvalidArgumentException("Invalid argument $enum for option 'enum' in method " . __METHOD__);
        }

        $this->enum = $reflectionEnum;

        return $this;
    }

    public function isNullable(): bool
    {
        return $this->nullable;
    }

    public function setNullable(bool $nullable): static
    {
        $this->nullable = $nullable;

        return $this;
    }

    public function isValid($value): bool
    {
        if ($value === null && $this->isNullable()) {
            return true;
        }

        $reflectionEnum = $this->getEnum();
        $value = match ((string) $reflectionEnum->getBackingType()) {
            'int' => (int) $value,
            'string' => (string) $value
        };

        foreach ($reflectionEnum->getCases() as $reflectionEnumBackedCase) {
            if ($reflectionEnumBackedCase->getValue()->value ===  $value) {
                return true;
            }
        }

        $this->error(self::INVALID_BACKED_ENUM, $value);

        return false;
    }
}
