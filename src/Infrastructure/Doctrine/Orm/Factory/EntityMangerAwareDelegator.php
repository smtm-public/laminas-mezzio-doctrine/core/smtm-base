<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm\Factory;

use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManagerAwareInterface;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EntityMangerAwareDelegator implements DelegatorFactoryInterface
{
    protected string $entityManagerName;

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var EntityManagerAwareInterface $object */
        $object = $callback();
        /** @var ManagerRegistryInterface $managerRegistry */
        $managerRegistry =
            $container
                ->get(InfrastructureServicePluginManager::class)
                ->get(ManagerRegistryInterface::class);
        $object->setEntityManager($managerRegistry->getManager($this->entityManagerName));

        return $object;
    }
}
