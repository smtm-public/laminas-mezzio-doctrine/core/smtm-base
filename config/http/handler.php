<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../../.env.smtm.smtm-base.http.handler')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../../', '.env.smtm.smtm-base.http.handler'
    );
    $dotenv->load();
}

return [
    'index' => [
        'criteria' => [
            'translateUnknownQueryParamsAsCriteria' => filter_var(
                EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_HTTP_HANDLER_INDEX_CRITERIA_TRANSLATE_UNKNOWN_QUERY_PARAMS_AS_CRITERIA'),
                FILTER_VALIDATE_BOOLEAN
            ),
            'treatStringValueAsLooseMatch' => (int) EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_HTTP_HANDLER_INDEX_CRITERIA_TREAT_STRING_VALUE_AS_LOOSE_MATCH'),
        ]
    ],
];
