<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

use DateTimeInterface;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DateTimeExtractionStrategy extends AbstractExtractionStrategy
{
    public const OPTION_KEY_FORMAT = 'format';

    public function extract($value, ?object $object = null): ?string
    {
        if ($value instanceof DateTimeInterface) {
            return DateTimeHelper::format(
                $value,
                $this->options[static::OPTION_KEY_FORMAT] ?? DateTimeHelper::DEFAULT_FORMAT
            );
        }

        return $value;
    }
}
