<?php

declare(strict_types=1);

namespace Smtm\Base;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ConfigCollectionAwareTrait
{

    /* use ConfigAwareTrait; */

    protected array $configCollection = [];

    public function getConfigCollection(): array
    {
        return $this->configCollection;
    }

    public function setConfigCollection(array $configCollection): static
    {
        $this->configCollection = $configCollection;

        return $this;
    }

    public function addConfig(string $name, array $config): static
    {
        $this->configCollection[$name] = $config;

        return $this;
    }

    public function getConfigByName(string $name): ?array
    {
        return $this->configCollection[$name] ?? null;
    }

    public function removeConfig(string $name): static
    {
        unset($this->configCollection[$name]);

        return $this;
    }
}
