<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractIdAwareEntity implements IdAwareEntityInterface
{
    use IdAwareEntityTrait;
}
