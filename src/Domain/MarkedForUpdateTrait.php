<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait MarkedForUpdateTrait
{
    protected bool $__markedForUpdate = false;
    protected array $__changeSet = [];

    public function __getMarkedForUpdate(): bool
    {
        return $this->__markedForUpdate;
    }

    public function __setMarkedForUpdate(
        bool $markedForUpdate,
        ?string $propertyName = null,
        mixed $oldValue = null,
        mixed $newValue = null
    ): static {
        $this->__markedForUpdate = $markedForUpdate;

        if ($markedForUpdate) {
            if ($propertyName !== null) {
                if (!array_key_exists($propertyName, $this->__changeSet)) {
                    $this->__changeSet[$propertyName]['old'] = $oldValue;
                }

                $this->__changeSet[$propertyName]['new'] = $newValue;
            }
        } else {
            $this->__changeSet = [];
        }

        return $this;
    }

    public function __getChangeSet(): array
    {
        return $this->__changeSet;
    }
}
