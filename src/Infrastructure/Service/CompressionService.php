<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;
use Smtm\Base\Infrastructure\Exception\RuntimeException;
use JetBrains\PhpStorm\Pure;
use Laminas\Diactoros\Stream;
use Psr\Http\Message\StreamInterface;
use Ramsey\Uuid\Uuid;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CompressionService extends AbstractInfrastructureService
{

    public const FORMAT_ZIP = 'zip';
    public const FORMAT_ZLIB = 'zlib';
    public const FORMAT_BZIP2 = 'bzip2';
    public const FORMAT_RAR = 'rar';

    protected mixed $wrapped = [
        'container' => null,
        'statusCode' => null,
    ];

    public function compressArrayOfFilesToStream(string $fileName, array $files = [], string $format = self::FORMAT_ZIP): StreamInterface
    {
        $this->compressArrayOfFiles($fileName, $files, $format);

        return new Stream($fileName);
    }

    public function compressArrayOfFiles(string $fileName, array $files = [], string $format = self::FORMAT_ZIP): static
    {
        $this->clearWrapped();

        switch ($format) {
            case self::FORMAT_ZIP;
                $container = new \ZipArchive();
                $this->setWrappedContainer($container);
                $this->setWrappedStatusCode($container->open($fileName, \ZipArchive::CREATE));

                if ($this->getWrappedStatusCode() !== true) {
                    throw $this->createException();
                }

                foreach ($files as $file) {
                    if (isset($file['content'])) {
                        $container->addFromString($file['name'], $file['content']);
                    } elseif (isset($file['path'])) {
                        $container->addFile($file['name'], $file['path']);
                    } else {
                        $container->addFromString($file['name'], '');
                    }
                }

                $this->setWrappedStatusCode($container->close());

                if ($this->getWrappedStatusCode() === false) {
                    throw $this->createException('Error closing archive');
                }

                break;
            default:
                throw new RuntimeException('Unknown compression format `' . $format . '`');
        }

        return $this;
    }

    public function createException(?string $message = null, ?int $code = null): RuntimeException
    {
        if ($this->getWrappedContainer() instanceof \ZipArchive) {
            return new RuntimeException($this->getWrappedContainer()->getStatusString(), $this->getWrappedStatusCode());
        } else {
            return new RuntimeException($message, $code);
        }
    }

    protected function clearWrapped(): static
    {
        $this->wrapped = [
            'container' => null,
            'statusCode' => null,
        ];

        return $this;
    }

    protected function getWrappedContainer(): mixed
    {
        return $this->wrapped['container'];
    }

    protected function setWrappedContainer(mixed $container): static
    {
        $this->wrapped['container'] = $container;

        return $this;
    }

    protected function getWrappedStatusCode(): mixed
    {
        return $this->wrapped['statusCode'];
    }

    protected function setWrappedStatusCode(mixed $statusCode): static
    {
        $this->wrapped['statusCode'] = $statusCode;

        return $this;
    }
}
