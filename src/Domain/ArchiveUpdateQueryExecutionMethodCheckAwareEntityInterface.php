<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ArchiveUpdateQueryExecutionMethodCheckAwareEntityInterface
{
    public function getArchiveUpdateQueryExecutionMethodCheck(): ?int;
    public function setArchiveUpdateQueryExecutionMethodCheck(?int $archiveUpdateQueryExecutionMethodCheck): static;
}
