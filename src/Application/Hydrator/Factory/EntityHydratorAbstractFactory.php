<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Factory;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;
use Smtm\Base\Application\Hydrator\Strategy\HydrationStrategyPluginManager;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Laminas\Validator\ValidatorPluginManager;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EntityHydratorAbstractFactory implements AbstractFactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get(ApplicationServicePluginManager::class),
            $container->get(InfrastructureServicePluginManager::class),
            $container->get(HydrationStrategyPluginManager::class),
            $container->get(ValidatorPluginManager::class)
        );
    }

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return is_subclass_of($requestedName, DomainObjectHydrator::class);
    }
}
