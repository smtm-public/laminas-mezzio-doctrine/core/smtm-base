<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator\Factory;

use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Laminas\Validator\ValidatorPluginManager;
use Laminas\Validator\ValidatorPluginManagerAwareInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ValidatorPluginManagerAwareDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var ValidatorPluginManagerAwareInterface $validator */
        $validator = $callback();
        $validator->setValidatorPluginManager($container->get(ValidatorPluginManager::class));

        return $validator;
    }
}
