<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator\Factory;

use Smtm\Base\Infrastructure\Laminas\Validator\PasswordValidator;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Mihail Geshev <m.geshev@smtm.com>
 */
class PasswordValidatorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new PasswordValidator($options ?? []);
    }
}
