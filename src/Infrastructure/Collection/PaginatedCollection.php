<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Collection;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class PaginatedCollection extends Collection implements PaginatedCollectionInterface
{
    protected int $totalCount;

    public function __construct(array $values, int $totalCount)
    {
        parent::__construct($values);

        $this->totalCount = $totalCount;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }
}
