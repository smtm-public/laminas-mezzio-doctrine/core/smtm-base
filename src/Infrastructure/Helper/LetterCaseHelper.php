<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LetterCaseHelper
{
    public const LETTER_CASE_LOWER = 'lower';
    public const LETTER_CASE_LCFIRST = 'lcfirst';
    public const LETTER_CASE_UPPER = 'upper';
    public const LETTER_CASE_UCFIRST = 'ucfirst';
    public const LETTER_CASE_CAMEL = 'camel';
    public const LETTER_CASE_PASCAL = 'pascal';
    public const LETTER_CASE_SNAKE = 'snake';
    public const LETTER_CASE_KEBAB = 'kebab';
    public const LETTER_CASE_ACRONYM = 'acronym';
    public const LETTER_CASES = [
        self::LETTER_CASE_LOWER,
        self::LETTER_CASE_LCFIRST,
        self::LETTER_CASE_UPPER,
        self::LETTER_CASE_UCFIRST,
        self::LETTER_CASE_CAMEL,
        self::LETTER_CASE_PASCAL,
        self::LETTER_CASE_SNAKE,
        self::LETTER_CASE_KEBAB,
        self::LETTER_CASE_ACRONYM,
    ];

    public static function format(
        string $value,
        string|array $letterCaseCollection
    ): string {
        if (!is_array($letterCaseCollection)) {
            $letterCaseCollection = [$letterCaseCollection];
        }

        foreach ($letterCaseCollection as $letterCase) {
            $value = match ($letterCase) {
                static::LETTER_CASE_LOWER => strtolower($value),
                static::LETTER_CASE_LCFIRST => lcfirst($value),
                static::LETTER_CASE_UPPER => strtoupper($value),
                static::LETTER_CASE_UCFIRST => ucfirst($value),
                static::LETTER_CASE_CAMEL => lcfirst(
                    preg_replace_callback_array(
                        [
                            '#^[\W_]+#' => fn(array $match) => '',
                            '#[\W_]+$#' => fn(array $match) => '',
                            '#[\W_]+([a-zA-Z0-9])#' => fn(array $match) => strtoupper($match[1]),
                        ],
                        $value
                    )
                ),
                static::LETTER_CASE_PASCAL => ucfirst(
                    preg_replace_callback_array(
                        [
                            '#^[\W_]+#' => fn(array $match) => '',
                            '#[\W_]+$#' => fn(array $match) => '',
                            '#[\W_]+([a-zA-Z0-9])#' => fn(array $match) => strtoupper($match[1]),
                        ],
                        $value
                    )
                ),
                static::LETTER_CASE_SNAKE => preg_replace_callback_array(
                    [
                        '#^[\W_]+#' => fn(array $match) => '',
                        '#[\W_]+$#' => fn(array $match) => '',
                        '#[\W_]+([a-zA-Z0-9])#' => fn(array $match) => '_' . $match[1],
                        '#([a-z])([A-Z0-9])#' => fn(array $match) => $match[1] . '_' . $match[2],
                    ],
                    $value
                ),
                static::LETTER_CASE_KEBAB => preg_replace_callback_array(
                    [
                        '#^[\W_]+#' => fn(array $match) => '',
                        '#[\W_]+$#' => fn(array $match) => '',
                        '#[\W_]+([a-zA-Z0-9])#' => fn(array $match) => '-' . $match[1],
                        '#([a-z])([A-Z0-9])#' => fn(array $match) => $match[1] . '-' . $match[2],
                    ],
                    $value
                ),
                static::LETTER_CASE_ACRONYM => preg_replace(
                    '#(?!(^[a-zA-Z])|((?<=_)[a-z])|([A-Z])).#',
                    '',
                    $value
                ),
            };
        }

        return $value;
    }
}
