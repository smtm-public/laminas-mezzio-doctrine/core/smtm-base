<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Validator;

use Laminas\Validator\AbstractValidator;
use Laminas\Validator\EmailAddress;
use Laminas\Validator\Exception\InvalidArgumentException;
use Laminas\Validator\ValidatorPluginManagerAwareInterface;

/**
 * Bidirectional (is|is not) validation for expected value type: string
 *
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IsString extends AbstractValidator implements ValidatorPluginManagerAwareInterface
{

    use ValidatorPluginManagerAwareTrait;

    public const INVALID_TRUE = 'invalidValueIs';
    public const INVALID_FALSE = 'invalidValueIsNot';

    public const NOT_HEXADECIMAL = 'notHexadecimal';
    public const OPTION_HEXADECIMAL_CASE_INSENSITIVE = 1;
    public const NOT_HEXADECIMAL_CASE_UPPER = 'notHexadecimalCaseUpper';
    public const OPTION_HEXADECIMAL_CASE_UPPER = 2;
    public const NOT_HEXADECIMAL_CASE_LOWER = 'notHexadecimalCaseLower';
    public const OPTION_HEXADECIMAL_CASE_LOWER = 3;

    public const NOT_BASE64ENCODED = 'notBase64Encoded';
    public const OPTION_BASE64ENCODED = 1;
    public const NOT_BASE64ENCODED_URL_SAFE = 'notBase64EncodedUrlSafe';
    public const OPTION_BASE64ENCODED_URL_SAFE = 2;
    public const NOT_BASE64ENCODED_URL_UNSAFE = 'notBase64EncodedUrlUnsafe';
    public const OPTION_BASE64ENCODED_URL_UNSAFE = 3;

    public const NOT_URI_SCHEME_AND_URI = 'notUriSchemeAndUri';
    public const OPTION_URI_SCHEME_AND_URI = 1;

    public const NOT_EMAIL_ADDRESS = 'notEmailAddress';

    public const INVALID_MIN_LENGTH = 'invalidMinLength';
    public const INVALID_MAX_LENGTH = 'invalidMaxLength';

    protected array $messageTemplates = [
        self::INVALID_TRUE => 'Invalid value given. String expected.',
        self::INVALID_FALSE => 'Invalid value given. Cannot be a string.',
        self::NOT_HEXADECIMAL => 'Invalid value given. The string may contain only hexadecimal characters.',
        self::NOT_HEXADECIMAL_CASE_UPPER => 'Invalid value given. The string may contain only uppercase hexadecimal characters.',
        self::NOT_HEXADECIMAL_CASE_LOWER => 'Invalid value given. The string may contain only lowercase hexadecimal characters.',
        self::NOT_BASE64ENCODED => 'Invalid value given. The string may contain only the base64 character set (including the URL safe ones).',
        self::NOT_BASE64ENCODED_URL_SAFE => 'Invalid value given. The string may contain only the URL safe base64 character set.',
        self::NOT_BASE64ENCODED_URL_UNSAFE => 'Invalid value given. The string may contain only the URL unsafe (standard) base64 character set.',
        self::NOT_URI_SCHEME_AND_URI => 'Invalid value given. The string must be a valid URI including scheme.',
        self::NOT_EMAIL_ADDRESS => "Invalid value given. The string must be a valid email address.  Errors: '%emailValidatorMessages%'.",
        self::INVALID_MIN_LENGTH =>
            "Invalid string length. The minimum length of the string is '%minLength%'. Got '%length%'.",
        self::INVALID_MAX_LENGTH =>
            "Invalid string length. The maximum length of the string is '%maxLength%'. Got '%length%'.",
    ];

    protected $messageVariables = [
        'minLength' => 'minLength',
        'maxLength' => 'maxLength',
        'length' => 'length',

        'emailValidatorMessages' => 'emailValidatorMessages',
    ];

    protected ?int $minLength = null;
    protected ?int $maxLength = null;
    protected ?int $length = null;

    protected ?string $emailValidatorMessages = null;

    protected array $options = [];

    /**
     * $options can have the following keys:
     * - is - true(default)|false - whether it should be of type string (true) or not (false)
     * - hexadecimal - 0(default)|1|2|3 - whether the string should contain only mixed case hexadecimal characters (1), or uppercase hexadecimal characters (2), or lowercase hexadecimal characters (3)
     * - uri - 0(default)|1 - whether the string should represent a valid URI with scheme (1)
     * - email - 0(default)|1 - whether the string should represent a valid email address (1)
     * - minLength - integer - the minimum length of the string
     * - maxLength - integer - the maximum length of the string
     */
    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    public function isValid($value)
    {
        $return = true;

        $is = (bool) ($this->options['is'] ?? true);
        $nullable = $this->options['nullable'] ?? true;

        if (($is && !is_string($value)) || (!$is && is_string($value)) || (!$nullable && $value === null)) {
            $this->error(constant('static::INVALID_' . strtoupper(var_export($is, true))));

            return false;
        }

        if ($this->getMinLength() !== null
            && ($length = strlen($value)) < $this->getMinLength()
        ) {
            $this->setLength($length);
            $this->error(static::INVALID_MIN_LENGTH);

            $return = false;
        }

        if ($this->getMaxLength() !== null
            && ($length = strlen($value)) > $this->getMaxLength()
        ) {
            $this->setLength($length);
            $this->error(static::INVALID_MAX_LENGTH);

            $return = false;
        }

        $hexadecimal = (int) ($this->getOptions()['hexadecimal'] ?? 0);

        if ($hexadecimal) {
            if ($hexadecimal === static::OPTION_HEXADECIMAL_CASE_INSENSITIVE
                && !preg_match('#^[a-fA-F0-9]+$#', $value)
            ) {
                $this->error(static::NOT_HEXADECIMAL);

                $return = false;
            } elseif ($hexadecimal === static::OPTION_HEXADECIMAL_CASE_UPPER
                && !preg_match('#^[A-F0-9]+$#', $value)
            ) {
                $this->error(static::NOT_HEXADECIMAL_CASE_UPPER);

                $return = false;
            } elseif ($hexadecimal === static::OPTION_HEXADECIMAL_CASE_LOWER
                && !preg_match('#^[a-f0-9]+$#', $value)
            ) {
                $this->error(static::NOT_HEXADECIMAL_CASE_LOWER);

                $return = false;
            } else if (!in_array(
                $hexadecimal,
                [
                    0,
                    static::OPTION_HEXADECIMAL_CASE_INSENSITIVE,
                    static::OPTION_HEXADECIMAL_CASE_UPPER,
                    static::OPTION_HEXADECIMAL_CASE_LOWER,
                ]
            )) {
                throw new InvalidArgumentException(
                    'Invalid argument \'' . var_export($hexadecimal, true)
                    . '\' for option \'hexadecimal\' in validator \'' . static::class . '\''
                );
            }
        }

        $base64Encoded = (int) ($this->options['base64Encoded'] ?? 0);

        if ($base64Encoded) {
            if ($base64Encoded === static::OPTION_BASE64ENCODED
                && !preg_match('#^[a-fA-F0-9+/=_-]+$#', $value)
            ) {
                $this->error(static::NOT_BASE64ENCODED);

                $return = false;
            } elseif ($base64Encoded === static::OPTION_BASE64ENCODED_URL_SAFE
                && !preg_match('#^[A-F0-9_-]+$#', $value)
            ) {
                $this->error(static::NOT_BASE64ENCODED_URL_SAFE);

                $return = false;
            } elseif ($base64Encoded === static::OPTION_BASE64ENCODED_URL_UNSAFE
                && !preg_match('#^[a-f0-9+/]+$#', $value)
            ) {
                $this->error(static::NOT_BASE64ENCODED_URL_UNSAFE);

                $return = false;
            } else if (!in_array(
                $base64Encoded,
                [
                    0,
                    static::OPTION_BASE64ENCODED,
                    static::OPTION_BASE64ENCODED_URL_SAFE,
                    static::OPTION_BASE64ENCODED_URL_UNSAFE,
                ]
            )) {
                throw new InvalidArgumentException(
                    'Invalid argument \'' . var_export($base64Encoded, true)
                    . '\' for option \'base64Encoded\' in validator \'' . static::class . '\''
                );
            }
        }

        $uri = (int) ($this->getOptions()['uri'] ?? 0);

        if ($uri) {
            if ($uri === static::OPTION_URI_SCHEME_AND_URI) {
                $parsedUri = parse_url($value);

                if (!$parsedUri || empty($parsedUri['scheme'])) {
                    $this->error(static::NOT_URI_SCHEME_AND_URI);

                    $return = false;
                }
            } else if (!in_array(
                $uri,
                [
                    0,
                    static::OPTION_URI_SCHEME_AND_URI,
                ]
            )) {
                throw new InvalidArgumentException(
                    'Invalid argument \'' . var_export($uri, true)
                    . '\' for option \'uri\' in validator \'' . static::class . '\''
                );
            }
        }

        $email = (int) ($this->options['email'] ?? 0);

        if ($email) {
            /** @var EmailAddress $validator */
            $validator = $this->validatorPluginManager->get(
                EmailAddress::class,
                $this->options['emailValidatorOptions'] ?? []
            );

            if (!$validator->isValid($value)) {
                $this->setEmailValidatorMessages(json_encode($validator->getMessages()));
                $this->error(self::NOT_EMAIL_ADDRESS);
                $return = false;
            }
        }

        return $return;
    }

    public function getMinLength(): ?int
    {
        return $this->minLength;
    }

    public function setMinLength(?int $minLength): static
    {
        $this->minLength = $minLength;

        return $this;
    }

    public function getMaxLength(): ?int
    {
        return $this->maxLength;
    }

    public function setMaxLength(?int $maxLength): static
    {
        $this->maxLength = $maxLength;

        return $this;
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function setLength(int $length): static
    {
        $this->length = $length;

        return $this;
    }

    public function getEmailValidatorMessages(): ?string
    {
        return $this->emailValidatorMessages;
    }

    public function setEmailValidatorMessages(?string $emailValidatorMessages): static
    {
        $this->emailValidatorMessages = $emailValidatorMessages;

        return $this;
    }
}
