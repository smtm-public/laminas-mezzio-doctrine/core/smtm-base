<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../../.env.smtm.smtm-base.infrastructure.jwt')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../../',
        '.env.smtm.smtm-base.infrastructure.jwt'
    );
    $dotenv->load();
}

$jwtServices = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BASE_INFRASTRUCTURE_JWT_SERVICES'),
    true,
    flags: JSON_THROW_ON_ERROR
);

return [
    'services' => $jwtServices,
];
