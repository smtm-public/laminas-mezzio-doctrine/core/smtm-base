<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Exception;

use Smtm\Base\Infrastructure\Helper\HttpHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ServiceUnavailableException extends AbstractHttpException
{
    public const MESSAGE = 'Service unavailable';
    public const CODE = 0;
    public const STATUS = HttpHelper::STATUS_CODE_SERVICE_UNAVAILABLE;
    public const TYPE = 'https://example.com/problems/access-denied';

    protected int $status = self::STATUS;
    protected string $title = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_SERVICE_UNAVAILABLE];
    protected string $detail = HttpHelper::STATUS_CODES_DESCRIPTIONS[HttpHelper::STATUS_CODE_SERVICE_UNAVAILABLE];
    protected string $type = self::TYPE;
}
