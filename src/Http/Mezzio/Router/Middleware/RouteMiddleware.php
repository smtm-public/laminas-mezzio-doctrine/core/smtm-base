<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Mezzio\Router\Middleware;

use Smtm\Base\Http\Exception\MethodNotAllowedException;
use Smtm\Base\Http\Exception\NotFoundException;
use Mezzio\Router\RouteResult;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RouteMiddleware implements MiddlewareInterface
{
    public function __construct(
        protected \Mezzio\Router\Middleware\RouteMiddleware $wrappedRouteMiddleware,
        protected \Mezzio\Router\Middleware\MethodNotAllowedMiddleware $wrappedMethodNotAllowedMiddleware,
    ) {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $result = $this->wrappedRouteMiddleware->getRouter()->match($request);

        // Inject the actual route result, as well as individual matched parameters.
        $request = $request->withAttribute(RouteResult::class, $result);

        if ($result->isSuccess()) {
            /** @var mixed $value */
            foreach ($result->getMatchedParams() as $param => $value) {
                $request = $request->withAttribute($param, $value);
            }
        } else {
            if ($result instanceof RouteResult) {
                if ($result->isMethodFailure()) {
                    $allowedMethods = $result->getAllowedMethods();
                    assert(is_array($allowedMethods));

                    throw new MethodNotAllowedException(
                        'Method not allowed. Supported methods for this route are [' . implode(', ', $allowedMethods) . ']'
                    );
                }
            }

            throw new NotFoundException(
                'The requested resource does not exist.'
            );
        }

        return $handler->handle($request);
    }
}
