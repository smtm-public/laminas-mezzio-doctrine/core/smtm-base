<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait CreatedByAwareEntityTrait
{
    protected $created;

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy): static
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
