<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Mezzio\Response;

use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Http\Helper\ErrorResponseHelper;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Service\Log\LoggerCollectionAwareInterface;
use Smtm\Base\Infrastructure\Service\Log\LoggerCollectionAwareTrait;
use Laminas\Diactoros\ServerRequest;
use Laminas\Stratigility\Utils;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ServerRequestErrorResponseGenerator implements ConfigAwareInterface, LoggerCollectionAwareInterface
{

    use ConfigAwareTrait, LoggerCollectionAwareTrait;

    public const TEMPLATE_DEFAULT = 'error::error';

    protected string $stackTraceTemplate = <<<'EOT'
%s raised in file %s line %d:
Message: %s
Stack Trace:
%s

EOT;
    protected string $layout;

    /**
     * @param (callable():ResponseInterface)|ResponseFactoryInterface $responseFactory
     */
    public function __construct(
        protected mixed $responseFactory,
        protected bool $debug = false,
        protected ?TemplateRendererInterface $renderer = null,
        protected string $template = self::TEMPLATE_DEFAULT
    ) {
        $this->responseFactory = is_callable($responseFactory)
            ? new CallableResponseFactoryDecorator($responseFactory)
            : $responseFactory;
    }

    public function __invoke(Throwable $e): ResponseInterface
    {
        return ErrorResponseHelper::createResponseFromThrowable(
            new ServerRequest(),
            $this->responseFactory,
            $e,
            $this->config,
            $this->getLoggerCollection(),
            null
        );

//        return $this->renderer
//            ? ErrorResponseHelper::generateResponse(
//                $this->responseFactory,
//                HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR,
//                'text/html',
//                $this->renderer->render(
//                    $this->template,
//                    [
//                        'error'   => $e,
//                    ]
//                )
//            )
//            : ErrorResponseHelper::generateJsonResponse(
//                $this->responseFactory,
//                HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR,
//                'application/json',
//                $this->renderer->render($this->template, $templateData)
//            );
//
//        $response = $this->responseFactory->createResponse();
//        $response = $response->withStatus(Utils::getStatusCode($e, $response));
//
//        if ($this->renderer) {
//            return $this->prepareTemplatedResponse(
//                $e,
//                $this->renderer,
//                ,
//                $this->debug,
//                $response
//            );
//        }
//
//        return $this->prepareDefaultResponse($e, $this->debug, $response);
    }

    public function getResponseFactory(): ResponseFactoryInterface
    {
        return $this->responseFactory;
    }

    private function prepareTemplatedResponse(
        Throwable $e,
        TemplateRendererInterface $renderer,
        array $templateData,
        bool $debug,
        ResponseInterface $response
    ): ResponseInterface {
        if ($debug) {
            $templateData['error'] = $e;
        }

        $response->getBody()
            ->write($renderer->render($this->template, $templateData));

        return $response;
    }

    private function prepareDefaultResponse(
        Throwable $e,
        bool $debug,
        ResponseInterface $response
    ): ResponseInterface {
        $message = 'An unexpected error occurred';

        if ($debug) {
            $message .= "; stack trace:\n\n" . $this->prepareStackTrace($e);
        }

        $response->getBody()->write($message);

        return $response;
    }

    /**
     * Prepares a stack trace to display.
     */
    private function prepareStackTrace(Throwable $e): string
    {
        $message = '';
        do {
            $message .= sprintf(
                $this->stackTraceTemplate,
                $e::class,
                $e->getFile(),
                $e->getLine(),
                $e->getMessage(),
                $e->getTraceAsString()
            );
        } while ($e = $e->getPrevious());

        return $message;
    }
}
