<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Cache\Storage\Adapter\Factory;

use Laminas\Cache\Service\StorageAdapterFactory;
use Laminas\Cache\Service\StorageAdapterFactoryInterface;
use Laminas\Cache\Service\StoragePluginFactoryInterface;
use Laminas\Cache\Storage\Adapter\Redis;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
final class RedisFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): Redis {
        /** @var StorageAdapterFactory $storageFactory */
        $storageFactory = $container->get(StorageAdapterFactoryInterface::class);

//        // All at once:
//        $cache = $storageFactory->create(
//            'redis',
//            $container->get('config')['laminas']['cache']['storage']['adapter']['redis'],
//            [
//                ['name' => 'serializer'],
//            ]
//        );

        // Alternately, via discrete factory methods:
        /** @var Redis $cache */
        $cache  = $storageFactory->create(
            'redis',
            $container->get('config')['laminas']['cache']['storage']['adapter']['redis']
        );

        $pluginFactory = $container->get(StoragePluginFactoryInterface::class);
        $plugin = $pluginFactory->create('serializer');
        $cache->addPlugin($plugin);

        return $cache;
    }
}
