<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator;

use Laminas\ServiceManager\AbstractPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class HydratorPluginManager extends AbstractPluginManager
{
    protected $instanceOf = DomainObjectHydrator::class;
}
