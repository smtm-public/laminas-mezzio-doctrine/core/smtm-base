<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait IdAwareEntityTrait
{

    use EntityTrait;

    protected array|string|float|int $id;

    public function getId(): array|string|float|int
    {
        return $this->id;
    }

    public function setId(array|string|float|int $id): static
    {
        $this->id = $id;

        return $this;
    }
}
