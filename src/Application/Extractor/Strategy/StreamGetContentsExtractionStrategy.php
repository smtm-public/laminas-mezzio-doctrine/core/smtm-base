<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class StreamGetContentsExtractionStrategy extends AbstractExtractionStrategy
{
    public function extract($value, ?object $object = null): object|array|string|float|int|bool|null
    {
        return stream_get_contents($value);
    }
}
