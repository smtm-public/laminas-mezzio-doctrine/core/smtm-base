<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Laminas\ServiceManager\AbstractPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AbstractMappingFilterPluginManager extends AbstractPluginManager
{
    protected $instanceOf = AbstractMappingFilter::class;
}
