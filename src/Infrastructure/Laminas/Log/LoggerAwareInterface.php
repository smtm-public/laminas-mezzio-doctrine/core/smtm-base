<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Log;

use Laminas\Log\LoggerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface LoggerAwareInterface extends \Laminas\Log\LoggerAwareInterface
{
    public function getLogger(): ?LoggerInterface;
}
