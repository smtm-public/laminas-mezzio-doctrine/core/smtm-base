<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware;

use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareTrait;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ReconfigMiddleware implements
    ApplicationServicePluginManagerAwareInterface,
    InfrastructureServicePluginManagerAwareInterface,
    MiddlewareInterface
{

    use ApplicationServicePluginManagerAwareTrait, InfrastructureServicePluginManagerAwareTrait;

    protected array $applicationServicePluginManagerConfig = [];
    protected array $infrastructureServicePluginManagerConfig = [];

    public function __construct(
        ?array $applicationServicePluginManagerConfig = null,
        ?array $infrastructureServicePluginManagerConfig = null
    ) {
        $this->applicationServicePluginManagerConfig = $applicationServicePluginManagerConfig ?? $this->applicationServicePluginManagerConfig;
        $this->infrastructureServicePluginManagerConfig = $infrastructureServicePluginManagerConfig ?? $this->infrastructureServicePluginManagerConfig;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->applicationServicePluginManager->setAllowOverride(true);
        $this->applicationServicePluginManager->configure($this->applicationServicePluginManagerConfig);
        $this->applicationServicePluginManager->setAllowOverride(false);
        $this->infrastructureServicePluginManager->setAllowOverride(true);
        $this->infrastructureServicePluginManager->configure($this->infrastructureServicePluginManagerConfig);
        $this->infrastructureServicePluginManager->setAllowOverride(false);

        return $handler->handle($request);
    }
}
