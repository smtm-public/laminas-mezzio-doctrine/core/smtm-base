<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\WebDriver;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface WebDriverAwareInterface
{
    public function getWebDriver(): WebDriver;
    public function setWebDriver(WebDriver $webDriver): static;
}
