<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Persistence\Factory;

use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistry;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Doctrine\Common\Proxy\Proxy;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry as DoctrineManagerRegistryInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
final class DoctrineManagerRegistryFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): DoctrineManagerRegistryInterface {
        $config = $container->get('config')['doctrine'];

        if (!isset($config['connections'])) {
            throw new RuntimeException('Missing doctrine connection config');
        }

        /**
         * Already calling this logic in src/Infrastructure/Doctrine/Persistence/Factory/ManagerRegistryFactory.php
         */
        /*
        if (!empty($config['dbal']['types'])) {
            foreach ($config['dbal']['types'] as $name => $class) {
                Type::addType($name, $class);
            }
        }
        */

        /** @var EntityManager[] $entityManagers */
        $entityManagers = [];

        foreach ($config['connections'] as $connectionName => $connectionConfig) {
            $entityManagers[$connectionName] = $container
                ->get(InfrastructureServicePluginManager::class)
                ->build(
                    EntityManagerInterface::class,
                    [
                        'connection' => $connectionConfig,
                        'dbal' => $config['dbal'],
                        'orm' => $config['orm'],
                        'secondLevelCache' => $config['secondLevelCache'],
                        'logging' => $config['logging'],
                    ]
                );
        }

        $managerRegistry = new ManagerRegistry(
            'defaultManagerRegistry',
            array_combine(
                array_keys($entityManagers),
                array_map(
                    fn(EntityManagerInterface $entityManager) => $entityManager->getConnection(),
                    $entityManagers
                )
            ),
            $entityManagers,
            ManagerRegistryInterface::DEFAULT_CONNECTION_NAME,
            ManagerRegistryInterface::DEFAULT_MANAGER_NAME,
            Proxy::class
        );

        return $managerRegistry;
    }
}
