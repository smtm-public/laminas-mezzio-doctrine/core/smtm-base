<?php

declare(strict_types=1);

namespace Smtm\Base\Context\System\Context\EnvInfo\Application\Extractor;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;

class EnvInfoExtractor extends AbstractDomainObjectExtractor
{
    protected array $properties = [
        'gmDate' => null,
        'date' => null,
        'timeZone' => null,
    ];
}
