<?php

declare(strict_types=1);

namespace Smtm\Base\Http\InputFilter;

use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterSpecificationInterface;
use Laminas\Validator\Uuid;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UuidRouteParamRequestValidatingInputFilterCollection extends AbstractRequestValidatingInputFilterCollection
{
    protected array $pathParamsInputFilterSpecification = [
        InputFilterSpecificationInterface::INPUT_COLLECTION => [
            [
                'name' => 'uuid',
                'required' => true,
                'validators' => [
                    [
                        'name' => Uuid::class,
                    ],
                ],
            ],
        ],
    ];
}
