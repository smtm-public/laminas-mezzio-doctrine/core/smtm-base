<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface NotArchivedAwareEntityInterface extends EntityInterface
{
    public function getNotArchived(): ?bool;
    public function isArchived(): bool;
    public function setNotArchived(?bool $notArchived): static;
}
