<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

use DateTime;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ArchivedAtAwareEntityTrait
{
    protected ?DateTime $archivedAt = null;

    public function getArchivedAt(): ?DateTime
    {
        return $this->archivedAt;
    }

    public function setArchivedAt(?DateTime $archivedAt): static
    {
        $this->archivedAt = $archivedAt;

        return $this;
    }
}
