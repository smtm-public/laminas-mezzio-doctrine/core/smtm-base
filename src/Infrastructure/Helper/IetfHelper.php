<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

use JetBrains\PhpStorm\ArrayShape;

/**
 * IETF stands for (the) Internet Engineering Task Force
 * https://www.ietf.org/
 *
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IetfHelper
{
    public const RFC_7239_TITLE = 'Forwarded HTTP Extension';
    public const RFC_7807_TITLE = 'Problem Details for HTTP APIs';
    public const RFC_7807_URN = 'urn:ietf:rfc:7807';

    public static function buildRfc7239ForwardedHeader(
        #[ArrayShape([
            'by' => 'string | null',
            'for' => 'string | null',
            'host' => 'string | null',
            'proto' => 'string | null',
        ])] array $values
    ): string {
        return implode(
            ';',
            array_map(
                fn (string $key, ?string $value) => $key . '=' . $value,
                array_keys($values),
                $values
            )
        );
    }

    #[ArrayShape([
        'by' => 'string | null',
        'for' => 'string | null',
        'host' => 'string | null',
        'proto' => 'string | null',
        'path' => 'string | null',
    ])] public static function parseRfc7239ForwardedHeader(string $value): array
    {
        $forwardedPairs = explode(';', $value);

        $forwardedPairsElements = [];
        $parsed = [];

        foreach ($forwardedPairs as $forwardedPair) {
            $forwardedPairsElements[] = preg_split('#,\s*#', $forwardedPair);
        }

        foreach ($forwardedPairsElements as $forwardedPairsElement) {
            foreach ($forwardedPairsElement as $forwardedPairsElementValue) {
                $keyValue = explode('=', $forwardedPairsElementValue, 2);

                if ($keyValue[0] === '') {
                    continue;
                }

                if (count($keyValue) < 2) {
                    continue;
                }

                $parameterValue = trim($keyValue[1], '"');

                if ($keyValue[0] === 'path') {
                    $parameterValue = urldecode($parameterValue);
                }

                $parsed[strtolower($keyValue[0])] ??= [];
                $parsed[strtolower($keyValue[0])][] = $parameterValue;
            }
        }

        return [
            'by' => $parsed['by'] ?? null,
            'for' => $parsed['for'] ?? null,
            'host' => $parsed['host'] ?? null,
            'proto' => $parsed['proto'] ?? null,
            'path' => $parsed['path'] ?? null,
        ];
    }
}
