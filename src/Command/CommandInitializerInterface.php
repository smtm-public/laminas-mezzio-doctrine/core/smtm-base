<?php

declare(strict_types=1);

namespace Smtm\Base\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface CommandInitializerInterface
{
    public static function initialize(Command $command, InputInterface $input, OutputInterface $output): void;
}
