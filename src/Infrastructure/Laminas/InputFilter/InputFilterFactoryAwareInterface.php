<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\InputFilter;

use Laminas\InputFilter\Factory;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface InputFilterFactoryAwareInterface
{
    public function getInputFilterFactory(): Factory;
    public function setInputFilterFactory(Factory $inputFilterFactory): static;
}
