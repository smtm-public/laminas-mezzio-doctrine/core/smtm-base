<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Laminas\Filter;

use Laminas\Filter\AbstractFilter;
use Laminas\Filter\Exception\InvalidArgumentException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractMappingFilter extends AbstractFilter
{
    public const MAPPING = [];

    /**
     * @var array
     */
    protected $options = [
        'leftToRight' => true,
    ];

    public function __construct($options = [])
    {
        if (!static::isOptions($options)) {
            throw new InvalidArgumentException(static::class . ' filter needs options to work.');
        }

        $this->setOptions($options);
    }

    public static function getMapping(): array
    {
        return static::MAPPING;
    }

    public static function getMappingKeys(): array
    {
        return array_keys(static::MAPPING);
    }

    /**
     * Sets the leftToRight option
     */
    public function setLeftToRight($flag = true): static
    {
        $this->options['leftToRight'] = (bool) $flag;

        return $this;
    }

    /**
     * Whether the value should be resolved from key to value or the other way around
     */
    public function getLeftToRight(): bool
    {
        return $this->options['leftToRight'];
    }

    /**
     * @inheritDoc
     */
    public function filter($value)
    {
        if ($value === null) {
            return null;
        }

        return $this->getLeftToRight() ? static::MAPPING[$value] : array_search($value, static::MAPPING);
    }
}
