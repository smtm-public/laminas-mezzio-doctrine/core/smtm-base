<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

use Smtm\Base\Infrastructure\Exception\RuntimeException;
use ErrorException;
use Exception;
use SimpleXMLElement;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class XmlHelper
{
    public static function sanitizeData(object|array|string|float|int|bool|null $data): array
    {
        // Ensure any objects are flattened to arrays first
        $contentUtf8 = mb_convert_encoding($data, 'UTF-8', 'UTF-8');

        try {
            $contentEncoded = json_encode($contentUtf8, JSON_THROW_ON_ERROR);
        } catch (\Throwable $t) {
            throw $t;
        }

        try {
            $content = json_decode($contentEncoded, true, JSON_THROW_ON_ERROR);
        } catch (\Throwable $t) {
            throw $t;
        }
        // END: Ensure any objects are flattened to arrays first

        array_walk_recursive(
            $content,
            function (&$item) {
                $item = $item ?? '';
            }
        );

        // ensure all keys are valid XML can be json_encoded
        return static::cleanKeys($content);
    }

    /**
     * Ensure all keys in this associative array are valid XML tag names by replacing invalid
     * characters with an `_`.
     */
    public static function cleanKeys(array $input): array
    {
        $return = [];

        foreach ($input as $key => $value) {
            $key = str_replace("\n", '_', (string)$key);
            $startCharacterPattern =
                '[A-Z]|_|[a-z]|[\xC0-\xD6]|[\xD8-\xF6]|[\xF8-\x{2FF}]|[\x{370}-\x{37D}]|[\x{37F}-\x{1FFF}]|'
                . '[\x{200C}-\x{200D}]|[\x{2070}-\x{218F}]|[\x{2C00}-\x{2FEF}]|[\x{3001}-\x{D7FF}]|[\x{F900}-\x{FDCF}]'
                . '|[\x{FDF0}-\x{FFFD}]';
            $characterPattern = $startCharacterPattern . '|\-|\.|[0-9]|\xB7|[\x{300}-\x{36F}]|[\x{203F}-\x{2040}]';

            $key = preg_replace('/(?!' . $characterPattern . ')./u', '_', $key);
            $key = preg_replace('/^(?!' . $startCharacterPattern . ')./u', '_', $key);

            if (is_array($value)) {
                $value = static::cleanKeys($value);
            }
            $return[$key] = $value;
        }

        return $return;
    }

    public static function createReader(string $data, int $options = 0, bool $dataIsUrl = false): SimpleXMLElement
    {
        $priorSetting = libxml_use_internal_errors(true);

        try {
            libxml_clear_errors();
            $xmlElement = new SimpleXMLElement($data, $options, $dataIsUrl);

            if ($error = libxml_get_last_error()) {
                throw new RuntimeException($error->message);
            }
        } catch (Exception $e) {
            throw new ErrorException("Cannot load invalid XML: {$e->getMessage()}");
        } finally {
            libxml_use_internal_errors($priorSetting);
        }

        return $xmlElement;
    }
}
