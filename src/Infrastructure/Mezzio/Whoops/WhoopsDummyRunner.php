<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Mezzio\Whoops;

use Whoops\RunInterface;

class WhoopsDummyRunner implements RunInterface
{

    public function pushHandler($handler)
    {
        // TODO: Implement pushHandler() method.
    }

    public function popHandler()
    {
        // TODO: Implement popHandler() method.
    }

    public function getHandlers()
    {
        // TODO: Implement getHandlers() method.
    }

    public function clearHandlers()
    {
        // TODO: Implement clearHandlers() method.
    }

    public function register()
    {
        // TODO: Implement register() method.
    }

    public function unregister()
    {
        // TODO: Implement unregister() method.
    }

    public function allowQuit($exit = null)
    {
        // TODO: Implement allowQuit() method.
    }

    public function silenceErrorsInPaths($patterns, $levels = 10240)
    {
        // TODO: Implement silenceErrorsInPaths() method.
    }

    public function sendHttpCode($code = null)
    {
        // TODO: Implement sendHttpCode() method.
    }

    public function sendExitCode($code = null)
    {
        // TODO: Implement sendExitCode() method.
    }

    public function writeToOutput($send = null)
    {
        // TODO: Implement writeToOutput() method.
    }

    public function handleException($exception)
    {
        // TODO: Implement handleException() method.
    }

    public function handleError($level, $message, $file = null, $line = null)
    {
        // TODO: Implement handleError() method.
    }

    public function handleShutdown()
    {
        // TODO: Implement handleShutdown() method.
    }

    public function getFrameFilters()
    {
        // TODO: Implement getFrameFilters() method.
    }

    public function clearFrameFilters()
    {
        // TODO: Implement clearFrameFilters() method.
    }

    public function addFrameFilter($filterCallback)
    {
        // TODO: Implement addFrameFilter() method.
    }
}
