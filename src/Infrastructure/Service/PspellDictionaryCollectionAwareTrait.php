<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait PspellDictionaryCollectionAwareTrait
{
    protected array $pspellDictionaryCollection = [];

    public function getPspellDictionaryCollection(): array
    {
        return $this->pspellDictionaryCollection;
    }

    public function setPspellDictionaryCollection(array $pspellDictionaryCollection): static
    {
        $this->pspellDictionaryCollection = $pspellDictionaryCollection;

        return $this;
    }

    public function addPspellDictionary(string $name, \Pspell\Dictionary $pspellDictionary): static
    {
        $this->pspellDictionaryCollection[$name] = $pspellDictionary;

        return $this;
    }

    public function getPspellDictionaryByName(string $name): ?\Pspell\Dictionary
    {
        return $this->pspellDictionaryCollection[$name] ?? null;
    }

    public function removePspellDictionary(string $name): static
    {
        unset($this->pspellDictionaryCollection[$name]);

        return $this;
    }
}
