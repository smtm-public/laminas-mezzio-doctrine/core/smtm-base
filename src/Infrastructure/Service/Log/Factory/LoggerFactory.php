<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Log\Factory;

use Laminas\Log\Logger;
use Laminas\Log\ProcessorPluginManager;
use Laminas\Log\WriterPluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LoggerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, ?array $options = null)
    {
        if (is_subclass_of($name, Logger::class)) {
            $logConfig = array_merge(
                [
                    'writer_plugin_manager' => $container->get(WriterPluginManager::class),
                    'processor_plugin_manager' => $container->get(ProcessorPluginManager::class),
                ],
                $options ?? $container->get('config')['log'] ?? []
            );
            return new $name($logConfig);
        }

        return new $name;
    }
}
