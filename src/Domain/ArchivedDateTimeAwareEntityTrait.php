<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

use DateTime;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ArchivedDateTimeAwareEntityTrait
{
    protected ?DateTime $archived = null;

    public function getArchived(): ?DateTime
    {
        return $this->archived;
    }

    public function setArchived(?DateTime $archived): static
    {
        $this->archived = $archived;

        return $this;
    }
}
