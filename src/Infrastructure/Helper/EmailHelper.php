<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EmailHelper
{
    public static function getEmailUsername(string $email): string
    {
        return substr($email, 0, strpos($email, '@'));
    }

    public static function getEmailDomain(string $email): string
    {
        return substr($email, strpos($email, '@') + 1);
    }
}
