<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService;

use Smtm\Base\Application\Service\ApplicationService\DbPersistStrategy\DbPersistStrategyInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait DbPersistStrategyAwareServiceTrait
{
    protected DbPersistStrategyInterface $dbPersistStrategy;

    public function getDbPersistStrategyDefinition(): array
    {
        return [];
    }

    public function getDbPersistStrategy(): DbPersistStrategyInterface
    {
        return $this->dbPersistStrategy;
    }

    public function setDbPersistStrategy(DbPersistStrategyInterface $dbPersistStrategy): static
    {
        $this->dbPersistStrategy = $dbPersistStrategy;

        return $this;
    }
}
