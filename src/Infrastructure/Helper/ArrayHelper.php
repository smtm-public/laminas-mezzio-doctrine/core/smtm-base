<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

use Smtm\Base\Infrastructure\Exception\RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 * @author Nikolay Nikolov <n.nikolov@smtm.bg>
 */
class ArrayHelper
{
    public const ELEMENT_WITH_PATH_THROW_ON_NON_EXISTING_PATH_KEY = 0b0001;
    public const ELEMENT_WITH_PATH_RETURN_REFERENCE = 0b0010;

    /**
     * Converts a string of integer values definitions into an array of values.
     *
     * - range: [1-1000]
     * - list: [1,10,100,9999]
     * - values can be added and subtracted: [1-1000] - [1,10,100,9999] + [2000-3500]
     */
    public static function parseIntegerValueSetDefinition(string $valueSetDefinition): array
    {
        $values =
            array_map(
                fn(string $item) => preg_split('#\s*\[#', trim($item, '[]')),
                preg_split('#]\s*(?=[-+])#', $valueSetDefinition)
            );

        $values = array_reduce(
            $values,
            function (array $carry , array $item) {
                $idsDefinition = array_pop($item);
                $ids = [];

                if (str_contains($idsDefinition, '-')) {
                    $ids = preg_split('#\s*-\s*#', $idsDefinition);
                    $ids = range($ids[0], $ids[1]);
                } else {
                    $ids = array_map(fn (string $value) => (int) $value, preg_split('#\s*,\s*#', $idsDefinition));
                }

                if (count($item) > 0) {
                    if ($item[0] === '-') {
                        $carry = array_diff($carry, $ids);
                    } else {
                        $carry = array_merge($carry, $ids);
                    }
                } else {
                    $carry = $ids;
                }

                return $carry;
            },
            []
        );

        return $values;
    }

    public static function buildFileNamePartFromIntegerValueSetDefinition(
        string | bool | null $valueSetDefinition,
        string $defaultValue = 'all'
    ): string {
        $logFileNamePart = preg_replace('#[^\w-]#', '_', ($valueSetDefinition ?? '') ?: '');

        if (strlen($logFileNamePart) > 150) {
            $collection =
                $valueSetDefinition
                    ? array_values(ArrayHelper::parseIntegerValueSetDefinition($valueSetDefinition))
                    : $defaultValue;

            if (is_array($collection)) {
                $logFileNamePart = array_values($collection)[0] . '_-_' . array_values($collection)[array_key_last($collection)];
            } else {
                $logFileNamePart = $collection;
            }
        }

        return $logFileNamePart;
    }

    public static function resolveToElementWithPath(
        array $array,
        array|string|null $path,
        string|null $delimiter = '.',
        int $flags = 0b0000
    ): object|array|string|float|int|bool|null {
        ($flags & self::ELEMENT_WITH_PATH_RETURN_REFERENCE) ? $element = $array : $element = &$array;

        if (!empty($path)) {
            $pathKeys = is_string($path) ? explode($delimiter, $path) : $path;

            if (!empty($pathKeys)) {
                if (($flags & self::ELEMENT_WITH_PATH_THROW_ON_NON_EXISTING_PATH_KEY)
                    && ($flags & self::ELEMENT_WITH_PATH_RETURN_REFERENCE)
                ) {
                    do {
                        $currentKey = array_shift($pathKeys);

                        if (!array_key_exists($currentKey, $element)) {
                            throw new RuntimeException('Path key (' . $currentKey . ') does not exist in (' . gettype($element) . ')');
                        }

                        $element = &$element[$currentKey];
                    } while (!empty($pathKeys));
                } elseif (($flags & self::ELEMENT_WITH_PATH_THROW_ON_NON_EXISTING_PATH_KEY)
                    && !($flags & self::ELEMENT_WITH_PATH_RETURN_REFERENCE)
                ) {
                    do {
                        $currentKey = array_shift($pathKeys);

                        if (!array_key_exists($currentKey, $element)) {
                            throw new RuntimeException('Path key (' . $currentKey . ') does not exist in (' . gettype($element) . ')');
                        }

                        $element = $element[$currentKey];
                    } while (!empty($pathKeys));
                } elseif (!($flags & self::ELEMENT_WITH_PATH_THROW_ON_NON_EXISTING_PATH_KEY)
                    && ($flags & self::ELEMENT_WITH_PATH_RETURN_REFERENCE)
                ) {
                    do {
                        $currentKey = array_shift($pathKeys);

                        if (array_key_exists($currentKey, $element)) {
                            $element = &$element[$currentKey];
                        } else {
                            $element = null;

                            break;
                        }
                    } while (!empty($pathKeys));
                } else {
                    do {
                        $currentKey = array_shift($pathKeys);

                        if (array_key_exists($currentKey, $element)) {
                            $element = $element[$currentKey];
                        } else {
                            $element = null;

                            break;
                        }
                    } while (!empty($pathKeys));
                }
            }
        }

        return $element;
    }

    public static function replaceElementWithPath(
        array &$array,
        object|array|string|float|int|bool|null $value,
        array|string|null $path,
        string|null $delimiter = '.',
        int $flags = 0b0000
    ): int {
        $element = &$array;

        if (!empty($path)) {
            $pathKeys = is_string($path) ? explode($delimiter, $path) : $path;

            if (!empty($pathKeys)) {
                if (($flags & self::ELEMENT_WITH_PATH_THROW_ON_NON_EXISTING_PATH_KEY)) {
                    do {
                        $currentKey = array_shift($pathKeys);

                        if (!array_key_exists($currentKey, $element)) {
                            throw new RuntimeException('Path key (' . $currentKey . ') does not exist in (' . gettype($element) . ')');
                        }

                        $element = &$element[$currentKey];
                    } while (count($pathKeys) > 1);

                    $element[array_shift($pathKeys)] = $value;
                } elseif (!($flags & self::ELEMENT_WITH_PATH_THROW_ON_NON_EXISTING_PATH_KEY)) {
                    do {
                        $currentKey = array_shift($pathKeys);

                        if (array_key_exists($currentKey, $element)) {
                            $element = &$element[$currentKey];
                        } else {
                            $element = null;

                            return 1;
                        }
                    } while (count($pathKeys) > 1);

                    $element[array_shift($pathKeys)] = $value;
                }
            }
        }

        return 0;
    }

    public static function convertKeyLetterCase(
        array $array,
        string|array $letterCaseCollection
    ): array {
        return array_combine(
            array_map(
                fn (string $value) => LetterCaseHelper::format($value, $letterCaseCollection),
                array_keys($array)
            ),
            array_values($array)
        );
    }

    public static function convertValueLetterCase(
        array $array,
        string|array $letterCaseCollection
    ): array {
        return array_combine(
            array_keys($array),
            array_map(
                fn (mixed $value) => is_string($value) ? LetterCaseHelper::format($value, $letterCaseCollection) : $value,
                array_values($array)
            )
        );
    }

    public static function get(array $array, string $key, $default = null)
    {
        if (!is_array($array)) {
            return $default;
        }

        if (is_null($key)) {
            return $array;
        }

        if (array_key_exists($key, $array)) {
            return $array[$key];
        }

        foreach (explode('.', $key) as $segment) {
            if (is_array($array) && array_key_exists($segment, $array)) {
                $array = $array[$segment];
            } else {
                return $default;
            }
        }

        return $array;
    }

    public static function has(array $array, array|string $keys): bool
    {
        if (is_null($keys)) {
            return false;
        }

        $keys = (array) $keys;

        if (!$array) {
            return false;
        }

        if ($keys === []) {
            return false;
        }

        foreach ($keys as $key) {
            $subKeyArray = $array;

            if (array_key_exists($key, $array)) {
                continue;
            }

            foreach (explode('.', $key) as $segment) {
                if (is_array($subKeyArray) && array_key_exists($segment, $subKeyArray)) {
                    $subKeyArray = $subKeyArray[$segment];
                } else {
                    return false;
                }
            }
        }

        return true;
    }

    public static function arrayDepth(array $array): int
    {
        $maxDepth = 1;

        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = self::arrayDepth($value) + 1;

                if ($depth > $maxDepth) {
                    $maxDepth = $depth;
                }
            }
        }

        return $maxDepth;
    }

    public static function arrayAssocDiff(array $compareFrom, array $compareTo): array
    {
        return array_udiff_uassoc(
            $compareFrom,
            $compareTo,
            function ($key1, $key2) {
                if (is_array($key1) && is_array($key2)) {
                    $key1 = json_encode($key1);
                    $key2 = json_encode($key2);
                }

                if ($key1 === $key2) {
                    return 0;
                }

                return $key1 > $key2 ? 1 : -1;
            },
            function ($value1, $value2) {
                if ($value1 === $value2) {
                    return 0;
                }

                return $value1 > $value2 ? 1 : -1;
            }
        );
    }

    public static function filter(
        array $array,
        callable $callback = null,
        int $mode = 0,
        bool $trimStringElements = false
    ): array {
        if ($trimStringElements) {
            $array = array_map(fn(mixed $element) => is_string($element) ? trim($element) : $element, $array);
        }

        return array_filter($array, $callback, $mode);
    }

    public static function implodeRecursive(
        array|string $separator,
        array $array = null,
        string $prependSeparator = null,
        int $currentLevel = 0,
        string $prepend = null
    ): string {
        $separatorToUse = $separator;
        $arrayToProcess = $array;
        $prependSeparatorToUse = $prependSeparator === null ? '' : $prependSeparator;

        if ($array === null) {
            $separatorToUse = '';
            $arrayToProcess = $separator;
        }

        $currentLevelItems = [];

        foreach ($arrayToProcess as $key => $element) {
            if (is_array($element)) {
                $prependToUse = is_string($key) ? $key : null;

                if ($prepend !== null) {
                    $prependToUse = $prepend . $prependSeparator . ($prependToUse ?? '');
                }

                $currentLevelItems[] = static::implodeRecursive(
                    $separator,
                    $element,
                    $prependSeparator,
                    $currentLevel + 1,
                    $prependToUse
                );
            } elseif (is_numeric($key) && is_string($element)) {
                $currentLevelItems[] = ($prepend === null ? '' : $prepend . $prependSeparator) . $element;
            } elseif (is_string($key) && empty($element)) {
                $currentLevelItems[] = ($prepend === null ? '' : $prepend . $prependSeparator) . $key;
            }
        }

        return implode($separator, $currentLevelItems);
    }

    public static function buildQuery(
        array|string $separator,
        array $array,
        int $currentLevel = 0,
        string $currentPath = ''
    ): string {
        $currentLevelItems = [];

        foreach ($array as $key => $element) {
            $urlEncodedKey = urlencode((string) $key);
            $currentPathKey = $currentPath . ($currentLevel === 0 ? $urlEncodedKey : '[' . $urlEncodedKey . ']');

            if (is_array($element)) {
                $currentLevelItems[] = static::buildQuery(
                    $separator,
                    $element,
                    $currentLevel + 1,
                    $currentPathKey
                );
            } else {
                $currentLevelItems[] = $currentPathKey . '=' . urlencode($element);
            }
        }

        return implode($separator, $currentLevelItems);
    }

    public static function filterRecursive(array $array, callable $callback): array
    {
        array_walk(
            $array,
            fn (mixed &$element, string|int $key) =>
                $element = is_array($element)
                    ? static::filterRecursive($element, $callback)
                    : $element
        );

        return array_filter($array, $callback);
    }

    public static function mapKeys(callable $callable, array $array): array
    {
        return array_combine(
            array_map($callable, array_keys($array)),
            $array
        );
    }
}
