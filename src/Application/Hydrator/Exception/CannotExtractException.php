<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Exception;

use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CannotExtractException extends RuntimeException
{

}
