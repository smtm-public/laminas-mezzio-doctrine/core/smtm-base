<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\DbService;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use JetBrains\PhpStorm\Pure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AbstractConfigAwareDbService extends AbstractDbService implements ConfigAwareInterface
{

    use ConfigAwareTrait;

    #[Pure] public function __construct(
        ApplicationServicePluginManager $applicationServicePluginManager,
        InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected HydratorPluginManager $hydratorPluginManager,
        protected ExtractorPluginManager $extractorPluginManager,
        protected \Laminas\Hydrator\HydratorPluginManager $baseHydratorPluginManager,
        protected ManagerRegistryInterface $entityManagerRegistry,
        protected array $config
    ) {
        parent::__construct(
            $applicationServicePluginManager,
            $infrastructureServicePluginManager,
            $hydratorPluginManager,
            $extractorPluginManager,
            $baseHydratorPluginManager,
            $entityManagerRegistry
        );
    }
}
