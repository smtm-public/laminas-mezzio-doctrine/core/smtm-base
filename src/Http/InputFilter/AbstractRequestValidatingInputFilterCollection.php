<?php

declare(strict_types=1);

namespace Smtm\Base\Http\InputFilter;

use Smtm\Base\Infrastructure\Laminas\InputFilter\Factory\InputFilterFactory;
use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterSpecificationInterface;
use Smtm\Base\Infrastructure\Laminas\InputFilter\OptionalInputsInputFilter;
use Laminas\InputFilter\InputFilterInterface;
use Mezzio\Router\RouteResult;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractRequestValidatingInputFilterCollection
{
    public const KEY_HEADERS = 'headers';
    public const KEY_PATH_PARAMS = 'path';
    public const KEY_QUERY_PARAMS = 'query';
    public const KEY_PARSED_BODY = 'body';

    public const KEY_RESULTS_IS_VALID = 'isValid';
    public const KEY_RESULTS_MESSAGES = 'messages';

    public const DEFAULT_TYPE = OptionalInputsInputFilter::class;

    public InputFilterInterface $headersInputFilter;
    public InputFilterInterface $pathParamsInputFilter;
    public InputFilterInterface $queryParamsInputFilter;
    public InputFilterInterface $parsedBodyInputFilter;

    protected bool $isInitialized = false;

    protected array $headersInputFilterSpecification = [
        InputFilterSpecificationInterface::TYPE => self::DEFAULT_TYPE,
    ];
    protected array $pathParamsInputFilterSpecification = [
        InputFilterSpecificationInterface::TYPE => self::DEFAULT_TYPE,
    ];
    protected array $queryParamsInputFilterSpecification = [
        InputFilterSpecificationInterface::TYPE => self::DEFAULT_TYPE,
    ];
    protected array $parsedBodyInputFilterSpecification = [
        InputFilterSpecificationInterface::TYPE => self::DEFAULT_TYPE,
    ];

    public function __construct(protected InputFilterFactory $inputFilterFactory)
    {

    }

    protected function initialize(): void
    {
        if ($this->isInitialized) {
            return;
        }

        $this->headersInputFilter = $this->inputFilterFactory->createInputFilter(
            $this->headersInputFilterSpecification + [
                InputFilterSpecificationInterface::TYPE => self::DEFAULT_TYPE
            ]
        );

        $this->pathParamsInputFilter = $this->inputFilterFactory->createInputFilter(
            $this->pathParamsInputFilterSpecification + [
                InputFilterSpecificationInterface::TYPE => self::DEFAULT_TYPE
            ]
        );

        $this->queryParamsInputFilter = $this->inputFilterFactory->createInputFilter(
            $this->queryParamsInputFilterSpecification + [
                InputFilterSpecificationInterface::TYPE => self::DEFAULT_TYPE
            ]
        );

        $this->parsedBodyInputFilter = $this->inputFilterFactory->createInputFilter(
            $this->parsedBodyInputFilterSpecification + [
                InputFilterSpecificationInterface::TYPE => self::DEFAULT_TYPE
            ]
        );

        $this->isInitialized = true;
    }

    public function setDataFromRequest(ServerRequestInterface $request): AbstractRequestValidatingInputFilterCollection
    {
        $this->initialize();

        $this->headersInputFilter->setData($request->getHeaders());
        $this->pathParamsInputFilter->setData($request->getAttribute(RouteResult::class)->getMatchedParams());
        $this->queryParamsInputFilter->setData($request->getQueryParams());
        $this->parsedBodyInputFilter->setData(
            array_replace_recursive(
                $request->getParsedBody(),
                $request->getUploadedFiles()
            )
        );

        return $this;
    }

    /**
     * @return array
     */
    public function validate(): array
    {
        $this->initialize();

        return [
            static::KEY_HEADERS => [
                static::KEY_RESULTS_IS_VALID => $this->headersInputFilter->isValid(),
                static::KEY_RESULTS_MESSAGES => $this->headersInputFilter->getMessages(),
            ],
            static::KEY_PATH_PARAMS => [
                static::KEY_RESULTS_IS_VALID => $this->pathParamsInputFilter->isValid(),
                static::KEY_RESULTS_MESSAGES => $this->pathParamsInputFilter->getMessages(),
            ],
            static::KEY_QUERY_PARAMS => [
                static::KEY_RESULTS_IS_VALID => $this->queryParamsInputFilter->isValid(),
                static::KEY_RESULTS_MESSAGES => $this->queryParamsInputFilter->getMessages(),
            ],
            static::KEY_PARSED_BODY => [
                static::KEY_RESULTS_IS_VALID => $this->parsedBodyInputFilter->isValid(),
                static::KEY_RESULTS_MESSAGES => $this->parsedBodyInputFilter->getMessages(),
            ]
        ];
    }
}
