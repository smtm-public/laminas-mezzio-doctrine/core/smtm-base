<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor;

use Smtm\Base\Application\Extractor\Exception\ArgumentIsNotAbstractExtractionNamingStrategyException;
use Smtm\Base\Application\Extractor\Exception\ArgumentIsNotAbstractExtractionStrategyException;
use Smtm\Base\Application\Extractor\Exception\CannotHydrateException;
use Smtm\Base\Application\Extractor\NamingStrategy\AbstractExtractionNamingStrategy;
use Smtm\Base\Application\Extractor\NamingStrategy\ExtractionNamingStrategyPluginManager;
use Smtm\Base\Application\Extractor\Strategy\AbstractExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\AbstractExtractorPluginManagerAwareExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\ExtractionStrategyPluginManager;
use Smtm\Base\Infrastructure\Helper\ArrayHelper;
use Smtm\Base\OptionsAwareInterface;
use Laminas\Hydrator;
use Laminas\Hydrator\Filter\FilterInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractDomainObjectExtractor extends Hydrator\AbstractHydrator
{
    protected ?array $namingStrategyDefinition = null;
    protected array $properties = [];
    /**
     * @inheritDoc
     *
     * @var AbstractExtractionStrategy[]
     */
    protected $strategies = [];
    /**
     * @inheritDoc
     *
     * @var null|AbstractExtractionNamingStrategy
     */
    protected $namingStrategy;
    protected array $extractionPropertiesCache = [];
    protected FilterInterface $optionalParametersFilter;

    protected string $includes = '';

    public function __construct(
        protected ExtractorPluginManager $extractorPluginManager,
        protected ExtractionNamingStrategyPluginManager $extractionNamingStrategyPluginManager,
        protected ExtractionStrategyPluginManager $extractionStrategyPluginManager,
        array $options = null
    ) {
        if ($options === null) {
            $options = [];
        }

        $this->optionalParametersFilter = new Hydrator\Filter\OptionalParametersFilter();

        $compositeFilter = $this->getCompositeFilter();
        $compositeFilter->addFilter('is', new Hydrator\Filter\IsFilter());
        $compositeFilter->addFilter('has', new Hydrator\Filter\HasFilter());
        $compositeFilter->addFilter('get', new Hydrator\Filter\GetFilter());

        if (isset($options['filters'])) {
            foreach ($options['filters'] as $filterSpec) {
                $this->addFilter($filterSpec['name'], $filterSpec['filter']);
            }
        }

        if (isset($options['enabledProperties'])) {
            $this->properties = array_intersect_key(
                $this->properties,
                array_combine($options['enabledProperties'], $options['enabledProperties'])
            );
        }

        if (isset($options['includes'])) {
            $this->setIncludes($options['includes']);
        }

        if ($this->namingStrategyDefinition !== null) {
            /** @var AbstractExtractionNamingStrategy $namingStrategy */
            $namingStrategy = $this->extractionNamingStrategyPluginManager->get($this->namingStrategyDefinition['name']);
            if (!empty($this->namingStrategyDefinition['options'])) {
                $namingStrategy->setOptions($this->namingStrategyDefinition['options']);
            }
            $this->setNamingStrategy($namingStrategy);
        }

        foreach ($this->properties as $propertyName => $propertyExtractionDefinition) {
            $usedPropertyName = $propertyName;

            if (isset($propertyExtractionDefinition['property'])) {
                $usedPropertyName = $propertyExtractionDefinition['property'];
            }

            $this->extractionPropertiesCache[$propertyName] = $usedPropertyName;

            if (is_array($propertyExtractionDefinition)) {
                if (!empty($propertyExtractionDefinition['strategy'])) {
                    /** @var AbstractExtractionStrategy $strategy */
                    $strategy = $this->extractionStrategyPluginManager->get(
                        $propertyExtractionDefinition['strategy']['name']
                    );

                    if (!empty($propertyExtractionDefinition['strategy']['options'])) {
                        $strategy->setOptions($propertyExtractionDefinition['strategy']['options']);
                    }

                    $this->addStrategy(
                        $propertyName,
                        $strategy
                    );
                }
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function setNamingStrategy(Hydrator\NamingStrategy\NamingStrategyInterface $strategy): void
    {
        if (!$strategy instanceof AbstractExtractionNamingStrategy) {
            throw new ArgumentIsNotAbstractExtractionNamingStrategyException(
                'The provided naming strategy of type ' . get_class($strategy)
                . ' is not a ' . AbstractExtractionNamingStrategy::class
            );
        }

        parent::setNamingStrategy($strategy);
    }

    /**
     * @inheritDoc
     */
    public function getNamingStrategy(): AbstractExtractionNamingStrategy
    {
        return parent::getNamingStrategy();
    }

    /**
     * @inheritDoc
     */
    public function addStrategy(string $name, Hydrator\Strategy\StrategyInterface $strategy): void
    {
        if (!$strategy instanceof AbstractExtractionStrategy) {
            throw new ArgumentIsNotAbstractExtractionStrategyException(
                'The provided strategy of type ' . get_class($strategy)
                . ' is not a ' . AbstractExtractionStrategy::class
            );
        }

        parent::addStrategy($name, $strategy);
    }

    /**
     * @inheritDoc
     */
    public function getStrategy(string $name): AbstractExtractionStrategy
    {
        return parent::getStrategy($name);
    }

    /**
     * This class cannot hydrate
     */
    public function hydrate(array $data, object $object): void
    {
        throw new CannotHydrateException('The ' . __CLASS__ .  ' cannot hydrate.');
    }

    public function extractValue(string $name, mixed $value, ?object $object = null, ?array $options = null)
    {
        $strategy = $this->hasStrategy($name) ? $this->getStrategy($name) : null;

        if ($strategy instanceof AbstractExtractorPluginManagerAwareExtractionStrategy) {
            $strategy->mergeOptions($options, OptionsAwareInterface::OPTION_MERGE_STRATEGY_REPLACE_RECURSIVE_IF_NOT_NULL);
        }

        return $strategy !== null
            ? $strategy->extract($value, $object)
            : $value;
    }

    public function getIncludes(): string
    {
        return $this->includes;
    }

    public function setIncludes(array|string $includes): static
    {
        $this->includes = is_array($includes) ? implode(',', $includes) : $includes;

        return $this;
    }

    /**
     * Determines the getters/setters of the given $object.
     * Extract values from the given object using the determined class methods
     *
     * {@inheritDoc}
     */
    public function extract(object $object): array
    {
        $filter  = $this->initCompositeFilter($object);

        $includes = [];
        $filteredIncludes = ArrayHelper::filter(
            explode(',', $this->includes),
            trimStringElements: true
        );

        foreach ($filteredIncludes as $include) {
            $includeComponents = explode('.', $include);
            $firstIncludeComponent = array_shift($includeComponents);
            $includes[$firstIncludeComponent] ??= [];
            $includes[$firstIncludeComponent][] = implode('.', $includeComponents);
        }

        foreach ($includes as $key => $include) {
            $includes[$key] = implode(',', $include);
        }

        $values = [];

        foreach ($this->extractionPropertiesCache as $usedPropertyName => $attributeName) {
            $include = $includes[$attributeName] ?? null;

            if (isset($this->properties[$attributeName])
                && !empty($this->properties[$attributeName]['include'])
                && ($include === null)
            ) {
                continue;
            }

            $usedObject = $object;
            $usedAttributeName = $attributeName;

            if (str_contains($usedAttributeName, '.')) {
                while(($dotPos = strpos($usedAttributeName, '.')) !== false) {
                    $methodName = 'get' . ucfirst(substr($usedAttributeName, 0, $dotPos));
                    $usedObject = $usedObject->$methodName();
                    $usedAttributeName = substr($usedAttributeName, $dotPos + 1);
                }
            }

            $objectClass = $usedObject::class;
            $isAnonymous = str_contains($objectClass, '@anonymous');

            if ($isAnonymous) {
                $objectClass = spl_object_hash($usedObject);
            }

            $methodName = 'get' . ucfirst($usedAttributeName);
            $methodFqn = $isAnonymous
                ? $methodName
                : $objectClass . '::' . $methodName;

            if (
                ! $filter->filter($methodFqn, $isAnonymous ? $usedObject : null)
                || ! $this->optionalParametersFilter->filter($methodFqn, $isAnonymous ? $usedObject : null)
            ) {
                continue;
            }

            $value = $usedObject->$methodName();
            $realAttributeName          = $this->extractName($usedPropertyName, $object);
            $values[$realAttributeName] = $this->extractValue(
                $usedPropertyName,
                $value,
                $object,
                [
                    AbstractExtractorPluginManagerAwareExtractionStrategy::OPTION_KEY_EXTRACTOR_OPTIONS => [
                        AbstractExtractorPluginManagerAwareExtractionStrategy::OPTION_KEY_EXTRACTOR_OPTIONS_KEY_INCLUDES =>
                            $include,
                    ],
                ]
            );
        }

        return $values;
    }

    protected function initCompositeFilter(object $object): Hydrator\Filter\FilterComposite
    {
        if ($object instanceof Hydrator\Filter\FilterProviderInterface) {
            return new Hydrator\Filter\FilterComposite(
                [$object->getFilter()],
                [new Hydrator\Filter\MethodMatchFilter('getFilter')]
            );
        }

        return $this->getCompositeFilter();
    }
}
