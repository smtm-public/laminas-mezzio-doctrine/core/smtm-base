<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait TablePopulatingMigrationTrait
{
    protected array $models = [];

    public function populateTables(Connection $connection, Schema $schema): void
    {
        foreach ($this->models as $table => $models) {
            foreach ($models as $data) {
                foreach ($data as &$datum) {
                    if (is_array($datum)) {
                        $where =
                            implode(
                                ' AND ',
                                array_map(
                                    fn($key, $value) =>
                                        $connection->quoteIdentifier($key) . '=' . $connection->quote($value),
                                    array_keys($datum['where']),
                                    $datum['where']
                                )
                            );
                        $sql =
                            'SELECT '
                            . $connection->quoteIdentifier($datum['column'])
                            . ' FROM '
                            . $connection->quoteIdentifier($datum['table'])
                            . ' WHERE '
                            . $where
                        ;
                        $value = $this->connection->fetchOne($sql);
                        $this->abortIf(
                            $value === null,
                            $datum['column']
                                . ' from ' . $datum['table']
                            . ' where (' . $where . ') not found.'
                        );
                        $datum = $value;
                    }
                }

                $connection->insert($table, $data);
            }
        }
    }
}
