<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\Factory;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ApplicationServiceAbstractFactory implements AbstractFactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            applicationServicePluginManager: $container->get(ApplicationServicePluginManager::class),
            infrastructureServicePluginManager: $container->get(InfrastructureServicePluginManager::class),
            hydratorPluginManager: $container->get(HydratorPluginManager::class),
            extractorPluginManager: $container->get(ExtractorPluginManager::class),
            baseHydratorPluginManager: $container->get(\Laminas\Hydrator\HydratorPluginManager::class)
        );
    }

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return is_subclass_of($requestedName, AbstractApplicationService::class);
    }
}
