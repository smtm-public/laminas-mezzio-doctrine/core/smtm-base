<?php

declare(strict_types=1);

namespace Smtm\Base;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ConfigAwareInterface
{
    public function getConfig(): array;
    public function setConfig(array $config): static;
}
