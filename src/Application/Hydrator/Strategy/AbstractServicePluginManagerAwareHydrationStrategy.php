<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Hydrator\Strategy;

use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManagerAwareTrait;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractServicePluginManagerAwareHydrationStrategy extends AbstractHydrationStrategy
    implements ApplicationServicePluginManagerAwareInterface, InfrastructureServicePluginManagerAwareInterface
{
    use ApplicationServicePluginManagerAwareTrait,
        InfrastructureServicePluginManagerAwareTrait;
}
