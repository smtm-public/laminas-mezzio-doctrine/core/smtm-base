<?php

declare(strict_types=1);

namespace Smtm\Base\Command;

use Smtm\Base\Command\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * Example:
 * vendor/bin/symfony-console base:throw-exception -vvv
 */
class ThrowException extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('base:throw-exception')
            // the short description shown while running "php bin/console list"
            ->setDescription(
                'Smtm - Base - Throw Exception'
            )
            // the full command description shown when running the command with
            // the "--help" option
            //->setHelp('Could write some useful text here')
        ;
    }

    public function __construct(string $name = null)
    {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        throw new RuntimeException('Test command exception');

        return Command::SUCCESS;
    }
}
