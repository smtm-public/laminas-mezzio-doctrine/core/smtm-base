<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService;

use Smtm\Base\ConfigAwareInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface DbConfigurableEntityManagerNameServiceInterface extends ConfigAwareInterface
{

}
