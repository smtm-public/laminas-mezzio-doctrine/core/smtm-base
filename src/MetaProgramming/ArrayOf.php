<?php

declare(strict_types=1);

namespace Smtm\Base\MetaProgramming;

use Attribute;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
#[Attribute(Attribute::TARGET_FUNCTION | Attribute::TARGET_METHOD | Attribute::TARGET_PARAMETER | Attribute::TARGET_PROPERTY)]
class ArrayOf
{
    public function __construct(
        protected string $type
    ) {

    }
}
