<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Factory;

use Smtm\Base\Infrastructure\Service\AbstractRemoteServiceConnector;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use GuzzleHttp\ClientInterface;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RemoteServiceConnectorAbstractFactory implements AbstractFactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get(InfrastructureServicePluginManager::class),
            $container->get(InfrastructureServicePluginManager::class)->get(ClientInterface::class),
            $options
        );
    }

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return is_subclass_of($requestedName, AbstractRemoteServiceConnector::class);
    }
}
