<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface UuidAwareEntityInterface extends EntityInterface
{
    public function getUuid(): string;
    public function setUuid(string $uuid): static;
}
