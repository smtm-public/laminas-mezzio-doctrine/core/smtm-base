<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Persistence;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ManagerRegistryAwareTrait
{
    protected ManagerRegistryInterface $managerRegistry;

    public function getManagerRegistry(): ManagerRegistryInterface
    {
        return $this->managerRegistry;
    }

    public function setManagerRegistry(ManagerRegistryInterface $managerRegistry): static
    {
        $this->managerRegistry = $managerRegistry;

        return $this;
    }
}
