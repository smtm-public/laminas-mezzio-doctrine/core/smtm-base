<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Helper;

use Smtm\Base\Infrastructure\Exception\InvalidArgumentException;
use http\QueryString;
use http\Url;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class HttpHelper
{
    public const METHOD_CONNECT = 'CONNECT';
    public const METHOD_DELETE = 'DELETE';
    public const METHOD_GET = 'GET';
    public const METHOD_HEAD = 'HEAD';
    public const METHOD_OPTIONS = 'OPTIONS';
    public const METHOD_PATCH = 'PATCH';
    public const METHOD_POST = 'POST';
    public const METHOD_PUT = 'PUT';
    public const METHOD_TRACE = 'TRACE';
    public const METHODS = [
        self::METHOD_CONNECT,
        self::METHOD_DELETE,
        self::METHOD_GET,
        self::METHOD_HEAD,
        self::METHOD_OPTIONS,
        self::METHOD_PATCH,
        self::METHOD_POST,
        self::METHOD_PUT,
        self::METHOD_TRACE,
    ];

    public const STATUS_CODE_SWITCHING_PROTOCOLS = 101;
    public const STATUS_CODE_PROCESSING = 102;
    public const STATUS_CODE_OK = 200;
    public const STATUS_CODE_CREATED = 201;
    public const STATUS_CODE_ACCEPTED = 202;
    public const STATUS_CODE_NON_AUTHORITATIVE_INFORMATION = 203;
    public const STATUS_CODE_NO_CONTENT = 204;
    public const STATUS_CODE_RESET_CONTENT = 205;
    public const STATUS_CODE_PARTIAL_CONTENT = 206;
    public const STATUS_CODE_MULTI_STATUS = 207;
    public const STATUS_CODE_ALREADY_REPORTED = 208;
    public const STATUS_CODE_IM_USED = 226;
    public const STATUS_CODE_MULTIPLE_CHOICES = 300;
    public const STATUS_CODE_MOVED_PERMANENTLY = 301;
    public const STATUS_CODE_FOUND = 302;
    public const STATUS_CODE_SEE_OTHER = 303;
    public const STATUS_CODE_NOT_MODIFIED = 304;
    public const STATUS_CODE_USE_PROXY = 305;
    public const STATUS_CODE_RESERVED = 306;
    public const STATUS_CODE_TEMPORARY_REDIRECT = 307;
    public const STATUS_CODE_PERMANENTLY_REDIRECT = 308;
    public const STATUS_CODE_BAD_REQUEST = 400;
    public const STATUS_CODE_UNAUTHORIZED = 401;
    public const STATUS_CODE_PAYMENT_REQUIRED = 402;
    public const STATUS_CODE_FORBIDDEN = 403;
    public const STATUS_CODE_NOT_FOUND = 404;
    public const STATUS_CODE_METHOD_NOT_ALLOWED = 405;
    public const STATUS_CODE_NOT_ACCEPTABLE = 406;
    public const STATUS_CODE_PROXY_AUTHENTICATION_REQUIRED = 407;
    public const STATUS_CODE_REQUEST_TIMEOUT = 408;
    public const STATUS_CODE_CONFLICT = 409;
    public const STATUS_CODE_GONE = 410;
    public const STATUS_CODE_LENGTH_REQUIRED = 411;
    public const STATUS_CODE_PRECONDITION_FAILED = 412;
    public const STATUS_CODE_REQUEST_ENTITY_TOO_LARGE = 413;
    public const STATUS_CODE_REQUEST_URI_TOO_LONG = 414;
    public const STATUS_CODE_UNSUPPORTED_MEDIA_TYPE = 415;
    public const STATUS_CODE_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
    public const STATUS_CODE_EXPECTATION_FAILED = 417;
    public const STATUS_CODE_I_AM_A_TEAPOT = 418;
    public const STATUS_CODE_UNPROCESSABLE_ENTITY = 422;
    public const STATUS_CODE_LOCKED = 423;
    public const STATUS_CODE_FAILED_DEPENDENCY = 424;
    public const STATUS_CODE_RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL = 425;
    public const STATUS_CODE_UPGRADE_REQUIRED = 426;
    public const STATUS_CODE_PRECONDITION_REQUIRED = 428;
    public const STATUS_CODE_TOO_MANY_REQUESTS = 429;
    public const STATUS_CODE_REQUEST_HEADER_FIELDS_TOO_LARGE = 431;
    public const STATUS_CODE_INTERNAL_SERVER_ERROR = 500;
    public const STATUS_CODE_NOT_IMPLEMENTED = 501;
    public const STATUS_CODE_BAD_GATEWAY = 502;
    public const STATUS_CODE_SERVICE_UNAVAILABLE = 503;
    public const STATUS_CODE_GATEWAY_TIMEOUT = 504;
    public const STATUS_CODE_VERSION_NOT_SUPPORTED = 505;
    public const STATUS_CODE_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL = 506;
    public const STATUS_CODE_INSUFFICIENT_STORAGE = 507;
    public const STATUS_CODE_LOOP_DETECTED = 508;
    public const STATUS_CODE_NOT_EXTENDED = 510;
    public const STATUS_CODE_NETWORK_AUTHENTICATION_REQUIRED = 511;
    public const STATUS_CODES = [
        self::STATUS_CODE_SWITCHING_PROTOCOLS,
        self::STATUS_CODE_PROCESSING,
        self::STATUS_CODE_OK,
        self::STATUS_CODE_CREATED,
        self::STATUS_CODE_ACCEPTED,
        self::STATUS_CODE_NON_AUTHORITATIVE_INFORMATION,
        self::STATUS_CODE_NO_CONTENT,
        self::STATUS_CODE_RESET_CONTENT,
        self::STATUS_CODE_PARTIAL_CONTENT,
        self::STATUS_CODE_MULTI_STATUS,
        self::STATUS_CODE_ALREADY_REPORTED,
        self::STATUS_CODE_IM_USED,
        self::STATUS_CODE_MULTIPLE_CHOICES,
        self::STATUS_CODE_MOVED_PERMANENTLY,
        self::STATUS_CODE_FOUND,
        self::STATUS_CODE_SEE_OTHER,
        self::STATUS_CODE_NOT_MODIFIED,
        self::STATUS_CODE_USE_PROXY,
        self::STATUS_CODE_RESERVED,
        self::STATUS_CODE_TEMPORARY_REDIRECT,
        self::STATUS_CODE_PERMANENTLY_REDIRECT,
        self::STATUS_CODE_BAD_REQUEST,
        self::STATUS_CODE_UNAUTHORIZED,
        self::STATUS_CODE_PAYMENT_REQUIRED,
        self::STATUS_CODE_FORBIDDEN,
        self::STATUS_CODE_NOT_FOUND,
        self::STATUS_CODE_METHOD_NOT_ALLOWED,
        self::STATUS_CODE_NOT_ACCEPTABLE,
        self::STATUS_CODE_PROXY_AUTHENTICATION_REQUIRED,
        self::STATUS_CODE_REQUEST_TIMEOUT,
        self::STATUS_CODE_CONFLICT,
        self::STATUS_CODE_GONE,
        self::STATUS_CODE_LENGTH_REQUIRED,
        self::STATUS_CODE_PRECONDITION_FAILED,
        self::STATUS_CODE_REQUEST_ENTITY_TOO_LARGE,
        self::STATUS_CODE_REQUEST_URI_TOO_LONG,
        self::STATUS_CODE_UNSUPPORTED_MEDIA_TYPE,
        self::STATUS_CODE_REQUESTED_RANGE_NOT_SATISFIABLE,
        self::STATUS_CODE_EXPECTATION_FAILED,
        self::STATUS_CODE_I_AM_A_TEAPOT,
        self::STATUS_CODE_UNPROCESSABLE_ENTITY,
        self::STATUS_CODE_LOCKED,
        self::STATUS_CODE_FAILED_DEPENDENCY,
        self::STATUS_CODE_RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL,
        self::STATUS_CODE_UPGRADE_REQUIRED,
        self::STATUS_CODE_PRECONDITION_REQUIRED,
        self::STATUS_CODE_TOO_MANY_REQUESTS,
        self::STATUS_CODE_REQUEST_HEADER_FIELDS_TOO_LARGE,
        self::STATUS_CODE_INTERNAL_SERVER_ERROR,
        self::STATUS_CODE_NOT_IMPLEMENTED,
        self::STATUS_CODE_BAD_GATEWAY,
        self::STATUS_CODE_SERVICE_UNAVAILABLE,
        self::STATUS_CODE_GATEWAY_TIMEOUT,
        self::STATUS_CODE_VERSION_NOT_SUPPORTED,
        self::STATUS_CODE_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL,
        self::STATUS_CODE_INSUFFICIENT_STORAGE,
        self::STATUS_CODE_LOOP_DETECTED,
        self::STATUS_CODE_NOT_EXTENDED,
        self::STATUS_CODE_NETWORK_AUTHENTICATION_REQUIRED,
    ];
    public const STATUS_CODES_DESCRIPTIONS = [
        100 => 'Continue',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        208 => 'Already Reported',
        226 => 'IM Used',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        421 => 'Misdirected Request',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        510 => 'Not Extended',
        511 => 'Network Authentication Required'
    ];

    public const AUTHENTICATION_BASIC = 'basic';
    public const AUTHENTICATION_TOKEN = 'token';
    public const AUTHENTICATION_BEARER = 'bearer';
    public const AUTHENTICATION_OAUTH = 'oauth';
    public const AUTHENTICATION_DIGEST = 'digest';
    public const AUTHENTICATION_NTLM = 'ntlm';
    public const AUTHENTICATIONS = [
        self::AUTHENTICATION_BASIC,
        self::AUTHENTICATION_TOKEN,
        self::AUTHENTICATION_BEARER,
        self::AUTHENTICATION_OAUTH,
        self::AUTHENTICATION_DIGEST,
        self::AUTHENTICATION_NTLM,
    ];

    public static function getStatusCodeDescription(int $code): ?string
    {
        return self::STATUS_CODES_DESCRIPTIONS[$code] ?? null;
    }

    public static function getRequestUrl(): string
    {
        return (empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    public static function parseUrl(object|array|string $url): array
    {
        $parsedUrl = new Url($url);
        $parsedUrlArray = $parsedUrl->toArray();

        return array_merge(
            $parsedUrlArray,
            [
                'parsedQuery' => static::parseStr($parsedUrlArray['query'] ?? ''),
            ]
        );
    }

    public static function urlAddQueryParams(object|array|string $url, array $queryParams): string
    {
        $parsedUrl = new Url($url);
        $query = new QueryString($parsedUrl->query);
        $newQuery = $query->mod($queryParams);
        $newUrl = clone $parsedUrl;
        $newUrl->query = (string) $newQuery;

        return (string) $newUrl;
    }

    public static function httpBuildQueryDecoded(
        object|array|string $data
    ): string {
        $query = new QueryString($data);

        return urldecode((string) $query);
    }

    public static function parseStr(?string $queryString): array
    {
        if ($queryString === null) {
            return [];
        }

        $queryParamsRegex = '/(?:(?<key>[^&=]*?)(?:(?:(?<array>(?:(?:(?:\[)|(?:%5B))[^=&]*(?:(?:\])|(?:%5D)))+)(?<discard>[^&=]*)))?(?:=(?<value>[^&]*))?)(?:&|$)/ui';
        preg_match_all($queryParamsRegex, $queryString, $queryParamsMatches, PREG_SET_ORDER);

        $query = [];

        foreach ($queryParamsMatches as $queryParamMatch) {
            $urlDecodedKey = urldecode(str_replace('[', '_', $queryParamMatch['key']));

            if ($urlDecodedKey === '') {
                continue;
            }

            $urlDecodedValue = '';

            if (array_key_exists('value', $queryParamMatch)) {
                $urlDecodedValue = urldecode($queryParamMatch['value']);
            }

            $queryParam = [];

            if (!empty($queryParamMatch['array'])) {
                preg_match_all('#(((?:(?:\[)|(?:%5B))(?<key>.*?)(?:(?:\])|(?:%5D)))(?<discard>.*?))#', $queryParamMatch['array'], $queryParamArrayIndexMatches, PREG_SET_ORDER);

                if (array_key_exists($urlDecodedKey, $query) && is_array($query[$urlDecodedKey])) {
                    $queryParam = [$urlDecodedKey => $query[$urlDecodedKey]];
                } else {
                    $queryParam = [$urlDecodedKey => []];
                }

                $currentPath = &$queryParam[$urlDecodedKey];

                foreach ($queryParamArrayIndexMatches as $queryParamArrayIndexMatchKey => $queryParamArrayIndexMatch) {
                    $urlDecodedKey = urldecode($queryParamArrayIndexMatch['key']);
                    $value = $queryParamArrayIndexMatchKey === array_key_last($queryParamArrayIndexMatches) ? $urlDecodedValue : [];

                    if (!empty($urlDecodedKey)) {
                        $currentPath[$urlDecodedKey] = $value;
                    } else {
                        $currentPath[] = $value;
                    }

                    $currentPath = &$currentPath[array_key_last($currentPath)];
                }
            } else {
                $queryParam = [$urlDecodedKey => $urlDecodedValue];
            }

            $query = array_replace_recursive($query, $queryParam);
        }

        return $query;
    }

    public static function buildHeaderAuthorizationBasicValue(string $username, ?string $password = null): string
    {
        return 'Basic ' . base64_encode($username . ':' . $password);
    }

    public static function buildHeaderAuthorizationOAuthValue(
        string $consumerKey,
        string $consumerSecret,
        string $signatureMethod,
        string $httpMethod,
        string $url,
        ?string $callback = null,
        ?string $verifier = null,
        ?string $realm = null
    ): string {
        return 'OAuth '
            . OAuth1Helper::buildHeaderAuthorizationOAuthValue(
                $consumerKey,
                $consumerSecret,
                $signatureMethod,
                $httpMethod,
                $url,
                $callback,
                $verifier,
                $realm
            );
    }

    public static function buildHeaderAuthorizationTokenValue(string $token): string
    {
        return 'Token ' . $token;
    }

    public static function buildHeaderAuthorizationBearerValue(string $token): string
    {
        return 'Bearer ' . $token;
    }

    public static function parseAuthenticationHeaderValues(array $authorizationHeaderValues): ?array
    {
        if (empty($authorizationHeaderValues) || empty($authorizationHeaderValues[0])) {
            return null;
        }

        $authorizationHeader = $authorizationHeaderValues[0];

        if (str_contains($authorizationHeader, 'Basic ')) {
            $basicAuth = base64_decode(str_replace('Basic ', '', $authorizationHeader), true);

            if (!str_contains($basicAuth, ':')) {
                throw new InvalidArgumentException('Invalid basic authentication.');
            }

            $basicAuth = explode(':', $basicAuth);
            $username = array_shift($basicAuth);
            $password = implode(':', $basicAuth);

            return [
                'type' => self::AUTHENTICATION_BASIC,
                'credentials' => [
                    'username' => $username,
                    'password' => $password,
                ],
            ];
        } elseif (str_contains($authorizationHeader, 'Bearer ')) {
            $token = str_replace('Bearer ', '', $authorizationHeader);

            return [
                'type' => self::AUTHENTICATION_BEARER,
                'credentials' => [
                    'token' => $token,
                ],
            ];
        }

        throw new InvalidArgumentException('Invalid authorization header.');
    }

    public static function formatHeadersArrayAsString(array $headers): string
    {
        return implode(
            "\n",
            array_map(
                function (string $headerName, array $headerValues) {
                    $result = [];

                    foreach ($headerValues as $headerValue) {
                        $result[] = $headerName . ': ' . $headerValue;
                    }

                    return implode("\n", $result);
                },
                array_keys($headers),
                $headers
            )
        );
    }

//    public function parseExtended($uri): array
//    {
//        $scheme = null;
//        $isMimeContent = false;
//        $isJavaScript = false;
//        $isMailTo = false;
//        $content = null;
//        $isQuery = false;
//        $isFragment = false;
//        $userInfo = null;
//        $host = null;
//        $port = null;
//        $path = null;
//        $query = null;
//        $fragment = null;
//
//        $tld = null;
//        $domain = null;
//        $subDomains = null;
//
//        $isSchemeRelative = false;
//        $isPathRelative = false;
//        $isHostIpAddress = false;
//
//        if(str_starts_with($uri, 'javascript:')) {
//            $isMimeContent = true;
//            $isJavaScript = true;
//            $content = substr($uri, 11);
//        } else {
//            if(str_starts_with($uri, 'mailto:')) {
//                $isMimeContent = true;
//                $isMailTo = true;
//                $content = substr($uri, 7);
//            } else {
//                if (
//                    (substr_count($uri, '@') > 1)
//                    ||
//                    (substr_count($uri, '#') > 1)
//                    ||
//                    (substr_count($uri, ':') > 3)
//                ) {
//                    throw new Exception\UriFormatException(
//                        Exception\UriFormatException::MESSAGE_INVALID_URI_FORMAT,
//                        Exception\UriFormatException::CODE_INVALID_URI_FORMAT
//                    );
//                }
//
//                if(str_starts_with($uri, '#')) {
//                    $isFragment = true;
//                    $fragment = substr($uri, 1);
//                } elseif(str_starts_with($uri, '?')) {
//                    $isQuery = true;
//                    $query = substr($uri, 1);
//                } else {
//                    $schemeUri        = explode('://', $uri);
//                    $isSchemeRelative = (count($schemeUri) > 1) && (reset($schemeUri) === '');
//                    $noSchemeUri      = array_pop($schemeUri);
//                    $scheme           = $isSchemeRelative ? null : array_shift($schemeUri);
//
//                    $userInfoUri   = explode('@', $noSchemeUri);
//                    $noUserInfoUri = array_pop($userInfoUri);
//                    $userInfoArray = array_shift($userInfoUri);
//                    $userInfo      = $userInfoArray === null ? null : explode(':', $userInfoArray);
//
//                    $hostUri         = preg_split('#[\?/\#]#', $noUserInfoUri);
//                    $hostPort        = array_shift($hostUri);
//                    $hostPortUri     = explode(':', $hostPort);
//                    $port            = (count($hostPortUri) > 1) ? array_pop($hostPortUri) : null;
//                    $port            = ($port !== '') ? $port : null;
//                    $host            = implode($hostPortUri);
//                    $validHostPort   = !($this->hostValidator !== null) || $this->hostValidator->isValid($hostPort);
//                    $validHost       = !($this->hostValidator !== null) || $this->hostValidator->isValid($host);
//                    $validHost       = $host === 'localhost' || $validHost;
//                    $host            = ($validHost || ($port !== null)) ? $host : null;
//                    $isHostIpAddress = ($this->ipValidator !== null) ? $this->ipValidator->isValid($host) : null;
//
//                    if (!$isHostIpAddress) {
//                        $domains    = explode('.', $host);
//                        $tld        = array_pop($domains);
//                        $domain     = array_pop($domains);
//                        $subDomains = implode('.', $domains);
//                    }
//
//                    $pathQueryFragmentUri =
//                        ($host !== null)
//                            ? substr($noUserInfoUri, strlen($hostPort))
//                            : $noUserInfoUri;
//                    $fragmentUri          = explode('#', $pathQueryFragmentUri);
//                    $fragment             = (count($fragmentUri) > 1) ? array_pop($fragmentUri) : null;
//
//                    $pathQueryUri = implode($fragmentUri);
//                    $queryUri     = explode('?', $pathQueryUri);
//                    $query        = (count($queryUri) > 1) ? array_pop($queryUri) : null;
//
//                    $isPathRelative = !str_starts_with($pathQueryUri, '/');
//                    $path           = implode($queryUri);
//                    $path           = ($path !== '') ? $path : null;
//                }
//            }
//        }
//
//        return [
//            'content' => $content,
//
//            'scheme' => $scheme,
//            'userInfo' => $userInfo,
//            'user' => array_shift($userInfo),
//            'password' => array_shift($userInfo),
//            'host' => $host,
//            'port' => $port,
//            'path' => $path,
//            'query' => $query,
//            'fragment' => $fragment,
//
//            'tld' => $tld,
//            'domain' => $domain,
//            'subDomains' => $subDomains,
//
//            'isQuery' => $isQuery,
//            'isFragment' => $isFragment,
//            'isMimeContent' => $isMimeContent,
//            'isJavaScript' => $isJavaScript,
//            'isMailTo' => $isMailTo,
//            'isSchemeRelative' => $isSchemeRelative,
//            'isPathRelative' => $isPathRelative,
//            'isHostIpAddress' => $isHostIpAddress,
//        ];
//    }

    public static function filterQueryParamBool(array|string|float|int|bool|null $value): bool
    {
        return !(empty($value) || $value === 'false');
    }
}
