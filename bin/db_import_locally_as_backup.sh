#!/usr/bin/env bash

set -evx

# Examples:
#
# With SSH:
# vendor/smtm/adex-sales/bin/db_import_as_backup.sh "EXTRA_NAME_SEGMENT" "smtm-example-app-envname-db-client-container" "mariadb" "root" "DB_NAME_SOURCE" "DB_NAME_TARGET" "root" "123.123.123.123"
# bin/db_import_as_backup.sh "EXTRA_NAME_SEGMENT" "smtm-example-app-envname-db-client-container" "mariadb" "root" "DB_NAME_SOURCE" "DB_NAME_TARGET" "root" "123.123.123.123"
#
# Without SSH:
# vendor/smtm/adex-sales/bin/db_import_as_backup.sh "EXTRA_NAME_SEGMENT" "smtm-example-app-envname-db-client-container" "mariadb" "root" "DB_NAME_SOURCE" "DB_NAME_TARGET"
# bin/db_import_as_backup.sh "EXTRA_NAME_SEGMENT" "smtm-example-app-envname-db-client-container" "mariadb" "root" "DB_NAME_SOURCE" "DB_NAME_TARGET"

DB_EXTRA_NAME_SEGMENT="$1"
DB_CONTAINER_NAME="$2"
DB_HOST_SOURCE="$3"
DB_USER_SOURCE="$4"
DB_NAME_SOURCE="$5"
DB_NAME_TARGET="$6"
SSH_USER="$7"
SSH_HOST="$8"

DB_DUMP_FILE_NAME_COMPLETED_LOCALIZED=$(./db_backup "${DB_CONTAINER_NAME}" "${DB_HOST_SOURCE}" "${DB_USER_SOURCE}" "${DB_NAME_SOURCE}" "${SSH_USER}" "${SSH_HOST}")

mysql -h "mariadb" --port "3306" -u "root" -p -e "DROP DATABASE IF EXISTS "'`'"backup-${DB_EXTRA_NAME_SEGMENT}${DB_DUMP_DATE}-${DB_NAME_TARGET}"'`'
mysql -h "mariadb" --port "3306" -u "root" -p -e "CREATE DATABASE "'`'"backup-${DB_EXTRA_NAME_SEGMENT}${DB_DUMP_DATE}-${DB_NAME_TARGET}"'`'
mysql -h "mariadb" --port "3306" -u "root" -p "backup-${DB_EXTRA_NAME_SEGMENT}${DB_DUMP_DATE}-${DB_NAME_TARGET}" < "${DB_DUMP_FILE_NAME_COMPLETED_LOCALIZED}"
