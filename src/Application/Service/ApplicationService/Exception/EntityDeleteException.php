<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\Exception;

use Smtm\Base\Application\Exception\RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EntityDeleteException extends RuntimeException
{
    public static function formatMessageForEntity(string $entityName): string
    {
        return 'The ' . $entityName . ' could not be deleted.';
    }
}
