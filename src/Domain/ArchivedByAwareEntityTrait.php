<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ArchivedByAwareEntityTrait
{
    protected $archivedBy;

    public function getArchivedBy()
    {
        return $this->archivedBy;
    }

    public function setArchivedBy($archivedBy): static
    {
        $this->archivedBy = $archivedBy;

        return $this;
    }
}
