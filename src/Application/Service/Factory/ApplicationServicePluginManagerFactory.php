<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\Factory;

use Smtm\Base\Application\Service\ApplicationService\Factory\DbServiceAbstractFactory;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ApplicationServicePluginManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ApplicationServicePluginManager(
            $container,
            [
                'abstract_factories' => [
                    DbServiceAbstractFactory::class,
                    ApplicationServiceAbstractFactory::class,
                ],
            ]
        );
    }
}
