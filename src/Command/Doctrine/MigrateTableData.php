<?php

declare(strict_types=1);

namespace Smtm\Base\Command\Doctrine;

use Smtm\Base\Command\Command;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryAwareInterface;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryAwareTrait;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Helper\LetterCaseHelper;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\ForeignKeyConstraint;
use Doctrine\DBAL\Schema\Index;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\BlobType;
use Doctrine\DBAL\Types\BooleanType;
use Doctrine\DBAL\Types\DateImmutableType;
use Doctrine\DBAL\Types\DateTimeImmutableType;
use Doctrine\DBAL\Types\DateTimeType;
use Doctrine\DBAL\Types\DateType;
use Doctrine\DBAL\Types\DecimalType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\SmallIntType;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * Example:
 * vendor/bin/symfony-console -vvv base:doctrine:migrate-table-data --table-prefix='#^some_table_prefix_.*$#'
 */
class MigrateTableData extends Command implements ManagerRegistryAwareInterface
{

    use ManagerRegistryAwareTrait;

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('base:doctrine:migrate-table-data')
            // the short description shown while running "php bin/console list"
            ->setDescription(
                'Smtm - Base - Doctrine - Migrate Table Data'
            )
            ->addOption(
                'connection',
                'a',
                InputOption::VALUE_REQUIRED,
                'Connection name' . "\n"
            )
            ->addOption(
                'table-prefix',
                'd',
                InputOption::VALUE_REQUIRED,
                'Target tables prefix' . "\n"
            )
            // the full command description shown when running the command with
            // the "--help" option
            //->setHelp('Could write some useful text here')

            //->addArgument('doctrineMigrationCommand', InputArgument::OPTIONAL,
            //    'The actual Doctrine migration command to run')
        ;
    }

    public function __construct(string $name = null)
    {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $options = [];
        $options['connection'] = $input->getOption('connection') ? $input->getOption('connection') : ManagerRegistryInterface::DEFAULT_CONNECTION_NAME;
        $options['name'] = $input->getOption('name');
        $options['matchRegexTableInclude'] = $input->getOption('match-regex-table-include') ?: null;
        $options['matchRegexTableExclude'] = $input->getOption('match-regex-table-exclude') ?: null;
        $options['matchRegexIndexExclude'] = $input->getOption('match-regex-index-exclude') ?: null;
        $options['replaceStringIndex'] = $input->getOption('replace-string-index') ? json_decode($input->getOption('replace-string-index'), true, flags: JSON_THROW_ON_ERROR) : null;
        $options['replaceStringForeignKey'] = $input->getOption('replace-string-foreign-key') ? json_decode($input->getOption('replace-string-foreign-key'), true, flags: JSON_THROW_ON_ERROR) : null;

        $connection = $this->managerRegistry->getConnection($options['connection']);
        $schemaManager = $connection->createSchemaManager();
        $tablesGenerator = $connection->iterateNumeric('show tables');
        $tables = [];
        $tableIndexes = [];

        foreach ($tablesGenerator as $tableIndex => $table) {
            $tableName = $table[array_key_first($table)];

            if ($options['matchRegexTableInclude'] !== null && !preg_match($options['matchRegexTableInclude'], $tableName)) {
                continue;
            }

            if ($options['matchRegexTableExclude'] !== null && preg_match($options['matchRegexTableExclude'], $tableName)) {
                continue;
            }

            $tableIndexes[$tableName] = $tableIndex;
        }

        $output = [];
        $methods = [];
        $columnTypeClasses = [];

        foreach ($tableIndexes as $tableName => $tableIndex) {
            $indexes = $schemaManager->listTableIndexes($tableName);
            $primaryKey = null;
            $primaryKeyName = null;

            /** @var Index $index */
            foreach ($indexes as $indexName => $index) {
                if ($index->isPrimary()) {
                    $primaryKey = $index;
                    $primaryKeyName = $indexName;
                }
            }

            if ($primaryKeyName !== null) {
                unset($indexes[$primaryKeyName]);
            }

            $tables[$tableName] = [
                'namePascalCase' => LetterCaseHelper::format($tableName, LetterCaseHelper::LETTER_CASE_PASCAL),
                'nameCamelCase' => LetterCaseHelper::format($tableName, LetterCaseHelper::LETTER_CASE_CAMEL),
                'columns' => $schemaManager->listTableColumns($tableName),
                'primaryKey' => $primaryKey,
                'indexes' => $indexes,
                'foreignKeys' => $schemaManager->listTableForeignKeys($tableName),
            ];
            $methodName = 'create' . $tables[$tableName]['namePascalCase'] . 'Table';
            $methods[$tableName] = '$this->' . $methodName . '($schema);';
            $varName = '$' . $tables[$tableName]['nameCamelCase'] . 'Table';

            $outputTable = <<< EOT
                public function $methodName(Schema \$schema): void
                {
                    $varName = \$schema->createTable('$tableName');
            
            EOT;

            /** @var Column $column */
            foreach ($tables[$tableName]['columns'] as $column) {
                $columnTypeClass = $column->getType()::class;
                $columnTypeClasses[] = $columnTypeClass;

                $columnType = match ($columnTypeClass) {
                    BooleanType::class => 'Types::BOOLEAN',

                    BigIntType::class => 'Types::BIGINT',
                    IntegerType::class => 'Types::INTEGER',
                    SmallIntType::class => 'Types::SMALLINT',

                    DecimalType::class => 'Types::DECIMAL',

                    DateType::class => 'Types::DATE_MUTABLE',
                    DateImmutableType::class => 'Types::DATE_IMMUTABLE',
                    DateTimeType::class => 'Types::DATETIME_MUTABLE',
                    DateTimeImmutableType::class => 'Types::DATETIME_IMMUTABLE',

                    StringType::class => 'Types::STRING',
                    TextType::class => 'Types::TEXT',
                    BlobType::class => 'Types::BLOB',
                };

                $columnOptions = [];

                if ($column->getUnsigned()) {
                    $columnOptions['unsigned'] = $column->getUnsigned();
                }

                if ($column->getLength()) {
                    $columnOptions['length'] = $column->getLength();
                }

                if ($columnTypeClass === DecimalType::class) {
                    $columnOptions['scale'] = $column->getScale();
                    $columnOptions['precision'] = $column->getPrecision();
                }

                if ($column->getNotnull()) {
                    $columnOptions['notNull'] = $column->getNotnull();
                }

                if ($column->getDefault() !== null) {
                    $columnOptions['default'] = $column->getDefault();
                }

                if ($column->getComment() !== null) {
                    $columnOptions['comment'] = $column->getComment();
                }

                $columnOptionsCode = '';

                if (!empty($columnOptions)) {
                    $columnOptionsCode =
                        ', ['
                        . implode(
                            ', ',
                            array_map(
                                fn (string $key, mixed $value) => '\'' . $key . '\' => ' . var_export($value, true),
                                array_keys($columnOptions),
                                $columnOptions
                            )
                        )
                    . ']';
                }

                $autoIncrementCode = '';

                if ($column->getAutoincrement()) {
                    $autoIncrementCode = '->setAutoincrement(' . var_export($column->getAutoincrement(), true) . ')';
                }

                $outputTable .= <<< EOT
                        {$varName}->addColumn('{$column->getName()}', {$columnType}{$columnOptionsCode}){$autoIncrementCode};
                
                EOT;
            }

            if ($tables[$tableName]['primaryKey'] !== null) {
                $columnNamesCode = '[\'' . implode('\', \'', $tables[$tableName]['primaryKey']->getColumns()) . '\']';
                $outputTable .= <<< EOT
                        {$varName}->setPrimaryKey($columnNamesCode);

                EOT;
            }

            /** @var Index $index */
            foreach ($tables[$tableName]['indexes'] as $index) {
                if ($options['matchRegexIndexExclude'] !== null && preg_match($options['matchRegexIndexExclude'], $index->getName())) {
                    continue;
                }

                $columnNamesCode = '[\'' . implode('\', \'', $index->getColumns()) . '\']';
                $columnNamesName = implode('_', $index->getColumns());

                if ($options['replaceStringIndex'] !== null) {
                    foreach ($options['replaceStringIndex'] as $replace => $with) {
                        $columnNamesName = str_replace($replace, $with, $columnNamesName);
                    }
                }

                if ($index->isUnique()) {
                    $outputTable .= <<< EOT
                            {$varName}->addUniqueIndex(
                                $columnNamesCode,
                                substr('idx_un_' . {$varName}->getName() . '_$columnNamesName', 0, SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT)
                            );

                    EOT;
                } else {
                    $outputTable .= <<< EOT
                            {$varName}->addIndex(
                                $columnNamesCode,
                                substr('idx_' . {$varName}->getName() . '_$columnNamesName', 0, SqlHelper::IDENTIFIER_LENGTH_INDEX)
                            );

                    EOT;
                }
            }

            /** @var ForeignKeyConstraint $foreignKey */
            foreach ($tables[$tableName]['foreignKeys'] as $foreignKey) {
                $foreignTableName = $foreignKey->getForeignTableName();

                if (array_key_exists($foreignTableName, $tableIndexes) && $tableIndexes[$foreignTableName] > $tableIndexes[$tableName]) {
                    $oldIndex = $tableIndexes[$tableName];
                    $tableIndexes[$tableName] = $tableIndexes[$foreignTableName];
                    $tableIndexes[$foreignTableName] = $oldIndex;
                }

                $localColumnNamesCode = '[\'' . implode('\', \'', $foreignKey->getLocalColumns()) . '\']';
                $localColumnNamesName = implode('_', $foreignKey->getLocalColumns());

                if ($options['replaceStringForeignKey'] !== null) {
                    foreach ($options['replaceStringForeignKey'] as $replace => $with) {
                        $localColumnNamesName = str_replace($replace, $with, $localColumnNamesName);
                    }
                }

                $foreignColumnNamesCode = '[\'' . implode('\', \'', $foreignKey->getForeignColumns()) . '\']';

                $outputTable .= <<< EOT
                        {$varName}->addForeignKeyConstraint(
                            '$foreignTableName',
                            $localColumnNamesCode,
                            $foreignColumnNamesCode,
                            [],
                            substr('fk_' . {$varName}->getName() . '_$localColumnNamesName', 0, SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT)
                        );

                EOT;
            }

            $outputTable .= <<< EOT
                }
            
            EOT;
            $output[$tableName] = $outputTable;
        }

        $tableIndexesSorted = $tableIndexes;
        asort($tableIndexesSorted);

        $outputSorted = [];
        $methodsSorted = [];

        foreach ($tableIndexesSorted as $tableName => $tableIndex) {
            $outputSorted[$tableName] = $output[$tableName];
            $methodsSorted[$tableName] = $methods[$tableName];
        }

        $columnTypeClasses = array_unique($columnTypeClasses);
        $outputMethodCalls = implode("\n        ", $methodsSorted);
        $outputMigration = implode("\n", $outputSorted);
        $outputMigration = <<< EOT
        <?php
        
        declare(strict_types=1);
        
        namespace Smtm\Base\Migration;
        
        use Smtm\Base\Infrastructure\Doctrine\Migration\CommonMigrationTrait;
        use Smtm\Base\Infrastructure\Helper\SqlHelper;
        use Doctrine\DBAL\Schema\Schema;
        use Doctrine\DBAL\Types\Types;
        use Doctrine\Migrations\AbstractMigration;
        
        class {$options['name']} extends AbstractMigration
        {
        
            use CommonMigrationTrait;
        
            public function up(Schema \$schema): void
            {
                $outputMethodCalls
            }
            
        $outputMigration
        }
        EOT;

        $this->logger->info('------------------------------------------------------------------------------------');
        $this->logger->info("\n" . $outputMigration);

        return Command::SUCCESS;
    }
}
