<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Codeception\Support;

use Codeception\Module\Doctrine2;
use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManagerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
 */
class FunctionalTester extends \Codeception\Actor
{
    /**
     * Define custom actions here
     */
}
