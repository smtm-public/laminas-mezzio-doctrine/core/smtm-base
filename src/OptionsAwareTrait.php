<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Base\Infrastructure\Helper\ArrayHelper;
use Smtm\Base\Infrastructure\Helper\ReflectionHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait OptionsAwareTrait
{
    protected array $options;

    public function getOption(string $name, mixed $default = null): array
    {
        $options = [];

        if (ReflectionHelper::isPropertyInitialized('options', $this)) {
            $options = $this->options;
        }

        return $options[$name] ?? $default;
    }

    public function getOptions(): array
    {
        if (ReflectionHelper::isPropertyInitialized('options', $this)) {
            return $this->options;
        }

        return [];
    }

    public function setOption(string $key, mixed $value): static
    {
        $this->options[$key] = $value;

        return $this;
    }

    public function setOptions(array $options): static
    {
        $this->options = $options;

        return $this;
    }

    public function addOption(string $name, mixed $value): static
    {
        if (!ReflectionHelper::isPropertyInitialized('options', $this)) {
            $this->options = [];
        }

        $this->options[$name] = $value;

        return $this;
    }

    public function mergeOption(
        string $name,
        mixed $value,
        string $strategy = OptionsAwareInterface::OPTION_MERGE_STRATEGY_MERGE_RECURSIVE
    ): static {
        if (!ReflectionHelper::isPropertyInitialized('options', $this)) {
            $this->options = [];
        }

        if (!is_array($this->options[$name])) {
            $this->options[$name] = [$this->options[$name]];
        }

        $this->options[$name] = match ($strategy) {
            OptionsAwareInterface::OPTION_MERGE_STRATEGY_UNION => $this->options[$name] + $value,
            OptionsAwareInterface::OPTION_MERGE_STRATEGY_MERGE => array_merge($this->options[$name], $value),
            OptionsAwareInterface::OPTION_MERGE_STRATEGY_REPLACE => array_replace($this->options[$name], $value),
            OptionsAwareInterface::OPTION_MERGE_STRATEGY_REPLACE_RECURSIVE => array_replace_recursive($this->options[$name], $value),
            default => array_merge_recursive($this->options[$name], $value),
        };

        return $this;
    }

    public function mergeOptions(
        array $options,
        string $strategy = OptionsAwareInterface::OPTION_MERGE_STRATEGY_MERGE_RECURSIVE
    ): static {
        if (!ReflectionHelper::isPropertyInitialized('options', $this)) {
            $this->options = [];
        }

        $this->options = match ($strategy) {
            OptionsAwareInterface::OPTION_MERGE_STRATEGY_UNION => $this->options + $options,
            OptionsAwareInterface::OPTION_MERGE_STRATEGY_MERGE => array_merge($this->options, $options),
            OptionsAwareInterface::OPTION_MERGE_STRATEGY_MERGE_RECURSIVE_IF_NOT_NULL =>
            array_merge_recursive(
                $this->options,
                ArrayHelper::filterRecursive($options, fn (mixed $value) => $value !== null && $value !== [])
            ),
            OptionsAwareInterface::OPTION_MERGE_STRATEGY_REPLACE => array_replace($this->options, $options),
            OptionsAwareInterface::OPTION_MERGE_STRATEGY_REPLACE_RECURSIVE => array_replace_recursive($this->options, $options),
            OptionsAwareInterface::OPTION_MERGE_STRATEGY_REPLACE_RECURSIVE_IF_NOT_NULL =>
                array_replace_recursive(
                    $this->options,
                    ArrayHelper::filterRecursive($options, fn (mixed $value) => $value !== null && $value !== [])
                ),
            default => array_merge_recursive($this->options, $options),
        };

        return $this;
    }
}
