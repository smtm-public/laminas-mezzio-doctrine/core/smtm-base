<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\League\FactoryMuffin\Stores;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RepositoryStore extends \League\FactoryMuffin\Stores\RepositoryStore
{
    public function deleteSaved()
    {
        parent::deleteSaved();

        try {
            $this->flush();
        } catch (Exception $e) {
            throw new DeletingFailedException([$e]);
        }
    }
}
