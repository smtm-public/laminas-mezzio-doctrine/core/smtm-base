<?php

declare(strict_types=1);

namespace Smtm\Base\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface UnarchiveUpdateQueryExecutionMethodCheckAwareEntityInterface
{
    public function getUnarchiveUpdateQueryExecutionMethodCheck(): ?int;
    public function setUnarchiveUpdateQueryExecutionMethodCheck(?int $unarchiveUpdateQueryExecutionMethodCheck): static;
}
