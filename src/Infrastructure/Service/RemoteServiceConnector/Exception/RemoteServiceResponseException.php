<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\RemoteServiceConnector\Exception;

use Smtm\Base\Infrastructure\Exception\RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RemoteServiceResponseException extends RuntimeException implements RemoteServiceResponseExceptionInterface
{

    use RemoteServiceResponseExceptionTrait;
}
