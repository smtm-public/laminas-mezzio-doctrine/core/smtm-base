<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\RemoteServiceConnector;

use JetBrains\PhpStorm\Pure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RemoteServiceConnectorAuth
{
    public const TYPE_BASIC = 'basic';
    public const TYPE_DIGEST = 'digest';
    public const TYPE_NTLM = 'ntlm';
    public const TYPE_OAUTH = 'oauth';
    public const TYPE_TOKEN = 'token';
    public const TYPE_BEARER = 'bearer';

    #[Pure] public function __construct(
        protected string $type,
        protected array $params
    ) {

    }

    public static function create(
        string $type,
        array $params
    ): static {
        return new static($type, $params);
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): static
    {
        $this->params = $params;

        return $this;
    }
}
