<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Codeception\Module;

use Codeception\Lib\Interfaces\DependsOnModule;
use Codeception\Module;
use Codeception\TestInterface;
use Smtm\Base\Infrastructure\Codeception\Module\Mezzio;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class SymfonyConsole extends Module implements DependsOnModule
{
    protected Mezzio $dependentModule;
    protected Application $application;

    public function _depends(): array
    {
        return [$this->config['depends']];
    }

    public function _inject(Mezzio $dependentModule = null): void
    {
        $this->dependentModule = $dependentModule;
    }

    public function _before(TestInterface $test)
    {
        /** @var \Psr\Container\ContainerInterface $container */
        $container = $this->dependentModule->container;
        $config = $container->get('config');
        $commands = $config['commands'] ?? [];

        $this->application = new Application();

        foreach($commands as $command) {
            if($container->has($command)) {
                $this->application->add($container->get($command));
            } else {
                $this->application->add(new $command());
            }
        }
    }

    public function runConsoleCommand(string $command)
    {
        $application = clone $this->application;
        $application->setAutoExit(false);
        $params['command'] = $command;
        $params['--verbose'] = 'vvv';

        $input = new ArrayInput($params);
        $output = new BufferedOutput();
        $application->run($input, $output);

        return $output->fetch();
    }
}
