<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EntityCreateNonUniqueException extends EntityCreateException
{
    public static function formatMessageForEntity(string $entityName): string
    {
        return 'The ' . $entityName . ' could not be created.';
    }
}
