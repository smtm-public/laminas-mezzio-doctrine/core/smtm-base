<?php

declare(strict_types=1);

namespace Smtm\Base\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * Example:
 * vendor/bin/symfony-console base:hello-world -vvv
 */
class HelloWorld extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('base:hello-world')
            // the short description shown while running "php bin/console list"
            ->setDescription(
                'Smtm - Base - Hello World'
            )
            // the full command description shown when running the command with
            // the "--help" option
            //->setHelp('Could write some useful text here')

            //->addArgument('doctrineMigrationCommand', InputArgument::OPTIONAL,
            //    'The actual Doctrine migration command to run')
        ;
    }

    public function __construct(string $name = null)
    {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->info('Info: Hello World!');
        $this->logger->error('Error: Goodbye Cruel World!');

        return Command::SUCCESS;
    }
}
