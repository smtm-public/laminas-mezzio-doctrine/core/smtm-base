<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Migration;

use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait UnarchivedByIpAddressVarcharColumnMigrationTrait
{
    protected function addUnarchivedByIpAddressVarcharColumn(Table $table): void
    {
        $table->addColumn(
            'unarchived_by_ip_address',
            Types::STRING,
            ['notNull' => false, 'length' => 15]
        );
    }

    protected function addUnarchivedByIpAddressIndex(Table $table): void
    {
        $table->addIndex(
            ['unarchived_by_ip_address'],
            substr(
                'fk_' . $table->getName() . '_unarchived_by_ip_address',
                0,
                SqlHelper::IDENTIFIER_LENGTH_INDEX
            )
        );
    }
}
