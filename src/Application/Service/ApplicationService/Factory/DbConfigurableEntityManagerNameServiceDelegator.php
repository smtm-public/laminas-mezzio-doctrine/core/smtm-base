<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Service\ApplicationService\Factory;

use Smtm\Base\Application\Service\ApplicationService\DbArchivedAccessServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbConfigurableEntityManagerNameServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbReaderServiceInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DbConfigurableEntityManagerNameServiceDelegator implements DelegatorFactoryInterface
{
    protected array|string $configKeyEntityManagerName = 'entityManagerName';
    protected array|string $configKeyEntityManagerReaderName = 'entityManagerReaderName';
    protected array|string $configKeyEntityManagerArchivedAccessName = 'entityManagerArchivedAccessName';
    protected array|string $configKeyEntityManagerArchivedAccessReaderName = 'entityManagerArchivedAccessReaderName';

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var DbConfigurableEntityManagerNameServiceInterface $object */
        $object = $callback();
        $configKey = null;

        if ($object instanceof DbReaderServiceInterface && $object instanceof DbArchivedAccessServiceInterface) {
            $configKey = $this->configKeyEntityManagerArchivedAccessReaderName;
        } elseif ($object instanceof DbReaderServiceInterface) {
            $configKey = $this->configKeyEntityManagerReaderName;
        } elseif ($object instanceof DbArchivedAccessServiceInterface) {
            $configKey = $this->configKeyEntityManagerArchivedAccessName;
        } else {
            $configKey = $this->configKeyEntityManagerName;
        }

        $config = $object->getConfig();
        $entityManagerName = null;

        if (is_string($configKey)) {
            $entityManagerName = $config[$configKey];
        } else {
            do {
                $config = $config[array_shift($configKey)];
            } while (!empty($configKey));

            $entityManagerName = $config;
        }

        $object->setEntityManagerName($entityManagerName);

        return $object;
    }
}
