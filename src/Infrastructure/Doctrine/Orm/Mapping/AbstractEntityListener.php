<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Doctrine\Orm\Mapping;

use Smtm\Base\Infrastructure\Doctrine\Orm\EntityManagerDecorator;
use Smtm\Base\Infrastructure\Service\InfrastructureServiceInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManagerAwareTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractEntityListener implements
    InfrastructureServiceInterface,
    InfrastructureServicePluginManagerAwareInterface
{

    use InfrastructureServicePluginManagerAwareTrait;

    protected EntityManagerDecorator $entityManager;
    protected ?int $authenticatedUserId = null;

    public function getEntityManager(): EntityManagerDecorator
    {
        return $this->entityManager;
    }

    public function setEntityManager(EntityManagerDecorator $entityManager): static
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    public function getAuthenticatedUserId(): ?int
    {
        return $this->authenticatedUserId;
    }

    public function setAuthenticatedUserId(?int $authenticatedUserId): static
    {
        $this->authenticatedUserId = $authenticatedUserId;

        return $this;
    }
}
