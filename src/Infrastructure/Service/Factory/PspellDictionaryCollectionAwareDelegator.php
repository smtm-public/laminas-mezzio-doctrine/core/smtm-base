<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service\Factory;

use Smtm\Base\Infrastructure\Service\PspellDictionaryCollectionAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class PspellDictionaryCollectionAwareDelegator implements DelegatorFactoryInterface
{
    protected array $pspellDictionaryNameCollection = [];

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var PspellDictionaryCollectionAwareInterface $object */
        $object = $callback();

        foreach ($this->pspellDictionaryNameCollection as $pspellDictionaryName) {
            $object->addPspellDictionary(
                $pspellDictionaryName,
                pspell_new($pspellDictionaryName)
            );
        }

        return $object;
    }

    public function getPspellDictionaryNameCollection(): array
    {
        return $this->pspellDictionaryNameCollection;
    }

    public function setPspellDictionaryNameCollection(array $pspellDictionaryNameCollection): static
    {
        $this->pspellDictionaryNameCollection = $pspellDictionaryNameCollection;

        return $this;
    }
}
