<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy\Factory;

use Smtm\Base\Application\Extractor\Factory\ExtractorPluginManagerAwareDelegator;
use Smtm\Base\Application\Extractor\Strategy\Base64DecodeExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\Base64EncodeExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\ClassNameExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\ClosureExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\CollectionExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\CollectionReducingExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\DateTimeExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\DomainObjectExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\DomainObjectNullableExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\DomainObjectReducingExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\ExtractionStrategyChain;
use Smtm\Base\Application\Extractor\Strategy\ExtractionStrategyPluginManager;
use Smtm\Base\Application\Extractor\Strategy\FilterStrategy;
use Smtm\Base\Application\Extractor\Strategy\JsonDecodeExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\JsonEncodeExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\SetValueExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\TranslationExtractionStrategy;
use Smtm\Base\Application\Service\Factory\ApplicationServicePluginManagerAwareDelegator;
use Smtm\Base\Infrastructure\Laminas\I18n\Translator\Factory\TranslatorAwareDelegator;
use Smtm\Base\Infrastructure\Service\Factory\InfrastructureServicePluginManagerAwareDelegator;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExtractorStrategyPluginManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ExtractionStrategyPluginManager(
            $container,
            [
                'delegators' => [
                    ExtractionStrategyChain::class => [
                        ExtractionStrategyPluginManagerAwareDelegator::class,
                    ],
                    DomainObjectExtractionStrategy::class => [
                        ExtractorPluginManagerAwareDelegator::class,
                    ],
                    DomainObjectReducingExtractionStrategy::class => [
                        ExtractorPluginManagerAwareDelegator::class,
                    ],
                    DomainObjectNullableExtractionStrategy::class => [
                        ExtractorPluginManagerAwareDelegator::class,
                    ],
                    CollectionExtractionStrategy::class => [
                        ExtractorPluginManagerAwareDelegator::class,
                    ],
                    CollectionReducingExtractionStrategy::class => [
                        ExtractorPluginManagerAwareDelegator::class,
                    ],
                    ClosureExtractionStrategy::class => [
                        ExtractorPluginManagerAwareDelegator::class,
                        ApplicationServicePluginManagerAwareDelegator::class,
                        InfrastructureServicePluginManagerAwareDelegator::class,
                    ],
                    TranslationExtractionStrategy::class => [
                        TranslatorAwareDelegator::class,
                    ],
                ],
                'shared' => [
                    Base64DecodeExtractionStrategy::class => false,
                    Base64EncodeExtractionStrategy::class => false,
                    ClosureExtractionStrategy::class => false,
                    CollectionExtractionStrategy::class => false,
                    CollectionReducingExtractionStrategy::class => false,
                    DateTimeExtractionStrategy::class => false,
                    DomainObjectExtractionStrategy::class => false,
                    DomainObjectReducingExtractionStrategy::class => false,
                    DomainObjectNullableExtractionStrategy::class => false,
                    FilterStrategy::class => false,
                    JsonDecodeExtractionStrategy::class => false,
                    JsonEncodeExtractionStrategy::class => false,
                    SetValueExtractionStrategy::class => false,
                    ClassNameExtractionStrategy::class => false,
                ],
            ]
        );
    }
}
