<?php

declare(strict_types=1);

namespace Smtm\Base\Http\Middleware\Mezzio\Cors\Service\Factory;

use Smtm\Base\Http\Middleware\Mezzio\Cors\Service\Cors;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CorsFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        return new Cors();
    }
}
