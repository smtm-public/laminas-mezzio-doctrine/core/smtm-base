<?php

declare(strict_types=1);

namespace Smtm\Base\Application\Extractor\Strategy;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class JsonDecodeExtractionStrategy extends AbstractExtractionStrategy
{
    public const OPTION_KEY_ASSOCIATIVE = 'associative';

    public function extract($value, ?object $object = null): object|array|string|float|int|bool|null
    {
        return json_decode($value, $this->options[static::OPTION_KEY_ASSOCIATIVE] ?? false);
    }
}
