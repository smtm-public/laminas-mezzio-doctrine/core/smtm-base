<?php

declare(strict_types=1);

namespace Smtm\Base\Infrastructure\Service;

use Gettext\Generator\MoGenerator;
use Gettext\Loader\PoLoader;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class GettextService extends AbstractInfrastructureService
{
    public static function getFileNameDerivative(string $fileName, string $extension): string
    {
        $oldFileNameComponents = explode('.', $fileName);
        $newFileName = implode('.', array_slice($oldFileNameComponents, 0, count($oldFileNameComponents) - 1)) . '.' . $extension;

        return $newFileName;
    }

    public function generateMoFromPo(string $moFileName, string $poFileName, bool $ifNotExists = true): void
    {
        if ($ifNotExists === true && file_exists($moFileName)) {
            return;
        }

        $loader = new PoLoader();
        $translations = $loader->loadFile($poFileName);
        $generator = new MoGenerator();
        $generator->generateFile($translations, $moFileName);
    }

    public function generateMo(string $moFileName, bool $ifNotExists = true): void
    {
        $poFileName = static::getFileNameDerivative($moFileName, 'po');

        $this->generateMoFromPo($moFileName, $poFileName, $ifNotExists);
    }
}
